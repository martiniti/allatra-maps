<?php

/*
	It is recommended for you to change 'auth_login_incorrect_password' and 'auth_login_username_not_exist' into something vague.
	For example: Username and password do not match.
*/

$lang['auth_login_incorrect_password'] = "Неверно введен пароль.";
$lang['auth_login_username_not_exist'] = "Пользователя с таким именем не существует.";

$lang['auth_username_or_email_not_exist'] = "Такого пользователя либо Email адресса не существует.";
$lang['auth_not_activated'] = "Ваш аккаунт еще не был активирован. Пожалуйста, проверьте свой почтовый ящик.";
$lang['auth_request_sent'] = "Ваш запрос на смену пароля принят. Пожалуйста, проверьте свой почтовый ящик.";
$lang['auth_incorrect_old_password'] = "Неверно введен старый пароль.";
$lang['auth_incorrect_password'] = "Неверно введен пароль.";

// Email subject
$lang['auth_account_subject'] = "%s детали аккаунта";
$lang['auth_activate_subject'] = "%s активация";
$lang['auth_forgot_password_subject'] = "Запрос нового пароля";

// Email content
$lang['auth_account_content'] = "Добро пожаловать на %s,

Спасибо за регистрацию. Ваш аккаунт был успешно создан.

Вы можете авторизироватся со следующими логином и паролем:

Логин: %s
Email: %s
Пароль: %s

You can try logging in now by going to %s

We hope that you enjoy your stay with us.

Regards,
The %s Team";

$lang['auth_activate_content'] = "Добро пожаловать на сайт %s,

Чтоб активировать ваш аккаунт, следуйте по ссылке ниже:
%s

Пожалуйста, активируйте ваш аккаунт в течении %s часов, иначе вам необходимо будет повторно зарегистрироваться.

Для входа на сайт вы можете использовать email или логин.
Информация по вашему аккаунту ниже:

Логин: %s
Email: %s
Пароль: %s

Мы надеемся, что вам понравится ваше пребывание у нас :)

С уважением,
Команда %s";

$lang['auth_forgot_password_content'] = "%s,

Вы запросили изменение пароля, потому что вы забыли пароль.
Пожалуйста, перейдите по этой ссылке, чтобы завершить процесс смены пароля:
%s

Ваш новый пароль: %s
Ключ активации: %s

После успешного завершения процесса вы можете изменить указанный новый пароль на пароль, который вы хотите.

Если у вас возникли проблемы с получением доступа к вашей учетной записи, пожалуйста, свяжитесь с %s.

С уваженимем,
Команда %s";

/* End of file dx_auth_lang.php */
/* Location: ./application/language/english/dx_auth_lang.php */