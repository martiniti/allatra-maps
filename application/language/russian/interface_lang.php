<?php

//Header
$lang['language'] = 'Язык';
$lang['menu'] = 'Меню';
$lang['search'] = 'Поиск';
$lang['search_description'] = 'Введите слово или фразу...';

$lang['error_captcha_failed'] = 'Ошибка! Вы ввели некорректный код капчи.';
$lang['error_no_captcha'] = 'Ошибка! Вы не ввели капчу.';

// Footer
$lang['copyrights'] = 'ALLATRA-DA.COM '. date('Y').' / Все права защищены';
$lang['follow_us'] = 'Присоединяйтесь к нам:';
$lang['address'] = 'Адрес:';
$lang['blog'] = 'Блог';
$lang['subscribe'] = 'Подписка';
$lang['subscribe_description'] = 'Делимся интересными статьями рассылке. Присоединяйся!';
$lang['subscribe_email'] = 'Введите email';
$lang['do_subscribe'] = 'Подписаться';
$lang['subscribe_promise'] = '* Мы обязуемся не передавать третьим лицам ваш email.';

//Months
$lang['month-01'] = 'Янв';
$lang['month-02'] = 'Фев';
$lang['month-03'] = 'Мар';
$lang['month-04'] = 'Апр';
$lang['month-05'] = 'Май';
$lang['month-06'] = 'Июн';
$lang['month-07'] = 'Июл';
$lang['month-08'] = 'Авг';
$lang['month-09'] = 'Сен';
$lang['month-10'] = 'Окт';
$lang['month-11'] = 'Ноя';
$lang['month-12'] = 'Дек';

//Breadcrumbs
$lang['home'] = 'Главная';
$lang['home_breadcrumbs'] = 'ALLATRA :: дорожная аналитика';

//404
$lang['404_title'] = 'Страница не найдена!';
$lang['404_text'] = 'К сожалению, запрашиваемая Вами страница не найдена или была удалена.';
$lang['404_go_home'] = 'Вернуться на главную';

//SEO
$lang['seo_title'] = 'ALLATRA-DA.RAMIR.SPACE';
$lang['seo_description'] = 'ALLATRA-DA.RAMIR.SPACE';

$lang['no_title'] = 'ALLATRA-DA.RAMIR.SPACE';
$lang['no_description'] = 'ALLATRA-DA.RAMIR.SPACE';
$lang['no_keywords'] = 'ALLATRA-DA.RAMIR.SPACE';

// Material
$lang['material_comments'] = 'Комментариев';
$lang['material_rating'] = 'Рейтинг';
$lang['material_views'] = 'Просмотров';
$lang['material_print'] = 'Распечатать';
$lang['material_next'] = 'Следующая статья';
$lang['material_prev'] = 'Предыдущая статья';
$lang['material_related'] = 'Похожие статьи';
$lang['material_audio_update'] = 'Требуется обновление.<br /> Для проигрывания мультимедиа вам необходимо обновить ваш браузер до последней версии либо обновить ваш <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash плагин</a>.';
$lang['material_readmore'] = 'Подробнее';
$lang['material_likes'] = 'Мне нравится';
$lang['material_thank_you_for_like'] = 'Спасибо';
$lang['material_already_liked'] = 'Ваш голос уже учтен';
$lang['no_material'] = 'Нет такого материала';

$lang['your_opinion'] = 'Ваше мнение';
$lang['about_menu'] = 'О нас';
$lang['change_logs'] = 'Журнал изменений';
$lang['about'] = 'О проекте';
$lang['feedback'] = 'Обратная связь';
$lang['request_news'] = 'Предложить материал';
$lang['my_news'] = 'Предложить материал';
$lang['books'] = 'Книги';
$lang['make_homepage'] = 'Сделать стартовой';
$lang['language'] = 'Язык';
$lang['videos'] = 'Видео';
$lang['photos'] = 'Фото';
$lang['reviews'] = 'Отзывы';
$lang['news'] = 'Новости';
$lang['examination'] = 'Экспертизы';
$lang['pages'] = 'Страницы';
$lang['sections'] = 'Разделы';
$lang['contacts'] = 'Контакты';
$lang['go_down'] = 'Вниз';
$lang['our_partners'] = 'Наши партнёры';
$lang['our_advantages'] = 'Наши <span class="light first-color">цели</span>';
$lang['catalog'] = 'Каталог';
$lang['gps'] = 'ГПС';

//Auth
$lang['login'] = 'Войти';
$lang['logout'] = 'Выйти';
$lang['registration'] = 'Регистрация';
$lang['do_registration'] = 'Зарегистрироваться';
$lang['username'] = 'Имя пользователя';
$lang['password'] = 'Пароль';
$lang['confirm_password'] = 'Повторите пароль';
$lang['remember'] = 'Запомнить меня';
$lang['user_email'] = 'Email';
$lang['captcha'] = 'Код подтверждения';
$lang['authorization'] = 'Вход';
$lang['do_change_userdata_success'] = 'Вы успешно изменили свои личные данные!';
$lang['username_already_exist'] = 'Пользователь с таким именем уже существует. Пожалуйста, выберите другое имя.';
$lang['email_already_exist'] = 'Этот email уже зарегистрирован. Пожалуйста, перейдите на страницу восстановления пароля.';
$lang['captcha_expiried'] = 'Ваш код подтверждения истек. Пожалуйста, попробуйте еще раз.';
$lang['captcha_mismatch'] = 'Ваш код подтверждения не соответствует коду в изображении. Попробуйте еще раз.';
$lang['already_authorized'] = 'Вы уже авторизированы.';
$lang['info_page'] = 'Информационная страница';
$lang['success_registration_with_email_activation'] = 'Вы были успешно зарегистрированы. Проверьте Вашу почту для активации аккаунта.';
$lang['success_registration'] = 'Вы были успешно зарегистрированы.';
$lang['registration_not_active'] = 'Функция регистрации не активна.';
$lang['logout_for_registration'] = 'Для регистрации вы сперва должны выйти.';
$lang['account_activation_success'] = 'Ваш профиль был успешно активирован.';
$lang['account_activation_fail'] = 'Код активации неверен. Пожалуйста, проверьте ваш email еще раз и повторите попытку.';
$lang['password_recovery'] = 'Восстановление пароля';
$lang['username_or_email'] = 'Имя пользователя или Email';
$lang['forgot_password_email_letter'] = 'На ваш Email была отправлена инструкция, как активировать новый пароль.';
$lang['password_reset_success'] = 'Вы успешно сбросили пароль, ';
$lang['password_reset_failed'] = 'Сброс пароля не удался. Ваше имя пользователя и ключ являются неправильными. Пожалуйста, проверьте свой Email снова и следуйте инструкциям.';
$lang['old_password'] = 'Старый пароль';
$lang['new_password'] = 'Новый пароль';
$lang['confirm_new_password'] = 'Подтвердите новый пароль';
$lang['password_changed'] = 'Ваш пароль был успешно изменен.';
$lang['role'] = 'Роль';
$lang['my_role'] = 'Моя роль';
$lang['my_permission'] = 'Мои функции';
$lang['edit_is_allowed'] = 'Редактирование разрещено';
$lang['edit_is_not_allowed'] = 'Редактирование запрещено';
$lang['delete_is_allowed'] = 'Удаление разрешено';
$lang['delete_is_not_allowed'] = 'Удаление запрешено';
$lang['save'] = 'Сохранить';
$lang['activation'] = 'Активация аккаунта';
$lang['or_sign_up_with_social_account'] = 'или войдите через ваш социальный аккаунт';
$lang['sign_up_with_facebook'] = 'Вход через Facebook';
$lang['sign_up_with_google+'] = 'Вход через Google+';
$lang['sign_up_with_twitter'] = 'Вход через Twitter';

//Header
$lang['search'] = 'Поиск';
$lang['search_placeholder'] = '';
$lang['search_example'] = 'Например';
$lang['do_search'] = 'Искать';
$lang['good_news'] = 'Хорошие новости';

//Main menu
$lang['all_materials'] = 'Все материалы';

//Right bar
$lang['weather'] = 'Погода';
$lang['calendar'] = 'Календарь событий';
$lang['farewell'] = 'Цитата дня';
$lang['subscribe_to_news'] = 'ПОДПИСКА на НОВОСТИ';
$lang['subscriber_name'] = 'Ваше имя';
$lang['subscriber_email'] = 'Ваш E-mail';
$lang['do_subscribe'] = 'Подписаться';

//Homepage
$lang['latest_news'] = 'Новости';
$lang['daily_news'] = 'Лента новостей';
$lang['daily_news_more'] = 'Показать еще';

// Comments
$lang['comments'] = 'Комментарии';
$lang['comment_answer'] = 'Ответить';
$lang['comment_answerer'] = 'Кому';
$lang['comment_thank'] = 'Большое спасибо за ваш комментарий! После проверки модератором, он будет опубликован.';
$lang['comment_leave'] = 'Оставить комментарий';
$lang['commentator_name'] = 'Имя';
$lang['commentator_email'] = 'Email';
$lang['commentator_address'] = 'Город, страна';
$lang['commentator_rating'] = 'Насколько Вам понравилась статья?';
$lang['commentator_message'] = 'Сообщение';
$lang['comment_capcha'] = 'Введите цифры с картинки';
$lang['comment_required_field'] = 'Это поле обязательное для заполнения';
$lang['comment_add'] = 'Добавить комментарий';
$lang['all_comments'] = 'Все комментарии';

//Feedback
$lang['feedback_name'] = 'Имя';
$lang['feedback_enter_your_name'] = 'Введите ваше имя';
$lang['feedback_email'] = 'Email';
$lang['feedback_enter_your_email'] = 'Введите ваш email';
$lang['feedback_topic'] = 'Тема сообщения';
$lang['feedback_message'] = 'Сообщение';
$lang['feedback_captcha'] = 'Введите цифры с картинки';
$lang['feedback_send'] = 'Отправить';

//Request news
$lang['requestnews_name'] = 'Имя';
$lang['requestnews_enter_your_name'] = 'Введите ваше имя';
$lang['requestnews_email'] = 'Email';
$lang['requestnews_city'] = 'Страна, город';
$lang['requestnews_topic'] = 'Тема статьи';
$lang['requestnews_enter_your_email'] = 'Введите ваш email';
$lang['requestnews_message'] = 'Текст';
$lang['requestnews_attach_file'] = 'Прикрепить файл';
$lang['requestnews_captcha'] = 'Введите цифры с картинки';
$lang['requestnews_send'] = 'Отправить';

//Top news
$lang['top_materials'] = 'ТОП материалов';
$lang['top_materials_week'] = 'За неделю';
$lang['top_materials_month'] = 'За месяц';
$lang['top_materials_year'] = 'За год';

// Events
$lang['event_views'] = 'Просмотров';
$lang['event_readmore'] = 'Подробнее';
$lang['event_print'] = 'Распечатать';

$lang['my_account'] = 'Мой аккаунт';
$lang['info_page'] = 'Информационная страница';
$lang['choose_language'] = 'Выбрать язык:';

$lang['articles_rating'] = 'Рейтинг темы';
$lang['from'] = 'из';
$lang['voters'] = 'проголосовавших';

$lang['latest_materials'] = 'Новые статьи';
$lang['max_commented'] = 'Самые комментируемые <br />за неделю';
$lang['new_comments'] = 'Новые комментарии';

$lang['rb_links_menu'] = 'Интересные рубрики';

$lang['project_aim'] = 'Цель проекта';

// Subscribe
$lang['subscribe_news'] = 'Подписка на новости';
$lang['subscribe_name'] = 'Имя';
$lang['subscribe_email'] = 'Email';
$lang['subscribe'] = 'Подписаться';
$lang['subscribe_field'] = 'Поля со звездочкой';
$lang['subscribe_field_1'] = 'обязательны для заполнения.';
$lang['do_subscribe_news'] = 'Подписаться на новости';

//Sections
$lang['articles'] = 'Статьи';

//Comments search
$lang['comment_search_author'] = 'Имя комментатора';
$lang['comment_search_text'] = 'Текст комментария';
$lang['comment_search_data_begin'] = 'Дата начала';
$lang['comment_search_data_finish'] = 'Дата завершения';

//Comment Notifications
$lang['comment_direct_request_denied'] = 'Вы обратились к файлу напрямую, не нажав кнопку "Комментировать"';
$lang['comment_written_from_site'] = 'комментарий к статье с сайта';
$lang['comment_written_by_name'] = 'Написал(а)';
$lang['comment_written_by_email'] = 'Email';
$lang['comment_written_text'] = 'Текст комментария';
$lang['comment_written_link'] = 'Ссылка';
$lang['comment_written_site'] = 'Сайт';
$lang['commentator_information'] = 'К вашему комментарию в статье';
$lang['commentator_answer_came'] = 'пришел ответ';
$lang['commentator_site'] = 'сайт';
$lang['comment_reply'] = 'Ответить';
$lang['thank_for_your_comment'] = 'Большое спасибо за ваш комментарий! После проверки модератором, он будет опубликован.';
$lang['notification_failed_captcha'] = 'Неверно введены цифры с картинки.';
$lang['notification_failed_form'] = 'Проверьте правильность заполнения формы.';

//social shares
$lang['share_in_vk'] = 'Поделиться в ВКонтакте';
$lang['share_in_fb'] = 'Поделиться в Facebook';
$lang['share_in_tw'] = 'Поделиться в Twitter';
$lang['share_in_gp'] = 'Поделиться в Google+';
$lang['share_in_ok'] = 'Поделиться в Одноклассниках';
$lang['share_in_mr'] = 'Поделиться в Mail.ru';
$lang['share_in_lj'] = 'Поделиться в LiveJournal';
$lang['share_in_li'] = 'Поделиться в LinkedIn';

$lang['toggle_navigation'] = 'Переключение навигации';
$lang['close'] = 'Закрыть';

$lang['author_has_no_one_materials'] = 'У данного автора пока нет ни одного материала';
$lang['message_you_can_login'] = '<strong>Внимание!</strong> Для более удобного комментирования без проверочного кода и ввода личных данных можно:';
$lang['or'] = 'или';

$lang['articles_rating'] = 'Рейтинг темы';
$lang['from'] = 'из';
$lang['voters'] = 'проголосовавших';

$lang['nothing_was_found'] = 'По данному запросу ничего не найдено';
$lang['found_items'] = 'Найденных элементов';

$lang['message_has_been_sent'] = 'Ваше сообщение отправлено. Если оно требует ответа, мы свяжемся с вами в кратчайшие сроки!';
$lang['wrong_captcha'] = 'Вы ввели неправильные цифры с картинки.';

//AUTH
$lang['forgot_password'] = 'Забыли пароль';
//Registration
$lang['registration_title'] = 'Пожалуйста, заполните следующие поля:';
$lang['required_fields'] = 'Поля отмеченные звездочкой <strong style="color: red;">*</strong> обязательны для заполнения!';
$lang['registration_name'] = 'Логин (латиницей без пробелов) <strong style="color: red;">*</strong>';
$lang['registration_email'] = 'Email <strong style="color: red;">*</strong>';
$lang['registration_password'] = 'Пароль <strong style="color: red;">*</strong>';
$lang['registration_password_confirm'] = 'Подтверждение пароля <strong style="color: red;">*</strong>';
$lang['registration_captcha'] = 'Код подтверждения <strong style="color: red;">*</strong>';
$lang['registration_enter_code'] = 'Введите код с картинки.';
//login
$lang['login_title'] = 'Пожалуйста, заполните следующие поля:';
$lang['login_name'] = 'Логин или Email';
$lang['login_password'] = 'Пароль';
$lang['login_captcha'] = 'Код подтверждения';
$lang['login_enter_code'] = 'Введите код с картинки.';
$lang['login_remember'] = 'Запомнить меня';
//forgot password
$lang['forgot_passwor_title'] = 'Пожалуйста, заполните следующие поля:';
$lang['forgot_passwor_name'] = 'Введите Ваш логин или Email';
$lang['forgot_passwor_submit'] = 'Восстановить пароль';
//my account
$lang['my_account_profile'] = 'Профиль';
$lang['my_account_articles'] = 'Статьи';
$lang['my_account_comments'] = 'Комментарии';
$lang['my_account_role_user'] = 'Пользователь';
$lang['my_account_role_admin'] = 'Админ';
$lang['my_account_role_moder'] = 'Модератор';
$lang['my_account_role_author'] = 'Автор';
$lang['my_account_creation_date'] = 'Дата создания:';
$lang['my_account_last_enter_date'] = 'Последний вход:';
$lang['my_account_status'] = 'Статус:';
$lang['my_account_status_active'] = 'Активен';
$lang['my_account_status_blocked'] = 'Заблокирован';
$lang['my_account_avatar'] = 'Аватар:';
$lang['my_account_created_articles'] = 'Созданные статьи';
$lang['my_account_edited_articles'] = 'Отредактированные статьи';
$lang['my_account_my_comments'] = 'Мои комментарии';

$lang['categories'] = 'Категория';
$lang['material_by'] = 'Автор';

$lang['map'] = 'Карта';

//404
$lang['404_title'] = 'Страница не найдена';
$lang['404_text'] = 'Информация была удалена или ее вообще не существует.';
$lang['404_search_in_here'] = 'Введите слово или предложение...';
$lang['404_do_search'] = 'Искать';

//SafeRoad
$lang['banners_block_1'] = 'Воздействие знака на физиологию человека';
$lang['banners_block_2'] = 'Воздействие знака на биологические среды и воду';
$lang['participate'] = 'Принять участие';
$lang['read_more'] = 'Подробнее';