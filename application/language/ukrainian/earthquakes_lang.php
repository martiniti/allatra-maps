<?php
$lang['seismo']         = 'Сейсмо';
$lang['monitoring']     = 'мониторинг';
$lang['slogan']         = 'Карта землетрясений в режиме реального времени';
$lang['add_to_favorites'] = 'Добавить в избранное';
$lang['add_to_website'] = 'Добавить на сайт';
$lang['all']            = 'Все';
$lang['during_24_hours'] = 'За 24 часа';
$lang['yesterday']      = 'Вчера';
$lang['today']          = 'Сегодня';
$lang['choose_a_date']  = 'Выбрать дату';
$lang['choose_a_period'] = 'Выбрать период';
$lang['date_begin']     = 'Дата начала';
$lang['date_end']       = 'Дата завершения';
$lang['apply']          = 'Применить';
$lang['total_amount_of_the_earthquakes'] = 'Общее количество землетрясений';
$lang['the_strongest_earthquake'] = 'Самое сильное землетрясение';
$lang['number_of_earthquakes_more_4'] = 'Количество землетрясений >4 балов';
$lang['solutions']      = 'Пути решения глобальных климатических изменений';
$lang['earthquakes_table'] = 'Таблица землетрясений';
$lang['entries_per_page'] = '{select} Записей на страницу';
$lang['search']         = 'Поиск...';
$lang['no_entries_to_found'] = 'Записей не найдено';
$lang['per_page']       = 'Показано {start} до {end} из {rows} записей';
$lang['region']         = 'Регион';
$lang['date']           = 'Дата';
$lang['magnitude']      = 'Магнитуда';
$lang['depth_km']       = 'Глубина, км';
$lang['location']       = 'Нахождение';
$lang['source']         = 'Источник';
$lang['programs_with_im_danilov'] = 'Передачи с И.М.ДАНИЛОВЫМ';
$lang['climate_change'] = 'Изменение климата';
$lang['tectonic_faults'] = 'Тектонические разломы';
$lang['ctrl_d']         = 'Нажмите {combination} для добавления страницы в закладки';
$lang['modal_content']  = 'Для добавления на свой сайт мониторинга:<i><br/> 1) установите javascript "...";<br/> 2) добавьте код "..." в месте, где будет отображаться сейсмо мониторинг.</i>';
$lang['leave_comment']  = 'Оставить комментарий';
$lang['share_your_opinion']  = 'Нам важно Ваше мнение,<br/>  поделитесь им в коментариях';