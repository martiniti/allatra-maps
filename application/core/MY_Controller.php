<?php

/**
 * MY Controller class
 *
 * Расширяет основной класс и загружает QS_Controller
 *
 */
class  MY_Controller  extends  CI_Controller  {
    
    private $language;

    public $menu	            = '';
    public $tmenu	            = '';
    public $bmenu	            = '';
    //public $latest_news         = '';
    public $banners	            = '';
    public $farewell            = '';
    public $languages           = '';

    // Текстовые значения типов материалов для добавления комментов на конкретную страницу
    public $comment_types = array(
        'material' => 1,
        'page' => 2,
    );

    function __construct ()  {
        
        parent::__construct();

        // определяем язык
        $lang = $this->uri->segment(1);

        // по умолчанию ставим русский
        if($lang != 'ua' && $lang != 'en'): $lang = 'ru'; endif;
        
        $this->language = $lang;

        // подгружаем нужный язык
        switch($lang):
            
            case 'ru':
            $this->lang->load('interface', 'russian');
            $this->config->set_item('language', 'russian');
            break;
            
            case 'ua':
            $this->lang->load('interface', 'ukrainian');
            $this->config->set_item('language', 'ukrainian');
            break;
            
            case 'en':
            $this->lang->load('interface', 'english');
            $this->config->set_item('language', 'english');
            break;

            default:
            $this->lang->load('interface', 'russian');
            $this->config->set_item('language', 'russian');
            break;
        
        endswitch;
        
        if($lang == ''): $lang = 'ru'; endif;
        
        $this->_load_settings($lang);
        
        $this->menu	                = $this->my_crud->get_menu_tierd(1,$lang);
        //$this->latest_news	        = $this->my_crud->get_latest_news(4,$lang);
        $this->banners	            = $this->my_crud->get_banners($lang);
        $this->farewell             = $this->my_crud->get_farewell(LANGUAGE);

	}


    /**
     * Загружаем настройки сайта;
     *
     * @access	private
     * @return	void
     */
    function _load_settings($lang)
    {

		$this->db->where('setting_id', 1);
		$this->db->limit(1);
		$query = $this->db->get('settings');

		if($query->num_rows())
		{
			
            $row = $query->row();
            
            define("LANGUAGE", $lang);
            if($lang == 'ru'){
                define("SITE_NAME", $row->site_name_ru);
                define("TEXT", $row->text_ru);
                define("ADDRESS", $row->address_ru);
                define("PRICE", $row->price_ru);
            }elseif($lang == 'ua'){
                define("SITE_NAME", $row->site_name_ua);
                define("TEXT", $row->text_ua);
                define("ADDRESS", $row->address_ua);
                define("PRICE", $row->price_ua);
            }else{
                define("SITE_NAME", $row->site_name_en);
                define("TEXT", $row->text_en);
                define("ADDRESS", $row->address_en);
                define("PRICE", $row->price_en);
            }
			define("THEME", $row->theme);
			define("MATERIALS_PER_PAGE", $row->materials_per_page);
            define("PRODUCTS_PER_PAGE", $row->products_per_page);
			define("PHONE_1", $row->phone_1);
            define("PHONE_2", $row->phone_2);
            define("PHONE_3", $row->phone_3);
            define("PHONE_4", $row->phone_4);
			define("EMAIL", $row->email);
            define("EMAIL_2", $row->email_2);
            define("EMAIL_3", $row->email_3);
            define("VK", $row->vk);
            define("FACEBOOK", $row->facebook);
            define("TW", $row->tw);
            define("YOUTUBE", $row->youtube);
            define("GPLUS", $row->gplus);
            define("SKYPE", $row->skype);
            define("LINKEDIN", $row->linkedin);
            define("INSTAGRAM", $row->instagram);
            define("BLOGSPOT", $row->blogspot);

        }
        
        

    }



}