<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model(array('category_model','pages_model'));
	}

	function index()
	{
        //language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
        
        $data['info']       = $this->pages_model->get_by_url('katalog');
        $data['content']    = $this->pages_model->get_content($data['info']['page_id'], LANGUAGE);
        $data['available_languages'] = $this->pages_model->check_languages($data['info']['page_id'], LANGUAGE);
        
        if(empty($data['content']))
        {
            
            redirect(base_url());
            
        }else{
            
            //Загружаем каталог;
            $data['menu'] = $this->category_model->get_tree(0,LANGUAGE);
            
            $data['cats']       = $this->pages_model->get_cats(LANGUAGE);
            
            $name = 'catalog/maincont'; 
            $this->display_lib->user_page($data,$name);
            
        }        
        
	}	
    
   	function view($category_url = '')
	{
        //language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
        
        $data['info']       = $this->category_model->get_by_url($category_url);
        $data['content']    = $this->category_model->get_content($data['info']['category_id'], LANGUAGE);
        $data['advantages'] = $this->pages_model->get_advantages(LANGUAGE);
        $data['services']   = $this->category_model->get_childrens_categories(1,LANGUAGE);
        $data['partners']   = $this->pages_model->get_partners(LANGUAGE);
        $data['available_languages'] = $this->category_model->check_languages($data['info']['category_id'], LANGUAGE);
        
        if(empty($data['content']))
        {
            
            redirect(base_url());
            
        }else{
        
            $this->load->library(array('pagination','pagination_lib'));
            
            //Загружаем каталог;
            $data['menu'] = $this->category_model->get_tree(0,LANGUAGE);
    
            $data['breadcrumbs']	= array(
                '1' => array('url' => '', 'title' => $data['content']['title'])
            );
    
            $last_segment           = $this->uri->segment($this->uri->total_segments());
    
            if($last_segment != $data['info']['url'] && is_numeric($last_segment)){
                
                $start_from  = $last_segment;
                
            }else{
                
                $start_from  = 0;
                
            }

            $data['cats']       = $this->category_model->get_cats($data['info']['category_id'],LANGUAGE);
            $data['responsible'] = $this->category_model->get_responsible($data['info']['responsible'],LANGUAGE);
    
            $limit       = PRODUCTS_PER_PAGE;
            $total       = $this->category_model->count_products_by($data['info']['category_id'], LANGUAGE);
            $link        = $data['info']['url'];
            $settings    = $this->pagination_lib->get_settings('products', $link, $total, $limit);
    
            //Применяем настройки;
            $this->pagination->initialize($settings);
            
            $data['page_nav']    = $this->pagination->create_links();
    
            $data['products']    = $this->category_model->get_products_by($data['info']['category_id'], $limit, $start_from, LANGUAGE);

            $name = 'catalog/language';
            $this->display_lib->user_page($data,$name);
        
        }

	}
    
}