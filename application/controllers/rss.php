<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rss extends CI_Controller {

	public function index()
	{
        $data = array('feeds' => $this->administration_model->feeds_info());
        $this->load->view('rss_view',$data);
	}
}