<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rss_import extends MY_Controller
{

    protected $limit = 6,
              $rss_links = [
                    'https://allatra.tv/rss/publications?category=im-danilov',
                    'https://allatra.tv/en/rss/publications?category=im-danilov',
                    'https://allatra.tv/cs/rss/publications?category=im-danilov',
                    'https://allatra.tv/sk/rss/publications?category=im-danilov',
                    'https://allatra.tv/it/rss?type=videos&category=im-danilov',
                    'https://allatra.tv/rss/publications?category=climate',
                    'https://allatra.tv/en/rss/publications?category=climate',
                    'https://allatra.tv/cs/rss?type=videos&category=zmena-klimatu',
                    'https://allatra.tv/sk/rss?type=videos&category=zmeny-klimy',
                    'https://allatra.tv/it/rss?type=videos&category=cambiamento-climatico',
              ];

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $this->db->truncate('rss');

        foreach ($this->rss_links as $rss_link) {

            // Если надо выхватывать из других мест просто меняем ссылку
            $string = file_get_contents($rss_link);
            $xml = simplexml_load_string($string);

            echo '<br/>';
            dump($rss_link);

            $data = [];

            //$entry = count($xml->entry) == 0 ?

            if(count($xml->entry) > 0){

                foreach ($xml->entry as $item) {

                    preg_match('/src="([^"]*)"/i', $item->summary->__toString(), $matches);

                    $img_url = $matches[1];

                    if (empty($img_url)) continue;

                    $data[] = [
                        'link' => $rss_link,
                        'title' => $item->title->__toString(),
                        'url' => $item->link->attributes()->href->__toString(),
                        'img' => $img_url,
                    ];

                    if (count($data) == $this->limit) break;

                }

            } else {

                foreach ($xml->channel->item as $item) {

                    preg_match('/src="([^"]*)"/i', $item->description->__toString(), $matches);

                    $img_url = $matches[1];

                    if (empty($img_url)) continue;

                    $data[] = [
                        'link' => $rss_link,
                        'title' => $item->title->__toString(),
                        'url' => $item->link->__toString(),
                        'img' => $img_url,
                    ];

                    if (count($data) == $this->limit) break;

                }

            }

            foreach ($data as $item) {
                $this->db->insert('rss', $item);
            }

        }

        exit;

    }

}
