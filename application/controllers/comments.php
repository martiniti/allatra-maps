<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comments extends MY_Controller {

	public function __construct() {

		parent::__construct();

		$this->load->model(array(
			'comments_model',
			'pages_model',
			'materials_model',
		));

		$this->load->library(array('captcha_lib'));

	}

	public function index() {
		redirect(base_url());
	}

	public function add_comment() {

		$this->load->helper('date');

        $this->load->library(array('form_validation', 'table', 'typography'));

        if (!isset($_POST['post_comment'])) {

            echo json_encode(array('err' => 1, 'content' => $this->lang->line('comment_direct_request_denied')));

            die;
            
		} else {

		    // Насильно преобразуем в инт  
		    $id         = force_int($this->input->post('id'));
			$material   = $this->pages_model->get($id);
			$content    = $this->pages_model->get_content($id, LANGUAGE);

			if (!empty($material) && !empty($content)) {
				
                if($this->dx_auth->is_logged_in()){

                    $this->form_validation->set_rules($this->comments_model->add_logged_rules);

                }else{

                    $this->form_validation->set_rules($this->comments_model->add_rules);

                }
                
				$val_res = $this->form_validation->run();

				if ($val_res == TRUE) {
				    
                    //if user logged in!
                    if($this->dx_auth->is_logged_in()){
                        
                        $this->load->library('email');
                        // Заголовок материала
						$title = $content['title'];
						// Ссылка на материал
						$anchor = anchor(base_url($material['url']), $title);

						$comment_text = $this->input->post('comment_text');
						$comment_text = $this->typography->auto_typography($comment_text, TRUE);

						$comment_data = array();

						$comment_data['foreign_id']   = $id;
						$comment_data['parent_id']    = $this->input->post('parent_id');
                        $comment_data['type']         = $this->input->post('type');
                        $comment_data['language']     = $this->input->post('language');
						$comment_data['author']       = $this->input->post('author');
						$comment_data['email']        = $this->input->post('email');
						$comment_data['address']      = $this->input->post('address');
						$comment_data['comment_text'] = $comment_text;
						$comment_data['date']         = date('Y-m-d H:i:s');
						//$comment_data['rating']       = $this->input->post('rating');
						if ($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0) {
							$comment_data['uid']      = $this->session->userdata('DX_user_id');
						}

						$comment_id = $this->comments_model->add_new($comment_data);

						$author = $this->input->post('author');
						$email = $this->input->post('email');
						//$rating  = $this->input->post('rating');

						$comment_text = wordwrap($comment_text, 70);
						$comment_text = strip_tags($comment_text);

						$address = EMAIL;
						$subject = '"'.$title.'" ('.$this->lang->line('comment_written_from_site').' '.SITE_NAME.')';
						$message = '<p><b>'.$this->lang->line('comment_written_by_name').':</b> '.$author.'<br /><b>'.$this->lang->line('comment_written_by_email').':</b> '.$email.'<br /><b>'.$this->lang->line('comment_written_text').':</b><br />'.$comment_text.'<br /><p>'.$this->lang->line('comment_written_link').':</p> '.$anchor.'</p>';                        
                        
                        $this->email->clear();
                        $this->email->from('martiniti@ukr.net', $this->lang->line('comment_written_site').' '.SITE_NAME);
						$this->email->to($address);
						$this->email->subject($subject);
						$this->email->message($message);
                        $this->email->send();

						$data['imgcode'] = $this->captcha_lib->captcha_actions();

						$new_comment = "<a href='#' class='pull-left'><img alt='' src='http://paintball.ramir.net/assets/img/commentator.jpg' class='media-object' /></a><div class='media-body'><h4 id=" . $comment_id . " class='media-heading'>$author <span>$comment_data[date] / <a id=" . $comment_id . " class='reply' title=" . $comment_data['author'] . " href='#'>".$this->lang->line('comment_reply')."</a></span></h4><p></p><p>$comment_text</p><p></p><hr></div>";

						echo json_encode(array('err' => 0, 'content' => $this->lang->line('thank_for_your_comment'), 'new_comment' => $new_comment, 'captcha' => $data['imgcode']));

						die;
                        
                    }else{
                        
                        $entered_captcha = $this->input->post('captcha');

    					if ($entered_captcha == $this->session->userdata('rnd_captcha')) {
    
    						$this->load->library('email');
                            // Заголовок материала
    						$title = $content['title'];
    						// Ссылка на материал
    						$anchor = anchor(base_url($material['url']), $title);
    
    						$comment_text = $this->input->post('comment_text');
    						$comment_text = $this->typography->auto_typography($comment_text, TRUE);
    
    						$comment_data = array();
    
    						$comment_data['foreign_id']   = $id;
    						$comment_data['parent_id']    = $this->input->post('parent_id');
                            $comment_data['type']         = $this->input->post('type');
                            $comment_data['language']     = $this->input->post('language');
    						$comment_data['author']       = $this->input->post('author');
    						$comment_data['email']        = $this->input->post('email');
    						$comment_data['address']      = $this->input->post('address');
    						$comment_data['comment_text'] = $comment_text;
    						$comment_data['date']         = date('Y-m-d H:i:s');
    						//$comment_data['rating']       = $this->input->post('rating');
    						if ($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0) {
    							$comment_data['uid']      = $this->session->userdata('DX_user_id');
    						}
    
    						$comment_id = $this->comments_model->add_new($comment_data);
    
    						$author = $this->input->post('author');
    						$email = $this->input->post('email');
    						//$rating  = $this->input->post('rating');
    
    						$comment_text = wordwrap($comment_text, 70);
    						$comment_text = strip_tags($comment_text);
    
    						$address = EMAIL;
    						$subject = '"'.$title.'" ('.$this->lang->line('comment_written_from_site').' '.SITE_NAME.')';
    						$message = '<p><b>'.$this->lang->line('comment_written_by_name').':</b> '.$author.'<br /><b>'.$this->lang->line('comment_written_by_email').':</b> '.$email.'<br /><b>'.$this->lang->line('comment_written_text').':</b><br />'.$comment_text.'<br /><p>'.$this->lang->line('comment_written_link').':</p> '.$anchor.'</p>';                        
                            
                            $this->email->clear();
                            $this->email->from('martiniti@ukr.net', $this->lang->line('comment_written_site').' '.SITE_NAME);
    						$this->email->to($address);
    						$this->email->subject($subject);
    						$this->email->message($message);
                            $this->email->send();
    
    						$data['imgcode'] = $this->captcha_lib->captcha_actions();
    
    						$new_comment = "<a href='#' class='pull-left'><img alt='' src='http://rgdn.info/assets/img/commentator.jpg' class='media-object' /></a><div class='media-body'><h4 id=" . $comment_id . " class='media-heading'>$author <span>$comment_data[date] / <a id=" . $comment_id . " class='reply' title=" . $comment_data['author'] . " href='#'>".$this->lang->line('comment_reply')."</a></span></h4><p></p><p>$comment_text</p><p></p><hr></div>";
    
    						echo json_encode(array('err' => 0, 'content' => $this->lang->line('thank_for_your_comment'), 'new_comment' => $new_comment, 'captcha' => $data['imgcode']));
    
    						die;
    					}
    
    					// Если капча не совпадает
    					else {
    						//получаем код капчи
    						$data['imgcode'] = $this->captcha_lib->captcha_actions();
    
    						echo json_encode(array('err' => 1, 'content' => $this->lang->line('notification_failed_captcha'), 'captcha' => $data['imgcode']));
    
    						die;
    					}
                        
                    }
                    
					
				}

				//Если валидация не пройдена
				else {
					//получаем код капчи
					$data['imgcode'] = $this->captcha_lib->captcha_actions();

					echo json_encode(array('err' => 1, 'content' => $this->lang->line('notification_failed_form'), 'captcha' => $data['imgcode']));

					die;
				}
			}
		}
	}

}

/* End of file comments.php */
/* Location: ./application/controllers/comments.php */