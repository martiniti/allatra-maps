<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model(array('service_model','pages_model'));
	}

	function index()
	{
        redirect(base_url());
	}	
    
   	function view($service_url = '')
	{
        //language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
        
        $data['info']       = $this->service_model->get_by_url($service_url);
        $data['content']    = $this->service_model->get_service_content($data['info']['service_id'], LANGUAGE);
        $data['services']   = $this->service_model->get_childrens_categories(1,LANGUAGE);
        $data['partners']   = $this->pages_model->get_partners(LANGUAGE);
        $data['available_languages'] = $this->service_model->check_service_languages($data['info']['service_id'], LANGUAGE);
        $data['cats']       = $this->service_model->get_services(LANGUAGE);
        
        if(empty($data['content']))
        {
            
            redirect(base_url());
            
        }else{
    
            $data['breadcrumbs']	= array(
                '1' => array('url' => '', 'title' => $data['content']['title'])
            );

            //Recaptcha
            $data['widget']     = $this->recaptcha->getWidget();
            $data['script' ]    = $this->recaptcha->getScriptTag();

            $data['advantages']  = $this->service_model->get_advantages($data['info']['service_id'],LANGUAGE);
            $data['benefits']    = $this->service_model->get_benefits($data['info']['service_id'],LANGUAGE);
            $data['languages']   = $this->service_model->get_languages($data['info']['service_id'],LANGUAGE);
            $data['responsible'] = $this->service_model->get_responsible($data['info']['responsible'],LANGUAGE);
            if($data['info']['head']!=0){
                $data['head']        = $this->service_model->get_responsible($data['info']['head'],LANGUAGE);
            }

            $name = 'service/content';
            $this->display_lib->service_page($data,$name);
        
        }

	}
    
}