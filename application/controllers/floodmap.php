<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Floodmap extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
			'floodmap_model',
		));

    }

	public function index()
	{
	    $data = [];

	    $this->load->view('pages/floodmap_view', $data);

	}

}