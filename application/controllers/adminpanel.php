<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminpanel extends CI_Controller {

    public $all_userdata;

	function __construct()
	{
		parent::__construct();

        $this->all_userdata = $this->session->all_userdata();
        $this->load->library(array('image_CRUD','grocery_CRUD'));
	}
	
	function _my_output($output = null)
	{
       $name = 'main';

       $this->display_lib->admin_page($output ,$name); 		
	}
	
	function index()
	{
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator','author')))
        {
    		$this->_my_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
        
        }else{
            
            redirect('auth/login');
        }
	}	
    
    function content($table, $lang, $fid)
	{
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator','author')))
        {
            $languages_completed = $this->languages_completed($table,$lang,$fid);

            $c = new grocery_CRUD();
            
            if ($this->dx_auth->is_role(array('moderator','author'))){
                //$c->unset_add();
                //$c->unset_edit();
                //$c->unset_delete();
            }

            if($languages_completed){
                $c->unset_add();
            }
            
            $c->unset_read();

            $c->set_theme('datatables'); 
            $c->set_table('content');

            if($table!='categories'){
                $c->unset_texteditor(array('video_url'));
            }
            
            $c->where('table',$table);
            $c->where('language',$lang);
            $c->where('fid',$fid);
    
            $c->columns('content_id','title','text');
            $c->fields('title','seo_title','keywords','description','anons','text','table','fid','language','user_add','user_update','img_url','video_url','show');
            $c->set_subject('контент');
            $c->required_fields();
            
            $c->display_as('content_id','#')
              ->display_as('title','Заголовок')
              ->display_as('seo_title','SEO заголовок')
              ->display_as('keywords','Ключевики')
              ->display_as('description','Описание')
              ->display_as('anons','Анонс')
              ->display_as('text','Текст')
              ->display_as('video_url','Видео')
              ->display_as('img_url','Картинка')
              ->display_as('show','Выводить');

            if($table=='categories'){
                $c->display_as('video_url','Прайс');
            }

            $c->field_type('table','hidden',$table);
            $c->field_type('fid','hidden',$fid);
            $c->field_type('language','hidden',$lang);
            
            $state = $this->grocery_crud->getState();
            
            if($state == 'add'){
                $c->change_field_type('user_update','hidden');
                $c->change_field_type('user_add', 'hidden', $this->dx_auth->get_user_id());  
            }elseif($state == 'edit'){
               $c->change_field_type('user_add','hidden');
               $c->change_field_type('user_update', 'hidden', $this->dx_auth->get_user_id());  
            }
            
            $c->set_field_upload('img_url', 'assets/uploads/images/content/');
            
            $c->callback_after_upload(array($this,'callback_content_after_upload'));
                 
    	    $output = $c->render();
    			
    	    $this->_my_output($output);
         
        }else{
            
            redirect('auth/login');
        }

    }

    function albums()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('albums');
            $c->columns('album_id','title','keywords','description');
            $c->set_subject('галерею');
            $c->required_fields('title');

            $c->add_action('Фото', '', 'adminpanel/photos','ui-icon-image');

            $c->unset_read();

            $c->display_as('album_id','ID')
                ->display_as('title','Заголовок')
                ->display_as('keywords','Ключевые слова')
                ->display_as('description','Описание');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function photos($album_id)
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $crud = new image_CRUD();

            $crud->set_table('photos');
            $crud->set_primary_key_field('photo_id');
            $crud->set_url_field('img_url');
            $crud->set_title_field('title');

            $crud->set_relation_field('album')->set_ordering_field('priority')->set_image_path("assets/uploads/images/photos");

            $output = $crud->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }
    }


    function farewells()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('farewells');

            $c->unset_read();

            $c->columns('farewell_id','text_ru','date');
            $c->set_subject('напутствие');
            $c->required_fields('date');

            $c->display_as('farewell_id','№')
                ->display_as('text_ru','Текст (RU)')
                ->display_as('text_en','Текст (EN)')
                ->display_as('text_fr','Текст (FR)')
                ->display_as('author_ru','Автор (RU)')
                ->display_as('author_en','Автор (EN)')
                ->display_as('author_fr','Автор (FR)')
                ->display_as('date','Дата вывода');

            $c->unset_texteditor(array('text_ru','text_fr','text_en'));

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function cost()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('cost');

            $c->unset_read();

            $c->set_relation('auto','cost_autos','name');
            $c->set_relation('service','cost_services','name');

            $c->columns('id','auto','service','price','euro','dollar');
            $c->set_subject('цену');
            $c->required_fields('auto','service','price','euro','dollar');

            $c->display_as('id','#')
                ->display_as('auto','Авто')
                ->display_as('service','Услуга')
                ->display_as('price','Цена, грн.')
                ->display_as('euro','Цена, €')
                ->display_as('dollar','Цена, $');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function cost_autos()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('cost_autos');

            $c->unset_read();

            $c->set_relation('type','cost_types','name');

            $c->columns('id','name','type','img_url');
            $c->set_subject('авто');
            $c->required_fields('name','type','img_url');

            $c->display_as('id','#')
                ->display_as('name','Название')
                ->display_as('type','Тип')
                ->display_as('img_url','Картинка');

            $c->set_field_upload('img_url', 'assets/uploads/images/autos/');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function cost_services()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('cost_services');

            $c->unset_read();

            $c->columns('id','name');
            $c->set_subject('название услуги');
            $c->required_fields('name');

            $c->display_as('id','#')
                ->display_as('name','Название услуги');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function cost_types()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('cost_types');

            $c->unset_read();

            $c->columns('id','name');
            $c->set_subject('тип авто');
            $c->required_fields('name');

            $c->display_as('id','#')
                ->display_as('name','Название типа авто');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function banners()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('banners');

            $c->unset_read();
            $c->unset_texteditor(array('text'));

            $c->columns('priority','title','img_url','position','lang');
            $c->set_subject('баннер');
            $c->required_fields('title','img_url');

            $c->set_relation('position','positions','title');
            $c->set_relation('lang','languages','title');

            $c->display_as('banner_id','#')
                ->display_as('title','Заголовок')
                ->display_as('text','Текст')
                ->display_as('img_url','Картинка')
                ->display_as('link','Ссылка')
                ->display_as('position','Позиция')
                ->display_as('priority','Очередность')
                ->display_as('lang','Язык');

            $c->set_field_upload('img_url', 'assets/uploads/images/banners/');
            //$c->callback_after_upload(array($this,'callback_banners_after_upload'));

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function teams()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('teams');

            $c->unset_read();

            $c->columns('priority','name','position','photo_url');
            $c->set_subject('сотрудника');
            $c->required_fields('name','position','photo_url');

            $c->display_as('priority','#')
                ->display_as('name_ru','Имя')
                ->display_as('name','Ім\'я')
                ->display_as('name','Name')
                ->display_as('position','Должность')
                ->display_as('position','Посада')
                ->display_as('position','Position')
                ->display_as('photo_url','Фото');

            $c->set_field_upload('photo_url', 'assets/uploads/images/team/');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function advantages()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('advantages');

            $c->unset_read();
            $c->unset_texteditor(array('text'));

            $c->columns('priority','text','language');
            $c->set_subject('преимущество');
            $c->required_fields('text','language','priority');
            $c->set_relation('language','languages','title');

            $c->display_as('text','Текст')
              ->display_as('language','Язык')
              ->display_as('priority','№')
              ->display_as('mp','На главную');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function services()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('services');

            $c->unset_read();

            $c->add_action('RU', '', 'adminpanel/services_content/ru','ui-icon-plus');
            $c->add_action('UA', '', 'adminpanel/services_content/ua','ui-icon-plus');
            $c->add_action('EN', '', 'adminpanel/services_content/en','ui-icon-plus');

            $c->columns('priority','title','url');
            $c->set_subject('услугу');
            $c->required_fields('title','img_url','icon');

            $c->display_as('title','Заголовок')
                ->display_as('url','URL')
                ->display_as('image','Картинка')
                ->display_as('icon','Иконка')
                ->display_as('responsible','Ответственный')
                ->display_as('head','Руководитель')
                ->display_as('advantages','Цифры TF')
                ->display_as('benefits','Преимущества')
                ->display_as('languages','Языки')
                ->display_as('priority','№');

            $c->set_relation('responsible','teams','{name_ru} - {position_ru}');
            $c->set_relation('head','teams','{name_ru} - {position_ru}');

            //$c->set_relation_n_n('advantages', 'service_advantage', 'advantages', 'service_id', 'advantage_id', 'title', 'priority');
            $c->set_relation_n_n('benefits', 'service_benefit', 'benefits', 'service_id', 'benefit_id', 'title_ru','priority');
            //$c->set_relation_n_n('languages', 'service_language', 'categories', 'service_id', 'language_id', 'title', 'priority');

            $c->set_field_upload('image', 'assets/uploads/images/services/');

            $c->change_field_type('route_id', 'hidden');

            $c->callback_before_insert(array($this, 'add_service_valid_url'));
            $c->callback_before_update(array($this, 'update_service_valid_url'));
            $c->callback_before_delete(array($this, 'delete_service_url'));

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function services_content($lang, $fid)
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator','author')))
        {
            $languages_completed = $this->services_content_completed($lang,$fid);

            $c = new grocery_CRUD();

            if($languages_completed){
                $c->unset_add();
            }

            $c->unset_read();

            $c->unset_texteditor(array('block1','video'));

            $c->set_theme('datatables');
            $c->set_table('services_content');

            $c->where('language',$lang);
            $c->where('fid',$fid);

            $c->columns('services_content_id','title');
            $c->fields('title','seo_title','keywords','description','block1','block2','video','img_url','fid','language','user_add','user_update','show');
            $c->set_subject('контент');
            $c->required_fields();

            $c->display_as('services_content_id','#')
                ->display_as('title','Заголовок')
                ->display_as('seo_title','SEO заголовок')
                ->display_as('keywords','Ключевики')
                ->display_as('description','Описание')
                ->display_as('block1','Блок 1')
                ->display_as('block2','Блок 2')
                ->display_as('block3','Блок 3')
                ->display_as('block4','Блок 4')
                ->display_as('block5','Блок 5')
                ->display_as('block6','Блок 6')
                ->display_as('video','Видео')
                ->display_as('img_url','Картинка')
                ->display_as('show','Выводить');

            $c->set_field_upload('img_url', 'assets/uploads/images/services_content/');

            $c->field_type('fid','hidden',$fid);
            $c->field_type('language','hidden',$lang);

            $state = $this->grocery_crud->getState();

            if($state == 'add'){
                $c->change_field_type('user_update','hidden');
                $c->change_field_type('user_add', 'hidden', $this->dx_auth->get_user_id());
            }elseif($state == 'edit'){
                $c->change_field_type('user_add','hidden');
                $c->change_field_type('user_update', 'hidden', $this->dx_auth->get_user_id());
            }

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function benefits()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('benefits');

            $c->unset_read();

            $c->columns('priority','title_ru','description_ru','mp');
            $c->set_subject('преимущество');
            $c->required_fields('title_ru','title_ua','title_en','description_ru','description_ua','description_en','icon','priority');

            $c->display_as('title_ru','Заголовок')
                ->display_as('title_ua','Заголовок UA')
                ->display_as('title_en','Заголовок EN')
                ->display_as('description_ru','Текст')
                ->display_as('description_ua','Текст UA')
                ->display_as('description_en','Текст EN')
                ->display_as('icon','Иконка')
                ->display_as('priority','№')
                ->display_as('mp','На главную');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function faq_manage()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('faq');

            $c->unset_read();
            $c->unset_texteditor(array('text'));

            $c->columns('priority','title','language');
            $c->set_subject('вопрос-ответ');
            $c->required_fields('title','text','language','priority');
            $c->set_relation('language','languages','title');

            $c->display_as('title','Заголовок')
                ->display_as('text','Текст')
                ->display_as('language','Язык')
                ->display_as('priority','№');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function subjects()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('subjects');

            $c->unset_read();
            //$c->unset_texteditor(array('text'));

            $c->columns('priority','title_ru','img_url');
            $c->set_subject('тип авто');
            $c->required_fields('priority','title_ru','title_ua','title_en','img_url');

            $c->display_as('subject_id','#')
                ->display_as('title_ru','Заголовок')
                ->display_as('title_ua','Заголовок UA')
                ->display_as('title_en','Заголовок EN')
                ->display_as('img_url','Картинка')
                ->display_as('priority','Приоритет');

            $c->set_field_upload('img_url', 'assets/uploads/images/subjects/');

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }
    
    public function categories_manage($parent_id = 0)
    {      
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
            
            $c = new grocery_CRUD();

            $c->where('parent', $parent_id);
            
            $c->set_theme('datatables');
            $c->set_table('categories');
            
            $c->unset_read();

            if($parent_id == 0){
                $c->add_action('Подкатегории', '', 'adminpanel/categories_manage','ui-icon-plus');
            }
            $c->add_action('RU', '', 'adminpanel/content/categories/ru','ui-icon-plus');
            $c->add_action('UA', '', 'adminpanel/content/categories/ua','ui-icon-plus');
            $c->add_action('EN', '', 'adminpanel/content/categories/en','ui-icon-plus');
            
            //$c->add_action('Фото', '', 'adminpanel/photos','ui-icon-plus');

            $c->set_subject('услугу');
            $c->columns('priority','title','url','img_url');
            
            $c->display_as('priority','№')
              ->display_as('title','Название')
              ->display_as('img_url','Картинка')
              ->display_as('responsible','Ответственный');
              
            $c->callback_before_insert(array($this, 'add_category_valid_url'));
            $c->callback_before_update(array($this, 'update_category_valid_url'));
            $c->callback_before_delete(array($this, 'delete_category_url'));

            $c->set_relation('responsible','teams','{name} - {position}');
            
            $c->set_field_upload('img_url', 'assets/uploads/images/categories/');

            if($parent_id != 1) {
                //$c->callback_after_upload(array($this, 'callback_category_after_upload'));
            }

            if($parent_id != 1){
                $c->change_field_type('responsible', 'hidden');
                $c->set_field_upload('icon', 'assets/uploads/images/flags/');
            }
            
            $c->change_field_type('parent', 'hidden', $parent_id);

            $c->change_field_type('route_id', 'hidden');
            
            $output = $c->render();
            
            $this->_my_output($output);
        
        }
    }
    
    public function products_manage()
    {      
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
            
            $c = new grocery_CRUD();
            
            $c->set_theme('datatables');
            $c->set_table('products');
            
            $c->unset_read();

            $c->add_action('RU', '', 'adminpanel/content/products/ru','ui-icon-plus');
            $c->add_action('FR', '', 'adminpanel/content/products/fr','ui-icon-plus');
            $c->add_action('EN', '', 'adminpanel/content/products/en','ui-icon-plus');

            $c->set_subject('продукт');
            $c->fields('title','url','category','sku','price_euro','state','priority');
            $c->columns('product_id','title','category','img_url','price_ua','price_euro','priority');
            
            $c->display_as('product_id','ID')
              ->display_as('title','Название')
              ->display_as('url','URL')
              ->display_as('category','Категорию')
              ->display_as('sku','Артикул')
              ->display_as('price_ua','₴')
              ->display_as('price_euro','€')
              ->display_as('state','Статус')
              ->display_as('img_url','Картинка')
              ->display_as('priority','Очередность');
              
            $c->callback_before_insert(array($this, 'add_product_valid_url'));
            $c->callback_before_update(array($this, 'update_product_valid_url'));
            $c->callback_before_delete(array($this, 'delete_product_url'));
            
            //$c->set_field_upload('img_url', 'assets/uploads/images/products/');
            
            //$c->callback_after_upload(array($this,'callback_category_after_upload'));
            
            $c->set_relation('category','categories','title');
            
            $c->change_field_type('route_id', 'hidden');
            
            $output = $c->render();
            
            $this->_my_output($output);
        
        }
    }
    
   	function pages()
	{
	    if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
	    
            $c = new grocery_CRUD();
            
            //$c->unset_delete();
            
            $c->add_action('RU', '', 'adminpanel/content/pages/ru','ui-icon-plus');
            //$c->add_action('UA', '', 'adminpanel/content/pages/ua','ui-icon-plus');
            $c->add_action('EN', '', 'adminpanel/content/pages/en','ui-icon-plus');
            $c->add_action('CS', '', 'adminpanel/content/pages/cs','ui-icon-plus');
            $c->add_action('SK', '', 'adminpanel/content/pages/sk','ui-icon-plus');
            $c->add_action('IT', '', 'adminpanel/content/pages/it','ui-icon-plus');

            $c->set_theme('datatables'); 
            $c->set_table('pages');
            $c->columns('page_id','url','title','wp');
            $c->fields('url','route_id','title','enable_comments');
            $c->set_subject('страницу');
            $c->required_fields('title');
            
            $c->display_as('page_id','№')
              ->display_as('title','Заголовок')
              ->display_as('wp','Главная?')
              ->display_as('enable_comments','Включить комментарии?');
              
            $c->field_type('route_id', 'hidden');
              
            $c->callback_before_insert(array($this, 'add_page_valid_url'));
            $c->callback_before_update(array($this, 'update_page_valid_url'));
            $c->callback_before_delete(array($this, 'delete_page_url'));
                 
    	    $output = $c->render();
           
            $this->_my_output($output);
       
       }else{
            
            redirect('auth/login');
       }
	}
    
    function slides()
	{
	    
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
	    
            $c = new grocery_CRUD();
            
            //$crud->unset_delete();
            //$crud->unset_sort();
            $c->unset_texteditor('text');
            
            $c->set_theme('datatables'); 
            $c->set_table('slides');
            $c->columns('priority','title','text','img_url','language');
            $c->fields('title','text','img_url','link','language','priority');
            $c->set_subject('слайд');
            $c->required_fields('title','img_url','language');
            
            $c->display_as('priority','№')
                ->display_as('title','Заголовок')
                ->display_as('text','Текст')
                ->display_as('img_url','Картинка')
                ->display_as('link','Ссылка')
                ->display_as('video','Видео') 
                ->display_as('language','Язык');
                 
            $c->set_relation('language','languages','title');
            
            $c->set_field_upload('img_url', 'assets/uploads/images/slides/');

            $c->callback_after_upload(array($this,'callback_slides_after_upload'));
                 
            $output = $c->render();
           
            $this->_my_output($output);
       
       }else{
            
            redirect('auth/login');
       }
       
	}
    
    function partners_manage()
	{
	    
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
	    
            $c = new grocery_CRUD();
            
            $c->set_theme('datatables'); 
            $c->set_table('partners');
            $c->columns('priority','title','img_url','language');
            $c->set_subject('партнера');
            $c->required_fields('title','img_url','language');
            
            $c->display_as('priority','№')
                ->display_as('title','Заголовок')
                ->display_as('img_url','Картинка')
                ->display_as('link','Ссылка')
                ->display_as('text','Текст')
                ->display_as('language','Язык');
                 
            $c->set_relation('language','languages','title');
            
            $c->set_field_upload('img_url', 'assets/uploads/images/partners/');
                 
            $output = $c->render();
           
            $this->_my_output($output);
       
       }else{
            
            redirect('auth/login');
       }
       
	}
    
    function certificates_manage()
	{
	    
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
	    
            $c = new grocery_CRUD();
            
            $c->set_theme('datatables'); 
            $c->set_table('certificates');
            $c->columns('priority','title','img_url','language');
            $c->fields('title','img_url','language','priority');
            $c->set_subject('сертификат');
            $c->required_fields('title','img_url','language');
            
            $c->display_as('priority','№')
                ->display_as('title','Заголовок')
                ->display_as('img_url','Картинка')
                ->display_as('language','Язык');
                 
            $c->set_relation('language','languages','title');
            
            $c->set_field_upload('img_url', 'assets/uploads/images/certificates/');
            
            $c->callback_after_upload(array($this,'callback_certificate_after_upload'));
                 
            $output = $c->render();
           
            $this->_my_output($output);
       
       }else{
            
            redirect('auth/login');
       }
       
	}
    
    function prices()
	{
	    
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
	    
            $c = new grocery_CRUD();

            $c->set_theme('datatables'); 
            $c->set_table('prices');
            $c->columns('priority','title','file_url','language');
            $c->set_subject('прайс');
            $c->required_fields('title','file_url','language');
            
            $c->display_as('priority','№')
                ->display_as('title','Заголовок')
                ->display_as('file_url','Файл')
                ->display_as('language','Язык');
                 
            $c->set_relation('language','languages','title');
            
            $c->set_field_upload('file_url', 'assets/uploads/files/prices/');
                 
            $output = $c->render();
           
            $this->_my_output($output);
       
       }else{
            
            redirect('auth/login');
       }
       
	}
    
    function video()
	{
	    
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
	    
            $c = new grocery_CRUD();
            
            $c->unset_texteditor('video_url');

            $c->set_theme('datatables'); 
            $c->set_table('video');
            $c->columns('video_id','title','video_url','language');
            $c->set_subject('видео');
            $c->required_fields('title','video_url','language');
            
            $c->display_as('video_id','№')
                ->display_as('title','Заголовок')
                ->display_as('video_url','Ссылка на видео')
                ->display_as('language','Язык');
                 
            $c->set_relation('language','languages','title');
                 
            $output = $c->render();
           
            $this->_my_output($output);
       
       }else{
            
            redirect('auth/login');
       }
       
	}
    
   	function sections($parent_id = 0)
	{
	    if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {
    	    
            $c = new grocery_CRUD();
            
            $c->add_action('RU', '', 'adminpanel/content/sections/ru','ui-icon-plus');
            $c->add_action('EN', '', 'adminpanel/content/sections/en','ui-icon-plus');
            $c->add_action('UA', '', 'adminpanel/content/sections/ua','ui-icon-plus');
            //$c->add_action('Подкатегории', '', 'adminpanel/sections','ui-icon-plus');
            
            $c->unset_read();
            
            if ($this->dx_auth->is_role('moderator')){
                $c->unset_delete();
            }
            
            $c->where('parent_id',$parent_id);
            
            $c->set_theme('datatables'); 
            $c->set_table('sections');
            
            $c->columns('priority','url','title','type');
            $c->fields('url','title','type','priority');
            $c->set_subject('рубрику');
            $c->required_fields('title','type');
            //$c->set_rules('url','URL','required|alpha_dash');
            $c->set_relation('type','sections_types','name');
            
            $c->field_type('route_id', 'hidden');
            
            $c->display_as('section_id','#')
              ->display_as('url','URL')
              ->display_as('title','Заголовок')
              ->display_as('count_views','Просмотров')
              ->display_as('sequence','Порядок')
              ->display_as('type','Тип')
              ->display_as('mp','Лента на главной')
              ->display_as('show_latest_news','Лента в футере');

            $c->change_field_type('parent_id', 'hidden', $parent_id);  
           
            $c->callback_before_insert(array($this, 'add_section_valid_url'));
            $c->callback_before_update(array($this, 'update_section_valid_url'));
            $c->callback_before_delete(array($this, 'delete_section_url'));
                 
    	    $output = $c->render();
    			
    	    $this->_my_output($output);
         
        }else{
            
            redirect('auth/login');
        }
	
    }
    
   	function sections_types()
	{
	    if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role('admin'))
        {
    	    
            $c = new grocery_CRUD();
            
            $c->unset_read();
            
            $c->set_theme('datatables'); 
            $c->set_table('sections_types');
    
            $c->columns('sections_types_id','abb','name');
            $c->set_subject('тип раздела');
            $c->required_fields('abb','name');
            //$c->set_rules('url','URL','required|alpha_dash');
            
            $c->display_as('sections_types_id','#')
              ->display_as('abb','Аббревиа')
              ->display_as('name','Название');
                 
    	    $output = $c->render();
    			
    	    $this->_my_output($output);
         
        }else{
            
            redirect('auth/login');
        }
	
    }
    
    function materials()
	{
	    if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator','author')))
        {

            $c = new grocery_CRUD();

//            $c->add_action('RU', '', 'adminpanel/photos_ru','ui-icon-image');
//            $c->add_action('UA', '', 'adminpanel/photos_ua','ui-icon-image');
//            $c->add_action('EN', '', 'adminpanel/photos_en','ui-icon-image');

            if ($this->dx_auth->is_role(array('admin','moderator'))) {

                $c->add_action('RU', '', 'adminpanel/content/materials/ru','ui-icon-plus');
                $c->add_action('UA', '', 'adminpanel/content/materials/ua','ui-icon-plus');
                $c->add_action('EN', '', 'adminpanel/content/materials/en','ui-icon-plus');
                
            }

            $c->unset_read();

            $c->set_theme('datatables');
            $c->set_table('materials');
            $c->columns('material_id','title','date');
            $c->set_relation('section','sections','title');
			$c->set_relation('author_id','authors','name_ru');
            $c->set_subject('материал');
            $c->fields('title','url','section','author_id','date','route_id','user_add','user_update');
            $c->required_fields('title','section','date');
            //$c->set_rules('url','URL','required|alpha_dash');
            $c->unset_texteditor(array('anons'));

            $c->display_as('material_id','#')
              ->display_as('url','URL')
              ->display_as('title','Заголовок')
              ->display_as('date','Дата добавления')
              ->display_as('section','Раздел')
              ->display_as('author_id','Автор')
              ->display_as('count_views','Просмотров')
              ->display_as('user_add','Автор')
              ->display_as('user_update','Редактор');

            $c->change_field_type('route_id','hidden');
            $c->field_type('count_views','readonly');

            $state = $this->grocery_crud->getState();

            if($state == 'add'){
                $c->change_field_type('user_update','hidden');
                $c->change_field_type('user_add', 'hidden', $this->dx_auth->get_user_id());
            }elseif($state == 'edit'){
               $c->change_field_type('user_add','hidden');
               $c->change_field_type('user_update', 'hidden', $this->dx_auth->get_user_id());
            }

            $c->callback_before_insert(array($this, 'add_material_valid_url'));
            $c->callback_before_update(array($this, 'update_material_valid_url'));
            $c->callback_before_delete(array($this, 'delete_material_url'));

            $c->callback_column('date',array($this,'_callback_date'));

    	    $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

	}

    function comments()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $c = new grocery_CRUD();

            $c->set_theme('datatables');
            $c->set_table('comments');

            $c->add_action('Ссылка на статью', base_url('assets/grocery_crud/themes/flexigrid/css/images/next.gif'), '' ,'', array($this,'get_comment_url'));

            $c->fields('author','email','comment_text','img_url','date','rating','active','language');
            $c->columns('comment_id','author','email','comment_text','date','active','language');
            $c->set_subject('коммент');
            $c->required_fields('author','email','comment_text','date','language');

            $c->set_relation('language','languages','title');

            $c->display_as('comment_id','ID')
                ->display_as('author','Автор')
                ->display_as('email','Email')
                ->display_as('comment_text','Текст')
                ->display_as('date','Дата добавления')
                ->display_as('rating','Рейтинг')
                ->display_as('active','Активно?')
                ->display_as('img_url','Картинка')
                ->display_as('language','Язык');

            $c->set_field_upload('img_url', 'assets/uploads/images/comments/');

            $c->callback_after_upload(array($this,'callback_comment_after_upload'));

            $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

    }

    function feedback()
    {
        if ($this->dx_auth->is_role(array('admin')))
        {

            $crud = new grocery_CRUD();

            $crud->set_theme('datatables');
            $crud->set_table('feedback');
            $crud->columns('feedback_id','form_name','name','time','phone','email');
            $crud->set_subject('сообщения');
            $crud->required_fields('name','email','message','time');

            $crud->display_as('feedback_id','ID')
                ->display_as('name','Имя отправителя')
                ->display_as('form_name','Форма')
                ->display_as('email','Email')
                ->display_as('phone','Телефон')
                ->display_as('message','Сообщение')
                ->display_as('time','Время отправления');

            $crud->order_by('feedback_id','desc');

            $crud->callback_column('time',array($this,'_callback_date'));

            $output = $crud->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');

        }
    }

    function set()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator','author')))
        {
            $c = new grocery_CRUD();
            
            $c->unset_delete();
            $c->unset_add();
            
            $c->set_theme('datatables'); 
            $c->set_table('settings');
            $c->where('setting_id',1);
            $c->columns('site_name_ru','email');
            $c->set_subject('настройки');
            //$c->fields('site_name_ru','text_ru','materials_per_page','products_per_page','email','email_2','email_3','first_phone','second_phone','third_phone','address_ru','vk','facebook','tw','youtube','gplus','skype','linkedin','instagram');
            $c->required_fields('site_name_ru','materials_per_page','products_per_page','email');
            
            $c->display_as('setting_id','Идентификатор')
              ->display_as('site_name_ru','Название сайта RU')
              ->display_as('site_name_ua','Название сайта UA')
              ->display_as('site_name_en','Название сайта EN')
              ->display_as('text_ru','Текст RU')
              ->display_as('text_ua','Текст UA')
              ->display_as('text_en','Текст EN')
              ->display_as('theme','Тема')
              ->display_as('materials_per_page','Пагинация материалов')
              ->display_as('products_per_page','Пагинация товаров')
              ->display_as('phone_1','Телефон 1')
              ->display_as('phone_2','Телефон 2')
              ->display_as('phone_3','Телефон 3')
              ->display_as('phone_4','Телефон 4')
              ->display_as('email','Email')
              ->display_as('email_2','Email 2')
              ->display_as('email_3','Email 3')
              ->display_as('address_ru','Адресс RU')
              ->display_as('address_ua','Адресс UA')
              ->display_as('address_en','Адресс EN')
              ->display_as('price_ru','Прайс RU')
              ->display_as('price_ua','Прайс UA')
              ->display_as('price_en','Прайс EN');

            $c->set_field_upload('price_ru','assets/uploads/files/prices');
            $c->set_field_upload('price_ua','assets/uploads/files/prices');
            $c->set_field_upload('price_en','assets/uploads/files/prices');

            $output = $c->render();
           
            $this->_my_output($output);
       }
    }

    function photos_ru($material_id)
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $crud = new image_CRUD();

            $crud->set_table('photos');
            $crud->set_primary_key_field('photo_id');
            $crud->set_url_field('img_url');
            $crud->set_title_field('title_ru');

            $crud->set_relation_field('material')->set_ordering_field('priority')->set_image_path("assets/uploads/images/photos");

            $output = $crud->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }
    }

    function photos_en($material_id)
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $crud = new image_CRUD();

            $crud->set_table('photos');
            $crud->set_primary_key_field('photo_id');
            $crud->set_url_field('img_url');
            $crud->set_title_field('title_en');

            $crud->set_relation_field('material')->set_ordering_field('priority')->set_image_path("assets/uploads/images/photos");

            $output = $crud->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }
    }

    function photos_ua($material_id)
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin','moderator')))
        {

            $crud = new image_CRUD();

            $crud->set_table('photos');
            $crud->set_primary_key_field('photo_id');
            $crud->set_url_field('img_url');
            $crud->set_title_field('title_ua');

            $crud->set_relation_field('material')->set_ordering_field('priority')->set_image_path("assets/uploads/images/photos");

            $output = $crud->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }
    }

    function menu($parent_id = 0)
	{

        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin')))
        {

            $c = new grocery_CRUD();

            $c->unset_read();

            $c->where('parent_id',$parent_id);

            $c->add_action('Контент', '', 'adminpanel/menu_content/','ui-icon-plus');
            $c->add_action('Подменю', '', 'adminpanel/menu','ui-icon-plus');

            $c->set_theme('datatables');
            $c->set_table('menu');
            $c->columns('sequence','title','url','show');
            $c->set_subject('пункт меню');
            $c->required_fields('title','url','sequence');

            $c->display_as('sequence','#')
              ->display_as('title','Заголовок')
              ->display_as('url','URL')
              ->display_as('show','Выводить?');

            $c->change_field_type('parent_id', 'hidden', $parent_id);

    	    $output = $c->render();

            $this->_my_output($output);

        }else{

            redirect('auth/login');
        }

	}

    function menu_content($fid)
	{
	    if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin')))
        {
            $c = new grocery_CRUD();

            $c->unset_read();

            $c->set_theme('datatables');
            $c->set_table('menu_content');

            $c->where('fid',$fid);

            $c->columns('menu_content_id','title','language');
            $c->set_subject('контент пункта меню');
            $c->required_fields('title','language');

            $c->set_relation('language','languages','title');

            $c->display_as('menu_content_id','#')
              ->display_as('title','Заголовок')
              ->display_as('language','Язык');

            $c->field_type('fid','hidden',$fid);

            $state = $this->grocery_crud->getState();

            if($state == 'add'){
                $c->change_field_type('user_update','hidden', $this->dx_auth->get_user_id());
                $c->change_field_type('user_add', 'hidden', $this->dx_auth->get_user_id());
            }elseif($state == 'edit'){
               $c->change_field_type('user_add','hidden', $this->dx_auth->get_user_id());
               $c->change_field_type('user_update', 'hidden', $this->dx_auth->get_user_id());
            }

    	    $output = $c->render();

    	    $this->_my_output($output);

        }else{

            redirect('auth/login');
            
        }

    }

    function languages_completed($table,$lang,$fid){

        $this->load->model('front_crud');

        $languages_completed = $this->front_crud->languages_completed($table,$lang,$fid);

        return $languages_completed;

    }

    function services_content_completed($lang,$fid){

        $this->load->model('front_crud');

        $services_content_completed = $this->front_crud->services_content_completed($lang,$fid);

        return $services_content_completed;

    }
    


    function callback_category_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');
     
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
        $file_thumbs_path = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;
     
        $this->image_moo->
            load($file_uploaded)->
            resize(640,640)->
            save($file_uploaded,true)->
            resize(200,200)->
            save($file_thumbs_path,true);
        return true;
    }

    function callback_certificate_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');
     
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
        $file_thumbs_path = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;
     
        $this->image_moo->
            load($file_uploaded)->
            resize(1024,768)->
            save($file_uploaded,true)->
            resize(320,240)->
            save($file_thumbs_path,true);
        return true;
    }

    function callback_comment_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        $file_thumbs_path = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;

        $this->image_moo->
            load($file_uploaded)->
            resize(1024,768)->
            save($file_uploaded,true)->
            resize_crop(480,420)->
            save($file_thumbs_path,true);
        return true;
    }

    function callback_slides_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        $file_thumbs_path = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;

        $this->image_moo->
        load($file_uploaded)->
        set_jpeg_quality(100)->
        resize(2150,1280)->
        save($file_uploaded,true)->
        resize_crop(200,133)->
        save($file_thumbs_path,true);
        return true;
    }

    function callback_content_after_upload($uploader_response,$field_info, $files_to_upload){
        
        $this->load->library('image_moo');
     
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        $file_medium_path = $field_info->upload_path.'/medium/'.$uploader_response[0]->name;
        $file_thumbs_path = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;

        
        $folder         = $field_info->upload_path.'/';
        $folder_thumb   = $field_info->upload_path.'/thumbs/';
        $folder_medium  = $field_info->upload_path.'/medium/';
        
        //$watermark="watermark.png";
        
    	if(!is_dir($folder))
    	{
    		mkdir($folder, 0777);
    	
            if(!is_dir($folder_thumb))
    	    {
    	       mkdir($folder_thumb, 0777);
            }

            if(!is_dir($folder_medium))
            {
                mkdir($folder_medium, 0777);
            }

        }   
     
        $this->image_moo->
            load($file_uploaded)->
            //load_watermark('assets/'.$watermark)->
            resize(1140,640)->
            //watermark(1)->
            save($file_uploaded,true)->
            resize_crop(400,400)->
            save($file_medium_path,true)->
            resize_crop(128,128)->
            save($file_thumbs_path,true);
        echo $this->image_moo->display_errors();
        
        return true;
        
    }
    
    function callback_product_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');
     
        //Is only one file uploaded so it ok to use it with $uploader_response[0].
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
        $file_thumbs_path = $field_info->upload_path.'/thumbs/'.$uploader_response[0]->name;
        
        $folder = $field_info->upload_path.'/';
        $folder_thumb = $field_info->upload_path.'/thumbs/';
        
        $watermark="watermark.png";
        
    	if(!is_dir($folder))
    	{
    		mkdir($folder, 0777);
    	
         if(!is_dir($folder_thumb))
    	    {
    	       mkdir($folder_thumb, 0777);
            }           	       
                    
        }   
     
        $this->image_moo->
            load($file_uploaded)->
            load_watermark('assets/'.$watermark)->
            resize(1024,768)->
            watermark(1)->
            save($file_uploaded,true)->
            resize_crop(128,128)->
            save($file_thumbs_path,true);
        echo $this->image_moo->display_errors();
        
        return true;
    }

    function add_page_valid_url($post_array = array(), $primary_key = null)
    {
        
        $this->load->model('Routes_model');
        
        if(empty($post_array['url']) || $post_array['url']=='')
		{
            $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
		}else{
		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
		}
        
        // validation URL
        $url			= $this->Routes_model->validate_url($post_array['url']);
	    $route['url']	= $url;	
        $route['route']	= 'pages/view/'.$url;	
	    $route_id		= $this->Routes_model->save($route);
        
        $post_array['url']      = $url;
	    $post_array['route_id'] = $route_id;
        
        return $post_array;
    }
    
    function update_page_valid_url($post_array = array(), $primary_key = null)
    {
		$this->load->model('Routes_model');
        
        if(empty($post_array['url']) || $post_array['url']=='')
		{
            $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
		}else{
		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
		}
        
        // validation URL
        $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
        $route['id']        = $post_array['route_id'];
        $route['url']	    = $url;
        $route['route']	    = 'pages/view/'.$url;	
	    $route_id           = $this->Routes_model->save($route);
        
        $post_array['url']      = $url;
        $post_array['route_id'] = $route_id;
        
        return $post_array;

    }
	
    function delete_page_url($primary_key)
    {
        $this->load->model('Routes_model');
        
        $this->db->where('page_id',$primary_key);
        $query = $this->db->get('pages')->row();
    			
        $this->Routes_model->delete($query->route_id);
    }
    
    
    function add_category_valid_url($post_array = array(), $primary_key = null)
    {
        
        $this->load->model('Routes_model');
        
        if(empty($post_array['url']) || $post_array['url']=='')
		{
            $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
		}else{
		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
		}
        
        // validation URL
        $url			= $this->Routes_model->validate_url($post_array['url']);
	    $route['url']	= $url;	
        $route['route']	= 'category/view/'.$url;	
	    $route_id		= $this->Routes_model->save($route);
        
        $post_array['url']      = $url;
	    $post_array['route_id'] = $route_id;
        
        return $post_array;
        
    }
    
    function update_category_valid_url($post_array = array(), $primary_key = null)
    {
		
        $this->load->model('Routes_model');
        
        if(empty($post_array['url']) || $post_array['url']=='')
		{
            $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
		}else{
		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
		}
        
        // validation URL
        $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
        $route['id']        = $post_array['route_id'];
        $route['url']	    = $url;
        $route['route']	    = 'category/view/'.$url;	
	    $route_id           = $this->Routes_model->save($route);
        
        $post_array['url']      = $url;
        $post_array['route_id'] = $route_id;
        
        return $post_array;

    }
	
    function delete_category_url($primary_key)
    {
        
        $this->load->model('Routes_model');
        
        $this->db->where('category_id',$primary_key);
        $query = $this->db->get('categories')->row();
    			
        $this->Routes_model->delete($query->route_id);
        
    }

    function add_section_valid_url($post_array = array(), $primary_key = null)
    {
        
        $this->load->model('Routes_model');
        
        if($post_array['type'] != 'link'){
            
            if(empty($post_array['url']) || $post_array['url']=='')
		    {
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
    		}else{
    		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
    		}
            
            // validation URL
            $url			= $this->Routes_model->validate_url($post_array['url']);
    	    $route['url']	= $url;	
            $route['route']	= 'sections/view/'.$url;	
    	    $route_id		= $this->Routes_model->save($route);
            
            $post_array['url']      = $url;
    	    $post_array['route_id'] = $route_id;
            
            return $post_array;
            
        }else{
            
            if(empty($post_array['url']) || $post_array['url']=='')
		    {
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
    		}else{
    		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
    		}
            
            // validation URL
            $url			= $this->Routes_model->validate_url($post_array['url']);
    	    $route['url']	= $url;	
            $route['route']	= $url;	
    	    $route_id		= $this->Routes_model->save($route);
            
            $post_array['url']      = $url;
    	    $post_array['route_id'] = $route_id;
            
            return $post_array;
            
        }
        
        
    }
    
    function update_section_valid_url($post_array = array(), $primary_key = null)
    {
		$this->load->model('Routes_model');
        
        if($post_array['type'] != 'link'){
        
            if(empty($post_array['url']) || $post_array['url']=='')
    		{
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
    		}else{
    		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
    		}
            
            // validation URL
            $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
            $route['id']        = $post_array['route_id'];
            $route['url']	    = $url;
            $route['route']	    = 'sections/view/'.$url;	
    	    $route_id           = $this->Routes_model->save($route);
            
            $post_array['url']      = $url;
            $post_array['route_id'] = $route_id;
            
            return $post_array;
            
         }else{
            
            if(empty($post_array['url']) || $post_array['url']=='')
    		{
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
    		}else{
    		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
    		}
            
            // validation URL
            $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
            $route['id']        = $post_array['route_id'];
            $route['url']	    = $url;
            $route['route']	    = $url;	
    	    $route_id           = $this->Routes_model->save($route);
            
            $post_array['url']      = $url;
            $post_array['route_id'] = $route_id;
            
            return $post_array;
            
         }

    }
    
    function delete_section_url($primary_key)
    {
        $this->load->model('Routes_model');
        
        $this->db->where('section_id',$primary_key);
        $query = $this->db->get('sections')->row();
    			
        $this->Routes_model->delete($query->route_id);
    }

    function add_service_valid_url($post_array = array(), $primary_key = null)
    {

        $this->load->model('Routes_model');

        if($post_array['type'] != 'link'){

            if(empty($post_array['url']) || $post_array['url']=='')
            {
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
            }else{
                $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);
            }

            // validation URL
            $url			= $this->Routes_model->validate_url($post_array['url']);
            $route['url']	= $url;
            $route['route']	= 'service/view/'.$url;
            $route_id		= $this->Routes_model->save($route);

            $post_array['url']      = $url;
            $post_array['route_id'] = $route_id;

            return $post_array;

        }else{

            if(empty($post_array['url']) || $post_array['url']=='')
            {
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
            }else{
                $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);
            }

            // validation URL
            $url			= $this->Routes_model->validate_url($post_array['url']);
            $route['url']	= $url;
            $route['route']	= $url;
            $route_id		= $this->Routes_model->save($route);

            $post_array['url']      = $url;
            $post_array['route_id'] = $route_id;

            return $post_array;

        }


    }

    function update_service_valid_url($post_array = array(), $primary_key = null)
    {
        $this->load->model('Routes_model');

        if($post_array['type'] != 'link'){

            if(empty($post_array['url']) || $post_array['url']=='')
            {
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
            }else{
                $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);
            }

            // validation URL
            $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
            $route['id']        = $post_array['route_id'];
            $route['url']	    = $url;
            $route['route']	    = 'service/view/'.$url;
            $route_id           = $this->Routes_model->save($route);

            $post_array['url']      = $url;
            $post_array['route_id'] = $route_id;

            return $post_array;

        }else{

            if(empty($post_array['url']) || $post_array['url']=='')
            {
                $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
            }else{
                $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);
            }

            // validation URL
            $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
            $route['id']        = $post_array['route_id'];
            $route['url']	    = $url;
            $route['route']	    = $url;
            $route_id           = $this->Routes_model->save($route);

            $post_array['url']      = $url;
            $post_array['route_id'] = $route_id;

            return $post_array;

        }

    }

    function delete_service_url($primary_key)
    {
        $this->load->model('Routes_model');

        $this->db->where('service_id',$primary_key);
        $query = $this->db->get('services')->row();

        $this->Routes_model->delete($query->route_id);
    }
    
    function add_product_valid_url($post_array = array(), $primary_key = null)
    {
        
        $this->load->model('Routes_model');
        
        if(empty($post_array['url']) || $post_array['url']=='')
		{
            $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
		}else{
		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
		}
        
        // validation URL
        $url			= $this->Routes_model->validate_url($post_array['url']);
	    $route['url']	= $url;	
        $route['route']	= 'product/view/'.$url;	
	    $route_id		= $this->Routes_model->save($route);
        
        $post_array['url']      = $url;
	    $post_array['route_id'] = $route_id;
        
        return $post_array;
    }
    
    function update_product_valid_url($post_array = array(), $primary_key = null)
    {
		$this->load->model('Routes_model');
        
        if(empty($post_array['url']) || $post_array['url']=='')
		{
            $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
		}else{
		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);  
		}
        
        // validation URL
        $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
        $route['id']        = $post_array['route_id'];
        $route['url']	    = $url;
        $route['route']	    = 'product/view/'.$url;	
	    $route_id           = $this->Routes_model->save($route);
        
        $post_array['url']      = $url;
        $post_array['route_id'] = $route_id;
        
        return $post_array;

    }
	
    function delete_product_url($primary_key)
    {
        $this->load->model('Routes_model');
        
        $this->db->where('product_id',$primary_key);
        $query = $this->db->get('products')->row();
    			
        $this->Routes_model->delete($query->route_id);
    }
    
    function add_material_valid_url($post_array = array(), $primary_key = null)
    {

        $this->load->model('Routes_model');

        if(empty($post_array['url']) || $post_array['url']=='')
		{
            $post_array['url'] = url_title(convert_accented_characters($post_array['title']), 'dash', TRUE);
		}else{
		    $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);
		}

        // validation URL
        $url			= $this->Routes_model->validate_url($post_array['url']);
	    $route['url']	= $url;
        $route['route']	= 'materials/view/'.$url;
	    $route_id		= $this->Routes_model->save($route);

        $post_array['url']      = $url;
	    $post_array['route_id'] = $route_id;

        return $post_array;
    }

    function update_material_valid_url($post_array = array(), $primary_key = null)
    {
		$this->load->model('Routes_model');

        //die($post_array['route_id']);

        //if it's empty assign the name field
		if(empty($post_array['url']) || $post_array['url']=='')
		{
			$post_array['url'] = $post_array['title_ru'];
            $post_array['url'] = url_title(convert_accented_characters($post_array['url']), 'dash', TRUE);
		}

        // validation URL
        $url                = $this->Routes_model->validate_url($post_array['url'], $post_array['route_id']);
        $route['id']        = $post_array['route_id'];
        $route['url']	    = $url;
        $route['route']	    = 'materials/view/'.$url;
	    $route_id           = $this->Routes_model->save($route);

        $post_array['url']      = $url;
        $post_array['route_id'] = $route_id;

        return $post_array;

    }

    function delete_material_url($primary_key)
    {
        $this->load->model('Routes_model');

        $this->db->where('material_id',$primary_key);
        $query = $this->db->get('materials')->row();

        $this->Routes_model->delete($query->route_id);
    }
    
    function _callback_date($value, $row)
    {
        return "<span style='visibility:hidden;display:none;'>".date('Y-m-d H:i:s', strtotime($value))."</span>".date('d/m/Y H:i:s', strtotime($value));
    }

    function get_comment_url($primary_key , $row)
    {
        $this->load->model(array('materials_model','pages_model'));

        if($row->type == 1){

            $data['material'] = $this->materials_model->get($row->foreign_id);

            return base_url().$data['material']['url'];

        }elseif($row->type == 2){

            $data['page'] = $this->pages_model->get($row->foreign_id);

            return base_url().$data['page']['url'];

        }

    }

    /* ??? */
    public function reorder($pcat)
    {
        
        $this->auth_lib->check_admin();
        $this->load->model('catalog_model');
        
        $data['back'] = anchor("adminpanel/categories/$pcat", "К списку категорий",'class="back"');
        $data['cats_list'] = $this->catalog_model->get_subs($pcat);
        $data['cats_reorder'] = TRUE;
        
        $name = 'reorder_cats';
        $this->display_lib->admin_page($data,$name);
        
    }

    public function save_order()
    {
        
        $this->load->model('catalog_model');
        
        $items = $this->input->post('item');
        
        $this->catalog_model->catalog_reorder($items);
        
        $this->session->set_flashdata('result', 'Категории успешно отсортированы!');
    
        redirect('adminpanel/', 'refresh');
    
    }
    /* ??? */

    public function minify()
    {
        if (!empty($this->all_userdata['DX_role_name']) && $this->dx_auth->is_role(array('admin')))
        {

            $this->load->library('minify');

            $data = array();

            $name = 'minify';

            $this->display_lib->admin_page($data ,$name);

        }else{

            redirect('auth/login');

        }
    }

    function tinymce_image_upload($url = '')
    {

        $this->data['file_name'] 	= false;
        $this->data['error']		= false;

        $config['allowed_types'] 	= 'gif|jpg|png|jpeg';
        $config['max_size']			= 2048;
        if($url=='blog'){
            $config['upload_path']  = 'assets/uploads/images/tinymce/users-images/';
        }else{
            $config['upload_path']  = 'assets/uploads/images/tinymce/';
        }
        $config['encrypt_name'] 	= false;
        $config['remove_spaces'] 	= true;
        $config['max_width']     	= 2048;
        $config['max_height']    	= 1536;

        $this->load->library('upload', $config);

        foreach ($_FILES as $key => $value) {

            if ( ! $this->upload->do_upload($key))
            {

                $error = true;

                $response = array(
                    'responseText' => strip_tags($this->upload->display_errors()),
                );

                echo json_encode($response);

                die;

            } else {

                $upload_data= $this->upload->data();

                if($url=='blog'){
                    $this->do_blog_moo($upload_data);
                }else{
                    $this->do_moo($upload_data);
                }

                $response = array(
                    'success'   => true,
                    'location'  => $upload_data['file_name'],
                );

                echo json_encode($response);

                die;
            }

            if($this->upload->display_errors() != '')
            {
                $error = true;
                $response = array(
                    'responseText' => strip_tags($this->upload->display_errors()),
                );

                echo json_encode($response);

                die;
            }

        }

    }

    function do_moo($image)
    {

        $source_file = $image['file_path'].$image['file_name'];
        $medium = 'assets/uploads/images/tinymce/'.$image['file_name'];

        $this->load->library('image_moo');

        $this->image_moo->load($source_file)
            ->set_jpeg_quality(100)
            ->resize(1280,1024)
            ->save($medium,true);

        // if($this->image_moo->errors) print $this->image_moo->display_errors();
        if($this->image_moo->errors) return false;

        //if ( is_file( $_SERVER["DOCUMENT_ROOT"] ."/images/".$image['file_name'] )  ) unlink ( $_SERVER["DOCUMENT_ROOT"] ."/images/".$image['file_name'] );

        return true;
    }

    function import_csv(){

        /*for($i=2000;$i<3000;$i++){

            $this->db->where('geodata_id', $i);
            $this->db->delete('geodata');

        }*/

        dump_exit('Ready!');

        $this->load->library('csvimport');

        $data['file_name']  = false;
        $data['error']      = false;

        $config['allowed_types'] = 'csv';
        $config['max_size'] = '10000000';
        $config['upload_path'] = 'uploads/csv/';
        $config['encrypt_name'] = false;
        $config['remove_spaces'] = true;

        $data['file_name'] = 'data-20190220T1309-structure-20180314T1826.csv';
        $data['file_path'] = 'assets/uploads/files/csv/'.$data['file_name'];

        $csv_array = $this->csvimport->get_array($data['file_path'], FALSE, FALSE, FALSE, ',');

        foreach ($csv_array as $key => $value){

            //$value['reg_name'] = mb_convert_encoding($value['reg_name'], 'UTF-8', 'WINDOWS-1251');

            //dump_exit($value);

            //Check if this geotarget is already exist!
            $geo = $this->db->select('*')
                ->like('coordinates', '['.$value['longitude'].','.$value['latitude'].']')
                ->get('geodata')
                ->row();

            if(!$geo && empty($geo)){

                $geodata = [];

                $properties = new stdClass;
                $properties->desc = $value['address'].'<br/> Причина ДТП: '.$value['crash_reason'].'<br/> Пострадавших: '.$value['participants_amount'].'<br/> Смертей: '.$value['fatalities_amount'];
                $properties->image = null;
                $properties->source = 'https://безопасныедороги.рф';
                $properties->officially = 1;

                $geometry = new stdClass;
                $geometry->type = 'Point';
                $geometry->coordinates = [$value['longitude'],$value['latitude']];

                $features = new stdClass;
                $features->type = 'Feature';
                $features->properties = $properties;
                $features->geometry = $geometry;

                $content = [
                    'type' => 'FeatureCollection',
                    'features' => [
                        $features
                    ]
                ];
                $content = json_encode($content);

                $geodata['type'] = 'Point';
                $geodata['coordinates'] = '['.$value['longitude'].','.$value['latitude'].']';
                $geodata['description'] = $value['address'].'; Причина ДТП: '.$value['crash_reason'].'; Пострадавших: '.$value['participants_amount'].'; Смертей: '.$value['fatalities_amount'];
                $geodata['image'] = NULL;
                $geodata['content'] = $content;
                $geodata['source'] = 'https://безопасныедороги.рф';
                $geodata['officially'] = 1;
                //$geodata['date'] = date('Y-m-d H:i:s');
                $geodata['status'] = 1;
                $geodata['user_add'] = 16;

                //

                $this->db->insert('geodata', $geodata);

                //dump_exit($geodata);

            }

        }
    }

    function import_csv_russia(){

        /*for($i=2000;$i<3000;$i++){

            $this->db->where('geodata_id', $i);
            $this->db->delete('geodata');

        }*/

        //dump_exit('Ready!');

        $this->load->library('csvimport');

        $data['file_name']  = false;
        $data['error']      = false;

        $config['allowed_types'] = 'csv';
        $config['max_size'] = '10000000';
        $config['upload_path'] = 'uploads/csv/';
        $config['encrypt_name'] = false;
        $config['remove_spaces'] = true;

        $data['file_name'] = 'data-20190220T1309-structure-20180314T1826.csv';
        $data['file_path'] = 'assets/uploads/files/csv/'.$data['file_name'];

        $csv_array = $this->csvimport->get_array($data['file_path'], FALSE, FALSE, FALSE, ',');

        /*
         * //let`s find out max existing value length
         *
        $len = [];

        $len['reg_code'] = 0;
        $len['reg_name'] = 0;
        $len['road_code'] = 0;
        $len['road_name'] = 0;
        $len['road_type'] = 0;
        $len['oktmo'] = 0;
        $len['address'] = 0;
        $len['crash_type_name'] = 0;
        $len['crash_reason'] = 0;
        $len['fatalities_amount'] = 0;
        $len['victims_amount'] = 0;
        $len['vehicles_amount'] = 0;
        $len['participants_amount'] = 0;

        $i = 0;

        foreach ($csv_array as $key => $value){

            $len['reg_code'] =  max($len['reg_code'],strlen($value['reg_code']));
            $len['reg_name'] =  max($len['reg_name'],strlen($value['reg_name']));
            $len['road_code'] =  max($len['road_code'],strlen($value['road_code']));
            $len['road_name'] =  max($len['road_name'],strlen($value['road_name']));
            $len['road_type'] =  max($len['road_type'],strlen($value['road_type']));
            $len['oktmo'] =  max($len['oktmo'],strlen($value['oktmo']));
            $len['address'] =  max($len['address'],strlen($value['address']));
            $len['crash_type_name'] =  max($len['crash_type_name'],strlen($value['crash_type_name']));
            $len['crash_reason'] =  max($len['crash_reason'],strlen($value['crash_reason']));
            $len['fatalities_amount'] =  max($len['fatalities_amount'],strlen($value['fatalities_amount']));
            $len['victims_amount'] =  max($len['victims_amount'],strlen($value['victims_amount']));
            $len['vehicles_amount'] =  max($len['vehicles_amount'],strlen($value['vehicles_amount']));
            $len['participants_amount'] =  max($len['participants_amount'],strlen($value['participants_amount']));
            dump($i);

            $i++;

        }

        dump_exit($len);*/

        //let`s add all places

        for ($y = 0; $y < count($csv_array); $y++){

            //$csv_array[$y]['reg_name'] = mb_convert_encoding($csv_array[$y]['reg_name'], 'UTF-8', 'WINDOWS-1251');

            //Check if this geotarget is already exist!
            $geo = $this->db->select('*')
                ->like('latitude', $csv_array[$y]['latitude'])
                ->like('longitude', $csv_array[$y]['longitude'])
                ->get('geodata_dtp_russia')
                ->row();

            if(!$geo && empty($geo)){

                $geodata = [];

                $geodata['reg_code'] = $csv_array[$y]['reg_code'];
                $geodata['reg_name'] = $csv_array[$y]['reg_name'];
                $geodata['road_code'] = $csv_array[$y]['road_code'];
                $geodata['road_name'] = $csv_array[$y]['road_name'];
                $geodata['road_type'] = $csv_array[$y]['road_type'];
                $geodata['oktmo'] = $csv_array[$y]['oktmo'];
                $geodata['address'] = $csv_array[$y]['address'];
                $geodata['crash_type_name'] = $csv_array[$y]['crash_type_name'];
                $geodata['crash_datetime'] = date('Y-m-d H:i:s',strtotime($csv_array[$y]['crash_date'].$csv_array[$y]['crash_time']));
                $geodata['crash_reason'] = $csv_array[$y]['crash_reason'];
                $geodata['fatalities_amount'] = $csv_array[$y]['fatalities_amount'];
                $geodata['victims_amount'] = $csv_array[$y]['victims_amount'];
                $geodata['vehicles_amount'] = $csv_array[$y]['vehicles_amount'];
                $geodata['participants_amount'] = $csv_array[$y]['participants_amount'];
                $geodata['latitude'] = $csv_array[$y]['latitude'];
                $geodata['longitude'] = $csv_array[$y]['longitude'];
                $geodata['source'] = 'https://безопасныедороги.рф';

                $this->db->insert('geodata_dtp_russia', $geodata);

                dump($y);

            }

        }

        dump_exit('That`s all folks!');

    }

    /*function save_blog()
    {
        $this->load->model('Routes_model');

        $this->db->select('pavblog_blog.blog_id as material_id, meta_title as title, created as date, hits as count_views');
        $this->db->from('pavblog_blog');
        $this->db->join('pavblog_blog_description','pavblog_blog_description.blog_id = pavblog_blog.blog_id');
        $this->db->where('pavblog_blog_description.language_id',1);
        $this->db->order_by('pavblog_blog.blog_id','asc');
        $blogs = $this->db->get()->result_array();

//        foreach ($blogs as $blog){
//
//            if($blog['title'] == ''){
//
//                $blog['section'] = 16;
//                $blog['url'] = url_title(convert_accented_characters($blog['title']), 'dash', TRUE);
//                $blog['user_add'] = 16;
//                $blog['user_update'] = 16;
//                $blog['author_id'] = 1;
//                $blog['like'] = 10;
//
//                $url			= $this->Routes_model->validate_url($blog['url']);
//                $route['url']	= $url;
//
//                //unset($blog['meta_title']);
//
//                $route['route']	= 'materials/view/'.$url;
//                $route_id		= $this->Routes_model->save($route);
//
//                $blog['url']    = $url;
//                $blog['route_id'] = $route_id;
//
//                $this->db->insert('materials', $blog);
//
//            }
//
//        }


        $this->db->select('pavblog_blog_description.*, pavblog_blog.image, pavblog_blog.tags');
        $this->db->from('pavblog_blog_description');
        $this->db->join('pavblog_blog','pavblog_blog.blog_id = pavblog_blog_description.blog_id');

        $query = $this->db->get()->result_array();

        //dump($query);

//        foreach ($query as $item){
//
//            if($item['title'] != ''){
//
//                $content['fid']         = (int)$item['blog_id'];
//                $content['table']       = 'materials';
//                $content['title']       = $item['title'];
//                $content['seo_title']   = $item['title'].' — DriveForce';
//                $content['keywords']    = $item['tags'];
//                $content['description'] = trim(strip_tags(html_entity_decode($item['description'], ENT_QUOTES, 'UTF-8')));
//                $content['anons']       = trim(strip_tags(html_entity_decode($item['description'], ENT_QUOTES, 'UTF-8')));
//                $content['text']        = html_entity_decode($item['content'], ENT_QUOTES, 'UTF-8');
//                $content['img_url']     = str_replace('data/','',$item['image']);
//                $content['video_url']   = '';
//                $content['show']        = 1;
//                if($item['language_id'] == 1){
//                    $content['language'] = 'ru';
//                }elseif ($item['language_id'] == 3){
//                    $content['language'] = 'en';
//                }elseif ($item['language_id'] == 4){
//                    $content['language'] = 'ua';
//                }
//                $content['user_add']    = 16;
//                $content['user_update'] = 16;
//
//                dump($content);
//
//                $this->db->insert('content', $content);
//
//            }
//
//        }

        print_r('test');
        die;

    }*/


    /*function mo_blog_image(){

        $this->load->library('image_moo');

        $this->db->select('content.*');
        $this->db->from('content');
        $this->db->where('table','materials');
        $this->db->where('fid !=',1);
        $this->db->where('fid !=',2);
        $this->db->where('img_url !=','');
        $blogs = $this->db->get()->result_array();

        $base_folder = "assets/uploads/images/content/";
        $folder      = "assets/uploads/images/content/thumbs/";

        foreach($blogs as $photo){

            $newfile = $folder.$photo['img_url'];
            $oldfile = $base_folder.$photo['img_url'];

            if (!copy($oldfile, $newfile)) {
                echo "Failed to copy file $oldfile...</br>";
            }else{
                echo "File was copied $newfile...</br>";
            }

            $newfilethumb = $folder.$photo['img_url'];
            $oldfilethumb = $base_folder.$photo['img_url'];

            if (!copy($oldfilethumb, $newfilethumb)) {
                echo "Failed to copy file $oldfilethumb...</br>";
            }else{
                echo "File was copied $newfilethumb...</br>";
            }

            $newfilemini = $folder.$photo['img_url'];

            $this->image_moo->load($oldfile)
                ->set_jpeg_quality(100)
                ->resize_crop(128,128)
                ->save($newfilemini,true);

        }

        //thumb
        //resize_crop(128,128)

    }*/
	
}