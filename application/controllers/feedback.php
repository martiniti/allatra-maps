
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('feedback_model');
    }

	public function index()
	{
        // move_uploaded_file ( string $filename , string $destination );
		$feedback = force_int($this->input->post('feedback'));

        if(!$this->input->post() || !$feedback)
        {
            echo json_encode(array('err' => 1, 'content' => 'Ошибка. Проверьте правильность заполнения полей.'));
			die();
        }

		$this->load->library(array(
            'typography',
            'email',
            'form_validation',
            ));

		switch($feedback){

			case 1:
                //Установка правил валидации
                $this->form_validation->set_rules($this->feedback_model->rules_1);
                //Если валидация пройдена
                if ($this->form_validation->run() == TRUE)
                {

                    // Массив для вставки данных из формы
                    $data = $this->feedback_model->array_from_post(array(
                        'name',
                        'email',
                        'topic',
                        'message',
                        'url',
                    ));

                    $data['message'] = '<p><b>URL:</b> '.$this->input->post('url').' <br/><b>Тема сообщения:</b> '.$this->input->post('topic').' <br/><b>Сообщение:</b> '.$this->input->post('message').'</p>';

                    // Массив для вставки данных по комментарию
                    $fb_data = array();

                    //Уже передан как параметр функции add
                    $fb_data['name']           = $this->input->post('name');
                    $fb_data['email']          = $this->input->post('email');
                    $fb_data['message']        = $data['message'];
                    $fb_data['form_name']      = 'Форма обратной связи';
                    $fb_data['time']           = date('Y-m-d H:i:s');

                    $this->feedback_model->add_new($fb_data);

                    // Если передан файл
                    if (!empty($_FILES['userfile']['tmp_name'])) {
                        $upload_data = $this->feedback_model->userfile_check();
                        if(!$upload_data){
                            echo json_encode(array('err' => 2, 'content' => 'Ошибка. Не поддерживаемый формат прикреплённого файла. Вы можете отправить файлы в форматах: gif, jpg, jpeg, png, jpeg, doc, docx, rtf, txt, pdf, zip, rar'));
                            die();
                        }
                    }

                    // Куда отправляется письмо
                    $address = EMAIL;
                    $address = 'safe.road@allatravesti.com';

                    // Тема письма как ее видит получатель
                    $subject = 'Сообщение из формы обратной связи';
                    // Формируем тело письма
                    $message = '<b>Имя:</b> '.$data['name'].'<br /><b>Email:</b> '.$data['email'].$data['message'];

                    $this->email->clear();
                    $this->email->set_mailtype('html');
                    $this->email->from('info@ramir.space', 'АЛЛАТРА :: Дорожный Эксперимент');
                    $this->email->to($address);
                    $this->email->bcc('martiniti@ukr.net');
                    $this->email->subject($subject);
                    $this->email->message($message);

                    if (!empty($upload_data)) {
                        $this->email->attach($upload_data["full_path"]);
                    }

                    // Отправляем письмо
                    $mail = $this->email->send();

                    //$mail = @ mail ($address,$subject,$message,"Content-type:text/html;charset = utf8");
                    // Если письмо отправлено
                    if($mail) {
                        // Возвращаем ответ об успехе
                        echo json_encode(array('err' => 0, 'content' => 'Спасибо! Сообщение успешно отправлено.'));
                    }
                    else {
                        echo json_encode(array('err' => 2, 'content' => 'Ошибка отправки сообщения на email. Свяжитесь, пожалуйста, с администратором.'));
                    }

                    die();


                }
                //Если валидация не пройдена
                else
                {
                    echo json_encode(array('err' => 3, 'content' => validation_errors()));

                    die();
                }

			break;

		}
	}

    public function geoData()
    {
        $this->form_validation->set_rules($this->feedback_model->add_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo = $this->input->post('content');
            $geo = json_decode($geo);

            $errors = 0;

            foreach ($geo->features as $feature){

                $fb_data = [];

                $fb_data['type']        = $feature->geometry->type;
                if($feature->geometry->type == 'Polygon'){
                    $fb_data['coordinates'] = json_encode($feature->geometry->coordinates[0]);
                }else{
                    $fb_data['coordinates'] = json_encode($feature->geometry->coordinates);
                }
                $fb_data['description'] = $feature->properties->desc;
                $fb_data['image']       = $feature->properties->image;
                $fb_data['content']     = $this->input->post('content');
                $fb_data['source']      = $feature->properties->source;
                $fb_data['officially']  = $feature->properties->officially;
                $fb_data['user_add']    = $this->session->userdata('DX_user_id');
                if($this->session->userdata('DX_role_id') == 2 || $this->session->userdata('DX_role_id') == 3){
                    $fb_data['status']    = 1;
                }else{
                    $fb_data['status']    = 0;
                }

                if(!$this->feedback_model->add_new_geo($fb_data)){
                    $errors++;
                }

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Спасибо! Данные были успешно загружены.'));
            }
            else {
                echo json_encode(array('err' => 2, 'content' => 'Ошибка при сохранении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            //echo json_encode(array('err' => 1, 'content' => validation_errors()));
            echo json_encode(array('err' => 1, 'content' => 'No valid!'));

            die();
        }

    }

    public function activate_geoData()
    {
        $this->form_validation->set_rules($this->feedback_model->activate_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo_id = (int)$this->input->post('geoid');

            $errors = 0;

            if(is_int($geo_id) && $geo_id > 0){

                if($this->feedback_model->activate_geo($geo_id)){
                    $errors = 0;
                }else{
                    $errors = 1;
                }

            }else{

                $errors = 1;

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Данные были успешно утвержены.'));
            }
            else {
                echo json_encode(array('err' => 1, 'content' => 'Ошибка при утверждении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            echo json_encode(array('err' => 1, 'content' => validation_errors()));

            die();
        }

    }

    public function delete_geoData()
    {
        $this->form_validation->set_rules($this->feedback_model->delete_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo_id = (int)$this->input->post('geoid');

            $errors = 0;

            if(is_int($geo_id) && $geo_id > 0){

                if($this->feedback_model->delete_geo($geo_id)){
                    $errors = 0;
                }else{
                    $errors = 1;
                }

            }else{

                $errors = 1;

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Данные были успешно удалены.'));
            }
            else {
                echo json_encode(array('err' => 1, 'content' => 'Ошибка при удалении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            echo json_encode(array('err' => 1, 'content' => validation_errors()));

            die();
        }

    }


    public function geoDataGeorge()
    {
        $this->form_validation->set_rules($this->feedback_model->add_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo = $this->input->post('content');
            $geo = json_decode($geo);

            $errors = 0;

            foreach ($geo->features as $feature){

                $fb_data = [];

                $fb_data['type']        = $feature->geometry->type;
                if($feature->geometry->type == 'Polygon'){
                    $fb_data['coordinates'] = json_encode($feature->geometry->coordinates[0]);
                }else{
                    $fb_data['coordinates'] = json_encode($feature->geometry->coordinates);
                }
                $fb_data['description'] = $feature->properties->desc;
                $fb_data['image']       = $feature->properties->image;
                $fb_data['content']     = $this->input->post('content');
                $fb_data['source']      = $feature->properties->source;
                $fb_data['officially']  = $feature->properties->officially;
                $fb_data['user_add']    = $this->session->userdata('DX_user_id');
                if($this->session->userdata('DX_role_id') == 2 || $this->session->userdata('DX_role_id') == 3){
                    $fb_data['status']    = 1;
                }else{
                    $fb_data['status']    = 0;
                }

                if(!$this->feedback_model->add_new_geo_george($fb_data)){
                    $errors++;
                }

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Спасибо! Данные были успешно загружены.'));
            }
            else {
                echo json_encode(array('err' => 2, 'content' => 'Ошибка при сохранении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            //echo json_encode(array('err' => 1, 'content' => validation_errors()));
            echo json_encode(array('err' => 1, 'content' => 'No valid!'));

            die();
        }

    }

    public function activate_geoDataGeorge()
    {
        $this->form_validation->set_rules($this->feedback_model->activate_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo_id = (int)$this->input->post('geoid');

            $errors = 0;

            if(is_int($geo_id) && $geo_id > 0){

                if($this->feedback_model->activate_geo_george($geo_id)){
                    $errors = 0;
                }else{
                    $errors = 1;
                }

            }else{

                $errors = 1;

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Данные были успешно утвержены.'));
            }
            else {
                echo json_encode(array('err' => 1, 'content' => 'Ошибка при утверждении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            echo json_encode(array('err' => 1, 'content' => validation_errors()));

            die();
        }

    }

    public function delete_geoDataGeorge()
    {
        $this->form_validation->set_rules($this->feedback_model->delete_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo_id = (int)$this->input->post('geoid');

            $errors = 0;

            if(is_int($geo_id) && $geo_id > 0){

                if($this->feedback_model->delete_geo_george($geo_id)){
                    $errors = 0;
                }else{
                    $errors = 1;
                }

            }else{

                $errors = 1;

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Данные были успешно удалены.'));
            }
            else {
                echo json_encode(array('err' => 1, 'content' => 'Ошибка при удалении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            echo json_encode(array('err' => 1, 'content' => validation_errors()));

            die();
        }

    }


}

/* End of file feedback.php */
/* Location: ./application/controllers/feedback.php */