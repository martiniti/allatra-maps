<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Earthquakes extends MY_Controller {

    public $hash_key = 'ed49984d5f9a0cf66d9652780dc48f18';

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
			'earthquakes_model',
		));

    }

	public function index()
	{
	    $data = [];

	    $this->load->view('pages/earthquakes_view', $data);

	}

    public function json($lang = 'en')
    {

        if(isset($_GET) && !empty($_GET)){

            if($_GET['begin']){

                $date_begin = date('Y-m-d', strtotime($_GET['begin'])).' 00:00:00';
                $date_end   = date('Y-m-d', strtotime($_GET['begin'])).' 23:59:59';

                if($_GET['end']){
                    $date_end = date('Y-m-d', strtotime($_GET['end'])).' 23:59:59';
                }

            } else {

                $date_begin = date("Y-m-d H:i:s", strtotime("-24 hour"));
                $date_end   = date("Y-m-d H:i:s");

            }

            $magnitude = $_GET['magnitude'] ? $_GET['magnitude'] : false;
            $source = false;

        }else{

            $date_begin = date("Y-m-d H:i:s", strtotime("-24 hour"));
            $date_end   = date("Y-m-d H:i:s");
            $magnitude = false;
            $source = false;

        }

        echo $this->earthquakes_model->get_by_period_quakes($date_begin,$date_end,$magnitude,$source,$lang);

    }



    /**
     * Экшн для сбора информации о землетрясениях
     */
	public function gather()
	{
        if($this->input->get('key') == $this->hash_key){
            $this->earthquakes_model->gather_data();
        }
	}

	public function csv()
	{
        if($this->input->get('key') == $this->hash_key){
            $array = $this->earthquakes_model->quakes_array();
            $array = array_reverse($array);
//            pre_exit($array);

            data_to_csv($array, TRUE, "quakes_export");
        }
	}

}