<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model(array('geoapi_model'));

	}
	
	public function get_nearest_dangerous_places($lng, $ltd, $radius, $limit = false, $start_from = false){

	    if(!$radius || $radius == ''){
	        $radius = 5;
        }

	    if(($lng && $lng != '') && ($ltd && $lng!='')){

            $data['places'] = $this->geoapi_model->get_nearest_places($lng, $ltd, $radius, $limit, $start_from);
            $data['num']    = $this->geoapi_model->count_nearest_places($lng, $ltd, $radius);

            echo json_encode($data);


	    }

    }

    public function get_all_dangerous_places($limit = false, $start_from = false){

        $data['places'] = $this->geoapi_model->get_all_places($limit, $start_from);

        echo json_encode($data);
    }

    public function test(){
        $url = 'https://maps.ramir.space/api/add_place';
        $data = array();

        $data["description"] = "4 аварії";
        $data["image"] = "";
        $data["latitude"] = "49.7886035473039";
        $data["longitude"] = "23.70290443665589";
        $data["officially"] = "1";
        $data["source"] = "https://varta1.com/2016/06/29/na-obїzdnij-m-gorodok-bilya-zapravki-okko-stalos-dtp/";

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }

        var_dump($result);
    }

    public function add_place(){

        if(!$_POST || empty($_POST))
        {
            echo json_encode(array('err' => 1, 'content' => 'Ошибка! Неправильный запрос.'));
            die();
        }

        //Установка правил валидации
        $this->form_validation->set_rules($this->geoapi_model->add_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {

            $this->load->model('feedback_model');

            $data = $this->feedback_model->array_from_post(array(
                'latitude',
                'longitude',
                'description',
                'image',
                'officially',
                'source',
            ));

            // Массив для вставки данных по комментарию
            $geo_data = array();

            //Уже передан как параметр функции add
            $geo_data['type']           = 'Point';
            $geo_data['coordinates']    = '['.$data['latitude'].','.$data['longitude'].']';
            $geo_data['latitude']       = $data['latitude'];
            $geo_data['longitude']      = $data['longitude'];
            $geo_data['image']          = $data['image'];
            $geo_data['content']        = '';
            $geo_data['description']    = $data['description'];
            $geo_data['source']         = $data['source'];
            $geo_data['officially']     = $data['officially'];
            $geo_data['date']           = date('Y-m-d H:i:s');
            $geo_data['user_add']       = 0;

            $this->geoapi_model->add_new($geo_data);

            echo json_encode(array('err' => 0, 'content' => 'Спасибо! После проверки модератором, данные будут добавлены на карту.'));
            die();

        }else{

            echo json_encode(array('err' => 1, 'content' => 'Ошибка! Неверно введены данные.'));
            die();

        }

    }

//    function create_geodata_lines(){
//
//        $this->db->select('geodata_id, coordinates, content');
//        $this->db->where('type','LineString');
//        $query = $this->db->get('geodata')->result_array();
//
//        $geodata = [];
//
//        foreach ($query as $line){
//
//            $coordinates = json_decode($line['coordinates']);
//            $geodata['line_id'] = $line['geodata_id'];
//
//            foreach ($coordinates as $coordinate){
//                $geodata['longitude'] = $coordinate[0];
//                $geodata['latitude'] = $coordinate[1];
//                $this->db->insert('geodata_lines', $geodata);
//            }
//
//        }
//
//        dump_exit(count($query));
//
//    }
	
}