<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Allatra_sign_study extends MY_Controller {

    public $hash_key = 'ed49984d5f9a0cf66d9652780dc48f18';

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
			'saferoad_model',
		));

    }

	public function index()
	{
	    $data = [];

	    $this->load->view('pages/allatra_sign_study_view', $data);

	}

	//1
    public function json($lang = 'ru')
    {

        $geoData = $this->saferoad_model->get_geodata($lang);

        echo $geoData;

    }

    public function add_geoData()
    {
        $this->form_validation->set_rules($this->feedback_model->add_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo = $this->input->post('content');
            $geo = json_decode($geo);

            $errors = 0;

            foreach ($geo->features as $feature){

                $fb_data = [];

                $fb_data['type']        = $feature->geometry->type;
                if($feature->geometry->type == 'Polygon'){
                    $fb_data['coordinates'] = json_encode($feature->geometry->coordinates[0]);
                }else{
                    $fb_data['coordinates'] = json_encode($feature->geometry->coordinates);
                }
                $fb_data['description'] = $feature->properties->desc;
                $fb_data['image']       = $feature->properties->image;
                $fb_data['content']     = $this->input->post('content');
                $fb_data['source']      = $feature->properties->source;
                $fb_data['officially']  = $feature->properties->officially;
                $fb_data['user_add']    = $this->session->userdata('DX_user_id');
                if($this->session->userdata('DX_role_id') == 2 || $this->session->userdata('DX_role_id') == 3){
                    $fb_data['status']    = 1;
                }else{
                    $fb_data['status']    = 0;
                }

                if(!$this->feedback_model->add_new_geo($fb_data)){
                    $errors++;
                }

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Спасибо! Данные были успешно загружены.'));
            }
            else {
                echo json_encode(array('err' => 1, 'content' => 'Ошибка при сохранении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            echo json_encode(array('err' => 1, 'content' => validation_errors()));

            die();
        }

    }

    public function activate_geoData()
    {
        $this->form_validation->set_rules($this->feedback_model->activate_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo_id = (int)$this->input->post('geoid');

            $errors = 0;

            if(is_int($geo_id) && $geo_id > 0){

                if($this->feedback_model->activate_geo($geo_id)){
                    $errors = 0;
                }else{
                    $errors = 1;
                }

            }else{

                $errors = 1;

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Данные были успешно утвержены.'));
            }
            else {
                echo json_encode(array('err' => 1, 'content' => 'Ошибка при утверждении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            echo json_encode(array('err' => 1, 'content' => validation_errors()));

            die();
        }

    }

    public function delete_geoData()
    {
        $this->form_validation->set_rules($this->feedback_model->delete_geodata_rules);

        //Если валидация пройдена
        if ($this->form_validation->run() == TRUE)
        {
            $geo_id = (int)$this->input->post('geoid');

            $errors = 0;

            if(is_int($geo_id) && $geo_id > 0){

                if($this->feedback_model->delete_geo($geo_id)){
                    $errors = 0;
                }else{
                    $errors = 1;
                }

            }else{

                $errors = 1;

            }

            if($errors <= 0) {
                // Возвращаем ответ об успехе
                echo json_encode(array('err' => 0, 'content' => 'Данные были успешно удалены.'));
            }
            else {
                echo json_encode(array('err' => 1, 'content' => 'Ошибка при удалении данных! Обратитесь к администратору ресурса.'));
            }

            die();
        }
        //Если валидация не пройдена
        else
        {
            echo json_encode(array('err' => 1, 'content' => validation_errors()));

            die();
        }

    }

}