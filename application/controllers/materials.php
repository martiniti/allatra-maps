<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Materials extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('category_model','comments_model','materials_model','sections_model'));
	}

    public function index()
	{
		redirect(base_url());
	}

	public function view($material_url = '')
	{

		//language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
        
        //$this->output->enable_profiler(TRUE);

        if(!isset($material_url) || $material_url == ''){

            redirect(base_url());

        }else{

            $data['info']                       = $this->materials_model->get_by_url($material_url);
            $data['info']['enable_comments']    = 1;
            $data['content']                    = $this->materials_model->get_content($data['info']['material_id'], LANGUAGE);
            $data['author']                     = $this->sections_model->get_author($data['info']['author_id'], LANGUAGE);
            $data['info']['title_description']  = $this->lang->line('title_description_materials');
            $data['available_languages']        = $this->materials_model->check_languages($data['info']['material_id'], LANGUAGE);

            if (empty($data['info']) || empty($data['content']))
        	{
                
                show_404();

        	}else{            

                   $this->load->library('captcha_lib');

                   //Загружаем менюшки;
                   $data['menu'] = $this->category_model->get_tree(0,LANGUAGE);

                   //Формируем массив для обновления поля count_views (текущее число показов материала +1)
                   $counter_data = array('count_views' => $data['info']['count_views'] + 1);
                   //Запускаем функцию обновления, меняющую значение счетчика в базе
                   $this->materials_model->update_counter($data['info']['material_id'],$counter_data);
                   
                   $data['section']             = $this->sections_model->get($data['info']['section']);
                   $data['section_content']     = $this->sections_model->get_content($data['info']['section'], LANGUAGE);
                   $data['photos']              = $this->materials_model->get_photos($data['info']['material_id'], LANGUAGE);

                   //Количество комментариев
                   $data['info']['comments_num'] =  $this->comments_model->count_comments($data['info']['material_id'],$this->comment_types['material'], LANGUAGE);
                   $data['info']['comments']     =  $this->comments_model->get_by($data['info']['material_id'],$this->comment_types['material'], 0, LANGUAGE);

                   $data['info']['comments_num_and_rating'] =  $this->comments_model->get_count_and_rating_comments($data['info']['material_id'], LANGUAGE);
                        
                   $data['breadcrumbs']  = array(
                        '1' => array('url' => $data['section']['url'], 'title' => $data['section_content']['title']),
                        '2' => array('url' => '', 'title' => $data['content']['title'])
                   );

                   $data['imgcode'] = $this->captcha_lib->captcha_actions();

                   $name = 'materials/material';

                   $this->display_lib->user_page($data,$name);


        	}
        }
	}
    

}

/* End of file materials.php */
/* Location: ./application/controllers/materials.php */