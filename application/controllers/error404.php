<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error404 extends MY_Controller{

    public function __construct()
    {
       parent::__construct();          
    }
        
        
    public function index()
    {
        //language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;

        $this->output->set_status_header('404');

        $data['content'] = array(
            'title'          => $this->lang->line('404_title'),
            'seo_title'      => '',
            'description'    => '',
            'keywords'       => '',
            'text'           => $this->lang->line('404_text')
        );

        $name = 'pages/404';

        $this->display_lib->error_404_page($data,$name);
    }    


}
?>