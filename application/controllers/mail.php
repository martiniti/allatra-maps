<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends MY_Controller
{

	public function __construct()
	{
	   parent::__construct();
	   $this->load->model(array(
		   'mail_model',
		   ));
	}


	public function index()
	{
		redirect (base_url());
	}

	public function callback(){
		//language for links
		if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
		// Если это аякс запрос
		if ($this->input->is_ajax_request()){
			//POST данные
			$data = $this->mail_model->array_from_post(array(
				'name',
				'phone',
			));
			//Куда отправляется письмо
			$address = EMAIL; //EMAIL; //"martiniti@ukr.net"
			//Тема письма как ее видит получатель
			$subject = 'Форма заказа обратного звонка';
			$message = '
				<b>Имя:</b> '.$data['name'].'<br />
				<b>Телефон:</b> '.$data['phone'].'<br />';
			//Отправляем письмо
			$mailed = mail ($address, $subject, $message,"Content-type:text/html;charset = UTF-8");

			if($mailed){
				$message = $this->lang->line('callback_success');
			}
			else {
				$message = $this->lang->line('callback_error');
			}

			echo json_encode($message);
			die();
		}
		else{
			echo $this->lang->line('callback_ajax_needed');
			die();
		}
	}

	public function contact_us(){
		//language for links
		if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
		// Если это аякс запрос
		if ($this->input->is_ajax_request()){
			//POST данные
			$data = $this->mail_model->array_from_post(array(
				'name',
				'email',
				'topic',
				'message',
			));
			//Куда отправляется письмо
			$address = EMAIL; //EMAIL; //"martiniti@ukr.net"
			//Тема письма как ее видит получатель
			$subject = 'Форма обратной связи';
			$message = '
				<b>Имя:</b> '.$data['name'].'<br />
				<b>Email:</b> '.$data['email'].'<br />
				<b>Тема письма:</b> '.$data['topic'].'<br />
				<b>Сообщение:</b> '.$data['message'].'<br />';
			//Отправляем письмо
			$mailed = mail ($address, $subject, $message,"Content-type:text/html;charset = UTF-8");

			if($mailed){
				$message = $this->lang->line('feedback_success');
			}
			else {
				$message = $this->lang->line('feedback_error');
			}

			echo json_encode($message);
			die();
		}
		else{
			echo $this->lang->line('feedback_ajax_needed');
            die();
		}
	}

	public function buy_product(){
		//language for links
		if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;

		// Если это аякс запрос
		if ($this->input->is_ajax_request()){
			//POST данные
			$data = $this->mail_model->array_from_post(array(
				'name',
				'email',
				'phone',
				'title',
				'text',
				'url',
			));
			//Куда отправляется письмо
			$address = EMAIL; //EMAIL; //"martiniti@ukr.net"
			//Тема письма как ее видит получатель
			$subject = 'Заказ товара';
			$message = '
				<b>Имя:</b> '.$data['name'].'<br />
				<b>Email:<b> '.$data['email'].'<br />
				<b>Телефон:</b> '.$data['phone'].'<br />
				<b>Текст:</b> '.$data['text'].'<br />
				<b>Заказанный товар:</b> <a href="'.base_url($data['url']).'" >'.$data['title'].'</a>';
			//Отправляем письмо
			$mailed = @ mail ($address, $subject, $message,"Content-type:text/html;charset = UTF-8");

			if($mailed){
				$message = $this->lang->line('product_order_success');
			}
			else {
				$message = $this->lang->line('product_order_error');
			}

			echo json_encode($message);
//			echo json_encode($this->input->post());
			die();
		}
		else{
			echo $this->lang->line('product_order_ajax_needed');
		}
	}
}
