<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller
{
	// Тестовый режим для платёжной системы iPay
	public $ipay_test_mode = false;

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array('pages_model','comments_model'));
    }

    public function index()
	{
        $this->load->model(array('service_model'));

        //language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;

        $data['info']               = $this->pages_model->get_wellcome_page();
        $data['content']            = $this->pages_model->get_content($data['info']['page_id'], LANGUAGE);
        $data['slides']             = $this->pages_model->get_slider(LANGUAGE);
        $data['advantages']         = $this->pages_model->get_advantages(LANGUAGE);
        $data['subjects']           = $this->pages_model->get_subjects(LANGUAGE);
        $data['partners']           = $this->pages_model->get_partners(LANGUAGE);
        //$data['cats']               = $this->pages_model->get_childrens_categories(1,LANGUAGE);
        $data['cats']               = $this->service_model->get_services(LANGUAGE);
        $data['benefits']           = $this->pages_model->get_benefits(LANGUAGE);
        $data['clients_comments']	= $this->pages_model->get_clients_comments(LANGUAGE);

        if (empty($data['info']) || empty($data['content']))
    	{
    		show_404();
    	}

        $name = 'pages/mainpage';
        //$name = 'pages/mainpage_new';

        $this->display_lib->user_main_page($data,$name);

	}

    public function view($page_url = 'index')
    {
        //language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;

        if(!isset($page_url) || $page_url == ''){

            redirect(base_url());

        }else{

            $this->load->model('category_model');
            //$this->load->model('articles_model');

            $data['info']       = $this->pages_model->get_by_url($page_url);
            $data['content']    = $this->pages_model->get_content($data['info']['page_id'], LANGUAGE);
            $data['info']['title_description'] = $this->lang->line('title_description_pages');

            //Загружаем менюшки;
            $data['menu'] = $this->category_model->get_tree(0,LANGUAGE);

            $data['available_languages'] = $this->pages_model->check_languages($data['info']['page_id'], LANGUAGE);

            if(empty($data['content']))
            {
                show_404();
            }

            $data['breadcrumbs']  = array('1' => array('url' => '', 'title' => $data['content']['title']));

            // Если включены комментарии - достаём всё, что с ними связано
            if(!empty($data['info']['enable_comments'])){

                $this->load->library('captcha_lib');
                $this->load->model('comments_model');

                $data['info']['comments_num'] =  $this->comments_model->count_comments($data['info']['page_id'], $this->comment_types['page'], LANGUAGE);
                $data['info']['comments']     =  $this->comments_model->get_by($data['info']['page_id'], $this->comment_types['page'], 0, LANGUAGE);
                $data['info']['comments_num_and_rating'] =  $this->comments_model->get_count_and_rating_comments($data['info']['page_id'], $this->comment_types['page'], LANGUAGE);
                // Поделючаем капчу
                $data['imgcode']              =  $this->captcha_lib->captcha_actions();

                //dump($data['info']['comments']);

            }

            switch($page_url)
            {

            //Если страница "Контакты"
            case 'feedback':

                $this->load->library(array('captcha_lib'));

                //$data['widget']     = $this->recaptcha->getWidget();
                //$data['script' ]    = $this->recaptcha->getScriptTag();

                // Не нажата кнопка "Отправить"
                if ( ! isset($_POST['send_message']))
                {
                    //Получаем код картинки
                    $data['imgcode'] = $this->captcha_lib->captcha_actions();
                    $data['info']['message'] = ''; //Информационное сообщение
                    $name = 'pages/feedback';

                    $this->display_lib->user_contacts_page($data,$name);
                }

                // Нажата кнопка "Отправить"
                else
                {
                    //Установка правил валидации
                    $this->form_validation->set_rules($this->pages_model->contact_rules);

                	$val_res = $this->form_validation->run();

                    //Если валидация пройдена
                    if ($val_res == TRUE)
                    {
                         //Получаем значение поля капча
                	     $entered_captcha = $this->input->post('captcha');

                         //Если капча совпадает, отправляем письмо
                	     if ($entered_captcha == $this->session->userdata('rnd_captcha'))
                         {
                             $this->load->library('typography');

                             $name  = $this->input->post('name');
                             $email = $this->input->post('email');
                             $topic = $this->input->post('topic');

                             //////////////////////////////////////////////
                             $message = $this->input->post('message');

                             //Переносы после 70 знаков (ограничение mail в PHP)
                             $message = wordwrap($message,70);

                             // TRUE - более двух переводов строк все равно считаются за два перевода строки
                             $message = $this->typography->auto_typography($message,TRUE);
                             // Удаляем html-тэги для удобства чтения
                             $message = strip_tags($message);

                             //Куда отправляется письмо
                             $address = EMAIL;

                             //Тема письма как ее видит получатель
                             $subject = "Сообщение из формы контактов";
                             $message = "Имя:$name\nE-mail отправителя: $email\nТема:$topic\nСообщение:\n$message";

                             //Отправляем письмо
                	         mail ($address,$subject,$message,"Content-type:text/plain;charset = utf8\r\n");

                             $data['info']['message'] = 'Ваше сообщение отправлено.';
                             $name = 'info';

                             $this->display_lib->user_info_page($data,$name);
                	     }

                         // Если капча не совпадает
                         else
                         {
                             //Получаем код картинки;
                             $data['imgcode'] = $this->captcha_lib->captcha_actions();

                             $data['info']['message'] = 'Вы ввели неправильные цифры с картинки';
                             $name = 'pages/feedback';

                             $this->display_lib->user_page($data,$name);
                         }
                    }

                    //Если валидация не пройдена
                    else
                    {
                        //Получаем код картинки;
                        $data['imgcode'] = $this->captcha_lib->captcha_actions();
                        $data['info']['message'] = ''; //Информационное сообщение
                        $name = 'pages/feedback';

                        $this->display_lib->user_page($data,$name);
                    }
                }

                break;


            case 'comments':

                    $this->load->library('captcha_lib');

                    $this->load->model('comments_model');

                    // Сообщение, если неправильно введена капча
                    $data['fail_captcha'] = '';

                    // Сообщение, если комментарий успешно добавлен
                    $data['success_comment'] = '';

                    //Получаем код капчи
                    $data['imgcode'] = $this->captcha_lib->captcha_actions();

                    // Комментарии к материалу
                    $data['comments_list'] = $this->comments_model->get_all();

                    $data['dateformat'] = "%d/%m/%Y";

                    $name = 'pages/comment';
                    $this->display_lib->user_page($data,$name);

                break;


            case 'video':

                    // Комментарии к материалу
                    $data['video'] = $this->pages_model->get_video();

                    $name = 'pages/video';
                    $this->display_lib->user_page($data,$name);

                break;

            case 'transfer_calculator':

                $data['cost']           = $this->pages_model->get_cost();
                $data['cost_autos']     = $this->pages_model->get_cost_autos();
                $data['cost_services']  = $this->pages_model->get_cost_services();

                $name = 'pages/cost';
                $this->display_lib->user_page($data,$name);

                break;

            case 'route_calculator':

                $data['cost']           = $this->pages_model->get_cost();
                $data['cost_autos']     = $this->pages_model->get_cost_autos();
                $data['cost_services']  = $this->pages_model->get_cost_services();

                $name = 'pages/cost';
                $this->display_lib->user_page($data,$name);

                break;

            case 'timing_calculator':

                $data['cost']           = $this->pages_model->get_cost();
                $data['cost_autos']     = $this->pages_model->get_cost_autos();
                $data['cost_services']  = $this->pages_model->get_cost_services();

                $name = 'pages/cost';
                $this->display_lib->user_page($data,$name);

                break;

            case 'why_ukraine':

                    $name = 'pages/why_ukraine';
                    $this->display_lib->user_page($data,$name);

                break;

            case 'team':

                // Комментарии к материалу
                $data['teams'] = $this->pages_model->get_team(LANGUAGE);

                $name = 'pages/team';
                $this->display_lib->user_page($data,$name);

                break;

            // Оплата посредством IPay
            case 'pay_online':
                $this->load->model('pay_model');
                // Загружаем библиотеку для валидации форм
                $this->load->library('form_validation');
                // Определяем теги для вывода ошибки при ошибке валидации
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                // Назначаем правила
                $this->form_validation->set_rules($this->pay_model->rules);
                // Если валидация прошла успешно
                if ($this->form_validation->run() == TRUE){
                    // Передаём идентификатор мерчанта, приватный и публичный ключи в библиотеку Ipay
                    $params['MCH_ID'] = $this->config->item('ipay_merchant_id');
                    $params['MKEY'] = $this->config->item('ipay_merchant_key');
                    $params['SKEY'] = $this->config->item('ipay_sign_key');
                    // Загружаем библиотеку iPay
                    $this->load->library('ipay', $params);
                    // Загружаем хелпер iPay
                    $this->load->helper('ipay');

                    // Выбор тестового и основного режима
                    !empty($this->ipay_test_mode)? $this->ipay->set_mode('test') : $this->ipay->set_mode('real');
                    // Выбор языка
                    $this->ipay->set_lang(LANGUAGE);
                    $order_id = time();
                    $code = generate_transaction_id($order_id); //$order_id
                    $this->ipay->set_urls(base_url('pay_online_good/'.$order_id), base_url('pay_online_bad/'.$order_id));
                    // Определение параметров для передачи
                    $info = json_encode(['ref' => $code]);
                    $ipay_params['description']   = $this->lang->line('pay_in_taskforce').$this->lang->line('order_number').$order_id.$this->lang->line('order_code').$code;
                    $ipay_params['amount']        = $this->input->post('summ')*100;
                    $this->ipay->set_transaction($ipay_params['amount'], $ipay_params['description'], $info);
//                    dump($info, '$info');
//                    dump($ipay_params, '$ipay_params');
                    $xml_result = $this->ipay->create_payment();
//    				dump($xml_result);
                    $url2go = get_url2go($xml_result);
//    				dump($url2go);
                    if(!empty($url2go)){
                        $data['form'] = '
                            <form method="POST" action="'.$url2go.'" accept-charset="utf-8">
                                <button class="btn-u btn-u-lg" name="btn_text">'.$this->lang->line('pay_by_ipay').'</button>
                            </form>';

                        // Куда отправляется письмо
                        $addresses[] = EMAIL;//EMAIL;
                        $addresses[] = $this->input->post('email');//EMAIL;

                        // Тема письма как ее видит получатель
                        $subject = $ipay_params['description'];
                        // Формируем тело письма
                        $message = '<b>ФИО:</b> '.$this->input->post('fio').'<br /><b>EMail:</b> '.$this->input->post('email').'<br /><b>Сумма:</b> '.$this->input->post('summ').'<br /><b>Оплатить:</b><br /> '.$data['form'];
                        //pre_exit($data);
                        if(!empty($addresses)){
                            // Отправляем письмо
                            foreach($addresses as $address){
                                $mail = @ mail ($address, $subject, $message,"Content-type:text/html;charset = utf8");
                            }
                        }
                    }
                }

                $name = 'pages/pay_online/pay_online';
                $this->display_lib->user_page($data,$name);

                break;

            case 'pay_online_good':

                $name = 'pages/pay_online/pay_online_good';
                $this->display_lib->user_page($data,$name);

                break;

            case 'pay_online_bad':

                $name = 'pages/pay_online/pay_online_bad';
                $this->display_lib->user_page($data,$name);

                break;

            case 'faq':

                $data['faqs'] = $this->pages_model->get_faqs(LANGUAGE);

                $name = 'pages/faq';
                $this->display_lib->user_page($data,$name);

                break;

            case 'clients':

                $name = 'pages/clients';
                $this->display_lib->user_page($data,$name);

                break;

            case 'certificates':

                    $data['certificates'] = $this->pages_model->get_certificates(LANGUAGE);

                    $name = 'pages/certificates';
                    $this->display_lib->user_page($data,$name);

                break;

            case 'friends':

                    $data['partners']   = $this->pages_model->get_partners(LANGUAGE);

                    $name = 'pages/partners';
                    $this->display_lib->user_page($data,$name);

                break;

            case 'price':

                    $data['prices']   = $this->pages_model->get_prices(LANGUAGE);

                    $name = 'pages/prices';
                    $this->display_lib->user_page($data,$name);

                break;

            case 'citations':

                    $data['citations']   = $this->pages_model->get_all_farewells(LANGUAGE);

                    $name = 'pages/citations';
                    $this->display_lib->user_page($data,$name);

                break;

            default:

                $name = 'pages/page';

                $this->display_lib->user_page($data,$name);

                break;

            }

    }
}


}
?>