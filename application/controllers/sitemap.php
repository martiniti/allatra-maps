<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed!');

class Sitemap extends CI_Controller {

    function __construct(){
        parent::__construct();
    }
    
    function index(){
        header("Content-Type: text/xml;charset=utf8");
        echo '<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


        //MP
        echo  "
                <url>
                <loc>".base_url()."</loc>
                <changefreq>daily</changefreq>
                <priority>1.0</priority>
                </url>
                ";

        echo  "
                <url>
                <loc>".base_url('ua')."</loc>
                <changefreq>daily</changefreq>
                <priority>1.0</priority>
                </url>
                ";

        echo  "
                <url>
                <loc>".base_url('en')."</loc>
                <changefreq>daily</changefreq>
                <priority>1.0</priority>
                </url>
                ";

        //PAGES
        $pages_ru = $this->db->select('pages.url as url')
                             ->from('pages')
                             ->join('content', 'content.fid = pages.page_id')
                             ->where('content.table','pages')
                             ->where('content.language','ru')
                             ->where('url !=', 'index')
                             ->order_by('pages.page_id','asc')
                             ->get();

        foreach ($pages_ru->result() as $page_ru){

            echo  "
                <url>
                <loc>".base_url($page_ru->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.8</priority>
                </url>
                ";

        }

        $pages_en = $this->db->select('pages.url as url')
                             ->from('pages')
                             ->join('content', 'content.fid = pages.page_id')
                             ->where('content.table','pages')
                             ->where('content.language','en')
                             ->where('url !=', 'index')
                             ->order_by('pages.page_id','asc')
                             ->get();

        foreach ($pages_en->result() as $page_en){

            echo  "
                <url>
                <loc>".base_url('en/'.$page_en->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.8</priority>
                </url>
                ";

        }

        $pages_ua = $this->db->select('pages.url as url')
                             ->from('pages')
                             ->join('content', 'content.fid = pages.page_id')
                             ->where('content.table','pages')
                             ->where('content.language','ua')
                             ->where('url !=', 'index')
                             ->order_by('pages.page_id','asc')
                             ->get();

        foreach ($pages_ua->result() as $page_ua){

            echo  "
                <url>
                <loc>".base_url('ua/'.$page_ua->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.8</priority>
                </url>
                ";

        }

        //SECTIONS
        $sections_ru = $this->db->select('sections.url as url')
                                ->from('sections')
                                ->join('content', 'content.fid = sections.section_id')
                                ->where('content.show',1)
                                ->where('content.table','sections')
                                ->where('content.language','ru')
                                ->order_by('sections.section_id','desc')
                                ->get();

        foreach ($sections_ru->result() as $section_ru){

            echo  "
                <url>
                <loc>".base_url($section_ru->url)."</loc>
                <changefreq>daily</changefreq>
                <priority>0.9</priority>
                </url>
                ";

        }

        $sections_en = $this->db->select('sections.url as url')
                                ->from('sections')
                                ->join('content', 'content.fid = sections.section_id')
                                ->where('content.show',1)
                                ->where('content.table','sections')
                                ->where('content.language','en')
                                ->order_by('sections.section_id','desc')
                                ->get();

        foreach ($sections_en->result() as $section_en){

            echo  "
                <url>
                <loc>".base_url('en/'.$section_en->url)."</loc>
                <changefreq>daily</changefreq>
                <priority>0.9</priority>
                </url>
                ";

        }

        $sections_ua = $this->db->select('sections.url as url')
                                ->from('sections')
                                ->join('content', 'content.fid = sections.section_id')
                                ->where('content.show',1)
                                ->where('content.table','sections')
                                ->where('content.language','ua')
                                ->order_by('sections.section_id','desc')
                                ->get();

        foreach ($sections_ua->result() as $section_ua){

            echo  "
                <url>
                <loc>".base_url('ua/'.$section_ua->url)."</loc>
                <changefreq>daily</changefreq>
                <priority>0.9</priority>
                </url>
                ";

        }

        //MATERIALS
        $materials_ru = $this->db->select('materials.url as url')
                                 ->from('materials')
                                 ->join('content', 'content.fid = materials.material_id')
                                 ->where('content.show',1)
                                 ->where('content.table','materials')
                                 ->where('content.language','ru')
                                 ->order_by('materials.date','desc')
                                 ->order_by('materials.material_id','desc')
                                 ->get();

        foreach ($materials_ru->result() as $material_ru){

            echo  "
                <url>
                <loc>".base_url($material_ru->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        $materials_en = $this->db->select('materials.url as url')
                                 ->from('materials')
                                 ->join('content', 'content.fid = materials.material_id')
                                 ->where('content.show',1)
                                 ->where('content.table','materials')
                                 ->where('content.language','en')
                                 ->order_by('materials.date','desc')
                                 ->order_by('materials.material_id','desc')
                                 ->get();

        foreach ($materials_en->result() as $material_en){

            echo  "
                <url>
                <loc>".base_url('en/'.$material_en->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        $materials_ua = $this->db->select('materials.url as url')
                                 ->from('materials')
                                 ->join('content', 'content.fid = materials.material_id')
                                 ->where('content.show',1)
                                 ->where('content.table','materials')
                                 ->where('content.language','ua')
                                 ->order_by('materials.date','desc')
                                 ->order_by('materials.material_id','desc')
                                 ->get();

        foreach ($materials_ua->result() as $material_ua){

            echo  "
                <url>
                <loc>".base_url('ua/'.$material_ua->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        //CATEGORIES
        $categories_ru = $this->db->select('categories.url as url')
                                  ->from('categories')
                                  ->join('content', 'content.fid = categories.category_id')
                                  ->where('content.show',1)
                                  ->where('content.table','categories')
                                  ->where('content.language','ru')
                                  ->order_by('categories.category_id','desc')
                                  ->get();

        foreach ($categories_ru->result() as $category_ru){

            echo  "
                <url>
                <loc>".base_url($category_ru->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        $categories_en = $this->db->select('categories.url as url')
                                  ->from('categories')
                                  ->join('content', 'content.fid = categories.category_id')
                                  ->where('content.show',1)
                                  ->where('content.table','categories')
                                  ->where('content.language','en')
                                  ->order_by('categories.category_id','desc')
                                  ->get();

        foreach ($categories_en->result() as $category_ru){

            echo  "
                <url>
                <loc>".base_url('en/'.$category_ru->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        $categories_ua = $this->db->select('categories.url as url')
                                  ->from('categories')
                                  ->join('content', 'content.fid = categories.category_id')
                                  ->where('content.show',1)
                                  ->where('content.table','categories')
                                  ->where('content.language','ua')
                                  ->order_by('categories.category_id','desc')
                                  ->get();

        foreach ($categories_ua->result() as $category_ua){

            echo  "
                <url>
                <loc>".base_url('ua/'.$category_ua->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        //PRODUCTS
        $products_ru = $this->db->select('products.url as url')
                                ->from('products')
                                ->join('content', 'content.fid = products.product_id')
                                ->where('content.show',1)
                                ->where('content.table','products')
                                ->where('content.language','ru')
                                ->order_by('products.product_id','desc')
                                ->get();

        foreach ($products_ru->result() as $product_ru){

            echo  "
                <url>
                <loc>".base_url($product_ru->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        $products_en = $this->db->select('products.url as url')
                                ->from('products')
                                ->join('content', 'content.fid = products.product_id')
                                ->where('content.show',1)
                                ->where('content.table','products')
                                ->where('content.language','en')
                                ->order_by('products.product_id','desc')
                                ->get();

        foreach ($products_en->result() as $product_en){

            echo  "
                <url>
                <loc>".base_url('en/'.$product_en->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        $products_ua = $this->db->select('products.url as url')
            ->from('products')
            ->join('content', 'content.fid = products.product_id')
            ->where('content.show',1)
            ->where('content.table','products')
            ->where('content.language','ua')
            ->order_by('products.product_id','desc')
            ->get();

        foreach ($products_ua->result() as $product_ua){

            echo  "
                <url>
                <loc>".base_url('ua/'.$product_ua->url)."</loc>
                <changefreq>weekly</changefreq>
                <priority>0.7</priority>
                </url>
                ";

        }

        echo "</urlset>";
    }
}

?>