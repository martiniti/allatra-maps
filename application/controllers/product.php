<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model(array('category_model','pages_model','products_model'));	
	}

	function index()
	{
        
        redirect(base_url());  
        
	}	
    
   	function view($product_url = '')
	{
	    //language for links
        if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
        
        $data['info']       = $this->products_model->get_by_url($product_url);
        $data['content']    = $this->products_model->get_content($data['info']['product_id'], LANGUAGE);
        $data['info']['title_description'] = $this->lang->line('title_description_products');
        $data['available_languages'] = $this->products_model->check_languages($data['info']['product_id'], LANGUAGE);
        
        if(empty($data['content']))
        {
            
            redirect(base_url());
            
        }else{
            
            //Загружаем каталог;
            $data['menu'] = $this->category_model->get_tree(0,LANGUAGE);
            
            $limit = 10;
            
            $data['similar_products'] = $this->products_model->get_similar_products($data['info']['category'],$limit,LANGUAGE);
            
            //Get category info
            $data['category']            = $this->category_model->get($data['info']['category']);
            $data['category_content']    = $this->category_model->get_content($data['info']['category'], LANGUAGE);
            
            $data['breadcrumbs']  = array(
                '1' => array('url' => $data['category']['url'], 'title' => $data['category_content']['title']),
                '2' => array('url' => '', 'title' => $data['content']['title'])
            );
    
            $name = 'products/content';
    
            $this->display_lib->user_page($data,$name);
        
        }

	}
    
}