<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sections extends MY_Controller
{  

public function __construct()
{
   parent::__construct(); 
       
   $this->load->model(array('category_model','sections_model'));     
}
    
    
public function index()
{
    
    redirect(base_url());
    
}    
   

public function view($section_url)
{
    //language for links
    if(LANGUAGE == 'ru'): $data['lang'] = ''; else: $data['lang'] = LANGUAGE.'/'; endif;
    
    if(!isset($section_url) || $section_url == ''){
        
        redirect(base_url());
        
    }else{
        
        $data['info']       = $this->sections_model->get_by_url($section_url);
        $data['content']    = $this->sections_model->get_content($data['info']['section_id'], LANGUAGE);
        $data['info']['title_description'] = $this->lang->line('title_description_sections');
        $data['available_languages'] = $this->sections_model->check_languages($data['info']['section_id'], LANGUAGE);
        
        if(empty($data['content']))
        {
            
            redirect(base_url());
        
        }else{
            
            $this->load->library(array('pagination','pagination_lib'));
            
            //Загружаем менюшки;
            $data['menu'] = $this->category_model->get_tree(0,LANGUAGE);
            
            $data['breadcrumbs']	= array(1 => $data['content']['title']);
            
            $data['breadcrumbs']  = array(
                '1' => array('url' => '', 'title' => $data['content']['title'])
            );
    
            $last_segment           = $this->uri->segment($this->uri->total_segments());
    
            if($last_segment != $data['info']['url'] && is_numeric($last_segment)){
                
                $start_from  = $last_segment;
                
            }else{
                
                $start_from  = 0;
                
            }
            
            $limit       = MATERIALS_PER_PAGE;
            $total       = $this->sections_model->count_materials_by($data['info']['section_id'], LANGUAGE);
            $link        = $data['info']['url'];
            $settings    = $this->pagination_lib->get_settings('materials', $link, $total, $limit);
    
            //Применяем настройки;
            $this->pagination->initialize($settings);
            
            $data['page_nav']    = $this->pagination->create_links();
    
            $data['materials']    = $this->sections_model->get_materials_by($data['info']['section_id'], $limit, $start_from, LANGUAGE);

            $name = 'sections/section';

            $this->display_lib->user_page($data,$name);

        }
        
           

    }
        
}
   


}
?>