<!-- Body-->
<body>
    <!-- Navbar-->
    <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
    <header class="navbar <?php if(isset($info) && $info['wp'] == 1):?>homepage<?php endif;?>">
        <!-- Site Branding-->
        <div class="site-branding">
            <a class="site-logo hidden-xs-down" href="<?php echo base_url();?>">
                <?php echo theme_img('logo.svg',SITE_NAME);?>
                <div class="site-name">
                    <span class="allatra-name">АЛЛАТРА</span>
                    <span class="allatra-slogan">
                    <small>Дорожный</small>
                    <small>Эксперимент</small>
                </span>
                </div>
            </a>
            <a class="site-logo logo-sm hidden-sm-up" href="<?php echo base_url();?>">
                <?php echo theme_img('logo.svg',SITE_NAME);?>
                <div class="site-name">
                    <span class="allatra-name">АЛЛАТРА</span>
                    <span class="allatra-slogan">
                    <small>Дорожный</small>
                    <small>Эксперимент</small>
                </span>
                </div>
            </a>
        </div>
        <!-- Main Navigation-->
        <nav class="site-menu">
            <ul>
                <?php

                function display_menu($menu_array, $layer, $first='', $parent_url = '')
                {
                    if(LANGUAGE == 'ru'):

                        $lang = '';

                    else:

                        $lang = LANGUAGE.'/';

                    endif;


                    if($first)
                    {
                        echo '<ul class="'.$first.'" role="menu">';
                    }

                    foreach ($menu_array as $menu)
                    {

                        if (sizeof($menu['children']) > 0)
                        {

                            if($layer == 1)
                            {
                                echo '<li class="dropdown"><a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="'.base_url($lang.$menu['menu']->url).'">'.$menu['menu']->title.'<span class="angle"></span></a>'."\n";

                                $next = $layer+1;
                                echo '<span class="hasmore"></span>';
                                display_menu($menu['children'], $next, 'dropdown-menu children', $menu['menu']->url);
                            }
                            else
                            {
                                echo '<li><a href="'.base_url($lang.$menu['menu']->url).'"><span>'.$menu['menu']->title.'</span></a></li>'."\n";

                                $next = $layer+1;
                                display_menu($menu['children'], $next, 'list-unstyled children');
                            }


                        }else{


                            echo '<li><a href="'.base_url($lang.$menu['menu']->url).'"><span>'.$menu['menu']->title.'</span></a>'."\n";

                        }
                        echo '</li>';
                    }

                    if($first)
                    {
                        $parent = current($menu_array);
                        echo '</ul>';
                    }

                }

                display_menu($this->menu, 1);

                ?>
            </ul>
        </nav>
        <!-- Toolbar-->
        <div class="toolbar">
            <div class="inner">
                <a class="toolbar-toggle mobile-menu-toggle" href="#mobileMenu">
                    <i class="material-icons menu"></i>
                </a>
                <?php if(isset($info) && $info['wp'] == 1):?>
                <a class="toolbar-toggle search-toggle" href="#search">
                    <i class="material-icons map"></i>
                </a>
                <?php endif;?>
                <?php if($this->dx_auth->is_logged_in()):?>
                <a class="toolbar-toggle account-toggle" href="<?php echo base_url('auth/logout');?>">
                    <i class="material-icons exit_to_app"></i>
                </a>
                <?php else:?>
                <a class="toolbar-toggle account-toggle" href="#account">
                    <i class="material-icons person"></i>
                </a>
                <?php endif;?>

            </div>
            <!-- Toolbar Dropdown-->
            <div class="toolbar-dropdown">
                <!-- Mobile Menu Section-->
                <div class="toolbar-section" id="mobileMenu">
                    <!-- Slideable (Mobile) Menu-->
                    <nav class="slideable-menu mt-4">
                        <ul class="menu">

                            <?php display_menu($this->menu, 1);?>

                            <?php if($this->dx_auth->is_logged_in()):?>

                            <li><a href="<?php echo base_url('auth/logout');?>"><i class="material-icons exit_to_app"></i> <?php echo $this->lang->line('logout');?></a></li>

                            <?php else:?>

                            <li><a href="<?php echo base_url('auth');?>"><i class="material-icons perm_identity"></i> <?php echo $this->lang->line('authorization');?></a></li>
                            <li><a href="<?php echo base_url('auth/register');?>"><i class="material-icons assignment"></i> <?php echo $this->lang->line('registration');?></a></li>

                            <?php endif;?>

                        </ul>
                    </nav>
                </div>
                <!-- Search Section-->
                <div class="toolbar-section" id="search">
                    <div class="form-group">
                        <label class="text-muted" for="large-rounded-input">Наложить маску</label>
                        <select class="form-control GeoMasks">
                            <option value="">Без маски</option>
                            <option value="geoKiev">Георазломы Киев</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text-muted" for="large-rounded-input">Прозрачность маски</label>
                        <select class="form-control OverlayOpacity">
                            <option value="0.1">10%</option>
                            <option value="0.2">20%</option>
                            <option value="0.3">30%</option>
                            <option value="0.4">40%</option>
                            <option value="0.5">50%</option>
                            <option value="0.6">60%</option>
                            <option value="0.7">70%</option>
                            <option value="0.8">80%</option>
                            <option value="0.9">90%</option>
                            <option value="1.0">100%</option>
                        </select>
                    </div>
                </div>
                <!-- Account Section-->
                <div class="toolbar-section" id="account">
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li class="nav-item"><a class="nav-link active" href="#login" data-toggle="tab" role="tab"><?php echo $this->lang->line('authorization');?></a></li>
                        <li class="nav-item"><a class="nav-link" href="#signup" data-toggle="tab" role="tab"><?php echo $this->lang->line('registration');?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="login" role="tabpanel">
                            <form action="<?php echo base_url('auth');?>" autocomplete="off" id="login-form" method="post" role="form" accept-charset="utf-8">
                                <div class="form-group input-group">
                                    <input name="username" class="form-control" type="text" placeholder="<?php echo $this->lang->line('login_name');?>" required>
                                    <span class="input-group-addon"><i class="material-icons person"></i></span>
                                </div>
                                <div class="form-group input-group">
                                    <input name="password" class="form-control" type="password" placeholder="<?php echo $this->lang->line('password');?>" required>
                                    <span class="input-group-addon"><i class="material-icons lock"></i></span>
                                </div>
                                <div class="custom-control custom-checkbox form-group">
                                    <input name="remember" value="1" class="custom-control-input" type="checkbox" id="logged" checked>
                                    <label class="custom-control-label" for="logged"><?php echo $this->lang->line('login_remember');?></label>
                                </div>
                                <button name="login" class="btn btn-primary btn-block" type="submit"><?php echo $this->lang->line('login');?></button>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="signup" role="tabpanel">
                            <form autocomplete="off" id="signup-form" action="<?php echo base_url('auth/register');?>" method="post" role="form">
                                <div class="form-group">
                                    <input name="username" class="form-control" type="text" placeholder="<?php echo $this->lang->line('username');?>" required>
                                </div>
                                <div class="form-group">
                                    <input name="email" class="form-control" type="email" placeholder="<?php echo $this->lang->line('user_email');?>" required>
                                </div>
                                <div class="form-group">
                                    <input name="password" class="form-control" type="password" placeholder="<?php echo $this->lang->line('password');?>" required>
                                </div>
                                <div class="form-group">
                                    <input name="confirm_password" class="form-control" type="password" placeholder="<?php echo $this->lang->line('confirm_password');?>" required>
                                </div>
                                <button name="register" class="btn btn-primary btn-block" type="submit"><?php echo $this->lang->line('do_registration');?></button>
                                <p class="text-muted text-sm mt-4"><?php echo $this->lang->line('or_sign_up_with_social_account');?></p>
                                <a class="media-btn media-facebook" href="#"><i class="socicon-facebook"></i><span><?php echo $this->lang->line('sign_up_with_facebook');?></span></a>
                                <a class="media-btn media-google" href="#"><i class="socicon-googleplus"></i><span><?php echo $this->lang->line('sign_up_with_google+');?></span></a>
                                <a class="media-btn media-twitter" href="#"><i class="socicon-twitter"></i><span><?php echo $this->lang->line('sign_up_with_twitter');?></span></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?php if($this->uri->total_segments() > 0 && ($this->uri->segment($this->uri->total_segments()) != 'en' && $this->uri->segment($this->uri->total_segments()) != 'ru' && $this->uri->segment($this->uri->total_segments()) != 'ua')):?>
    <!-- Page Title-->
    <div class="page-title">
        <div class="container">
            <h1><?php echo $content['title'];?></h1>

            <?php if(isset($breadcrumbs)):?>
            <ul class="breadcrumbs">

                <?php

                if(!isset($info['wp']) || $info['wp'] != '1'){
                    $url_path	= '';
                    $count	 	= 1;
                    echo '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="'.base_url($lang).'"><span itemprop="title">'.$this->lang->line('home_breadcrumbs').'</span></a></li>';
                    foreach($breadcrumbs as $bc):

                        $url_path .= '/'.$bc['url'];
                        if($count == count($breadcrumbs)):?>
                            <?php echo '<li class="separator">&nbsp;/&nbsp;</li><li itemscope itemprop="child" class="active" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">'.$bc['title'].'</span></li>';?>
                        <?php else:?>

                            <li class="separator">&nbsp;/&nbsp;</li>
                            <li itemscope itemprop="child" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="<?php echo base_url($lang.$url_path);?>"><span itemprop="title"><?php echo $bc['title'];?></span></a></li>

                        <?php endif;
                        $count++;

                    endforeach;
                }
                ?>

            </ul>
            <?php endif;?>

        </div>
    </div>
    <?php endif;?>