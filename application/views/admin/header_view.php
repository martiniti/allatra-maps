<ul id="toolbar">
	<li class="menu_home">
        <?php echo anchor('', 'Перейти на сайт'); ?>
	</li>
	<li class="menu_logout icon">
        <?php echo anchor('auth/logout', 'Выход '.$this->session->userdata('username')); ?>
	</li>
</ul>


<div id="header">

    <br />
    <br />
	<div id="tabs">
    	<ul> 
            <?php if ($this->dx_auth->is_role('admin')):?>
            <li>
                <a href="<?php echo base_url()."adminpanel/set";?>">Настройки</a>
                <ul>
                    <li><a href="<?php echo base_url()."adminpanel/menu";?>">Меню</a></li>
                    <li><a href="<?php echo base_url()."adminpanel/set";?>">Настройки сайта</a></li>
                    <li><a href="<?php echo base_url()."backend/users";?>">Пользователи</a></li>
                    <li><a href="<?php echo base_url()."backend/roles";?>">Роли</a></li>
                    <li><a href="<?php echo base_url()."backend/uri_permissions";?>">URI доступы</a></li>
                    <li><a href="<?php echo base_url()."backend/custom_permissions";?>">Права пользователей</a></li>
                    <li><a href="<?php echo base_url()."backend/unactivated_users";?>">Активация пользователей</a></li>
                </ul>
            </li>
            <?php endif;?>
            <li><a href="<?php echo base_url()."adminpanel/faq_manage";?>">FAQ</a></li>
            <li><a href="<?php echo base_url()."adminpanel/comments";?>">Комментарии</a></li>

            <?php if ($this->dx_auth->is_role('admin')):?>
            <li>
                <a href="<?php echo base_url()."adminpanel/modules";?>">Модули</a>
            </li>
            <?php endif;?>
            <li>
                <a href="<?php echo base_url()."adminpanel/banners";?>">Баннеры</a>
            </li>
            <li>
                <a href="<?php echo base_url()."adminpanel/video";?>">Видео</a>
            </li>
            <li>
                <a href="<?php echo base_url()."adminpanel/pages";?>">Страницы</a>
            </li>
            <li>
                <a href="<?php echo base_url()."adminpanel/sections";?>">Рубрики материалов</a>
            </li>
            <li>
                <a href="<?php echo base_url()."adminpanel/materials";?>">Материалы</a>
            </li>
            <li>
                <a href="<?php echo base_url()."adminpanel/albums";?>">Галерея</a>
            </li>
    	</ul>
    </div>

</div>