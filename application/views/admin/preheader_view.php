<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Панель администрирования</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8"/>
<?php if(isset($css_files)):?>
    <?php foreach($css_files as $file): ?>
    	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
<?php endif;?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/admin.css" type="text/css" />
<?php if(isset($js_files)):?>		
    <?php foreach($js_files as $file): ?>
    	<script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
<?php endif;?>
<script>
    var base_url = "<?php echo base_url();?>";
    <?php if($this->uri->segment(2) == 'blogs'):?>
    //!!!FOR blogs we change img path in TinyMCE
    var images_upload_path      = "<?php echo base_url('assets/uploads/images/tinymce/users-images');?>/";
    var tinymce_image_upload    = "<?php echo base_url('adminpanel/tinymce_image_upload/blog');?>";
    <?php else:?>
    var images_upload_path      = "<?php echo base_url('assets/uploads/images/tinymce');?>/";
    var tinymce_image_upload    = "<?php echo base_url('adminpanel/tinymce_image_upload');?>";
    <?php endif;?>

    <?php if($this->uri->segment(2) == 'services_content'):?>
    var tinymce_width   = 1200;
    <?php else:?>
    var tinymce_width   = 900;
    <?php endif;?>

</script>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()."assets/admin/img/favicon.png";?>"/>
<link rel="shortcut icon" type="image/png" href="<?php echo base_url()."assets/admin/img/favicon.png";?>"/>
</head>
<body>