<?php // add css files
	$this->minify->css(array('plugins.min.css','settings.css','layers.css','navigation.css','style.css','homepages/index-gym.css','theme-skins/green.css'));
	$this->minify->deploy_css(TRUE);
	//Simple
	$this->minify->js(array('modernizr.js','plugins.min.js','jquery.validate.min.js','main.js','forms.js','init.js'));
	//MP
	//$this->minify->js(array('modernizr.js','plugins.min.js','jquery.validate.min.js','main.js','forms.js','jquery.themepunch.tools.min.js','jquery.themepunch.revolution.min.js','extensions/revolution.extension.actions.min.js','extensions/revolution.extension.carousel.min.js','extensions/revolution.extension.kenburn.min.js','extensions/revolution.extension.layeranimation.min.js','extensions/revolution.extension.migration.min.js','extensions/revolution.extension.navigation.min.js','extensions/revolution.extension.parallax.min.js','extensions/revolution.extension.slideanims.min.js','extensions/revolution.extension.video.min.js','mp.init.js','init.js'));
	//Service
	//$this->minify->js(array('modernizr.js','plugins.min.js','jquery.validate.min.js','main.js','forms.js','jquery.themepunch.tools.min.js','jquery.themepunch.revolution.min.js','extensions/revolution.extension.actions.min.js','extensions/revolution.extension.carousel.min.js','extensions/revolution.extension.kenburn.min.js','extensions/revolution.extension.layeranimation.min.js','extensions/revolution.extension.migration.min.js','extensions/revolution.extension.navigation.min.js','extensions/revolution.extension.parallax.min.js','extensions/revolution.extension.slideanims.min.js','extensions/revolution.extension.video.min.js','service.init.js','init.js'));
	$this->minify->deploy_js(TRUE);
?>

<div id="wrap"><div id="main">

<h1>Минимизация JS + CSS</h1>

<p>Проверьте, работают ли уменьшенные файлы у вас.</p>
<p>Кликните тут чтоб получить <a href="<?php echo $this->minify->assets_dir .'/' . $this->minify->js_file ?>">JavaScript</a> файл</p>
<p>Кликните тут чтоб получить <a href="<?php echo $this->minify->assets_dir .'/' . $this->minify->css_file ?>">CSS</a> файл</p>

</div></div>