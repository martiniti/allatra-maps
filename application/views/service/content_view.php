<style>
#breadcrumbs{
    display: none;
}

.service-content{
    min-height: 160px;
}

.service-content p{
    margin-bottom: 0px;
}
</style>

        <div id="content" role="main" style="margin-bottom: -50px;">

            <div id="revslider-container">
                <div id="revslider">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-1" data-transition="fade" data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1200" data-rotate="0" data-saveperformance="off" data-thumb="<?php echo base_url('assets/uploads/images/services_content/'.$content['img_url']);?>" data-title="<?php echo $content['title'];?>">

                            <!-- MAIN IMAGE -->
                            <img src="<?php echo base_url('assets/uploads/images/services_content/'.$content['img_url']);?>" alt="<?php echo $content['title'];?>" data-bgposition="center center" data-bgfit="cover" data-duration="11000" data-bgparallax="10" data-ease="Linear.easeNone" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <?php echo $content['block1'];?>
                        </li>
                        <!-- EOF. SLIDE  -->
                    </ul>
                    <div class="tp-bannertimer" style="height: 3px; background-color: rgba(0, 0, 0, 0.2);"></div>
                </div><!-- End #rev_slider -->
            </div><!-- END REVOLUTION SLIDER -->

            <div class="bg-custom container-fluid no-padding" id="id-to-scroll">
                <div class="row no-margin">
                    <?php foreach ($benefits as $benefit):?>
                    <div class="col-sm-3">
                        <div class="colored-box">
                            <span class="service-icon"><?php echo $benefit['icon'];?></span>
                            <div class="service-content">
                                <h3 class="text-white mb20"><?php echo $benefit['title'];?></h3>
                                <p><?php echo $benefit['description'];?></p>
                            </div>
                        </div><!-- End .colored-box -->
                    </div><!-- End .col-sm-3 -->
                    <?php endforeach;?>
                </div><!-- End .row -->
            </div><!-- End .container-fluid -->

            <?php echo $content['block2'];?>

            <div data-bgattach="<?php echo base_url('assets/template/driveforce/images/bg1.jpg');?>" class="parallax overlay-container pt70 pb70 skrollable skrollable-between" data-bottom-top="background-position:50% 50%" data-top-bottom="background-position:50% 90%" style="background-image: url(<?php echo base_url('assets/template/driveforce/images/bg1.jpg');?>); background-position: 50% 58.8543%;">
                <div class="overlay darker"></div><!-- End .overlay -->
                <div class="container">

                    <div class="row">
                        <div class="col-md-8 col-md-push-2 text-center">
                            <h2 class="title-underblock white mb40 text-white text-center"><?php echo $this->lang->line('driveforce_title');?></h2>
                            <p class="lead text-white"><?php echo $this->lang->line('driveforce_description');?></p>

                            <div class="mb20"></div><!-- space -->

                            <a href="#" class="btn btn-lg btn-danger no-radius min-width"><?php echo $this->lang->line('driveforce_do_download');?></a>

                        </div><!-- End .col-md-8 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div>

            <div class="mb60"></div><!--space -->

            <div class="container">
                <header class="title-block text-center mb60">
                    <h2 class="title-underblock custom text-center mb60">Возможно вас заинтересуют другие наши услуги?</h2>
                </header>

                <div class="container service-group pb10-xs">

                    <?php $cats_counter = 0; foreach ($cats as $cat):?>

                        <?php if($cats_counter%3 ==0):?>

                            <div class="row">

                        <?php endif;?>

                        <div class="col-sm-4">
                            <div class="service vertical text-center">
                                <a href="<?php echo base_url($lang.$cat['url']);?>">
                                    <span class="service-icon first-color"><?php echo $cat['icon'];?></span>
                                    <div class="service-content">
                                        <h3 class="service-title title-underblock custom text-uppercase text-center"><?php echo $cat['title'];?></h3>

                                        <p class="color-black"><?php echo $cat['description'];?></p>
                                    </div><!-- End .service-content -->
                                </a>
                            </div><!-- End .service -->
                        </div><!-- End .col-sm-4 -->

                        <?php if(($cats_counter +1)%3 ==0):?>

                            </div><!-- End .row -->

                        <?php endif;?>

                        <?php $cats_counter++; endforeach;?>

                </div>

            </div><!-- End .container -->

        </div>

    </div>
</div>