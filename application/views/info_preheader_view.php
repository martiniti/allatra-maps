<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html> <!--<![endif]-->
<head>

    <meta charset="utf-8">

    <title><?php if(isset($content['seo_title']) && $content['seo_title'] != ''): echo $content['seo_title']; else: echo $content['title'].' - '.$this->lang->line('seo_title'); endif;?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Mobile Specific Meta Tag-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets/template/allatra-da/img/icons/favicon.ico');?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/template/allatra-da/img/icons/favicon.png');?>">
    <link rel="apple-touch-icon" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-iphone.png');?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-ipad.png');?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-iphone-retina.png');?>">
    <link rel="apple-touch-icon" sizes="167x167" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-ipad-retina.png');?>">

    <script>
        var base_url        = "<?php echo base_url();?>";
        var base_url_lang   = "<?php echo base_url($lang);?>";
        var phone_1         = "<?php echo PHONE_1;?>";
        var phone_2         = "<?php echo PHONE_2;?>";
        var phone_3         = "<?php echo PHONE_3;?>";
        <?php if($this->dx_auth->is_logged_in()):?>
        var is_logged_in    = 1;
        <?php else:?>
        var is_logged_in    = 0;
        <?php endif;?>
    </script>

    <?php

    $_css = new CSSCrunch();
    $_css->addFile('vendor.min');
    $_css->addFile('styles');
    $_css->addFile('styles-00bcd4.min');
    $_css->addFile('custom');
    //leaflet + draw + search
    $_css->addFile('../../../leaflet/leaflet');
    $_css->addFile('../../../leaflet/plugins/draw/leaflet.draw');
    $_css->addFile('../../../leaflet/plugins/geosearch/l.geosearch');

    (ENVIRONMENT != 'production') ? $_css->crunch(true) : $_css->crunch();

    $_js = new JSCrunch();
    $_js->addFile('modernizr.min');
    $_js->addFile('vendor.min');
    $_js->addFile('scripts.min');
    // Leaflet
    $_js->addFile('../../../leaflet/leaflet');
    // Leaflet.Draw
    $_js->addFile('../../../leaflet/plugins/draw/Leaflet.draw');
    $_js->addFile('../../../leaflet/plugins/draw/Leaflet.Draw.Event');
    $_js->addFile('../../../leaflet/plugins/draw/Toolbar');
    $_js->addFile('../../../leaflet/plugins/draw/Tooltip');
    $_js->addFile('../../../leaflet/plugins/draw/ext/GeometryUtil');
    $_js->addFile('../../../leaflet/plugins/draw/ext/LatLngUtil');
    $_js->addFile('../../../leaflet/plugins/draw/ext/LineUtil.Intersect');
    $_js->addFile('../../../leaflet/plugins/draw/ext/Polygon.Intersect');
    $_js->addFile('../../../leaflet/plugins/draw/ext/Polyline.Intersect');
    $_js->addFile('../../../leaflet/plugins/draw/ext/TouchEvents');
    $_js->addFile('../../../leaflet/plugins/draw/draw/DrawToolbar');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Feature');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.SimpleShape');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Polyline');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Marker');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Circle');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.CircleMarker');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Polygon');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Rectangle');
    $_js->addFile('../../../leaflet/plugins/draw/edit/EditToolbar');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/EditToolbar.Edit');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/EditToolbar.Delete');
    $_js->addFile('../../../leaflet/plugins/draw/Control.Draw');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Poly');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.SimpleShape');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Rectangle');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Marker');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.CircleMarker');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Circle');
    // Leaflet.Mask
    $_js->addFile('../../../leaflet/plugins/rotate/Leaflet.ImageOverlay.Rotated');
    // Leaflet.Search
    $_js->addFile('../../../leaflet/plugins/geosearch/l.control.geosearch');
    $_js->addFile('../../../leaflet/plugins/geosearch/l.geosearch.provider.openstreetmap');
    // Leaflet.Print
    $_js->addFile('../../../leaflet/plugins/easyprint/bundle');
    // EOF Leaflet

    // Custom JS
    $_js->addFile('custom');

    (ENVIRONMENT != 'production') ? $_js->crunch(true) : $_js->crunch();

    ?>

</head>
<body>