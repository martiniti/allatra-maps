<!-- Modal Order Form-->
<div class="modal fade" id="modal-order-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true">
    <form id="meteringModalForm1" action = "javascript:void(null);" method="post" enctype="multipart/form-data">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $this->lang->line('modal_order_close');?></span></button>
                    <h3 class="modal-title" id="myModalLabel4"><?php echo $this->lang->line('modal_order_title');?></h3>
                </div><!-- End .modal-header -->
                <div class="modal-body">
                    <div id="form-message"></div>
                    <div class="form-group">
                        <input type="hidden" name="feedback" value="1" required/>
                        <input type="hidden" name="url" value="<?php echo base_url(uri_string());?>" required/>
                        <label for="name" class="input-desc"><?php echo $this->lang->line('modal_order_name');?>*</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo $this->lang->line('modal_order_name_ph');?>" required>
                    </div><!-- End .from-group -->
                    <div class="form-group">
                        <label for="name" class="input-desc"><?php echo $this->lang->line('modal_order_phone');?>*</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo $this->lang->line('modal_order_phone_ph');?>" required>
                    </div><!-- End .from-group -->
                    <div class="form-group">
                        <label for="email" class="input-desc"><?php echo $this->lang->line('modal_order_email');?>*</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $this->lang->line('modal_order_email_ph');?>" required>
                    </div><!-- End .from-group -->
                    <div class="form-group">
                        <label for="download-file" class="input-desc"><?php echo $this->lang->line('modal_order_file');?></label>
                        <input type="file" class="form-control" id="userfile" min="1" max="9999" name="userfile[]" multiple="true" placeholder="<?php echo $this->lang->line('modal_order_file_ph');?>">
                    </div><!-- End .from-group -->
                    <div class="form-group">
                        <label for="message" class="input-desc"><?php echo $this->lang->line('modal_order_message');?>*</label>
                        <textarea class="form-control" rows="5" id="message" name="message" placeholder="<?php echo $this->lang->line('modal_order_message_ph');?>" required></textarea>
                    </div><!-- End .from-group -->
                    <p style="text-align: left;"><i><?php echo $this->lang->line('subscribe_field');?>*<?php echo $this->lang->line('subscribe_field_1');?></i></p>
                </div><!-- End .modal-body -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-black" data-dismiss="modal"><?php echo $this->lang->line('modal_order_close');?></button>
                    <input type="submit" class="btn btn-custom" onClick="ga('send', 'event', 'costmodal', 'click');" value="<?php echo $this->lang->line('modal_order_do_order');?>" />
                </div><!-- End .modal-footer -->
            </div><!-- End .modal-content -->
        </div><!-- End .modal-dialog -->
    </form>
</div><!-- End .modal -->

<!-- Modal Order Form-->
<div class="modal fade" id="modal-cp-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5" aria-hidden="true">
    <form id="meteringForm7" action = "javascript:void(null);" method="post" enctype="multipart/form-data">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $this->lang->line('modal_order_close');?></span></button>
                    <h3 class="modal-title" id="myModalLabel4"><?php echo $this->lang->line('modal_order_title');?></h3>
                </div><!-- End .modal-header -->
                <div class="modal-body">
                    <div id="form-message"></div>
                    <div class="form-group">
                        <input type="hidden" name="feedback" value="6" required/>
                        <input type="hidden" name="url" value="<?php echo base_url(uri_string());?>" required/>
                        <label for="name" class="input-desc"><?php echo $this->lang->line('modal_order_name');?></label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo $this->lang->line('modal_order_name_ph');?>" required>
                    </div><!-- End .from-group -->
                    <div class="form-group">
                        <label for="name" class="input-desc"><?php echo $this->lang->line('modal_order_phone');?></label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo $this->lang->line('modal_order_phone_ph');?>" required>
                    </div><!-- End .from-group -->
                    <div class="form-group">
                        <label for="email" class="input-desc"><?php echo $this->lang->line('modal_order_email');?></label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $this->lang->line('modal_order_email_ph');?>" required>
                    </div><!-- End .from-group -->
                    <div class="form-group">
                        <label for="message" class="input-desc"><?php echo $this->lang->line('modal_order_message');?></label>
                        <textarea class="form-control" rows="5" id="message" name="message" placeholder="<?php echo $this->lang->line('modal_order_message_ph');?>" required></textarea>
                    </div><!-- End .from-group -->
                </div><!-- End .modal-body -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-black" data-dismiss="modal"><?php echo $this->lang->line('modal_order_close');?></button>
                    <button class="btn btn-custom"><?php echo $this->lang->line('taskforce_do_download');?></button>
                </div><!-- End .modal-footer -->
            </div><!-- End .modal-content -->
        </div><!-- End .modal-dialog -->
    </form>
</div><!-- End .modal -->