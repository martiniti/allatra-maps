<!-- Modal Contact Form-->
<div class="modal fade" id="modal-product-form-<?php echo $info['product_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myProductForm" aria-hidden="true">
    <form class="buy_product_form" id="buy_product_form-<?php echo $info['product_id'];?>" method="POST" action="#">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $this->lang->line('product_order_close');?></span></button>
                    <h3 class="modal-title" id="myProductForm"><?php echo $this->lang->line('product_order_title');?></h3>
                </div><!-- End .modal-header -->
                <div class="modal-body">

                    <div class="form-group">
                        <label for="name" class="input-desc"><?php echo $this->lang->line('product_order_name');?></label>
                        <input type="text" class="form-control" id="input-name" name="name" placeholder="<?php echo $this->lang->line('product_order_name_ph');?>" required>
                    </div><!-- End .from-group -->

                    <div class="form-group">
                        <label for="email" class="input-desc"><?php echo $this->lang->line('product_order_email');?></label>
                        <input type="email" class="form-control" id="input-email" name="email" placeholder="<?php echo $this->lang->line('product_order_email_ph');?>" required>
                    </div><!-- End .from-group -->

                    <div class="form-group">
                        <label for="phone" class="input-desc"><?php echo $this->lang->line('product_order_phone');?></label>
                        <input type="tel" class="form-control" id="input-phone" name="phone" placeholder="<?php echo $this->lang->line('product_order_phone_ph');?>" required>
                    </div><!-- End .from-group -->

                    <div class="form-group">
                        <label for="input-text" class="input-desc"><?php echo $this->lang->line('product_order_text');?></label>
                        <textarea class="form-control" id="input-text" name="text" rows="3"></textarea>
                    </div><!-- End .from-group -->

                    <input type="hidden" name="title" value="<?php echo $content['title']; ?>" required>
                    <input type="hidden" name="url" value="<?php echo $info['url']; ?>" required>

                </div><!-- End .modal-body -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal"><?php echo $this->lang->line('product_order_close');?></button>
                    <button class="btn btn-custom2 submit-buy"><?php echo $this->lang->line('product_order_do_order');?></button>
                </div><!-- End .modal-footer -->
            </div><!-- End .modal-content -->
        </div><!-- End .modal-dialog -->
    </form>
</div><!-- End .modal -->