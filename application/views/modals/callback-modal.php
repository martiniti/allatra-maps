<!-- Modal Contact Form-->
<div class="modal fade" id="modal-callback-form" tabindex="-1" role="dialog" aria-labelledby="myCallbackForm" aria-hidden="true">
    <form class="callback-me-form" id="callback-form" method="POST" action="#">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $this->lang->line('callback_close');?></span></button>
                    <h3 class="modal-title" id="myProductForm"><?php echo $this->lang->line('callback_title');?></h3>
                </div><!-- End .modal-header -->
                <div class="modal-body">

                    <input type="hidden" name="url" value="<?php echo base_url(uri_string());?>" required/>

                    <div class="form-group">
                        <label for="name" class="input-desc"><?php echo $this->lang->line('callback_name');?></label>
                        <input type="text" class="form-control" id="input-name" name="name" placeholder="<?php echo $this->lang->line('callback_name_ph');?>" required>
                    </div><!-- End .from-group -->

                    <div class="form-group">
                        <label for="phone" class="input-desc"><?php echo $this->lang->line('callback_phone');?></label>
                        <input type="tel" class="form-control" id="input-phone" name="phone" placeholder="<?php echo $this->lang->line('callback_phone_ph');?>" required>
                    </div><!-- End .from-group -->

                </div><!-- End .modal-body -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal"><?php echo $this->lang->line('callback_close');?></button>
                    <button class="btn btn-custom2 submit-buy"><?php echo $this->lang->line('callback_do_order');?></button>
                </div><!-- End .modal-footer -->
            </div><!-- End .modal-content -->
        </div><!-- End .modal-dialog -->
    </form>
</div><!-- End .modal -->