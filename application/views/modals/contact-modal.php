<!-- Modal Contact Form-->
<div class="modal fade" id="modal-callme-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel8" aria-hidden="true">
    <form id="meteringForm3" action = "javascript:void(null);" method="post">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $this->lang->line('callback_close');?></span></button>
                    <h3 class="modal-title" id="myModalLabel8"><?php echo $this->lang->line('callback_title');?></h3>
                </div><!-- End .modal-header -->
                <div class="modal-body">

                    <div class="form-group mb5">
                        <input type="hidden" name="url" value="<?php echo base_url(uri_string());?>" required/>
                        <input type="hidden" name="feedback" value="3" required/>
                        <label for="phone7" class="input-desc"><?php echo $this->lang->line('callback_phone');?> *</label>
                        <input type="phone" class="form-control mb5" id="phone7" name="phone" placeholder="380XXXXXXXX" required>
                    </div><!-- End .from-group -->
                    <p class="mb0"><small><?php echo $this->lang->line('confidence');?></small></p>

                </div><!-- End .modal-body -->
                <div class="modal-footer text-left">
                    <button type="button" class="btn btn-black" data-dismiss="modal"><?php echo $this->lang->line('callback_close');?></button>
                    <button class="btn btn-custom" onClick="ga('send', 'event', 'dinmodal', 'click');"><?php echo $this->lang->line('callback_do_order');?></button>
                </div><!-- End .modal-footer -->
            </div><!-- End .modal-content -->
        </div><!-- End .modal-dialog -->
    </form>
</div><!-- End .modal -->