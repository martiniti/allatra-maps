<!-- Modal Order Form-->
<div class="modal fade" id="modal-reply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true">
    <form id="meteringForm1" action = "javascript:void(null);" method="post">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $this->lang->line('callback_close');?></span></button>
                    <h3 class="modal-title" id="myModalLabel4"><?php echo $this->lang->line('reply_info_title');?></h3>
                </div><!-- End .modal-header -->
                <div class="modal-body">
                    <div id="reply-message"></div>
                </div><!-- End .modal-body -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-custom btn-black" data-dismiss="modal"><?php echo $this->lang->line('callback_close');?></button>
                </div><!-- End .modal-footer -->
            </div><!-- End .modal-content -->
        </div><!-- End .modal-dialog -->
    </form>
</div><!-- End .modal -->