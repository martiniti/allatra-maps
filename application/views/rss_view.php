<?php header("Content-type: text/xml");
echo '<?xml version = "1.0" encoding = "utf-8"?>'?>
<rss version = "2.0">
<channel>
<title>Сайт аква-студии «Стиль-М»</title>
<link><?=base_url()?></link>
<description>Сайт аква-студии «Стиль-М»</description>
<language>ru</language>

<?php foreach($feeds as $item):?>

<item>
    <title><?=$item['title']?></title>
    <link><?=base_url()?>news/<?=$item['news_id']?></link>
    <description><?php echo $item['main_text'];?></description>
    <guid><?=base_url()?>news/<?=$item['news_id']?></guid>
</item>

<?php endforeach;?>

</channel>
</rss>