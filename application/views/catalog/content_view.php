<?php //dump($info['img_url']);?>
    <div id="rev_slider_wrapper" class="slider-container-agency6 rev_slider_wrapper rev_container_1 fullwidthbanner-container" data-alias="classicslider1">
        <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-1" data-transition="fade" data-slotamount="4" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1200" data-rotate="0" data-saveperformance="off" data-title="Simple Agency">

                    <!-- MAIN IMAGE -->
                    <img src="<?php echo base_url('assets/uploads/images/categories/'.$info['img_url']);?>" alt="Slider bg 1" data-bgposition="center center" data-bgfit="cover" data-duration="11000" data-ease="Linear.easeNone" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- overlay -->
                    <div class="tp-caption tp-shape tp-shapewrapper rs-parallaxlevel-0"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['center','center','center','center']" data-voffset="['0','0','0','0']"
                         data-width="full"
                         data-height="full"
                         data-whitespace="nowrap"
                         data-transform_idle="o:0.5;"
                         data-transform_in="opacity:0;s:1500;"
                         data-transform_out="o:0;s:1000;"
                         data-start="1000"
                         data-basealign="slide"
                         data-responsive_offset="on"
                         data-responsive="off"
                         style="z-index: 3; background-color: rgba(0,0,0, 0.6);">
                    </div>

                    <div class="tp-caption tp-resizeme rs-parallaxlevel-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['center','center','center','center']" data-voffset="['-108','-106','-92','-84']"
                         data-fontsize="['18','16','14','12']"
                         data-fontweight="300"
                         data-lineheight="['20','18','16','16']"
                         data-color="#fff"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":600,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive_offset="on"
                         style="z-index: 4; white-space: nowrap; text-shadow:0 1px 2px rgba(0,0,0, 0.2); letter-spacing: 2px; text-transform: uppercase;">Мы предлагаем лучшее!
                    </div>

                    <div class="tp-caption tp-resizeme rs-parallaxlevel-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['center','center','center','center']" data-voffset="['-54','-42','-44','-40]"
                         data-fontsize="['64','68','52','42']"
                         data-fontweight="700"
                         data-lineheight="['64','90','80','70']"
                         data-color="#fff"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":1400,"speed":1500,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":600,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive_offset="on"
                         style="z-index: 5; text-shadow:0 1px 3px rgba(0,0,0, 0.2); letter-spacing: 1px;"> Апостиль и легализация
                    </div>

                    <div class="tp-caption tp-resizeme rs-parallaxlevel-5"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['center','center','center','center']" data-voffset="['30','20','14','8']"
                         data-fontsize="['42','68','52','42']"
                         data-fontweight="700"
                         data-lineheight="['42','90','80','70']"
                         data-color="#fff"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":600,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive_offset="on"
                         style="z-index: 5; text-shadow:0 1px 3px rgba(0,0,0, 0.2); letter-spacing: 0.5px;"> для предоставления ваших документов за границей
                    </div>

                    <a class="tp-caption btn btn-lg btn-custom min-width tp-resizeme rs-parallaxlevel-5"
                       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                       data-y="['center','center','center','center']" data-voffset="['116','106','96','76']"
                       data-witdh="none"
                       data-height="none"
                       data-whitespace="nowrap"
                       data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":600,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                       data-transform_hover="o:0.9;s:300;e:Linear.easeNone;"
                       data-style_hover="bg:#c00f3c;c:#fff;bc:#c00f3c;s:300;e:Linear.easeNone;"
                       data-responsive_offset="on"
                       style="z-index: 7; border-width: 1px;"
                       href="#">Узнать стоимость
                    </a>


                </li>
            </ul>
        </div><!-- End #rev_slider -->
    </div><!-- END REVOLUTION SLIDER -->

    <div class="callout dark" style="margin-bottom: 0px;">
        <div class="container">
            <div class="left">
                <h3 class="callout-title">
                    <?php echo $this->lang->line('callout_title');?>
                </h3>
            </div><!-- End .left -->
            <div class="right with-social">
                <?php if(FACEBOOK != ''):?>
                    <a href="<?php echo FACEBOOK;?>" class="count-icon custom social-icon" title="Facebook"><i class="fa fa-facebook"></i></a>
                <?php endif;?>
                <?php if(VK != ''):?>
                    <a href="<?php echo VK;?>" class="count-icon custom social-icon" title="Twitter"><i class="fa fa-vk"></i></a>
                <?php endif;?>
                <?php if(GPLUS != ''):?>
                    <a href="<?php echo GPLUS;?>" class="count-icon custom social-icon" title="Github"><i class="fa fa-google-plus"></i></a>
                <?php endif;?>
                <?php if(LINKEDIN != ''):?>
                    <a href="<?php echo LINKEDIN;?>" class="count-icon custom social-icon" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                <?php endif;?>
                <?php if(INSTAGRAM != ''):?>
                    <a href="<?php echo INSTAGRAM;?>" class="count-icon custom social-icon" title="Instagram"><i class="fa fa-instagram"></i></a>
                <?php endif;?>
                <?php if(SKYPE != ''):?>
                    <a href="<?php echo SKYPE;?>" class="count-icon custom social-icon" title="Youtube"><i class="fa fa-skype"></i></a>
                <?php endif;?>
                <?php if(BLOGSPOT != ''):?>
                    <a href="<?php echo BLOGSPOT;?>" class="count-icon custom social-icon" title="BlogSpot"><i class="blogspot-icon"></i></a>
                <?php endif;?>
                <a data-toggle="modal" data-target="#modal-order-form" href="javascript:void(0);" class="btn btn-green min-width"><?php echo $this->lang->line('do_callout');?></a>
            </div><!-- End .right -->
        </div><!-- End .container -->
    </div>

    <div class="mb60 mb50-sm mb40-xs"></div><!-- margin -->

<div class="container">
    <div class="row">
        <h2 class="title custom text-uppercase text-center mb30"><span>«Таск Форс» в цифрах:</span></h2>

        <div class="col-sm-2 col-xs-6 text-center">
            <span class="more-symbol green">&gt;</span>
            <span class="count" data-from="0" data-to="1000" data-speed="3000" data-refresh-interval="50">0</span>
            <h4 class="count-title">Проставленных апостилей</h4>
        </div><!-- End .col-sm-4 -->

        <div class="col-sm-2 col-xs-6 text-center">
            <span class="more-symbol green">&gt;</span>
            <span class="count" data-from="0" data-to="800" data-speed="3000" data-refresh-interval="50">0</span>
            <h4 class="count-title">Клиентов</h4>
        </div><!-- End .col-sm-4 -->

        <div class="col-sm-2 col-xs-6 text-center">
            <span class="count" data-from="0" data-to="50" data-speed="3000" data-refresh-interval="50">0</span>
            <h4 class="count-title">Языков</h4>
        </div><!-- End .col-sm-4 -->

        <div class="col-sm-2 col-xs-6 text-center">
            <span class="more-symbol green">&gt;</span>
            <span class="count" data-from="0" data-to="60" data-speed="3000" data-refresh-interval="50">0</span>
            <h4 class="count-title">Переводчиков узкой специализации</h4>
        </div><!-- End .col-sm-4 -->

        <div class="col-sm-2 col-xs-6 text-center">
            <span class="more-symbol green">&gt;</span>
            <span class="count" data-from="0" data-to="300" data-speed="3000" data-refresh-interval="50">0</span>
            <h4 class="count-title">Внештатных переводчиков</h4>
        </div><!-- End .col-sm-4 -->

        <div class="col-sm-2 col-xs-6 text-center">
            <span class="count" data-from="0" data-to="6" data-speed="3000" data-refresh-interval="50">0</span>
            <h4 class="count-title">Лет компания работает на рынке</h4>
        </div><!-- End .col-sm-4 -->

        <div class="clearfix"></div>

    </div><!-- End .row -->
</div>

<div class="mb60 mb50-sm mb40-xs"></div><!-- margin -->

    <div class="pb40">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $content['anons'];?>
                    <div class="table-responsive">
                        <?php echo $content['text'];?>
                    </div>
                </div><!-- End .col-md-6 -->
                <div class="col-md-6">
                    <?php if(!empty($responsible)):?>
                    <div class="mb10"></div><!-- margin -->
                    <p class="text-center"><b>Получите консультацию у сотрудника компании Таск Форс:</b></p>
                    <img class="img-responsive img-circle center-block" src="<?php echo base_url('assets/uploads/images/team/'.$responsible['photo_url']);?>" alt="<?php echo $responsible['name'];?>" title="<?php echo $responsible['name'];?>" />
                    <p class="text-center"><i><b><?php echo $responsible['name'];?></b></i><br><i><?php echo $responsible['position'];?></i></p>
                    <p class="text-center"><a class="btn btn-custom" data-toggle="modal" data-target="#modal-callme-form">Перезвоните мне</a></p>
                    <?php endif;?>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div>

    <?php if(!empty($responsible)):?>
    <div class="testimonials-container pt70 pb60 pt50-xs pb45-xs bg-image text-white overlay-container text-center" style="background-image:url(http://eonythemes.com/themes/wb/simple/main/assets/images/backgrounds/index-agency/testimonials-bg.jpg)">
        <div class="container">
            <h2 class="title text-center text-white text-uppercase mb30">Об услуге:</h2>

            <div class="testimonials-wrapper">

                <div class="testimonials-slider owl-carousel">
                    <div class="testimonial transparent">
                        <blockquote>
                            <figure>
                                <img src="<?php echo base_url('assets/uploads/images/team/'.$responsible['photo_url']);?>" alt="<?php echo $responsible['name'];?>">
                            </figure>
                            <p>«Если Ваш документ выдан за границей, то для того, чтобы официальные инстанции в Украине приняли его, нам нем должен стоять апостиль или легализация. Апостиль – это упрощенная форма легализации, выглядит как большая квадратная печать, одинаковая во всех странах. Если апостиля нет – проконсультируйтесь со мной, я подскажу, может ли этот документ быть принят в Украине. Однако в большинстве случаев – нет. И вам придется возвращать документ в ту страну, где он выдан, чтобы на нем проставили апостиль. Однако есть еще один вариант – можно пойти более сложным путем и проставить легализацию на такой документ здесь, в Украине (обычно через МИД Украины и посольство страны, которая выдала документ). Тем не менее, процедура легализации займет больше времени (от 30 дней) и будет стоить дороже».</p>
                            <cite><?php echo $responsible['name'];?></cite>
                        </blockquote>
                    </div><!-- End .testimonial -->
                </div><!-- End .testimonials-slider -->

            </div><!-- End .testimonials-wrapper -->
        </div><!-- end .container -->
        <div class="overlay custom" style="background-color: #000000;"></div><!-- End .overlay -->
    </div><!-- End .testimonial-container -->
    <?php endif;?>

    <div class="border pt55 pb40">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="table-responsive">
                        <?php echo $content['video_url'];?>
                    </div>
                    <!-- End .panel-group -->
                    <div class="mb5">&nbsp;</div>
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div>

    <div class="callout text-center dark" style="margin-bottom: 0px;">
        <div class="container">
            <h3 class="callout-title">
                Узнать стоимость<br/><small>Заполните форму и узнайте стоимость</small>
            </h3>
            <br/>
            <a data-toggle="modal" data-target="#modal-order-form" href="javascript:void(0);" class="btn btn-lg btn-green min-width">Заполнить форму</a>
        </div><!-- End .container -->
    </div>

    <div class="mb55 mb45-sm mb35-xs"></div><!-- margin -->

    <div class="bg-gray2 pt65 pb40 pt45-sm pb25-sm steps">

        <h2 class="title text-white gray text-uppercase text-center mb40">КАК ЭТО РАБОТАЕТ?</h2>

        <div class="container">
            <!-- SmartWizard html -->
            <div id="smartwizard">
                <ul>
                    <li class="done"><a href="#step-1">1. МЫ ПРОВЕРЯЕМ<br /><small>Ставится ли на ваш документ апостиль или легализация</small></a></li>
                    <li class="done"><a href="#step-2">2. НАШ КУРЬЕР<br /><small>Забирает документы</small></a></li>
                    <li class="done"><a href="#step-3">3. ПРОСТАВЛЯЕМ АПОСТИЛЬ<br /><small>И возвращаем Вам документ уже с апостилем, либо...</small></a></li>
                    <li class="done"><a href="#step-4">4. ПРОДОЛЖАЕМ РАБОТАТЬ<br /><small>Перевод, нотариальное заверение</small></a></li>
                </ul>

                <div>
                    <div id="step-1" class="">
                        <h2>Описание шага 1</h2>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                    <div id="step-2" class="">
                        <h2>Описание шага 2</h2>
                        <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </div>
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                    <div id="step-3" class="">
                        <h2>Шаг 3</h2>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        <div class="clearfix"></div>
                        <br/>
                    </div>
                    <div id="step-4" class="">
                        <h2>Шаг 4</h2>
                        <div class="panel panel-default">
                            <div class="panel-heading">My Details</div>
                            <table class="table">
                                <tbody>
                                <tr> <th>Name:</th> <td>Tim Smith</td> </tr>
                                <tr> <th>Email:</th> <td>example@example.com</td> </tr>
                                </tbody>
                            </table>
                            <div class="clearfix"></div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="container">
            <h2 class="title text-white gray text-uppercase text-center mb40">КАК ЭТО РАБОТАЕТ?</h2>

            <div class="row">
                <div class="how_it_work">
                    <div class="step step--1">
                        <span>Мы проверяем,</span><br>
                         ставится ли на ваш документ апостиль или легализация
                    </div>

                    <div class="step step--2">
                        <span>Наш курьер</span><br>
                        забирает документы
                    </div>

                    <div class="step step--3">
                        <span>Апостиль проставляем</span><br>
                        и возвращаем Вам документ уже с апостилем, либо...
                    </div>

                    <div class="step step--4">
                        <span>Продолжаем с документом работать</span><br>
                        (перевод, нотариальное заверение)
                    </div>
                </div>
            </div>

        </div>-->
    </div>

    <div class="container">
        <div class="row">

            <div class="mb80"></div><!-- margin -->

            <h2 class="title custom text-center">Для легализации дополнительно нам могут понадобиться:</h2>
            <div class="text-center">
                <ul>
                    <li>1. Письмо-заявка от компании;</li>
                    <li>2. Другие сопутствующие документы (например, оригиналы статутных документов);</li>
                    <li>3. Работа с нотариусами (афидевиты, нотариальное заверение документа перед легализацией).</li>
                </ul>
            </div>

            <div class="mb80"></div><!-- margin -->

        </div>
    </div>

    <div class="home-info-form-section fullscreen bg-image overlay-container languages" style="background-image:url('http://eonythemes.com/themes/wb/simple/main/assets/images/backgrounds/index9/home-bg.jpg')">
        <div class="overlay dark"></div>
        <div class="container">
            <h2 class="title text-white text-center text-uppercase mb15">ПЕРЕВОДЫ С КАКИХ И НА КАКИЕ ЯЗЫКИ МЫ ЗАВЕРЯЕМ НОТАРИАЛЬНО:</h2>
            <div class="clearfix"></div>
            <br/><br/>
            <?php $lang_counter = 1; foreach($this->languages as $language):?>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <a href="<?php echo base_url($language['url']);?>">
                        <img width="32" height="32" src="<?php echo base_url('assets/uploads/images/flags/'.$language['icon']);?>" /> <?php echo $language['title'];?>
                    </a>
                </div><!--close last item-->

                <?php if($lang_counter%4 == 0):?>
                    <div class="clearfix"></div>
                    <br/>
                <?php endif;?>
                <?php $lang_counter++; endforeach;?>
        </div>
    </div><!--closeitem-->

    <div class="container">
        <div class="row">

            <div class="mb40"></div><!-- margin -->

            <div class="mb40"></div><!-- margin -->

            <h2 class="title custom text-center">С нами выгодно сотрудничать, потому что мы:</h2>

            <div class="mb20"></div><!-- margin -->

            <div class="col-md-4 col-sm-6">
                <div class="service text-center">
                    <i class="fa fa-clock-o icon-bg custom"></i>
                    <h3 class="service-title">Экономим ваше время</h3>
                    <p>Ваше участие в процедурах легализации и апостилизации минимальное <em>(нам нужна будет доверенность от вас на представление интересов в случае легализации)</em></p>
                </div><!-- End .service -->
            </div><!-- End .col-md-4 -->

            <div class="col-md-4 col-sm-6">
                <div class="service text-center">
                    <i class="fa fa-life-ring icon-bg custom"></i>
                    <h3 class="service-title">Несем ответственность за сохранность документов</h3>
                    <p>Документы во все органы подает наш штатный курьер (принимает и передает документы вам под расписку)</p>
                </div><!-- End .service -->
            </div><!-- End .col-md-4 -->

            <div class="col-md-4 col-sm-6">
                <div class="service text-center">
                    <i class="fa fa-lightbulb-o icon-bg custom"></i>
                    <h3 class="service-title">Всегда в курсе всех изменений</h3>
                    <p>Наша компания постоянно следит за законодательными нововведениями в этой сфере</p>
                </div><!-- End .service -->
            </div><!-- End .col-md-4 -->

            <div class="clearfix"></div><!-- End .clearfix -->

            <div class="col-md-4 col-sm-6">
                <div class="service text-center">
                    <i class="fa fa-handshake-o icon-bg custom"></i>
                    <h3 class="service-title">Предоставляем бесплатную консультацию</h3>
                    <p>Наши менеджеры всегда готовы вас проконсультировать о том, как именно лучше заверить документ и в каком ведомстве в зависимости от ваших целей </p>
                </div><!-- End .service -->
            </div><!-- End .col-md-4 -->

            <div class="col-md-4 col-sm-6">
                <div class="service text-center">
                    <i class="fa fa-calendar-check-o icon-bg custom"></i>
                    <h3 class="service-title">Честно говорим о сроках проведения процедуры</h3>
                    <p>Если легализация или проставление апостиля займет много времени, мы об этом сообщаем сразу</p>
                </div><!-- End .service -->
            </div><!-- End .col-md-4 -->

            <div class="col-md-4 col-sm-6">
                <div class="service text-center">
                    <i class="fa fa-key icon-bg custom"></i>
                    <h3 class="service-title">Услуга под ключ</h3>
                    <p>Не только апостилируем и легализуем документы, но и организуем качественный и быстрый их перевод на нужный вам язык и нотариальное заверение. </p>
                </div><!-- End .service -->
            </div><!-- End .col-md-4 -->



        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="mb25"></div><!-- margin -->

    <div class="callout text-center" style="margin-bottom: 0px;">
        <div class="container">
            <h3 class="callout-title">
                Узнать стоимость<br/><small>Заполните форму и узнайте стоимость</small>
            </h3><br/>
            <a data-toggle="modal" data-target="#modal-order-form" href="javascript:void(0);" class="btn btn-lg btn-green min-width">Заполнить форму</a>
        </div><!-- End .container -->
    </div>

</div>