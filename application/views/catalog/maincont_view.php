<?php 
    function textFunc( $str, $maxLen )
    {
        if ( mb_strlen( $str ) > $maxLen )
    	{
    	  
           preg_match( '/^.{0,'.$maxLen.'} .*?/ui', $str, $match );
    	   return @$match[0].'...';
           
    	} else {
    	   
           return $str;

        }
    }
?>

    <div class="mb10"></div><!-- space -->
    <div class="container">
        <div class="row">

            <?php echo $content['text'];?>

            <?php $counter = 1; foreach($cats as $mp_cat):?>
                <div class="col-sm-6">
                    <div class="service service-big wow fadeInUp"  data-wow-delay="0.30s">
                        <a href="<?php echo base_url($lang.$mp_cat['url']);?>">
                                    <span class="service-icon">
                                        <img class="img-responsive" src="<?php echo base_url('assets/uploads/images/categories/thumbs/'.$mp_cat['img']);?>" title="" alt="" />
                                    </span>
                            <div class="service-content">
                                <h3 class="service-title text-uppercase"><?php echo $mp_cat['title'];?></h3>

                                <p><?php echo textFunc(strip_tags($mp_cat['anons']),120);?></p>
                            </div><!-- End .service-content -->
                        </a>
                    </div><!-- End .service -->
                </div><!-- End .col-sm-4 -->

                <?php if($counter%2 == 0):?>
                    <div class="clearfix"></div>
                <?php endif;?>

             <?php $counter++; endforeach;?>
            
        </div>
        
    </div><!-- End .container -->
</div><!-- End #content -->