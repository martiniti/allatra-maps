    <div class="pb40">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="title custom"><?php echo $content['title'];?>:</h2>
                    <?php echo $content['text'];?>
                </div><!-- End .col-md-6 -->
                <div class="col-md-6">
                    <div class="mb10"></div><!-- margin -->
                    <img src="<?php echo base_url('assets/uploads/images/categories/'.$info['img_url']);?>" alt="<?php echo $content['title'];?>" class="img-responsive center-block">
                </div><!-- End .col-md-6 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div>
    <div class="callout dark" style="margin-bottom: 0px;">
        <div class="container">
            <div class="left">
                <h3 class="callout-title">
                    <?php echo $this->lang->line('callout_title');?>
                </h3>
            </div><!-- End .left -->
            <div class="right">
                <a data-toggle="modal" data-target="#modal-order-form" href="javascript:void(0);" class="btn btn-green min-width"><?php echo $this->lang->line('do_callout');?></a>
            </div><!-- End .right -->
        </div><!-- End .container -->
    </div>
    
    <div class="table-row">
        <div class="table-cell cell-content larger bg-gray border text-muted">
            <div class="table-cell-wrapper">

                <?php if(!empty($services)):?>

                    <h2 class="title dark text-uppercase text-spaced mb30"><?php echo $this->lang->line('our_service');?></h2>

                    <?php for ($i = 1; $i<=count($services); $i+=2):?>
                        <div class="row">
                            <div class="col-xs-6 col-xss-12">
                                <div class="service icon-left">
                                    <a href="<?php echo base_url($services[$i]['url']);?>">
                                        <i class="fa <?php echo $services[$i]['icon'];?> icon-bg custom"></i>
                                        <h3 class="service-title text-light text-spaced"><?php echo $services[$i]['title'];?></h3>
                                        <p><?php echo strip_tags($services[$i]['description']);?></p>
                                    </a>
                                </div><!-- End .service -->
                            </div><!-- End .col-xs-6 -->

                            <div class="col-xs-6 col-xss-12">
                                <div class="service icon-left">
                                    <a href="<?php echo base_url($services[$i-1]['url']);?>">
                                        <i class="fa <?php echo $services[$i-1]['icon'];?> icon-bg custom"></i>
                                        <h3 class="service-title text-light text-spaced"><?php echo $services[$i-1]['title'];?></h3>
                                        <p><?php echo strip_tags($services[$i-1]['anons']);?></p>
                                    </a>
                                </div><!-- End .service -->
                            </div><!-- End .col-xs-6 -->
                        </div><!-- End .row -->
                        <div class="mb10"></div><!-- margin -->
                    <?php endfor;?>

                    <div class="row">
                        <div class="col-xs-12">
                            <hr class="mt10 mb40">
                        </div><!-- End .col-xs-12 -->
                    </div><!-- End .row -->

                <?php endif;?>

                <?php if(!empty($advantages)):?>
                    <div class="row">
                        <?php foreach ($advantages as $advantage):?>
                            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                                <?php echo $advantage['text'];?>
                            </div><!-- End .col-md-2 -->
                        <?php endforeach;?>
                    </div><!-- End .row -->
                <?php endif;?>
            </div><!-- end .table-cell-wrapper -->
        </div><!--End .table-cell  -->

        <div class="table-cell cell-image smaller overlay-container" style="background-image:url('<?php echo base_url();?>assets/template/taskforce/images/backgrounds/bg2.jpg');">
        </div><!-- End .table-cell -->
    </div>

    <?php if(!empty($partners)): ?>
        <div class="bg-custom border pt40 pb30">
            <div class="container">
                <h2 class="title text-white mb25">Среди наших партнеров</h2>
                <div class="clients-carousel owl-carousel">
                    <?php foreach($partners as $partner):?>
                        <a href="<?php echo $partner['link'];?>" class="client"  target="_blank" rel="nofollow">
                            <img src="<?php echo base_url('assets/uploads/images/partners/'.$partner['img_url']);?>" alt="<?php echo $partner['title'];?>" title="<?php echo $partner['title'];?>" />
                        </a>
                    <?php endforeach;?>
                </div><!-- End .clients-carousel -->
            </div><!-- End .container -->
        </div><!-- End .bg-custom -->
    <?php endif;?>
</div><!-- End .main -->