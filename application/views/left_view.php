<aside class="col-md-3 sidebar col-md-pull-9" role="complementary"> 
    <!--
    <div class="widget">
        <h3><?php echo $this->lang->line('catalog');?></h3>
        <?=$menu;?>
    </div>-->

    <div class="widget">
        <h3 class="title-border red title-bg-line"><span><?php echo $this->lang->line('farewell');?></span></h3>
        <div class="farewell">
            <div class="farewell-tale">
                <p>
                    <a href="<?php echo base_url('citations#citate-'.$this->farewell['farewell_id']);?>"><?php echo $this->farewell['text'];?></a>
                    <a class="pull-left" href="<?php echo base_url('citations#citate-'.$this->farewell['farewell_id']);?>">
                        <span><small><i><?php echo $this->farewell['author'];?></i></small></span>
                    </a>
                </p>
            </div>
        </div>
    </div>

    <?php if(isset($this->video_of_the_day) && count($this->video_of_the_day)>0):?>
        <div class="widget">
            <h3 class="title-border pink title-bg-line"><span><?php echo $this->lang->line('videos');?></span></h3>
            <?php foreach ($this->video_of_the_day as $day_video):?>
                <div class="embed-responsive embed-responsive-16by9">
                    <?php echo $day_video['video_url'];?>
                </div><!-- End .embed-responsive -->
            <?php endforeach;?>
        </div><!-- End .widget -->
    <?php endif;?>

    <?php if(isset($info['enable_comments']) && $info['enable_comments'] == 1 ):?>

    <?php else:?>
    <div class="widget">
        <h3 class="title-border blue title-bg-line"><span><?php echo $this->lang->line('weather');?></span></h3>
        <div id="weather"></div>
    </div>
    <?php endif;?>

    <!--
    <?php if(isset($this->photos_of_the_day) && count($this->photos_of_the_day)>0):?>
        <div class="widget">
            <h3 class="title-border green title-bg-line"><span><?php echo $this->lang->line('photos');?></span></h3>
            <ul class="flickr-widget no-radius flickr-widget-two clearfix">
                <?php foreach ($this->photos_of_the_day as $days_photo):?>
                    <?php foreach($days_photo['children'] as $day_photo):?>
                        <li>
                            <a href="<?php echo base_url($lang.$days_photo['section']['url']);?>">
                                <img src="<?php echo base_url().'assets/uploads/images/photos/thumb__'.$day_photo['img_url'];?>" alt="<?php echo $day_photo['title'];?>" title="<?php echo $day_photo['title'];?>">
                            </a>
                        </li>
                    <?php endforeach;?>
                <?php endforeach;?>
            </ul>
        </div>
    <?php endif;?>
    -->

</aside>