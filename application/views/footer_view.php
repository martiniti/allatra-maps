        <!-- Site Footer-->
        <footer class="site-footer">
            <div class="column text-center">
                <p class="text-sm mb-4">Присылайте свои наблюдения на Email <span class="text-primary">&nbsp;info@allatravesti.com</span></p>
                <a class="social-button sb-skype" href="#" data-toggle="tooltip" data-placement="top" title="Skype"><i class="socicon-skype"></i></a>
                <a class="social-button sb-facebook" href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a>
                <a class="social-button sb-google-plus" href="#" data-toggle="tooltip" data-placement="top" title="Google +"><i class="socicon-googleplus"></i></a>
                <a class="social-button sb-twitter" href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="socicon-twitter"></i></a>
                <a class="social-button sb-instagram" href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="socicon-instagram"></i></a>
                <p class="text-xxs text-muted mb-0 mt-3"><?php echo date('Y');?> © Все права защищены. Воспроизведение материалов сайта в любой форме и на любом носителе разрешается и приветствуется. При использовании материалов будем благодарны за ссылку на наш сайт.</p>
            </div>
            <div class="column">
                <h3 class="widget-title text-center">Подписка<small> Следите за новой информацией и новостями проекта.</small></h3>
                <form class="subscribe-form input-group" action="#" method="post" target="_blank" novalidate><span class="input-group-btn">
                    <button type="submit"><i class="material-icons send"></i></button></span>
                    <input class="form-control" type="email" name="EMAIL" placeholder="Ваш e-mail">
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true">
                        <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1" value>
                    </div>
                </form>
            </div>
            <div class="column">
                <h3 class="widget-title text-center">Партнёры<small> Мы поддерживаем Глобальное партнёрское соглашение АЛЛАТРА:</small></h3>
                <div class="footer-cards">
                    <a href="https://allatra-partner.org">
                        <?php echo theme_img('banners/gps.gif','Глобальное партнёрское соглашение АЛЛАТРА');?>
                    </a>
                </div>
            </div>
        </footer>
        <!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="material-icons trending_flat"></i></a>
        <!-- Backdrop-->
        <div class="site-backdrop"></div>

    </body>
</html>