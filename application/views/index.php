<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>STYLE-M</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link href="<?php echo base_url()."assets/bootstrap/css/bootstrap.min.css";?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()."assets/font-awesome/css/font-awesome.min.css";?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()."assets/template/style_new.css";?>" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?php echo base_url()."assets/bootstrap/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."assets/js/init.js";?>"></script>
</head>
<body>
<!--=== Header ===-->
<div class="header darkblue">

    <div class="navbar navbar-default" role="navigation">
        <div class="container blue">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Переключение навигации</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>">
                    <img id="logo-header" width="100%" src="<?php echo base_url()."assets/template/img/logo.png";?>">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="<?php echo base_url();?>">Главная</a></li>
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Галерея</a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Дизайнерам</a></li>
                    <li><a href="#">Цены</a></li>
                    <li><a href="#">Расчитать стоимость</a></li>
                    <li><a href="#">Контакты</a></li>
                    <li class="hidden-sm"><a class="search"><i class="search-btn fa fa-search"></i></a></li>
                </ul>
                <div class="search-open">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Поиск">
                        <span class="input-group-btn">
                            <button class="btn-u" type="button">Найти</button>
                        </span>
                    </div><!-- /input-group -->
                </div>                
            </div><!-- /navbar-collapse -->
        </div>    
    </div>
</div>
<!--/header-->
<!--=== End Header ===-->
<!--=== Waterfall ===-->
<script type="text/javascript" src="<?php echo base_url()."assets/js/swfobject.js";?>"></script>
<div id="flashbuttons">
    <div class="container">
        <h1>Аква-студия “Стиль-М”</h1>
        <span>Вместе мы реализуем любую вашу идею в реальность!</span><br />
        <div class="clear"></div>
        <a class="flashbutton blue" onclick="javascript: window.alert('test');" href="#">О нас</a>
        <a class="flashbutton red" onclick="javascript: window.alert('test');" href="#">Сделать заказ</a>
    </div>
</div>
<div id="flashcontent" onclick="javascript: window.alert('test');">
    <embed type="application/x-shockwave-flash" src="<?php echo base_url()."assets/swf/preload.swf";?>" width="1920" height="430" id="studio" name="studio" quality="high">
</div>
<script type="text/javascript">
    var so = new SWFObject("<?php echo base_url()."assets/swf/preload.swf";?>", "studio", "1920", "430", "7");
        so.addParam("wmode","transparent");
        so.write("flashcontent");
</script>
<!--/waterfall-->
<!--=== End Waterfall ===-->
<div class="content">
    <div class="container blue">
        <div class="col-md-3 left_blocks">
            <div id="left_title">Товары и услуги</div>
            <ul>
                <li><a href="#">Водопады</a></li>
                <li><a href="#">Пузырьковые системы</a></li>
                <li><a href="#">Аквариумы</a></li>
                <li><a href="#">Стелкяный пол</a></li>
            </ul>
            <div id="left_title">Это интересно</div>
            <ul>
                <li><a href="#">Водопады</a></li>
                <li><a href="#">Пузырьковые системы</a></li>
                <li><a href="#">Аквариумы</a></li>
                <li><a href="#">Стелкяный пол</a></li>
            </ul>
            <div id="left_title">Наши клубы</div>
            <p class="padding-left-40">Здесь вы можете выставлять свои работы, идеи, проэкты. Будем рады содействовать вам в реализации ваших идей.</p>
            <ul>
                <li><a href="#">Клуб дизайнеров и архитекторов</a></li>
            </ul>    
        </div>
        <div class="col-md-9 right_blocks">
            <h1 class="page_title">Аквариумы и водопады от аква-студии "Стиль-М"</h1>
            <a href="<?php echo base_url()."assets/img/content/waterfall.jpg";?>" rel="gb_imageset[nice_pics]"><img src="<?php echo base_url()."assets/img/content/thumbs/waterfall.jpg";?>" /></a>
            <p>Дизайнерские аквариумы всегда выглядят неотразимо и способны преобразить любой интерьер, наполнив его особой красотой. Содержание аквариума приносит в ваш дом кусочек живой природы, уравновешивает и успокаивает домашнюю атмосферу. Богатство форм и окрасок аквариумных растений, даёт возможность создавать разнообразные подводные ландшафты.</p>
            <p>Но путь к эффектно украшенному аквариуму не так легок, как кажется, потому что необходимо не только учитывать разнообразные условия роста, но и обратить особое внимание на правильный подбор и уход растений. </p>
            <p>В аквариуме, декорированном живыми растениями, рыбы чувствуют себя особенно уютно, проявляют всю свою красоту, находят в зарослях отличные укрытия и потому хорошо размножаются. Аква-студия "Стиль-М" с радостью поможет Вам подобрать аквариум, который  наилучшим образом впишется в интерьер Вашего дома, а также правильно подобрать аквариумных рыбок и наполнение.</p> 
            <p>Мы предоставляем полный спектр услуг по дизайну, изготовлению, наполнению и обслуживанию аквариумов. Вместе мы реализуем любую вашу идею в реальность!!!</p>
            <p>Ручные изделия наших мастеров украшают дома, квартиры, магазины, гостиницы, зимние сады, офисы и рестораны в разных уголках Украины.</p>
            
            <div id="title_blue">Наши клиенты</div>
            
            <ul id="clients" class="grid">
              <li class="altoids"><a href="#" rel="altoids"> <img src="<?php echo base_url()."assets/template/img/clients/Altoids.png";?>" alt="Altoids"></a></li>    
              <li class="facebook"><a href="#" rel="facebook"> <img src="<?php echo base_url()."assets/template/img/clients/Facebook.png";?>" alt="Facebook"></a></li>  
              <li class="ge"><a href="#" rel="general-electric"> <img src="<?php echo base_url()."assets/template/img/clients/GE.png";?>" alt="General Electric"></a></li>   
              <li class="orbit"><a href="#" rel="orbit"> <img src="<?php echo base_url()."assets/template/img/clients/orbitlogo.png";?>" alt="Orbit"></a></li>    
              <li class="skittles"><a href="#" rel="skittles"> <img src="images/clients/Skittles.png" alt="Skittles"></a></li>
              <li class="jameson"><a href="#" rel="jameson"> <img src="images/clients/Jameson.png" alt="Jameson"></a></li>    
              <li class="juicy_fruit"><a href="#" rel="juicy-fruit"> <img src="images/clients/Juicy-Fruit.png" alt="Juicy Fruit"></a></li>    
              <li class="microsoft"><a href="#" rel="microsoft"> <img src="images/clients/Microsoft.png" alt="Microsoft"></a></li>    
              <li class="a_e"><a href="#" rel="ae"> <img src="images/clients/AE.png" alt="A&amp;E"></a></li>    
              <li class="zynga"><a href="#" rel="zynga"> <img src="images/clients/Zynga.png" alt="Zynga"></a></li>  
              <li class="smuin"><a href="#" rel="smuin"> <img src="images/clients/Smuin.png" alt="Smuin"></a></li>    
              <li class="westfield"><a href="#" rel="westfield"> <img src="images/clients/Westfield.png" alt="Westfield"></a></li>
            </ul>
            
            <div id="title_blue">Новые работы</div>
                
            <div id="works" class="row">
                
                <div class="col-md-3">
                    <a href="<?php echo base_url()."assets/img/content/1.jpg";?>"><img src="<?php echo base_url()."assets/img/content/thumbs/1.jpg";?>" /></a>
                </div>
                <div class="col-md-3">
                    <a href="<?php echo base_url()."assets/img/content/2.jpg";?>"><img src="<?php echo base_url()."assets/img/content/thumbs/2.jpg";?>" /></a>
                </div>
                <div class="col-md-3">
                    <a href="<?php echo base_url()."assets/img/content/3.jpg";?>"><img src="<?php echo base_url()."assets/img/content/thumbs/3.jpg";?>" /></a>
                </div>
                <div class="col-md-3">
                    <a href="<?php echo base_url()."assets/img/content/4.jpg";?>"><img src="<?php echo base_url()."assets/img/content/thumbs/4.jpg";?>" /></a>
                </div>
            </div>
            
            <div id="title_white" class="page_title">Заказать услугу</div>
            <fieldset id="contact_form">
                <div id="msgs"></div>
                <form id="contact_form" action = "#" method="post">
                    <input type="text" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="Имя и фамилия *">
                    <input type="text" id="email" name="email" value="<?php echo set_value('email');?>" placeholder="Email *">
                    <input type="text" id="phone" name="phone" value="<?php echo set_value('phone');?>" placeholder="Телефон *">
                    <textarea id="msg" name="message" placeholder="Комментарий к заказу"></textarea>
                    <input type="text" id="captcha" name="captcha" size="10"/>
                    <button id="submit" class="button">Отправить заказ</button>
                </form>
            </fieldset>
        </div> 
        <div class="clearfix"></div>
        <br>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 md-margin-bottom-40">
                <!-- About -->
                <div class="headline"><span>О нас</span></div>  
                <p class="margin-bottom-25 md-margin-bottom-40">Компания Стиль-М специализируется на изготовлении аква-систем любой сложности используя новейшие технологии производства  и декорирования водных изделий.</p>    
                <!-- Monthly Newsletter -->
                <div class="headline"><span>RSS</span></div> 
                <p>Подпишитесь к нам на новости и получайте самую свежую информацию из мира акваизделий!</p>
                <form class="footer-subsribe">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Email">                            
                        <span class="input-group-btn">
                            <button class="btn-u" type="button">Подписаться</button>
                        </span>
                    </div><!-- /input-group -->                    
                </form>                         
            </div>
            
            <div class="col-md-4 md-margin-bottom-40">
                <div class="posts">
                    <div class="headline"><span>Новости</span></div>
                    <dl class="dl-horizontal">
                        <dt><a href="#"><img src="assets/img/sliders/elastislide/6.jpg" alt=""></a></dt>
                        <dd>
                            <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                    <dt><a href="#"><img src="assets/img/sliders/elastislide/10.jpg" alt=""></a></dt>
                        <dd>
                            <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                    <dt><a href="#"><img src="assets/img/sliders/elastislide/11.jpg" alt=""></a></dt>
                        <dd>
                            <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p> 
                        </dd>
                    </dl>
                </div>
            </div>
            
            <div class="col-md-4">
                <!-- Monthly Newsletter -->
                <div class="headline"><span>Контакты</span></div> 
                <address class="md-margin-bottom-40">
                    Тел.: +38 (044) 232 54 00<br>
                    Моб.: +38 (097) 605 35 75<br>
                    Email: <a href="mailto:2325400@mail.ru" class="">2325400@mail.ru</a>
                </address>

                <!-- Stay Connected -->
                <div class="headline"><span>Присоединяйтесь:</span></div> 
                <ul class="social-icons">
                    <li><a href="#" data-original-title="VK" class="social_vk"></a></li>
                    <li><a href="#" data-original-title="Feed" class="social_rss"></a></li>
                    <li><a href="#" data-original-title="Facebook" class="social_facebook"></a></li>
                    <li><a href="#" data-original-title="Twitter" class="social_twitter"></a></li>
                    <li><a href="#" data-original-title="Goole Plus" class="social_googleplus"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-6">                      
                <p class="copyright-space">
                    2014 © Стиль-М. Все права защищены. 
                </p>
            </div>
            <div class="col-md-6">  
                <a href="index.html">
                    <img id="logo-footer" class="pull-right" height="35px" src="<?php echo base_url()."assets/template/img/logo.png";?>">
                </a>
            </div>
        </div><!--/row-->
    </div><!--/container--> 
</div>
</body>
</html>