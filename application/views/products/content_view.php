    <div class="mb10"></div><!-- space -->
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <div class="row">
                    <div class="col-md-5">
                        <div class="product-gallery-container">
                            <div class="product-top">
                                <img id="product-zoom" src="<?php echo base_url('assets/uploads/images/content/'.$content['img_url']);?>" data-zoom-image="<?php echo base_url('assets/uploads/images/content/'.$content['img_url']);?>" alt="<?php echo $content['title'];?>"/>
                            </div><!-- End .product-top -->
                        </div><!-- End .product-gallery-container -->
                    </div><!-- End .col-md-5 -->

                    <div class="col-md-7">
                        <div class="product-details">
                            <h2 class="product-title"><?php echo $content['title'];?></h2>

                            <?php if($content['anons'] != ''):?>
                            <div class="product-anons">
                                <?php echo $content['anons'];?>
                            </div>
                            <?php endif;?>
                           
                            <div class="product-price-container">
                                <span class="product-price"><?php echo $info['price_euro'].' €';?></span>
                            </div><!-- End .product-price-container -->

                            <div class="product-action">
                                <a href="#" class="btn btn-custom min-width add-to-cart no-radius" data-toggle="modal" data-target="#modal-product-form-<?php echo $info['product_id'];?>" title="<?php echo $this->lang->line('product_do_order');?>"><i class="fa fa-shopping-cart"></i><?php echo $this->lang->line('product_do_order');?></a>
                            </div><!-- End .product-action -->

                            <div class="share-container">
                                <label class="input-desc"><?php echo $this->lang->line('product_share');?>:</label>
                                <div class="social-icons social-icons-bg social-icons-bg-hover social-icons-circle">
                                    <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,blogger,linkedin" data-counter=""></div>
                                </div><!-- End .social-icons -->
                            </div><!-- End .share-container -->

                        </div><!-- End .product-details -->
                    </div><!-- End .col-md-7 -->

                </div><!-- End .row -->
                
                <div class="product-tab-section">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab"><i class="fa fa-home"></i><?php echo $this->lang->line('product_description');?></a></li>
                        </ul>

                        <!-- Tab Panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="description">
                                <?php echo $content['text'];?>
                            </div><!-- End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- end role[tabpanel] -->
                </div><!-- End .product-tab-section -->

                <div class="mb50"></div><!-- space -->
                
                <?php if(isset($similar_products) && !empty($similar_products)):?>

                <h3 class="title-underblock custom mb80 text-center"><?php echo $this->lang->line('product_related');?></h3>
                <div class="owl-carousel related-products-carousel2 center-top-nav gray-nav">
                
                    <?php foreach($similar_products as $product):?>
                    <div class="product text-center">
                        <div class="product-top">
                            <figure>
                                <a href="<?php echo base_url($lang.$product['url']);?>" title="<?php echo $product['name'];?>">
                                    <img src="<?php echo base_url('assets/uploads/images/content/thumbs/'.$product['image']);?>" alt="<?php echo $product['name'];?>" class="product-image">
                                    <img src="<?php echo base_url('assets/uploads/images/content/thumbs/'.$product['image']);?>" alt="<?php echo $product['name'];?>" class="product-image-hover">
                                </a>
                            </figure>
                            <div class="product-action-container product-action-animate">
                                <a href="<?php echo base_url($lang.$product['url']);?>" class="btn btn-dark quick-view" title="<?php echo $this->lang->line('product_view');?>"><i class="fa fa-search-plus"></i></a>
                            </div><!-- end .product-action-container -->
                        </div><!-- End .product-top -->
                        <h3 class="product-title"><a href="<?php echo base_url($lang.$product['url']);?>" title="<?php echo $product['name'];?>"><?php echo $product['name'];?></a></h3>
                        <div class="ratings-container">
                            <?php if(LANGUAGE == 'ru' || LANGUAGE == 'ua'):?>
                            <a href="<?php echo base_url($lang.$product['url']);?>" class="product-ratings add-tooltip" title="5 <?php echo $this->lang->line('product_average');?>">
                                <span class="ratings" style="width:100%" ></span><!-- End .ratings -->
                            </a><!-- End .product-ratings -->
                            <?php else:?>
                            <a href="<?php echo base_url($lang.$product['url']);?>" class="product-ratings add-tooltip" title="<?php echo $this->lang->line('product_average');?> 5">
                                <span class="ratings" style="width:100%" ></span><!-- End .ratings -->
                            </a><!-- End .product-ratings -->
                            <?php endif;?> 
                        </div><!-- End .ratings-container -->
                        <div class="product-price-container">
                            <?php if(LANGUAGE == 'ru' || LANGUAGE == 'ua'):?>
                            <span class="product-price"><?php echo $product['price_ua'].' грн.';?></span>
                            <?php else:?>
                            <span class="product-price"><?php echo $product['price_euro'].' €';?></span>
                            <?php endif;?> 
                        </div><!-- End .product-price-container -->

                        <a href="<?php echo base_url($lang.$product['url']);?>" class="btn btn-custom add-to-cart"><?php echo $this->lang->line('product_view');?></a>

                    </div><!-- End .product -->
                    <?php endforeach;?>
                    
                </div><!-- End .owl-carousel -->
                
                <?php endif;?>
                
            </div><!-- End .col-md-9 -->
            
            
            <div class="mb60 clearfix visible-sm visible-xs"></div>
            <?php echo $this->load->view('left_view');?>
        </div>
        
    </div><!-- End .container -->
</div><!-- End #content -->

<?php $this->load->view('modals/product-order-modal');?>