    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-md-offset-1">

                <?php echo $content['text'];?>

                <?php if(!empty($materials)):?>

                <?php foreach($materials as $material):?>

                <article class="entry padding-bottom-2x">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="entry-media">
                                <?php if($material['video_url'] != ''):?>
                                <figure class="embed-responsive embed-responsive-16by9">
                                    <?php echo $material['video_url'];?>
                                </figure><!-- End .embed-responsive -->
                                <?php else:?>
                                <figure>
                                    <a href="<?php echo base_url($lang.$material['url']);?>">
                                        <img src="<?php echo base_url('assets/uploads/images/content/'.$material['image']);?>" class="img-responsive" alt="<?php echo $material['name'];?>">
                                    </a>
                                </figure>
                                <?php endif;?>
                            </div><!-- End .entry-media -->
                        </div><!-- End .col-md-6 -->
                        <div class="col-md-6">
                            <h2 class="post-title"><a style="text-decoration: none;" href="<?php echo base_url($lang.$material['url']);?>"><?php echo $material['name'];?></a></h2>
                            <div class="entry-content">
                                <p><?php echo strip_tags($material['anons']);?></p>
                                <a href="<?php echo base_url($lang.$material['url']);?>" class="btn btn-primary btn-sm no-radius"><?php echo $this->lang->line('material_readmore');?></a>
                            </div><!-- End .entry-content -->
                        </div><!-- End .col-md-6 -->
                    </div><!-- End .row -->
                </article>

                <?php endforeach;?>

                <?php endif;?>

                <?php echo $page_nav;?>

            </div><!-- End .col-md-12 -->

        </div><!-- End .row -->
    </div><!-- End .container -->
</div><!-- End .main -->