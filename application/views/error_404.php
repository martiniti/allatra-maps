<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html> <!--<![endif]-->
<head>
    
    <meta charset="utf-8">
    
    <title><?php $this->lang->line('404_title');?></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    
    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700,900,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,400italic,600italic,700italic,600,800,300,700,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <!-- Google Fonts -->
    
    <link id="favicon" href="<?php echo base_url()."assets/template/images/favicon.ico";?>" type="image/x-icon" rel="shortcut icon">
    <link id="png16favicon" rel="icon" href="<?php echo base_url()."assets/template/images/favicon_16.png";?>" sizes="16x16">
    <link id="png32favicon" rel="icon" href="<?php echo base_url()."assets/template/images/favicon_32.png";?>" sizes="32x32">
    
    <!-- Favicon and Apple Icons -->
    <link rel="icon" type="image/png" href="<?php echo base_url("assets/template/images/icons/favicon.png");?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url("assets/template/images/icons/faviconx57.png");?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url("assets/template/images/icons/faviconx72.png");?>">

    <link rel="stylesheet" href="<?php echo base_url("assets/template/css/animate.css");?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/template/css/bootstrap.min.css");?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/template/css/magnific-popup.css");?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/template/css/style.css");?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/template/css/revslider/revslider-index5.css");?>">
    <link rel="stylesheet" id="color-scheme" href="<?php echo base_url("assets/template/css/colors/green.css");?>">
    
    <script>
        var base_url = "<?php echo base_url();?>";
    </script>
    
    <!-- Modernizr -->
    <script src="<?php echo base_url()."assets/template/js/modernizr.js";?>"></script>

    <!--- jQuery -->
    <script src="<?php echo base_url()."assets/template/js/jquery.min.js";?>"></script>

    <!-- Queryloader -->
    <script src="<?php echo base_url()."assets/template/js/queryloader2.min.js";?>"></script>
    
    <!-- Fancybox -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/template/jquery.fancybox.css?v=2.0.6";?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/template/jquery.fancybox-buttons.css?v=1.0.2";?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()."assets/template/jquery.fancybox-thumbs.css?v=1.0.2";?>" />
    <script type="text/javascript" src="<?php echo base_url()."assets/js/fancybox/jquery.mousewheel-3.0.6.pack.js";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/js/fancybox/jquery.fancybox.js?v=2.0.6";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/js/fancybox/jquery.fancybox-buttons.js?v=1.0.2";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/js/fancybox/jquery.fancybox-thumbs.js?v=1.0.2";?>"></script>
    <script type="text/javascript" src="<?php echo base_url()."assets/js/fancybox/jquery.fancybox-media.js?v=1.0.0";?>"></script>
    
    <!-- Fancybox -->
    <script type="text/javascript" src="<?php echo base_url('assets/fancybox/jquery.mousewheel-3.0.6.pack.js');?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox.css?v=2.1.5');?>" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url('assets/fancybox/jquery.fancybox.pack.js?v=2.1.5');?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/fancybox/jquery.fancybox-buttons.css?v=1.0.5');?>" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url('assets/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6');?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7');?>" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo base_url('assets/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7');?>"></script>

</head>
<body>

<div class="boss-loader-overlay"></div><!-- End .boss-loader-overlay -->

<div id="wrapper">
    <div id="content" style="padding-bottom: 0;">
        <div class="error-page error-pagebg light text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-md-push-9">
                        <h2 class="error-title vertical"><span>4</span><span>0</span><span>4</span></h2>
                    </div><!-- End .col-md-3 -->
                    <div class="col-md-9 col-md-pull-3">
                        <h3 class="error-subtitle vertical"><?php echo $this->lang->line('404_title');?></h3>
    
                        <p class="error-text center-block"><?php echo $this->lang->line('404_text');?></p>
                        
                        <form method="get" action="http://www.google.com/search">
                            <div class="form-group">
                                <input name="q" class="form-control input-lg input-border-bottom white text-center" type="text" placeholder="<?php echo $this->lang->line('404_search_in_here');?>">
                            </div><!-- end .form-group -->
                            
                            <input type="hidden" name="sitesearch" value="<?php echo base_url();?>" />
                            
                            <div class="form-group">
                                <input type="submit" class="btn btn-white btn-border min-width" value="<?php echo $this->lang->line('404_do_search');?>">
                            </div><!-- end .form-group -->
                        </form>
                    </div><!-- End .col-md-9 -->
                </div><!-- End .row -->
                
            </div><!-- End .container -->
        </div><!-- End .error-page -->
    
    </div><!-- End #content -->
</div>

<!-- Smoothscroll -->
<script src="<?php echo base_url("assets/template/js/smoothscroll.js");?>"></script>

<script src="<?php echo base_url("assets/template/js/bootstrap.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.hoverIntent.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.nicescroll.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/waypoints.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/waypoints-sticky.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.debouncedresize.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/retina.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/owl.carousel.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jflickrfeed.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/twitter/jquery.tweet.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/skrollr.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.countTo.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/isotope.pkgd.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.magnific-popup.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.themepunch.tools.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.themepunch.revolution.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/wow.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/jquery.validate.min.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/contact.js");?>"></script>
<script src="<?php echo base_url("assets/template/js/main.js");?>"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script>
    (function () {
        "use strict";
        // Slider Revolution
        jQuery('#revslider').revolution({
            delay:8000,
            startwidth:1170,
            startheight:520,
            fullWidth:"on",
            hideTimerBar: "on",
            spinner:"spinner2",
            navigationStyle: "preview4",
            soloArrowLeftHOffset:0,
            soloArrowRightHOffset:0
        });
        
        /*---- Google map - Footer - top ----- */
        if (document.getElementById("footer-top-map")) {
            var locations = [
                ['<div class="map-info-box"><ul class="contact-info-list"><li><span><i class="fa fa-home fa-fw"></i></span> с. Софиевская Борщаговка, ул. Ленина, 16</li><li><span><i class="fa fa-phone fa-fw"></i></span> +38 (067) 449-25-50</li></ul></div>', 50.4156965, 30.3730856, 9],
            ];

            var map = new google.maps.Map(document.getElementById('footer-top-map'), {
                zoom: 13,
                center: new google.maps.LatLng(50.4156965, 30.3708969),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
            });

            var infowindow = new google.maps.InfoWindow();


            var marker, i;

            for (i = 0; i < locations.length; i++) {  
              marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: base_url + 'assets/template/images/pin.png',
              });

              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
                }
              })(marker, i));
            }
        }
    }());
    
    /* Map */
    (function () {
        "use strict";
        
        if (document.getElementById("map")) {
            var locations = [
                ['<div class="map-info-box"><ul class="contact-info-list"><li><span><i class="fa fa-home fa-fw"></i></span> с. Софиевская Борщаговка, ул. Ленина, 16</li><li><span><i class="fa fa-phone fa-fw"></i></span> +38 (067) 449-25-50</li></ul></div>', 50.4156965, 30.3730856, 9],
            ];

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: new google.maps.LatLng(50.4156965, 30.3708969),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
            });

            var infowindow = new google.maps.InfoWindow();


            var marker, i;

            for (i = 0; i < locations.length; i++) {  
              marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: base_url + 'assets/template/images/pin.png',
              });

              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
                }
              })(marker, i));
            }
        }

    }());
</script>

<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
            
	
</body>
</html>