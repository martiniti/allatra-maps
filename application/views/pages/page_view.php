    <div class="container single">
        <div class="col-md-12">

            <article>

                <?php if(isset($content['img_url']) && $content['img_url']!=''):?>

                <div class="row align-items-center padding-top-2x padding-bottom-2x">
                    <div class="col-md-5">
                        <img class="d-block" src="<?php echo base_url('assets/uploads/images/content/'.$content['img_url']);?>" alt="<?php echo $content['title'];?>" />
                    </div>
                    <div class="col-md-7 text-md-left text-center">
                        <?php echo $content['text'];?>
                    </div>
                </div>

                <?php else:?>

                <?php echo $content['text'];?>

                <?php endif;?>

                <?php if($info['url'] == 'about'):?>
                <!-- Popular Brands-->
                <section class="fw-section padding-top-3x padding-bottom-3x">
                    <div class="container">
                        <h3 class="text-center mb-30">Источники данных:</h3>
                        <div class="owl-carousel pt-2" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: false, &quot;loop&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2}, &quot;470&quot;:{&quot;items&quot;:3},&quot;630&quot;:{&quot;items&quot;:4},&quot;991&quot;:{&quot;items&quot;:5},&quot;1200&quot;:{&quot;items&quot;:6}} }">
                            <a class="d-block opacity-60" href="http://ukravtodor.gov.ua">
                                <img class=" h-101 d-block m-auto" src="<?php echo base_url('assets/uploads/images/partners/logo_uad.svg');?>" alt="ДЕРЖАВНЕ АГЕНТСТВО АВТОМОБІЛЬНИХ ДОРІГ УКРАЇНИ">
                            </a>
                            <a class="d-block opacity-60" href="http://www.casre.kiev.ua">
                                <img class="h-101 d-block m-auto" src="<?php echo base_url('assets/uploads/images/partners/cakis_logo_rus.png');?>" alt='Государственное учреждение "Научный Центр Аэрокосмических Исследований Земли Института Геологических Наук Национальной Академии Наук Украины"'>
                            </a>
                        </div>
                    </div>
                </section>
                <?php endif;?>


                <!-- uSocial -->
                <script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
                <div class="uSocial-Share" data-pid="f419db3cbef7b2d34eff79caab0802e0" data-type="share" data-options="round-rect,style3,default,absolute,horizontal,size24,eachCounter0,counter1,counter-after" data-social="fb,vk,twi,gPlus,ok,lj,lin,pinterest,telegram,bookmarks,email,print,spoiler" data-mobile="vi,wa,sms"></div>
                <!-- /uSocial -->

                <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>">
                <!--<script src="<?php /*echo base_url('assets/share/share-buttons.js');*/?>"></script>-->
                <script src="//maps.ramir.space/assets/templates/allatra-da/js/allatra.share.js" charset="utf-8"></script>
                <div id="ALLATRA_socials" data-options="" data-socials="vk,fb,tw"></div>

                <div class="clearfix"></div>
                <div class="mb-30"></div>

            </article>

            <?php if(!empty($info['enable_comments'])):?>

            <script>
                var material_id = "<?php echo $info['page_id'];?>";
                var material_url = "<?php echo base_url($info['url']);?>";
            </script>


            <?php if(count($info['comments'])>0):?>

            <div class="comments">

                <h3 class="title mb25"><span><?php echo $this->lang->line('comments');?> (<?php echo count($info['comments']);?>)</span></h3>

                <ul class="comments-list media-list">

                    <?php if($info['comments']):?>

                        <?php
                        function comment_loop($comments)
                        {
                            if(LANGUAGE == 'ru'):

                                $comment_answer = 'Ответить';

                            elseif(LANGUAGE == 'en'):

                                $comment_answer = 'Reply';

                            else:

                                $comment_answer = 'Відповісти';

                            endif;

                            foreach($comments as $comment)
                            {?>
                            <li id="comment-<?php echo $comment->comment_id;?>" class="media">
                                <div class="comment">
                                    <div class="media-left" href="#">
                                        <img width="100" class="media-object img-circle" src="<?php echo !empty($comment->image) ? base_url().'assets/uploads/images/avatars/thumbs/'.$comment->image : base_url().'assets/img/commentator.png';?>" alt="">
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <?php echo $comment->author;?> <?php if($comment->address != ''):?>(<?php echo $comment->address;?>)<?php endif;?>
                                            <?php if($comment->parent_author!=''): echo '<small>&#9998; '.$comment->parent_author.'</small>'; endif;?>
                                            <span class="comment-date">
                                                <?php echo date('d.m.Y H:i', strtotime($comment->date));?>
                                            </span>
                                        </h4>

                                        <p>
                                            <?php echo $comment->comment_text; ?>
                                        </p>

                                        <a id="<?php echo $comment->comment_id;?>" title="<?php echo $comment->author;?>" class="pull-right reply-link" href="#">
                                            <?php echo $comment_answer;?> &crarr;
                                        </a>
                                    </div>
                                </div>

                                <?php if(isset($comment->children) && count($comment->children) > 0):?>
                                <ul>
                                    <?php comment_loop($comment->children);?>
                                </ul>

                                </div>
                                </li>
                            <?php else:?>

                                <?php echo '</li>';?>

                            <?php endif;?>

                                <?php

                            }
                        }

                        comment_loop($info['comments']);

                        ?>

                    <?php endif;?>
                </ul>
                <div class="clearfix"></div>

            </div>

            <?php endif;?>
            <!-- End. Comments -->

            <!-- Comments Form -->
            <div id="respond" class="comment-respond">

                <h3 class="title mb25"><span><?php echo $this->lang->line('comment_leave');?></span></h3>

                <form id="comment_form" action = "<?php echo base_url().$lang."comments/add_comment/";?>" method="post">

                    <div class="notice alert alert-danger hidden"></div>

                    <div class="id">
                        <input name="id" type="hidden" value="<?php echo $info['page_id'];?>" />
                    </div>

                    <div class="parent_id">
                        <input name="parent_id" type="hidden" value="0" />
                    </div>

                    <div class="comment_type">
                        <input name="type" type="hidden" value="2" />
                    </div>

                    <div class="comment_language">
                        <input name="language" type="hidden" value="<?php echo LANGUAGE;?>" />
                    </div>

                    <div class="row margin-bottom-20">
                        <?php if($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0):?>
                            <br />
                            <div class="col-md-12">
                                <label><strong><?php echo $this->lang->line('commentator_name');?></strong>: <?php echo $this->session->userdata('DX_username') ;?></label>
                                <input name="author" type="hidden" value="<?php echo $this->session->userdata('DX_username') ;?>" />
                            </div>
                            <div class="clearfix"></div>
                            <br />
                        <?php else : ?>

                            <div class="col-md-6 col-md-offset-0">
                                <label><?php echo $this->lang->line('commentator_name');?> <span class="color-red">*</span></label>
                                <input name="author" type="text" class="form-control" required>
                            </div>

                        <?php endif ; ?>

                        <?php if($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0):?>
                            <div class="col-md-12">
                                <label><strong><?php echo $this->lang->line('commentator_email');?></strong>: <?php echo $this->session->userdata('DX_email') ;?></label>
                                <input name="email" type="hidden" value="<?php echo $this->session->userdata('DX_email') ;?>" />
                            </div>
                            <div class="clearfix"></div>
                            <br />
                        <?php else : ?>
                            <div class="col-md-6 col-md-offset-0">
                                <label><?php echo $this->lang->line('commentator_email');?> <span class="color-red">*</span></label>
                                <input name="email" type="email" class="form-control" required>
                            </div>
                        <?php endif ; ?>
                    </div>

                    <input name="address" type="hidden" value="" class="form-control">

                    <label><?php echo $this->lang->line('commentator_message');?> <span class="color-red">*</span></label>
                    <div class="row margin-bottom-20">
                        <div class="col-md-12 col-md-offset-0">
                            <textarea name="comment_text" class="textarea form-control" rows="10" required></textarea>
                        </div>
                    </div>

                    <?php if(!$this->dx_auth->is_logged_in()):?>
                        <div class="row margin-bottom-20">
                            <div class="col-md-4">
                                <label><?php echo $this->lang->line('comment_capcha');?> <span class="color-red">*</span></label>
                                <input name="captcha" type="text" class="form-control captcha" size="8" required>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label><div class="clearfix"></div>
                                <span id="image_key_capcha"><?php echo $imgcode;?></span>
                            </div>
                        </div>
                    <?php endif;?>

                    <p><button name="post_comment" id="add_comment" class="btn btn-dark" type="submit"><?php echo $this->lang->line('comment_add');?></button></p>

                    <div class="loading"></div>
                </form>
            </div>
            <!-- End. Comments Form -->

            <?php endif;?>

            <div class="clearfix"></div>

        </div>

    </div><!-- End .container -->
</div><!-- End .main -->