    <div class="container single">
        <article class="entry">

            <h1><?php echo $content['title'];?></h1>

            <?php echo $content['text'];?>
            <div class="clearfix"></div>

            <?php if(!empty($faqs)):?>
            <div class="panel-group" role="tablist" aria-multiselectable="true">
                <?php $faq_counter = 1; foreach ($faqs as $faq):?>

                    <div class="panel panel-custom">
                        <div class="panel-heading" role="tab" id="heading-<?php echo $faq_counter;?>">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse-<?php echo $faq_counter;?>" <?php if($faq_counter == 1):?>aria-expanded="true"<?php endif;?> aria-controls="collapse-<?php echo $faq_counter;?>">
                                    <?php echo $faq_counter.' '.$faq['title'];?>
                                </a>
                            </h4>
                        </div><!-- End .panel-heading -->
                        <div id="collapse-<?php echo $faq_counter;?>" class="panel-collapse collapse <?php if($faq_counter == 1):?>in<?php endif;?>" role="tabpanel" aria-labelledby="heading-<?php echo $faq_counter;?>">
                            <div class="panel-body">
                                <?php echo $faq['text'];?>
                            </div><!-- End .panel-body -->
                        </div><!-- End .panel-collapse -->
                    </div><!-- End .panel -->

                <?php $faq_counter++; endforeach;?>
            </div>
            <?php endif;?>

            <?php //dump($faqs);?>

        </article>

            <?php if(!empty($info['enable_comments'])):?>

            <script>
                var material_id = "<?php echo $info['page_id'];?>";
                var material_url = "<?php echo base_url($info['url']);?>";
            </script>


            <?php if(count($info['comments'])>0):?>

            <div class="comments">

                <h3 class="title mb25"><span><?php echo $this->lang->line('comments');?> (<?php echo count($info['comments']);?>)</span></h3>

                <ul class="comments-list media-list">

                    <?php if($info['comments']):?>

                        <?php
                        function comment_loop($comments)
                        {
                            if(LANGUAGE == 'ru'):

                                $comment_answer = 'Ответить';

                            elseif(LANGUAGE == 'en'):

                                $comment_answer = 'Reply';

                            else:

                                $comment_answer = 'Відповісти';

                            endif;

                            foreach($comments as $comment)
                            {?>
                            <li id="comment-<?php echo $comment->comment_id;?>" class="media">
                                <div class="comment">
                                    <div class="media-left" href="#">
                                        <img width="100" class="media-object img-circle" src="<?php echo !empty($comment->image) ? base_url().'assets/uploads/images/avatars/thumbs/'.$comment->image : base_url().'assets/img/commentator.png';?>" alt="">
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <?php echo $comment->author;?> <?php if($comment->address != ''):?>(<?php echo $comment->address;?>)<?php endif;?>
                                            <?php if($comment->parent_author!=''): echo '<small>&#9998; '.$comment->parent_author.'</small>'; endif;?>
                                            <span class="comment-date">
                                                <?php echo date('d.m.Y H:i', strtotime($comment->date));?>
                                            </span>
                                        </h4>

                                        <p>
                                            <?php echo $comment->comment_text; ?>
                                        </p>

                                        <a id="<?php echo $comment->comment_id;?>" title="<?php echo $comment->author;?>" class="pull-right reply-link" href="#">
                                            <?php echo $comment_answer;?> &crarr;
                                        </a>
                                    </div>
                                </div>

                                <?php if(isset($comment->children) && count($comment->children) > 0):?>
                                <ul>
                                    <?php comment_loop($comment->children);?>
                                </ul>

                                </div>
                                </li>
                            <?php else:?>

                                <?php echo '</li>';?>

                            <?php endif;?>

                                <?php

                            }
                        }

                        comment_loop($info['comments']);

                        ?>

                    <?php endif;?>
                </ul>
                <div class="clearfix"></div>

            </div>

            <?php endif;?>
            <!-- End. Comments -->

            <!-- Comments Form -->
            <div id="respond" class="comment-respond">

                <h3 class="title mb25"><span><?php echo $this->lang->line('comment_leave');?></span></h3>

                <form id="comment_form" action = "<?php echo base_url().$lang."comments/add_comment/";?>" method="post">

                    <div class="notice alert alert-danger hidden"></div>

                    <div class="id">
                        <input name="id" type="hidden" value="<?php echo $info['page_id'];?>" />
                    </div>

                    <div class="parent_id">
                        <input name="parent_id" type="hidden" value="0" />
                    </div>

                    <div class="comment_type">
                        <input name="type" type="hidden" value="2" />
                    </div>

                    <div class="comment_language">
                        <input name="language" type="hidden" value="<?php echo LANGUAGE;?>" />
                    </div>

                    <div class="row margin-bottom-20">
                        <?php if($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0):?>
                            <br />
                            <div class="col-md-12">
                                <label><strong><?php echo $this->lang->line('commentator_name');?></strong>: <?php echo $this->session->userdata('DX_username') ;?></label>
                                <input name="author" type="hidden" value="<?php echo $this->session->userdata('DX_username') ;?>" />
                            </div>
                            <div class="clearfix"></div>
                            <br />
                        <?php else : ?>

                            <div class="col-md-6 col-md-offset-0">
                                <label><?php echo $this->lang->line('commentator_name');?> <span class="color-red">*</span></label>
                                <input name="author" type="text" class="form-control" required>
                            </div>

                        <?php endif ; ?>

                        <?php if($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0):?>
                            <div class="col-md-12">
                                <label><strong><?php echo $this->lang->line('commentator_email');?></strong>: <?php echo $this->session->userdata('DX_email') ;?></label>
                                <input name="email" type="hidden" value="<?php echo $this->session->userdata('DX_email') ;?>" />
                            </div>
                            <div class="clearfix"></div>
                            <br />
                        <?php else : ?>
                            <div class="col-md-6 col-md-offset-0">
                                <label><?php echo $this->lang->line('commentator_email');?> <span class="color-red">*</span></label>
                                <input name="email" type="email" class="form-control" required>
                            </div>
                        <?php endif ; ?>
                    </div>

                    <input name="address" type="hidden" value="" class="form-control">

                    <label><?php echo $this->lang->line('commentator_message');?> <span class="color-red">*</span></label>
                    <div class="row margin-bottom-20">
                        <div class="col-md-12 col-md-offset-0">
                            <textarea name="comment_text" class="textarea form-control" rows="10" required></textarea>
                        </div>
                    </div>

                    <?php if(!$this->dx_auth->is_logged_in()):?>
                        <div class="row margin-bottom-20">
                            <div class="col-md-4">
                                <label><?php echo $this->lang->line('comment_capcha');?> <span class="color-red">*</span></label>
                                <input name="captcha" type="text" class="form-control captcha" size="8" required>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label><div class="clearfix"></div>
                                <span id="image_key_capcha"><?php echo $imgcode;?></span>
                            </div>
                        </div>
                    <?php endif;?>

                    <p><button name="post_comment" id="add_comment" class="btn btn-dark" type="submit"><?php echo $this->lang->line('comment_add');?></button></p>

                    <div class="loading"></div>
                </form>
            </div>
            <!-- End. Comments Form -->

            <?php endif;?>

            <div class="clearfix"></div>


    </div><!-- End .container -->
</div><!-- End .main -->