<html>
<head>
    <title>ALLATRA.maps :: Карта наводений онлайн</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php

    $_css = new CSSCrunch();
    // Leaflet
    $_css->addFile('../../../ol/ol');
    // Font Awesome
    $_css->addFile('../../../font-awesome/css/font-awesome');

    (ENVIRONMENT != 'production') ? $_css->crunch(true) : $_css->crunch();

    ?>

    <style>
        body{
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }

        /* Go FloodMap styles */

        #AmmaFloodWrapper{
            float: left;
            width: 100%;
            height: 100%;
        }

    </style>

</head>
<body>

    <div class="panel-body">
        <label>
            Sea level
            <input id="level" type="range" min="-100" max="1000" value="1"/>
            +<span id="output"></span> m
        </label>
        <br>
        Go to
        <a class="location" data-center="-122.3267,37.8377" data-zoom="11">San Francisco</a>,
        <a class="location" data-center="-73.9338,40.6861" data-zoom="11">New York</a>,
        <a class="location" data-center="72.9481,18.9929" data-zoom="11">Mumbai</a>, or
        <a class="location" data-center="120.831,31.160" data-zoom="9">Shanghai</a>
    </div>
    <div id="AmmaFloodWrapper"></div>

    <?php

    $_js = new JSCrunch();

    // Leaflet
    $_js->addFile('../../../ol/ol');

    (ENVIRONMENT != 'production') ? $_js->crunch(true) : $_js->crunch();

    ?>

    <script>

        function flood(pixels, data) {
            var pixel = pixels[0];
            if (pixel[3]) {
                var height =
                    -10000 + (pixel[0] * 256 * 256 + pixel[1] * 256 + pixel[2]) * 0.1;
                if (height <= data.level) {
                    // for https://server.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}
                    // pixel[0] = 65;
                    // pixel[1] = 215;
                    // pixel[2] = 218;
                    // pixel[3] = 255;
                    //for https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png
                    pixel[0] = 115;
                    pixel[1] = 182;
                    pixel[2] = 230;
                    pixel[3] = 255;
                } else {
                    pixel[3] = 0;
                }
            }
            return pixel;
        }

        var key = '0U3Kx9vx7zUOzfp0YtGq';
        var attributions =
            '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> ' +
            '<a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>';

        var elevation = new ol.layer.Tile({
            source: new ol.source.XYZ({
                url: 'https://api.maptiler.com/tiles/terrain-rgb/{z}/{x}/{y}.png?key=' + key,
                //url: 'https://api.maptiler.com/tiles/hillshades/{z}/{x}/{y}.png?key=' + key,
                //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}',
                //url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}',
                //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}',
                maxZoom: 10,
                crossOrigin: '',
            }),
        });

        // https://www.floodmap.net/
        // var fmTileUrl = '/getFMTile.ashx?x={x}&y={y}&z={z}&e=';
        // var floodLayer = L.tileLayer(fmTileUrl + elev, {
        //     attribution: '© <a href="https://openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="https://www.mapzen.com/rights">Mapzen</a>, <a href = "https://www.mapzen.com/rights/#services-and-data-sources">Others</a>',
        //     opacity: 0.5
        // })

        elevation.on('prerender', function (evt) {
            evt.context.imageSmoothingEnabled = false;
            evt.context.msImageSmoothingEnabled = false;
        });

        var raster = new ol.source.Raster({
            sources: [elevation],
            operation: flood,
        });

        var map = new ol.Map({
            target: 'AmmaFloodWrapper',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.XYZ({
                        attributions: attributions,
                        url: 'https://basemap.nationalmap.gov/arcgis/rest/services/USGSImageryOnly/MapServer/tile/{z}/{y}/{x}',
                        //url: 'https://api.maptiler.com/maps/streets/{z}/{x}/{y}.png?key=' + key,
                        //url: 'http://otile1.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpg',
                        //url: 'https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
                        //url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        tileSize: 128,
                    }),
                }),
                /*new ol.layer.Tile({
                    source: new ol.source.OSM({
                        attributions: ['All maps © <a href="http://www.openseamap.org/">OpenSeaMap</a>', '' ],
                        opaque: false,
                        url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}',
                        //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}',
                        //url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                        //url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}'
                    }),
                }),*/
                new ol.layer.Image({
                    opacity: 0.5,
                    source: raster,
                })],
            view: new ol.View({
                center: ol.proj.fromLonLat([15.3267, -12.8377]),
                zoom: 3,
            }),
        });

        var control = document.getElementById('level');
        var output = document.getElementById('output');
        control.addEventListener('input', function () {
            output.innerText = control.value;
            raster.changed();
        });
        output.innerText = control.value;

        raster.on('beforeoperations', function (event) {
            event.data.level = control.value - 30;
        });

        var locations = document.getElementsByClassName('location');
        for (var i = 0, ii = locations.length; i < ii; ++i) {
            locations[i].addEventListener('click', relocate);
        }

        function relocate(event) {
            var data = event.target.dataset;
            var view = map.getView();
            view.setCenter(ol.proj.fromLonLat(data.center.split(',').map(Number)));
            view.setZoom(Number(data.zoom));
        }

    </script>

</body>
</html>