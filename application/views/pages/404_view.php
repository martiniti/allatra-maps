    <div class="error-page text-center">
        <div class="container">
            <h1>404</h1>
            <h2><?php echo $content['title'];?></h2>
            <p><?php echo $content['text'];?></p>
            <a href="<?php echo base_url($lang);?>" class="btn btn-danger no-radius btn-md min-width"><?php echo $this->lang->line('404_go_home');?></a>
        </div><!-- End .container -->
    </div><!-- End .error-page -->
</div><!-- End .main -->