    <div class="mb10"></div><!-- space -->
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <?php echo $content['text'];?>
                
                <?php //dump($prices);?>
                
                <div class="row">
                    <?php foreach($prices as $price):?>
                    
                        <div class="price col-sm-3">
                            <a href="<?php echo base_url('assets/uploads/files/prices/'.$price['file_url']);?>">
                                <h5><?php echo $price['title'];?></h5>
                                <img class="img-responsive" src="<?php echo base_url('assets/template/images/excel.png');?>" />
                                <div class="clearfix"></div>
                                <span><?php echo $this->lang->line('download_price');?></span>
                            </a>
                        </div>
                    
                    <?php endforeach;?>
                </div>
                
            </div>
            <div class="mb60 clearfix visible-sm visible-xs"></div>
            <?php echo $this->load->view('left_view');?>
        </div>
        
    </div><!-- End .container -->
</div><!-- End #content -->