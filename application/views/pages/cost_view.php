    <div id="<?php echo $this->uri->segment($this->uri->total_segments());?>" class="container single">
        <div class="col-md-12">

                <div class="btn-group btn-group-justified mb50" role="group" aria-label="transport-types">
                    <div class="btn-group" role="group">
                        <a href="<?php echo base_url($lang.'transfer_calculator');?>" class="btn btn-default <?php if($this->uri->segment($this->uri->total_segments()) == 'transfer_calculator'):?>btn-custom<?php endif;?>">
                            <i class="icon-directions"></i> Трансфер
                        </a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="<?php echo base_url($lang.'timing_calculator');?>" class="btn btn-default <?php if($this->uri->segment($this->uri->total_segments()) == 'timing_calculator'):?>btn-custom<?php endif;?>">
                            <i class="icon-clock"></i> Почасово
                        </a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="<?php echo base_url($lang.'route_calculator');?>" class="btn btn-default <?php if($this->uri->segment($this->uri->total_segments()) == 'route_calculator'):?>btn-custom<?php endif;?>">
                            <i class="icon-location-pin"></i> Маршрут
                        </a>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="prices">
                    <?php foreach ($cost as $cost_item):?>
                        <div id="<?php echo 'price-'.$cost_item['service'].'-'.$cost_item['auto'];?>" attr-price="<?php echo $cost_item['price'];?>" attr-id="<?php echo $cost_item['id'];?>"></div>
                    <?php endforeach;?>
                </div>

                <div class="row mb50">
                    <div class="col-md-6">
                        <form class="form-horizontal calculator">

                            <input id="time" type="hidden" name="time" value="<?php echo date('d-m-Y H:i');?>">
                            <input id="feedback" type="hidden" name="feedback" value="1">
                            <input id="url" type="hidden" name="url" value="<?php echo current_url();?>">

                            <?php if($this->uri->segment($this->uri->total_segments()) == 'transfer_calculator'):?>
                            <div class="form-group">
                                <label for="inputRoute" class="col-sm-3 control-label">Откуда-Куда</label>
                                <div class="col-sm-9">
                                    <select name="route" id="inputRoute" class="form-control" onchange="myFunction(this)" required>
                                        <option selected hidden>Выберите маршрут</option>
                                        <?php foreach ($cost_services as $cost_service):?>
                                        <?php if($cost_service['id']!=9 && $cost_service['id']!=10):?>
                                        <option ltd_a="<?php echo $cost_service['ltd_a'];?>" lng_a="<?php echo $cost_service['lng_a'];?>" ltd_b="<?php echo $cost_service['ltd_b'];?>" lng_b="<?php echo $cost_service['lng_b'];?>"  value="<?php echo $cost_service['id'];?>"><?php echo $cost_service['name'];?></option>
                                        <?php endif;?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <?php endif;?>

                            <?php if($this->uri->segment($this->uri->total_segments()) == 'timing_calculator'):?>

                            <div class="form-group">
                                <label for="fromTiming" class="col-sm-3 control-label">Откуда</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control controls from-point autocompleteAddress" id="fromTiming" name="fromtiming" value="" placeholder="Место начала маршрута" autocomplete="off" required="" spellcheck="false" dir="auto" style="position: relative; vertical-align: top;">
                                </div>
                            </div>

                            <div class="form-group" id="duration_block" style="">
                                <label for="duration" class="col-sm-3 control-label">Длительность</label>
                                <div class="col-sm-9">
                                    <select id="duration" class="form-control" name="duration" required="required">
                                        <option disabled="" selected="" value="">Длительность</option>
                                        <option value="2">2 часа</option>
                                        <option value="3">3 часа</option>
                                        <option value="4">4 часа</option>
                                        <option value="5">5 часов</option>
                                        <option value="6">6 часов</option>
                                        <option value="8">8 часов</option>
                                        <option value="10">10 часов</option>
                                        <option value="24">1 день</option>
                                        <option value="48">2 дня</option>
                                        <option value="72">3 дня</option>
                                        <option value="96">4 дня</option>
                                        <option value="120">5 дней</option>
                                        <option value="240">10 дней</option>
                                        <option value="360">15 дней</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label class="checkbox-inline">
                                        <input class="hasFinish" type="checkbox">Завершить маршрут в другом месте
                                    </label>
                                </div>
                            </div>

                            <div id="hasFinish" class="form-group hidden">
                                <label for="toTiming" class="col-sm-3 control-label">Куда</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control controls to-point autocompleteAddress" id="toTiming" name="totiming" value="" placeholder="Место завершение маршрута" autocomplete="off" required="" spellcheck="false" dir="auto" style="position: relative; vertical-align: top;">
                                </div>
                            </div>
                            <?php endif;?>


                            <?php if($this->uri->segment($this->uri->total_segments()) == 'route_calculator'):?>
                                <div class="form-group">
                                    <label for="fromTiming" class="col-sm-3 control-label">Откуда</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control controls from-point autocompleteAddress" id="fromRoute" name="fromtiming" value="" placeholder="Место начала маршрута" autocomplete="off" required="" spellcheck="false" dir="auto" style="position: relative; vertical-align: top;">
                                    </div>
                                </div>

                                <div id="hasFinish" class="form-group">
                                    <label for="toTiming" class="col-sm-3 control-label">Куда</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control controls to-point autocompleteAddress" id="toRoute" name="totiming" value="" placeholder="Место завершение маршрута" autocomplete="off" required="" spellcheck="false" dir="auto" style="position: relative; vertical-align: top;">
                                    </div>
                                </div>

                                <input id="distance" type="hidden" name="distance" value="">
                            <?php endif;?>

                            <div class="form-group">
                                <label for="inputDatetime" class="col-sm-3 control-label">Дата и время</label>
                                <div class="col-sm-9">
                                    <input name="datetime" type="text" class="datetimepicker form-control" id="inputDatetime" placeholder="Введите нужное для Вас время">
                                </div>
                            </div>

                            <div class="form-group">
                                <!--<label for="inputEmail3" class="col-sm-3 control-label">Транспорт</label>-->
                                <div class="col-sm-12">

                                    <table class="table table-hover table-transport-type" style="cursor: pointer" tabindex="-1">
                                        <tbody>
                                        <?php foreach ($cost_autos as $auto):?>
                                        <tr>
                                            <td class="transport-type-input-sec">
                                                <label>
                                                    <input id="transport_type_<?php echo $auto['id'];?>" class="transport_type" name="transport_type[]" type="checkbox" value="<?php echo $auto['id'];?>">
                                                </label>
                                            </td>
                                            <td class="cartype-preview">
                                                <label for="transport_type_<?php echo $auto['id'];?>">
                                                    <img src="<?php echo base_url('assets/uploads/images/autos/'.$auto['img_url']);?>">
                                                </label>
                                            </td>
                                            <td>
                                                <label for="transport_type_<?php echo $auto['id'];?>">
                                                    <p><?php echo $auto['type'];?><br/><small style="font-weight: 300"><?php echo $auto['name'];?></small></p>
                                                </label>
                                            </td>
                                            <td class="price-cell">
                                                <label class="transport-price" for="transport_type_<?php echo $auto['id'];?>" attr-transport-id="<?php echo $auto['id'];?>" attr-price-id="">
                                                    <div class="min-price" style="display: none;">от <span></span></div>
                                                </label>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                            <div id="summary" style="display: none;">

                                <hr style="border-color: #eaeaea;"/>

                                <div class="form-group">

                                    <label for="inputPrice" class="col-sm-3 control-label">Итоговая сума:</label>
                                    <div class="col-sm-9">
                                        <div class="summary first-color">* <span class="summaryVal"></span></div>
                                    </div>

                                </div>

                                <hr style="border-color: #eaeaea;"/>

                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label class="checkbox-inline">
                                        <input class="hasNumber" type="checkbox">Номер рейса или поезда
                                    </label>
                                </div>
                            </div>

                            <div id="hasNumber" class="form-group hidden">
                                <label for="number" class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control controls" id="number" name="number" value="" placeholder="Введите номер рейса или поезда">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label class="checkbox-inline">
                                        <input class="hasNameplate" type="checkbox">Запросить встречу с табличкой
                                    </label>
                                </div>
                            </div>

                            <div id="hasNameplate" class="form-group hidden">
                                <label for="nameplate" class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control controls" id="nameplate" name="nameplate" value="" placeholder="Как подписать табличку?">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label class="checkbox-inline">
                                        <input class="hasChildrens" type="checkbox">Детские кресла
                                    </label>
                                </div>
                            </div>

                            <div id="hasChildrens" class="form-group hidden">
                                <label for="childrens" class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control controls" id="childrens" name="childrens" value="" placeholder="Сколько будет детей? Какой их возраст?">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Комментарий</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="comment" name="comment" placeholder="Укажите Ваши пожелания, особенные требования или задание для водителя" rows="4"></textarea>
                                </div>
                            </div>

                            <hr style="border-color: #eaeaea;"/>

                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input name="email" type="email" class="form-control" id="inputEmail" placeholder="Напр. info@driveforce.ua">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPhone" class="col-sm-3 control-label">Телефон</label>
                                <div class="col-sm-9">
                                    <input name="phone" type="text" class="form-control" id="inputPhone" placeholder="Как с Вами связаться">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="checkbox-inline">
                                    <label class="col-xs-12 control-label">
                                        <input id="terms_accepted" name="terms_accepted" type="checkbox" required="">
                                        Я прочитал и принял <a href="<?php echo base_url($lang.'terms_of_use');?>" target="_blank">условия использования</a>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <button type="submit" class="btn btn-custom" disabled>Оформить заказ</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-md-6">

                        <div id="calcMap"></div>

                        <div id="directions-panel"></div>

                    </div>
                </div>

                <div class="clearfix"></div>

                <?php echo $content['text'];?>

                <div class="text-center">
                    <!-- uSocial -->
                    <script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
                    <div class="uSocial-Share" data-pid="f419db3cbef7b2d34eff79caab0802e0" data-type="share" data-options="round-rect,style3,default,absolute,horizontal,size24,eachCounter0,counter1,counter-after" data-social="fb,vk,twi,gPlus,ok,lj,lin,pinterest,telegram,bookmarks,email,print,spoiler" data-mobile="vi,wa,sms"></div>
                    <!-- /uSocial -->
                </div>

                <div class="mb50"></div>

            <div class="clearfix"></div>

        </div>

    </div><!-- End .container -->
</div><!-- End .main -->

<?php if($this->uri->segment($this->uri->total_segments()) == 'transfer_calculator'):?>

<script>

/* Transfer */

function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('calcMap'), {
        zoom: 10,
        center: {lat: 50.5001598, lng: 30.4458535}
    });
    directionsDisplay.setMap(map);

    document.getElementById('inputRoute').addEventListener('change', function() {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {

    var waypoints = document.getElementById('inputRoute');

    text  = waypoints.options[waypoints.selectedIndex].text;
    ltd_a = waypoints.options[waypoints.selectedIndex].getAttribute('ltd_a');
    lng_a = waypoints.options[waypoints.selectedIndex].getAttribute('lng_a');
    ltd_b = waypoints.options[waypoints.selectedIndex].getAttribute('ltd_b');
    lng_b = waypoints.options[waypoints.selectedIndex].getAttribute('lng_b');

    var checkpointA = new google.maps.LatLng(ltd_a, lng_a);
    var checkpointB = new google.maps.LatLng(ltd_b, lng_b);

    var waypts = [];

    waypts.push(
        {
            location: checkpointA,
            stopover: false
        },
        {
            location: checkpointB,
            stopover: false
        },
    );

    directionsService.route({
        origin: checkpointA,
        destination: checkpointB,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
    }, function(response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<br/><div class="alert alert-success" role="alert"> Ваш маршрут: <br/>' + route.legs[i].start_address + ' — ' + route.legs[i].end_address + ' <br/>Расстояние: ' + route.legs[i].distance.text + '</div>';
            }
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}



</script>

<?php endif;?>

<?php if($this->uri->segment($this->uri->total_segments()) == 'timing_calculator'):?>

<script>

/* Timing */

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initMap() {
    var map = new google.maps.Map(document.getElementById('calcMap'), {
        mapTypeControl: false,
        center: {lat: 50.5001598, lng: 30.4458535},
        zoom: 10
    });

    new AutocompleteDirectionsHandler(map);
}

/**
 * @constructor
 */
function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'DRIVING';
    var originInput = document.getElementById('fromTiming');
    var destinationInput = document.getElementById('toTiming');
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);
    this.geocoder = new google.maps.Geocoder;

    var originAutocomplete = new google.maps.places.Autocomplete(
        originInput, {placeIdOnly: true});
    var destinationAutocomplete = new google.maps.places.Autocomplete(
        destinationInput, {placeIdOnly: true});

    this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
    this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

}

AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
    var me = this;
    autocomplete.bindTo('bounds', this.map);
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.place_id) {
            window.alert("Please select an option from the dropdown list.");
            return;
        }

        /* SET first step marker */
        me.geocoder.geocode({'placeId': place.place_id}, function(results, status) {

            if (status !== 'OK') {
                window.alert('Geocoder failed due to: ' + status);
                return;
            }

            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();

            var myLatlng = new google.maps.LatLng(latitude,longitude);

            me.map.setZoom(17);
            me.map.setCenter(myLatlng);

            marker = new google.maps.Marker({
                map: me.map,
                //draggable: true,
                //animation: google.maps.Animation.DROP,
                position: {lat: latitude, lng: longitude}
            });

        });

        if (mode === 'ORIG') {
            me.originPlaceId = place.place_id;
        } else {
            me.destinationPlaceId = place.place_id;
        }
        me.route();
    });

};

AutocompleteDirectionsHandler.prototype.route = function() {

    if (!this.originPlaceId || !this.destinationPlaceId) {

        return;
    }
    var me = this;

    this.directionsService.route({
        origin: {'placeId': this.originPlaceId},
        destination: {'placeId': this.destinationPlaceId},
        travelMode: 'DRIVING'
    }, function(response, status) {
        if (status === 'OK') {
            me.directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<br/><div class="alert alert-success" role="alert"> Ваш маршрут: <br/>' + route.legs[i].start_address + ' — ' + route.legs[i].end_address + ' <br/>Расстояние: ' + route.legs[i].distance.text + '</div>';
            }
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });





};

</script>

<?php endif;?>


<?php if($this->uri->segment($this->uri->total_segments()) == 'route_calculator'):?>

    <script>

        /* Timing */

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initMap() {
            var map = new google.maps.Map(document.getElementById('calcMap'), {
                mapTypeControl: false,
                center: {lat: 50.5001598, lng: 30.4458535},
                zoom: 10
            });

            new AutocompleteDirectionsHandler(map);
        }

        /**
         * @constructor
         */
        function AutocompleteDirectionsHandler(map) {
            this.map = map;
            this.originPlaceId = null;
            this.destinationPlaceId = null;
            this.travelMode = 'DRIVING';
            var originInput = document.getElementById('fromRoute');
            var destinationInput = document.getElementById('toRoute');
            this.directionsService = new google.maps.DirectionsService;
            this.directionsDisplay = new google.maps.DirectionsRenderer;
            this.directionsDisplay.setMap(map);
            this.geocoder = new google.maps.Geocoder;

            var originAutocomplete = new google.maps.places.Autocomplete(
                originInput, {placeIdOnly: true});
            var destinationAutocomplete = new google.maps.places.Autocomplete(
                destinationInput, {placeIdOnly: true});

            this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
            this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

        }

        AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
            var me = this;
            autocomplete.bindTo('bounds', this.map);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.place_id) {
                    window.alert("Please select an option from the dropdown list.");
                    return;
                }

                /* SET first step marker */
                me.geocoder.geocode({'placeId': place.place_id}, function(results, status) {

                    if (status !== 'OK') {
                        window.alert('Geocoder failed due to: ' + status);
                        return;
                    }

                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();

                    var myLatlng = new google.maps.LatLng(latitude,longitude);

                    me.map.setZoom(17);
                    me.map.setCenter(myLatlng);

                    marker = new google.maps.Marker({
                        map: me.map,
                        //draggable: true,
                        //animation: google.maps.Animation.DROP,
                        position: {lat: latitude, lng: longitude}
                    });

                });

                if (mode === 'ORIG') {
                    me.originPlaceId = place.place_id;
                } else {
                    me.destinationPlaceId = place.place_id;
                }
                me.route();
            });

        };

        AutocompleteDirectionsHandler.prototype.route = function() {

            if (!this.originPlaceId || !this.destinationPlaceId) {

                return;
            }
            var me = this;

            this.directionsService.route({
                origin: {'placeId': this.originPlaceId},
                destination: {'placeId': this.destinationPlaceId},
                travelMode: 'DRIVING'
            }, function(response, status) {
                if (status === 'OK') {
                    me.directionsDisplay.setDirections(response);
                    var route = response.routes[0];
                    var summaryPanel = document.getElementById('directions-panel');
                    summaryPanel.innerHTML = '';
                    // For each route, display summary information.
                    for (var i = 0; i < route.legs.length; i++) {
                        var routeSegment = i + 1;
                        summaryPanel.innerHTML += '<br/><div class="alert alert-success" role="alert"> Ваш маршрут: <br/>' + route.legs[i].start_address + ' — ' + route.legs[i].end_address + ' <br/>Расстояние: ' + route.legs[i].distance.text + '</div>';
                    }

                    document.getElementById("distance").value = route.legs[0].distance.text;

                    routeCalculation();

                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });





        };

    </script>

<?php endif;?>
