<html>
<head>
    <title>ALLATRA.maps :: Карта землетрясений онлайн</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php

    $_css = new CSSCrunch();
    // Leaflet
    $_css->addFile('../../../leaflet/leaflet');
    $_css->addFile('../../../leaflet/plugins/markercluster/MarkerCluster');
    $_css->addFile('../../../leaflet/plugins/markercluster/MarkerCluster.Default');
    $_css->addFile('../../../leaflet/plugins/fullscreen/leaflet.fullscreen');
    // Date & Time
    $_css->addFile('../../../lightpick/lightpick');
    // Datatables
    $_css->addFile('../../../vanilla_tables/vanilla-dataTables');
    // Font Awesome
    $_css->addFile('../../../font-awesome/css/font-awesome');
    // Custom Styles
    $_css->addFile('amma-earthquakes');

    (ENVIRONMENT != 'production') ? $_css->crunch(true) : $_css->crunch();

    ?>

</head>
<body style="width: 100%; height: 100%; padding: 0; margin: 0;">

    <div id="AmmaEqksWrapper"></div>

    <?php

    $_js = new JSCrunch();

    // Leaflet
    $_js->addFile('../../../leaflet/leaflet');
    $_js->addFile('../../../leaflet/plugins/markercluster/leaflet.markercluster-src');
    $_js->addFile('../../../leaflet/plugins/fullscreen/Leaflet.fullscreen');
    $_js->addFile('../../../leaflet/plugins/kmz/leaflet-kmz-src');
    // Date & Time
    $_js->addFile('../../../moment/moment.min');
    $_js->addFile('../../../lightpick/lightpick');
    // Datatables
    $_js->addFile('../../../vanilla_tables/vanilla-dataTables');
    // Carousel
    $_js->addFile('../../../siema/siema.min');
    // Share
    $_js->addFile('../../../share/share-buttons');
    // Custom JS
    $_js->addFile('amma-earthquakes');

    (ENVIRONMENT != 'production') ? $_js->crunch(true) : $_js->crunch();

    ?>

</body>
</html>