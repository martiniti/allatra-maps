<div class="pb40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo $content['text'];?>

                <?php if(!empty($teams)): ?>

                    <div class="row">
                        <?php foreach($teams as $team):?>
                            <div class="col-md-2">
                                <img class="img-responsive img-circle" src="<?php echo base_url('assets/uploads/images/team/'.$team['photo_url']);?>" alt="<?php echo $team['name'];?>" title="<?php echo $team['name'];?>" />
                                <p class="text-center"><i><b><?php echo $team['name'];?></b></i><br><i><?php echo $team['position'];?></i></p>
                            </div>
                        <?php endforeach;?>
                    </div><!-- End .row -->

                <?php endif;?>

            </div><!-- End .col-md-12 -->

        </div><!-- End .row -->
    </div><!-- End .container -->
</div>


</div><!-- End .main -->