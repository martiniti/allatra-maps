    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <?php echo $content['anons']; //echo $this->lang->line('feedback_title');?>
                <form action = "<?php echo base_url('feedback');?>" method="post">
                    <div id="form-message"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="hidden" name="feedback" value="5" required/>
                                <input type="hidden" name="url" value="<?php echo base_url(uri_string());?>" required/>
                                <label for="contactname" class="input-desc"><?php echo $this->lang->line('feedback_name');?> <strong style="color: red;">*</strong></label>
                                <input type="text" class="form-control" id="contactname" name="name" placeholder="<?php echo $this->lang->line('feedback_name_ph');?>" value="<?php echo set_value('name');?>" required>
                                <strong class="error"><?php echo form_error('name');?></strong>
                            </div><!-- End .from-group -->
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contactemail" class="input-desc"><?php echo $this->lang->line('feedback_email');?> <strong style="color: red;">*</strong></label>
                                <input type="email" class="form-control" id="contactemail" name="email" placeholder="<?php echo $this->lang->line('feedback_email_ph');?>" value="<?php echo set_value('email');?>" required>
                                <strong class="error"><?php echo form_error('email');?></strong>
                            </div><!-- End .from-group -->
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="contactsubject" class="input-desc"><?php echo $this->lang->line('feedback_topic');?> <strong style="color: red;">*</strong></label>
                        <input type="text" class="form-control" id="contactsubject" name="theme" value="<?php echo set_value('topic');?>" placeholder="<?php echo $this->lang->line('feedback_topic_ph');?>" required>
                        <strong class="error"><?php echo form_error('topic');?></strong>
                    </div><!-- End .from-group -->

                    <div class="form-group">
                        <label for="contactmessage" class="input-desc"><?php echo $this->lang->line('feedback_message');?> <strong style="color: red;">*</strong></label>
                        <textarea class="form-control" rows="6" id="contactmessage" name="message" placeholder="<?php echo $this->lang->line('feedback_message_ph');?>" required><?php echo set_value('message');?></textarea>
                        <strong class="error"><?php echo form_error('message');?></strong>
                    </div><!-- End .from-group -->

                    <?php echo $imgcode;?>
                    <div class="clearfix"></div>
                    <br/>

                    <div class="form-group">
                        <label for="contactCaptcha" class="input-desc"><?php echo $this->lang->line('feedback_captcha');?> <strong style="color: red;">*</strong></label>
                        <input type="text" class="form-control" id="contactCaptcha" name="captcha" value="" placeholder="" required>
                        <strong class="error"><?php echo form_error('captcha');?></strong>
                    </div><!-- End .from-group -->

                    <div class="mb10"></div><!-- space -->

                    <div class="form-group">
                        <input type="submit" name="send_message" class="btn btn-primary btn-md" data-loading-text="Sending..." onClick="ga('send', 'event', 'words', 'click');" value="<?php echo $this->lang->line('feedback_send');?>">
                    </div><!-- End .from-group -->
                </form>
            </div><!-- End .col-md-8 -->

            <div class="clearfix mb65 visible-sm visible-xs"></div><!-- margin -->

            <div class="col-md-4">
                <div class="contact-box">
                    <?php echo $content['text'];?>
                    <br/>
                    <h4 class="mb20"><?php echo $this->lang->line('follow_us');?></h4>
                    <div class="social-icons social-icons-bg">
                        <?php if(FACEBOOK != ''):?>
                            <a target="_blank" href="<?php echo FACEBOOK;?>" class="social-button shape-circle" title="Facebook"><i class="socicon-facebook"></i></a>
                        <?php endif;?>
                        <?php if(VK != ''):?>
                            <a target="_blank" href="<?php echo VK;?>" class="social-button shape-circle" title="Twitter"><i class="socicon-vkontakte"></i></a>
                        <?php endif;?>
                        <?php if(GPLUS != ''):?>
                            <a target="_blank" href="<?php echo GPLUS;?>" class="social-button shape-circle" title="Github"><i class="socicon-googleplus"></i></a>
                        <?php endif;?>
                        <?php if(LINKEDIN != ''):?>
                            <a target="_blank" href="<?php echo LINKEDIN;?>" class="social-button shape-circle" title="Linkedin"><i class="socicon-linkedin"></i></a>
                        <?php endif;?>
                        <?php if(INSTAGRAM != ''):?>
                            <a target="_blank" href="<?php echo INSTAGRAM;?>" class="social-button shape-circle" title="Instagram"><i class="socicon-instagram"></i></a>
                        <?php endif;?>
                        <?php if(YOUTUBE != ''):?>
                            <a target="_blank" href="<?php echo YOUTUBE;?>" class="social-button shape-circle" title="Youtube"><i class="socicon-youtube"></i></a>
                        <?php endif;?>
                        <?php if(SKYPE != ''):?>
                            <a target="_blank" href=skype:"<?php echo SKYPE;?>" class="social-button shape-circle" title="Skype"><i class="socicon-skype"></i></a>
                        <?php endif;?>
                        <?php if(TELEGRAM != ''):?>
                            <a target="_blank" href="<?php echo TELEGRAM;?>" class="social-button shape-circle" title="Telegram"><i class="socicon-telegram"></i></a>
                        <?php endif;?>
                    </div><!-- End .social-icons -->
                </div><!-- End .contact-box -->

            </div><!-- End .col-md-4 -->
        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="mb-5"></div><!-- margin -->

</div><!-- End .main -->