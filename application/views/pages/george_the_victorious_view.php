<html>
<head>
    <title>ALLATRA.maps :: Георгий Победоносец</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php

        $_css = new CSSCrunch();

        $_css->addFile('../../../leaflet/leaflet');
        $_css->addFile('../../../leaflet/plugins/draw/leaflet.draw');
        $_css->addFile('../../../leaflet/plugins/geosearch/l.geosearch');
        $_css->addFile('../../../leaflet/plugins/fullscreen/leaflet.fullscreen');
        $_css->addFile('../../../vanilla_tables/vanilla-dataTables');
        $_css->addFile('george_the_victorious');

        (ENVIRONMENT != 'production') ? $_css->crunch(true) : $_css->crunch();

    ?>

</head>
<body style="width: 100%; height: 100%; padding: 0; margin: 0;">

    <div id="safeRoadMap" style="width: 100%; height: 100%; padding: 0; margin: 0;"></div>

    <?php

    $_js = new JSCrunch();

    // Leaflet
    $_js->addFile('../../../leaflet/leaflet');
    // Leaflet.Draw
    $_js->addFile('../../../leaflet/plugins/draw/Leaflet.draw');
    $_js->addFile('../../../leaflet/plugins/draw/Leaflet.Draw.Event');
    $_js->addFile('../../../leaflet/plugins/draw/Toolbar');
    $_js->addFile('../../../leaflet/plugins/draw/Tooltip');
    $_js->addFile('../../../leaflet/plugins/draw/ext/GeometryUtil');
    $_js->addFile('../../../leaflet/plugins/draw/ext/LatLngUtil');
    $_js->addFile('../../../leaflet/plugins/draw/ext/LineUtil.Intersect');
    $_js->addFile('../../../leaflet/plugins/draw/ext/Polygon.Intersect');
    $_js->addFile('../../../leaflet/plugins/draw/ext/Polyline.Intersect');
    $_js->addFile('../../../leaflet/plugins/draw/ext/TouchEvents');
    $_js->addFile('../../../leaflet/plugins/draw/draw/DrawToolbar');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Feature');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.SimpleShape');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Polyline');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Marker');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Circle');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.CircleMarker');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Polygon');
    $_js->addFile('../../../leaflet/plugins/draw/draw/handler/Draw.Rectangle');
    $_js->addFile('../../../leaflet/plugins/draw/edit/EditToolbar');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/EditToolbar.Edit');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/EditToolbar.Delete');
    $_js->addFile('../../../leaflet/plugins/draw/Control.Draw');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Poly');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.SimpleShape');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Rectangle');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Marker');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.CircleMarker');
    $_js->addFile('../../../leaflet/plugins/draw/edit/handler/Edit.Circle');
    // Leaflet.Mask
    $_js->addFile('../../../leaflet/plugins/rotate/Leaflet.ImageOverlay.Rotated');
    // Leaflet.Search
    $_js->addFile('../../../leaflet/plugins/geosearch/l.control.geosearch');
    $_js->addFile('../../../leaflet/plugins/geosearch/l.geosearch.provider.openstreetmap');
    // Leaflet.Print
    $_js->addFile('../../../leaflet/plugins/easyprint/bundle');
    // Leaflet.FullScreen
    $_js->addFile('../../../leaflet/plugins/fullscreen/Leaflet.fullscreen');
    // EOF Leaflet

    $_js->addFile('../../../vanilla_tables/vanilla-dataTables');

    // Custom JS
    $_js->addFile('george_the_victorious');

    (ENVIRONMENT != 'production') ? $_js->crunch(true) : $_js->crunch();

    ?>

</body>
</html>