    <div class="mb10"></div><!-- space -->
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <?php echo $content['text'];?>
                
                <?php foreach($citations as $citation):?>

                    <a id="citate-<?php echo $citation['farewell_id'];?>"></a>

                    <div class="mb60"></div>

                    <div class="clearfix"></div>

                    <blockquote class="citation">
        
                        <div class="row">

                            <div class="col-md-12">


                                    <?php if($citation['author'] != ''):?>
                                    <p><strong><em><?php echo $citation['author'];?></em></strong></p>
                                    <?php endif;?>
                                    <p><?php echo $citation['text'];?></p>

                            </div><!-- End .col-md-6 -->

                        </div><!-- End .row -->
        
                    </blockquote>

                    <div class="clearfix"></div>
                
                <?php endforeach;?>
                
                <div class="clearfix"></div>
                
            </div>
            
            <div class="mb60 clearfix visible-sm visible-xs"></div>
            <?php echo $this->load->view('left_view');?>
        </div>
        
    </div><!-- End .container -->
</div><!-- End #content -->