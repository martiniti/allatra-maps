    <div class="container single">
        <article class="entry">

        <div class="col-md-10 col-md-offset-1">

            <h1 class="title custom mb50"><?php echo $content['title'];?></h1>
            <div class="clearfix"></div>

            <?php echo $content['text'];?>

            <?php if(count($info['comments'])>0):?>

            <?php foreach ($info['comments'] as $comment):?>


                <article class="entry entry-list">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="entry-media">
                                <figure>
                                    <a href="#"><img src="<?php echo base_url('assets/uploads/images/comments/thumbs/'.$comment->img_url);?>" alt="<?php echo $comment->author;?>"></a>
                                </figure>
                                <div class="entry-meta">
                                    <span><i class="fa fa-calendar"></i><?php echo date('d.m.Y H:i', strtotime($comment->date));?></span>
                                </div><!-- End .entry-media -->
                            </div><!-- End .entry-media -->
                        </div><!-- End .col-md-6 -->
                        <div class="col-md-10">
                            <h2 class="entry-title"><i class="fa fa-comment"></i><a href="#"><?php echo $comment->author;?></a></h2>
                            <div class="entry-content">
                                <?php echo preg_replace("/<p[^>]*>[\s|&nbsp;]*<\/p>/", '', $comment->comment_text);?>
                            </div><!-- End .entry-content -->
                        </div><!-- End .col-md-6 -->
                    </div><!-- End .row -->
                </article>


            <div class="clearfix"></div>

            <?php endforeach;?>

            <?php endif;?>

            </div>

        </article>

        <div class="clearfix"></div>

        <?php if(!empty($info['enable_comments'])):?>

        <script>
            var material_id = "<?php echo $info['page_id'];?>";
            var material_url = "<?php echo base_url($info['url']);?>";
        </script>

        <div class="col-md-10 col-md-offset-1">

            <!-- Comments Form -->
            <div id="respond" class="comment-respond">

                <h3 class="title custom mb25"><span><?php echo $this->lang->line('comment_leave');?></span></h3>

                <form id="comment_form" action = "<?php echo base_url($lang."comments/add_comment/");?>" method="post">

                    <div class="notice alert alert-danger hidden"></div>

                    <div class="id">
                        <input name="id" type="hidden" value="<?php echo $info['page_id'];?>" />
                    </div>

                    <div class="parent_id">
                        <input name="parent_id" type="hidden" value="0" />
                    </div>

                    <div class="comment_type">
                        <input name="type" type="hidden" value="2" />
                    </div>

                    <div class="comment_language">
                        <input name="language" type="hidden" value="<?php echo LANGUAGE;?>" />
                    </div>

                    <div class="row margin-bottom-20">
                        <?php if($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0):?>
                            <br />
                            <div class="col-md-12">
                                <label><strong><?php echo $this->lang->line('commentator_name');?></strong>: <?php echo $this->session->userdata('DX_username') ;?></label>
                                <input name="author" type="hidden" value="<?php echo $this->session->userdata('DX_username') ;?>" />
                            </div>
                            <div class="clearfix"></div>
                            <br />
                        <?php else : ?>

                            <div class="col-md-6 col-md-offset-0">
                                <label><?php echo $this->lang->line('commentator_name');?> <span class="color-red">*</span></label>
                                <input name="author" type="text" class="form-control" required>
                            </div>

                        <?php endif ; ?>

                        <?php if($this->session->userdata('DX_user_id') && $this->session->userdata('DX_user_id') > 0):?>
                            <div class="col-md-12">
                                <label><strong><?php echo $this->lang->line('commentator_email');?></strong>: <?php echo $this->session->userdata('DX_email') ;?></label>
                                <input name="email" type="hidden" value="<?php echo $this->session->userdata('DX_email') ;?>" />
                            </div>
                            <div class="clearfix"></div>
                            <br />
                        <?php else : ?>
                            <div class="col-md-6 col-md-offset-0">
                                <label><?php echo $this->lang->line('commentator_email');?> <span class="color-red">*</span></label>
                                <input name="email" type="email" class="form-control" required>
                            </div>
                        <?php endif ; ?>
                    </div>

                    <input name="address" type="hidden" value="" class="form-control">

                    <label><?php echo $this->lang->line('commentator_message');?> <span class="color-red">*</span></label>
                    <div class="row margin-bottom-20">
                        <div class="col-md-12 col-md-offset-0">
                            <textarea name="comment_text" class="textarea form-control" rows="10" required></textarea>
                        </div>
                    </div>

                    <?php if(!$this->dx_auth->is_logged_in()):?>
                        <div class="row margin-bottom-20">
                            <div class="col-md-4">
                                <label><?php echo $this->lang->line('comment_capcha');?> <span class="color-red">*</span></label>
                                <input name="captcha" type="text" class="form-control captcha" size="8" required>
                            </div>
                            <div class="col-md-4">
                                <label>&nbsp;</label><div class="clearfix"></div>
                                <span id="image_key_capcha"><?php echo $imgcode;?></span>
                            </div>
                        </div>
                    <?php endif;?>

                    <p><button name="post_comment" id="add_comment" class="btn btn-dark" type="submit"><?php echo $this->lang->line('comment_add');?></button></p>

                    <div class="loading"></div>
                </form>
            </div>
            <!-- End. Comments Form -->

            <?php endif;?>

        </div>

        <div class="clearfix"></div>


    </div><!-- End .container -->
</div><!-- End .main -->