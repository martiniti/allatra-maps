<html>
<head>
    <title>ALLATRA.maps :: Карта наводений онлайн</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php

    $_css = new CSSCrunch();
    // Leaflet
    $_css->addFile('../../../leaflet/leaflet');
    $_css->addFile('../../../leaflet/plugins/fullscreen/leaflet.fullscreen');
    // Font Awesome
    $_css->addFile('../../../font-awesome/css/font-awesome');

    (ENVIRONMENT != 'production') ? $_css->crunch(true) : $_css->crunch();

    ?>

    <style>
        body{
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }

        /* Go FloodMap styles */

        #AmmaFloodWrapper{
            float: left;
            width: 100%;
            height: 100%;
        }

    </style>

</head>
<body>

<div class="panel-body">
    <p><b>Set Elevation /Height / Water Level (Minus/Plus):</b>
    <input class="styled" type="text" id="elev" size="8" value="400" />
    <input type="button" value="Set" onclick="setElev(document.getElementById('elev').value);"/>&nbsp;&nbsp;<b><span id="curElev"></span> meters.</b></p>
    <p><b>Or Click on the map to get/set the flood water level at the location.</b></p>

    <div id="basemaps-wrapper" class="leaflet-bar">
        <select id="basemaps">
            <option value="Topographic">Topographic</option>
            <option value="Imagery">Imagery</option>
            <option value="ImageryClarity">Imagery (Clarity)</option>
            <option value="ImageryFirefly">Imagery (Firefly)</option>
            <option value="Streets">Streets</option>
            <option value="NationalGeographic">National Geographic</option>
            <option value="Oceans">Oceans</option>
            <option value="Gray">Gray</option>
            <option value="DarkGray">Dark Gray</option>
            <option value="ShadedRelief">Shaded Relief</option>
            <option value="Physical">Physical</option>
        </select>
    </div>

</div>

    <div id="AmmaFloodWrapper"></div>

    <?php

    $_js = new JSCrunch();

    // Leaflet
    $_js->addFile('../../../leaflet/leaflet');
    $_js->addFile('../../../leaflet/plugins/fullscreen/Leaflet.fullscreen');
    $_js->addFile('../../../leaflet/plugins/esri/esri-leaflet');

    (ENVIRONMENT != 'production') ? $_js->crunch(true) : $_js->crunch();

    ?>

    <script>
        var lat, lng, intitZoom;

        lat = 20;
        lng = -10;
        initZoom = 3;

        var map = L.map('AmmaFloodWrapper',
            {
                minZoom: 0,
                maxZoom: 15,
                fullscreenControl: true
            });
        map.setView([lat, lng], initZoom);
        var elev = document.getElementById('elev').value;

        var layer = L.esri.basemapLayer('Topographic').addTo(map);
        var layerLabels;

        document
            .querySelector('#basemaps')
            .addEventListener('change', function (e) {
                var basemap = e.target.value;
                setBasemap(basemap);
            });


        var floodLayer=L.tileLayer('https://www.floodmap.net/getFMTile.ashx?x={x}&y={y}&z={z}&e=' + elev, {
            attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
            opacity: 0.5
        }).addTo(map);
        var searchControl = L.esri.Geocoding.geosearch({ useMapBounds: 20, expanded:true }).addTo(map);

        var results = L.layerGroup().addTo(map);
        var marker;
        searchControl.on('results', function (data) {
            results.clearLayers();
            for (var i = data.results.length - 1; i >= 0; i--) {
                ga('send', 'event', 'Location', 'locsearch');
                addMarker(data.results[i].latlng);
            }
        });



        function setBasemap(basemap) {
            if (layer) {
                map.removeLayer(layer);
            }

            layer = L.esri.basemapLayer(basemap);

            map.addLayer(layer);

            if (layerLabels) {
                map.removeLayer(layerLabels);
            }

            if (
                basemap === 'ShadedRelief' ||
                basemap === 'Oceans' ||
                basemap === 'Gray' ||
                basemap === 'DarkGray' ||
                basemap === 'Terrain'
            ) {
                layerLabels = L.esri.basemapLayer(basemap + 'Labels');
                map.addLayer(layerLabels);
            } else if (basemap.includes('Imagery')) {
                layerLabels = L.esri.basemapLayer('ImageryLabels');
                map.addLayer(layerLabels);
            }
            floodLayer.bringToFront();
        }

    </script>

</body>
</html>