    <div class="mb10"></div><!-- space -->
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                
                <?php echo $content['text'];?>
                
                <div class="row">
                    <?php $i = 1; foreach($certificates as $certificate):?>
                    <div class="col-md-3">
                        <a class="fancybox" rel="group" href="<?php echo base_url('assets/uploads/images/certificates/'.$certificate['img_url']);?>"  title="<?php echo $certificate['title'];?>">
                            <img class="img-responsive" src="<?php echo base_url('assets/uploads/images/certificates/thumbs/'.$certificate['img_url']);?>" alt="<?php echo $certificate['title'];?>"  title="<?php echo $certificate['title'];?>"/>
                        </a>
                    </div>
                    <?php if($i%4 == 0):?>
                    <div class="clearfix"></div>
                    <br>
                    <?php endif;?>
                    <?php $i++; endforeach;?>
                </div>
                
            </div>
            <div class="mb60 clearfix visible-sm visible-xs"></div>
            <?php echo $this->load->view('left_view');?>
        </div>
        
    </div><!-- End .container -->
</div><!-- End #content -->