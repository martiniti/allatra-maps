    <div class="container single">
        <article class="entry">

            <h1><?php echo $content['title']; ?></h1>

            <div class="alert alert-success" role="alert">
                <?php echo $this->lang->line('good_payment');?>
            </div>

            <?php echo $content['text']; ?>
            <div class="clearfix"></div>
            <hr/>
        </article>
    </div><!-- End .container -->
</div><!-- End .main -->