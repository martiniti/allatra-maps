    <div class="container single">
        <article class="entry">

            <h1 class="text-center"><?php echo $content['title']; ?></h1>

            <div class="col-md-offset-2 col-md-8 ">

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-custom">
                        <div class="panel-heading" role="tab" id="heading_pay_1">
                            <h4 class="panel-title">
                                <a role="button" class="panel-collapse collapse" data-toggle="collapse" data-parent="#accordion" href="#collapse_pay_1" aria-expanded="true" aria-controls="collapse_pay_1">
                                    <?php echo $this->lang->line('info_payment') ;?>
                                </a>
                            </h4>
                        </div><!-- End .panel-heading -->
                        <div id="collapse_pay_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_pay_1" aria-expanded="true">
                            <div class="panel-body">
                                <?php echo $content['anons'];?>
                            </div><!-- End .panel-body -->
                        </div><!-- End .panel-collapse -->
                    </div><!-- End .panel -->
                    <div class="panel panel-custom">
                        <div class="panel-heading" role="tab" id="heading_pay_2">
                            <h4 class="panel-title">
                                <a role="button" class="panel-collapse collapse collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_pay_2" aria-expanded="false" aria-controls="collapse_pay_2">
                                    <?php echo $this->lang->line('other_payments') ;?>
                                </a>
                            </h4>
                        </div><!-- End .panel-heading -->
                        <div id="collapse_pay_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_pay_2" aria-expanded="false">
                            <div class="panel-body">
                                <?php echo $content['text'];?>
                            </div><!-- End .panel-body -->
                        </div><!-- End .panel-collapse -->
                    </div><!-- End .panel -->
                </div><!-- End .panel-group -->

                <div class="clearfix"></div>

                <form action="<?php echo base_url($lang.'pay_online');?>" method="post" name="pay_form" id="pay_form">
                    <fieldset>
                    <div class="form-group">
                        <label for="fio" class="input-desc">
                            <?php echo $this->lang->line('fio') ;?> <?php echo $this->lang->line('latin') ;?> <span class="red">*</span>
                        </label>
                        <input type="text" class="form-control" id="fio" name="fio" placeholder="" value="<?php echo field_value('fio');?>" required="" aria-required="true">
                        <strong class="error"></strong>
                    </div>
                    <div class="form-group">
                        <label for="fio" class="input-desc">
                            <?php echo $this->lang->line('summ') ;?> (<?php echo $this->lang->line('example') ;?>: 427.35). <?php echo $this->lang->line('currancy') ;?> <?php echo $this->lang->line('uah') ;?> <span class="red">*</span>
                        </label>
                        <input type="text" class="form-control" id="summ" name="summ" placeholder="" value="<?php echo field_value('summ');?>" required="" aria-required="true">
                        <strong class="error"></strong>
                    </div>
                    <div class="form-group">
                        <label for="fio" class="input-desc">
                            <?php echo $this->lang->line('email') ;?> <span class="red">*</span>
                        </label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="" value="<?php echo field_value('email');?>" required="" aria-required="true">
                        <strong class="error"></strong>
                    </div>
                    <div class="form-group">
                        <label for="fio" class="input-desc">
                            <?php echo $this->lang->line('phone') ;?>
                        </label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="" value="<?php echo field_value('phone');?>" required="" aria-required="true">
                        <strong class="error"></strong>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label class="custom-checkbox-wrapper">
                                <span class="custom-checkbox-container">
                                    <?php echo form_checkbox('offer_check', 1, (!empty(field_value('offer_check')) ? TRUE : FALSE));?>
                                    <span class="custom-checkbox-icon"></span>
                                </span>
                                <span style="vertical-align: baseline !important;"><?php echo $this->lang->line('offer_check_text') ;?></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center">
                        <button class="btn btn-lg btn-custom" onClick="ga('send', 'event', 'pay', 'click');" data-loading-text="Sending..."><?php echo $this->lang->line('more') ;?> »</button>
                    </div>
                    </fieldset>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="mb20"></div>
            <div class="text-center">
                <?php if(!empty($form)):?>
                    <?php echo $form;?>
                <?php endif;?>
            </div>
            <div class="clearfix"></div>
            <div class="mb40"></div>
        </article>

        <div class="clearfix"></div>


    </div><!-- End .container -->
</div><!-- End .main -->