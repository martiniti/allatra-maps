<style>
    .leaflet-container a.red{
        color: red;
    }

    .leaflet-container a.green{
        color: green;
    }

    .leaflet-container a.activateGeodata{
        margin-right: 10px;
    }

    .leaflet-popup-content p {
        margin: 10px 0;
    }

    .leaflet-popup-content p.actions{
        margin: 6px 0;
    }
</style>

<?php

$geoLines = [];

$i = 0; foreach ($geodata as $geo):

    $geoLines[$i] = json_decode($geo['content']);

    if ($geo['source'] != ''){
        $geoLines[$i]->features[0]->properties->desc = $geoLines[$i]->features[0]->properties->desc.'<p><i>Источник: '.$geo["source"].'</i></p>';
    }

    $actions = '';

    if ($this->dx_auth->is_role(array('admin','moderator'))){

        $actions = '<p class="actions text-right">';
        //activate
        if($geo['status'] == 0){
            $actions .= '<a class="green activateGeodata" geo-id="'.$geo["geodata_id"].'" href="#"><i class="material-icons done"></i>Одобрить</a>';
        }
        //delete
        $actions .= '<a class="red deleteGeodata" geo-id="'.$geo["geodata_id"].'" href="#"><i class="material-icons cancel"></i>Удалить</a>';
        $actions .= '</p>';
        $geoLines[$i]->features[0]->properties->desc = $geoLines[$i]->features[0]->properties->desc.$actions;
    }

    if($geo['status'] == 0){

        $geoLines[$i]->features[0]->properties->color   = 'gray';
        $geoLines[$i]->features[0]->properties->icon    = 'defaultIcon';

    }else{

        if($geo['officially'] == 0){

            $geoLines[$i]->features[0]->properties->color = 'blue';
            $geoLines[$i]->features[0]->properties->icon    = 'blackIcon';

        }else{

            $geoLines[$i]->features[0]->properties->color = 'red';
            $geoLines[$i]->features[0]->properties->icon    = 'AllatRaIcon';

        }

    }

    $geoLines[$i]->features[0]->properties->officially = $geo['officially'];

    $i++;

endforeach;

?>

<script>

    var geolines = <?php echo json_encode($geoLines);?>

</script>

<div class="main-container">
    <div class="container-maps-and-content">

        <div id="containerMaps">

            <div id="googleMaps"></div>

            <div id="yandexMaps"></div>
            <div id="leafletMaps"></div>
            <div id="containerOwnControls">
                <div class="container-gps-control">
                    <div class="group-header">GPS</div>
                    <div class="container-gps-control__wrapper-buttons">
                        <div id="btnDetectorGps">&nbsp;</div>
                    </div>
                </div>
                <div class="container-history-controls">
                    <div class="group-header">History</div>
                    <div class="container-history-controls__wrapper-buttons">
                        <div id="container-history-controls-container-btn_undo">&nbsp;</div>
                        <div id="container-history-controls-container-btn_redo">&nbsp;</div></div></div>
                <div class="container-toggle-maps">
                    <div class="group-header">Maps</div>
                    <div class="container-toggle-maps__wrapper-buttons">
                        <div id="buttonGoogle" class="medium-expand large-expand">&nbsp;</div>
                        <div id="buttonYandex" class="medium-expand large-expand">&nbsp;</div>
                        <div id="buttonLeaflet" class="medium-expand large-expand">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>

        <div id="containerContent">
            <div id="containerIcoOpenSlider">
                <div id="icoOpenSlider" style="transform: rotateY(180deg);">&nbsp;</div>
            </div>
        </div>
    </div>
</div>

</body>
</html>