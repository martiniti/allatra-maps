<?php /*if(!empty($partners)): */?><!--
    <div class="border pb30">
        <div class="container">
            <h2 class="title custom mb25"><?php /*echo $this->lang->line('our_partners');*/?></h2>
            <div class="clients-carousel owl-carousel">
                <?php /*foreach($partners as $partner):*/?>
                    <a href="<?php /*echo $partner['link'];*/?>" class="client"  target="_blank" rel="nofollow">
                        <img src="<?php /*echo base_url('assets/uploads/images/partners/'.$partner['img_url']);*/?>" alt="<?php /*echo $partner['title'];*/?>" title="<?php /*echo $partner['title'];*/?>" />
                    </a>
                <?php /*endforeach;*/?>
            </div>
        </div>
    </div>-->
<?php /*endif;*/?>
    <div class="pb40">
        <div class="container">
            <?php if(!empty($partners)): ?>
                <div class="row">
                    <?php foreach($partners as $partner):?>

                        <div class="col-xs-12">

                            <div class="partner">

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">

                                    <a href="<?php echo $partner['link'];?>" target="_blank" rel="nofollow">
                                        <img src="<?php echo base_url('assets/uploads/images/partners/'.$partner['img_url']);?>" alt="<?php echo $partner['title'];?>" title="<?php echo $partner['title'];?>">
                                    </a>

                                </div>

                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" style="text-align: left">

                                    <h3><?php echo $partner['title'];?></h3>

                                    <div class="description">
                                        <?php echo $partner['text'];?>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <br/>

                        <div class="clearfix"></div>
                        <br />

                    <?php endforeach;?>
                </div>
            <?php endif;?>
            <!--<div class="row">
                <div class="col-md-6">
                    <?php /*echo $content['text'];*/?>
                </div>
                <div class="col-md-6">
                    <div class="mb10"></div>
                    <img src="<?php /*echo base_url();*/?>assets/template/taskforce/images/tf_logo_ru.png" alt="TaskForce" class="img-responsive center-block">
                </div>
            </div>-->
        </div><!-- End .container -->
    </div>
</div><!-- End .main -->