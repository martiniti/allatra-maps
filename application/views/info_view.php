<div class="container single">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page_title1"><?php echo $this->lang->line('info_page');?></h1>
            <p><?php echo $info['message'];?></p>
            <div class="mb60"></div><!-- space -->
        </div>
    </div>
</div><!-- End .container -->