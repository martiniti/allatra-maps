<?php

$user_id = array(
	'name'	=> 'id',
	'id'	=> 'user_id',
    'class' => 'form-control',
	'type'	=> 'hidden',
	'value' => $user_profile->id
);

$username = array(
	'name'	=> 'username',
	'id'	=> 'username',
    'class' => 'form-control',
	'size'	=> 30,
	'value' => $user_profile->username
);

$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
    'class' => 'form-control',
	'maxlength'	=> 80,
	'size'	=> 30,
	'value'	=> $user_profile->email
);

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
    'class' => 'form-control',
	'size'	=> 30
);

$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
    'class' => 'form-control',
	'size'	=> 30
);

$submit = array(
	'name'	    => 'save_profile',
    'class'     => 'btn btn-default',
    'content'   => $this->lang->line('save'),
	'type'	    => 'submit'
);

?>

<div class="mb10"></div><!-- space -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="my_account">

                <?php if(isset($message) && $message != ''):?>
                    <div class="alert alert-success" role="alert"><?php echo $message;?></div>
                <?php endif;?>

                <div role="tabpanel">

                  <!-- Nav tabs -->
                  <ul id="my_account_tabs" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Профиль</a></li>
                    <li role="presentation" style="display: none;"><a href="#articles" aria-controls="articles" role="tab" data-toggle="tab">Статьи</a></li>
                    <li role="presentation" style="display: none;"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Комментарии</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content" id="account-profile">
                    <div role="tabpanel" class="tab-pane active" id="profile">
                        <fieldset id="my_account">

                            <br />

                            <?php
                                $attributes = array('role' => 'form');

                                echo form_open_multipart(base_url('my_account'), $attributes);
                            ?>

                            <?php echo $this->dx_auth->get_auth_error(); ?>

                            <?php echo form_input($user_id);?>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('username').':', $username['id']);?>
                                <?php echo form_input($username)?>
                                <?php echo form_error('username'); ?>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('role').':');?>
                                <p>
                                    <?php switch ($user_profile->role_id) {

                                        case 1:

                                            echo $this->lang->line('my_account_role_user');

                                        break;

                                        case 2:

                                            echo $this->lang->line('my_account_role_admin');

                                        break;

                                        case 3:

                                            echo $this->lang->line('my_account_role_moder');

                                        break;

                                        case 4:

                                            echo $this->lang->line('my_account_role_author');

                                        break;

                                    }?>
                                </p>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('my_account_creation_date'));?>
                                <p><?php echo date('d/m/Y H:i',strtotime($user_profile->created));?></p>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('my_account_last_enter_date'));?>
                                <p><?php echo date('d/m/Y H:i',strtotime($user_profile->modified));?></p>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('my_account_status'));?>
                                <p><?php if($user_profile->banned == 0): echo $this->lang->line('my_account_status_active'); else: echo $this->lang->line('my_account_status_blocked'); endif; ?></p>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('user_email').':', $email['id']);?>
                                <?php echo form_input($email);?>
                                <?php echo form_error($email['name']); ?>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('password').':', $password['id']);?></dt>
                                <?php echo form_password($password)?>
                                <?php echo form_error('password'); ?>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('confirm_password').':', $confirm_password['id']);?></dt>
                                <?php echo form_password($confirm_password)?>
                                <?php echo form_error('confirm_password'); ?>
                            </div>

                            <div class="form-group">
                                <?php echo form_label($this->lang->line('my_account_avatar'));?></dt>
                                <?php //echo form_upload('image');?>
                                <input type="file" name="image" size="20" />
                                <?php if($user_profile->image != ''):?>

                                <div style="text-align:center; padding:5px; border:1px solid #ddd;">
                                    <img src="<?php echo base_url('assets/uploads/images/avatars/thumbs/'.$user_profile->image);?>" alt="current"/><br/>
                                </div>

                                <?php endif;?>
                            </div>

                            <?php echo form_button($submit);?>


                            <?php echo form_close()?>
                        </fieldset>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="articles">

                        <?php if(isset($user_created_articles) && count($user_created_articles)>0):?>
                            <br />
                            <h2><?php echo $this->lang->line('my_account_created_articles');?></h2>
                            <br />
                            <?php foreach($user_created_articles as $created_article):?>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><?php echo $created_article['title'];?></div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><?php echo $created_article['date'];?></div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="text-align: right;">
                                    <a href="<?php echo base_url($created_article['url']);?>" target="_blank">
                                        <i class="fa fa-external-link"></i>
                                    </a>
                                    <a href="<?php echo base_url('adminpanel/materials/edit/'.$created_article['material_id']);?>">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                </div>
                            </div>
                            <hr />
                            <?php endforeach;?>

                        <?php endif;?>

                        <?php if(isset($user_updated_articles) && count($user_updated_articles)>0):?>
                            <div class="clearfix"></div>
                            <br />
                            <h2><?php echo $this->lang->line('my_account_edited_articles');?></h2>
                            <div class="clearfix"></div>
                            <br />
                            <?php foreach($user_updated_articles as $updated_article):?>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><?php echo $updated_article['title'];?></div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"><?php echo $updated_article['date'];?></div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="text-align: right;">
                                    <a href="<?php echo base_url($updated_article['url']);?>" target="_blank"><i class="fa fa-external-link fa-3x"></i></a>
                                    <a href="<?php echo base_url('adminpanel/materials/edit/'.$updated_article['material_id']);?>"><i class="fa fa-pencil-square-o fa-3x"></i></a>
                                </div>
                            </div>
                            <hr />
                            <?php endforeach;?>

                        <?php endif;?>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="comments">
                        <?php if(isset($user_comments) && count($user_comments)>0):?>
                        <br />
                        <h2><?php echo $this->lang->line('my_account_my_comments');?></h2>
                        <br />
                        <?php foreach($user_comments as $user_comment):?>
                        <div id="comment-<?php echo $user_comment['comment_id'];?>" class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <strong><?php echo $user_comment['date'];?></strong>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div data-type="textarea" data-pk="1" id="<?php echo $user_comment['comment_id'];?>" class="comment_text">
                                    <?php echo $user_comment['comment_text'];?>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <p style="font-size: 16px; text-align: right;">
                                    <a href="<?php echo base_url($user_comment['url']);?>" target="_blank"><i class="fa fa-external-link-square"></i></a>
                                    <a class="remove_comment" data-id="<?php echo $user_comment['comment_id'];?>" href="#"><i class="fa fa-times"></i></a>
                                </p>
                            </div>
                        </div>
                        <hr />
                        <?php endforeach;?>

                        <?php endif;?>
                    </div>
                  </div>

                </div>
                <br />

            </div>
        </div>
    </div>

</div><!-- End .container -->
</div><!-- End #content -->