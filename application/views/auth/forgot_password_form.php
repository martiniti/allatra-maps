<?php

$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
    'class' => 'form-control',
	'maxlength'	=> 80,
	'size'	=> 30,
	'value' => set_value('login')
);

$submit = array(
	'name'	    => 'reset',
    'class'     => 'btn btn-primary',
    'content'   => $this->lang->line('forgot_passwor_submit'),
	'type'	    => 'submit'
);

?>

<div class="container padding-bottom-3x mb-2">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-10">
            
            <h2 class="title-underblock custom mb30"><?php echo $this->lang->line('forgot_password');?></h2>
            
            <?php
                $attributes = array('class' => 'card');
                echo form_open($this->uri->uri_string(), $attributes);
            ?>

            <div class="card-body">

                <strong style="color: red;">
                    <?php echo $this->dx_auth->get_auth_error(); ?>
                </strong>

                <div class="form-group">
                    <?php echo form_label($this->lang->line('forgot_passwor_name'), $login['id']);?>
                    <?php echo form_input($login); ?>
                    <?php echo form_error($login['name']); ?>
                </div>

                <div class="forgot_pass_registration">

                    <?php echo anchor($this->dx_auth->login_uri, $this->lang->line('login'));?> &nbsp;&nbsp;&nbsp;
                    <?php
                    if ($this->dx_auth->allow_registration) {
                        echo anchor($this->dx_auth->register_uri, $this->lang->line('registration'));
                    };
                    ?>
                </div>

            </div>

            <div class="card-footer">
            
                <?php echo form_button($submit);?>

            </div>
            
            <?php echo form_close();?>

        </div>
    </div>
</div>
