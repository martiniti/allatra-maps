<?php
$username = array(
	'name'	=> 'username',
	'id'	=> 'username',
    'class' => 'form-control',
	'size'	=> 30,
	'value' =>  set_value('username')
);

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
    'class' => 'form-control',
	'size'	=> 30,
	'value' => set_value('password')
);

$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
    'class' => 'form-control',
	'size'	=> 30,
	'value' => set_value('confirm_password')
);

$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
    'class' => 'form-control',
	'maxlength'	=> 80,
	'size'	=> 30,
	'value'	=> set_value('email')
);

$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
    'class' => 'form-control'
);

$submit = array(
	'name'	=> 'register',
    'class' => 'btn btn-primary',
    'content' => $this->lang->line('registration'),
	'type'	=> 'submit'
);


?>

<div class="container padding-bottom-3x mb-2">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-10">

            <h2 class="title-underblock custom mb30"><?php echo $this->lang->line('registration');?></h2>
            <p><?php echo $this->lang->line('required_fields');?></p>
            <div class="clearfix"></div>
            <br />

            <?php $attributes = array('role' => 'form', 'class' => 'card'); echo form_open($this->uri->uri_string(), $attributes);?>

            <div class="card-body">

                <div class="form-group">
                    <?php echo form_label($this->lang->line('registration_name'), $username['id']);?>
                    <?php echo form_input($username)?>
                    <?php echo form_error($username['name']); ?>
                </div>

                <div class="form-group">
                    <?php echo form_label($this->lang->line('registration_email'), $email['id']);?>
                    <?php echo form_input($email);?>
                    <?php echo form_error($email['name']); ?>
                </div>

                <div class="form-group">
                    <?php echo form_label($this->lang->line('registration_password'), $password['id']);?>
                    <?php echo form_password($password)?>
                    <?php echo form_error($password['name']); ?>
                </div>

                <div class="form-group">
                    <?php echo form_label($this->lang->line('registration_password_confirm'), $confirm_password['id']);?>
                    <?php echo form_password($confirm_password);?>
                    <?php echo form_error($confirm_password['name']); ?>
                </div>

                <?php if ($this->dx_auth->captcha_registration): ?>

                <div class="form-group">
                    <div class="captcha-img">
                        <?php echo $this->lang->line('registration_enter_code');?>
                        <div class="clearfix"></div>
                        <?php echo $this->dx_auth->get_captcha_image(); ?>
                    </div>
                    <?php echo form_label($this->lang->line('registration_captcha'), $captcha['id']);?>

                    <?php echo form_input($captcha);?>
                    <?php echo form_error($captcha['name']); ?>
                </div>

                <?php endif; ?>

            </div>

            <div class="card-footer">

                <?php echo form_button($submit);?>

            </div>

            <?php echo form_close()?>

        </div>
    </div>
</div>
