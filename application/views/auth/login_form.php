<?php
$username = array(
	'name'	=> 'username',
	'id'	=> 'username',
    'label' => $this->lang->line('login_name'),
    'class' => 'form-control',
	'size'	=> 30,
	'value' => set_value('username')
);

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
    'class' => 'form-control',
    'label' => $this->lang->line('login_password'),
	'size'	=> 30
);

$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0'
);

$confirmation_code = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
    'class' => 'form-control',
    'label' => $this->lang->line('login_captcha'),
	'maxlength'	=> 8
);

$submit = array(
	'name'	=> 'login',
    'class' => 'btn btn-primary',
    'content' => $this->lang->line('login'),
	'type'	=> 'submit'
);

?>

<div class="container padding-bottom-3x mb-2">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-10">

            <h2><?php echo $this->lang->line('authorization');?></h2>

            <?php
            $attributes = array('role' => 'form', 'class' => 'card');

            echo form_open(base_url('auth'), $attributes);
            ?>

            <div class="card-body">

                <?php echo $this->dx_auth->get_auth_error(); ?>

                <div class="form-group">
                    <?php echo form_label($this->lang->line('login_name'), $username['id']);?>
                    <?php echo form_input($username)?>
                    <?php echo form_error('username'); ?>
                </div>

                <div class="form-group">
                    <?php echo form_label($this->lang->line('login_password'), $password['id']);?></dt>
                    <?php echo form_password($password)?>
                    <?php echo form_error('password'); ?>
                </div>

                <?php if ($show_captcha): ?>

                    <div class="form-group">
                        <div class="captcha-img">
                            <?php echo $this->lang->line('login_enter_code');?>
                            <?php echo $this->dx_auth->get_captcha_image(); ?>
                        </div>
                        <?php echo form_label($this->lang->line('login_captcha'), $confirmation_code['id']);?>

                        <?php echo form_input($confirmation_code);?>
                        <?php echo form_error($confirmation_code['name']); ?>
                    </div>

                <?php endif; ?>

                <div class="form-group clear-margin helper-group">
                    <?php echo anchor($this->dx_auth->forgot_password_uri, $this->lang->line('forgot_password'));?> &nbsp;&nbsp;&nbsp;
                    <?php if ($this->dx_auth->allow_registration) { echo anchor($this->dx_auth->register_uri, $this->lang->line('registration')); };?>
                </div>

                <div class="checkbox">
                    <label class="custom-checkbox-wrapper">
                            <span class="custom-checkbox-container">
                                <?php echo form_checkbox($remember);?>
                                <span class="custom-checkbox-icon"></span>
                            </span>
                        <span><?php echo $this->lang->line('login_remember');?></span>
                    </label>
                </div>

            </div>

            <div class="card-footer">

                <?php echo form_button($submit);?>

            </div>


            <?php echo form_close()?>


        </div>
    </div>
</div>