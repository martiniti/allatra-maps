    <div class="mb10"></div><!-- space -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title"><?php echo $this->lang->line('info_page');?></h1>
                <p><?php echo $auth_message;?></p>
                <div class="mb60"></div><!-- space -->
            </div>
        </div>

    </div><!-- End .container -->
</div><!-- End #content -->