<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html> <!--<![endif]-->
    <head>

        <meta charset="utf-8">

        <?php if(isset($content)):?>
            <title><?php if(isset($content['seo_title']) && $content['seo_title'] != ''): echo $content['seo_title']; else: echo $content['title'].' - '.$this->lang->line('seo_title'); endif;?></title>
            <meta name="description" content="<?php echo $content['description'].' - '.$this->lang->line('seo_description');?>">
            <meta name="keywords" content="<?php echo $content['keywords'];?>">
        <?php else:?>
            <title><?php $this->lang->line('no_title');?></title>
            <meta name="description" content="<?php $this->lang->line('no_description');?>">
            <meta name="keywords" content="<?php $this->lang->line('no_keywords');?>">
        <?php endif;?>

        <!-- Mobile Specific Meta Tag-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Favicon and Apple Icons-->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets/template/allatra-da/img/icons/favicon.ico');?>">
        <link rel="icon" type="image/png" href="<?php echo base_url('assets/template/allatra-da/img/icons/favicon.png');?>">
        <link rel="apple-touch-icon" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-iphone.png');?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-ipad.png');?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-iphone-retina.png');?>">
        <link rel="apple-touch-icon" sizes="167x167" href="<?php echo base_url('assets/template/allatra-da/img/icons/touch-icon-ipad-retina.png');?>">

        <!-- Modernizr-->
        <script src="<?php echo base_url('assets/template/allatra-da/js/modernizr.min.js');?>"></script>

        <!-- Template -->
        <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
        <link rel="stylesheet" media="screen" href="<?php echo base_url('assets/template/allatra-da/css/vendor.min.css');?>">
        <!-- Main Template Styles-->
        <link id="mainStyles" rel="stylesheet" media="screen" href="<?php echo base_url('assets/template/allatra-da/css/styles.css');?>">
        <link id="mainStyles" rel="stylesheet" media="screen" href="<?php echo base_url('assets/template/allatra-da/css/styles-00bcd4.min.css');?>">
        <!-- Custom Styles-->
        <link id="customStyles" rel="stylesheet" media="screen" href="<?php echo base_url('assets/template/allatra-da/css/custom.css');?>">
        <script src="<?php echo base_url('assets/template/allatra-da/js/vendor.min.js');?>"></script>
        <script src="<?php echo base_url('assets/template/allatra-da/js/scripts.min.js');?>"></script>
        <!-- EOF. Template -->

        <!-- LEAFLET  -->
        <script src="<?php echo base_url('assets/leaflet/leaflet.js');?>"></script>
        <link id="customStyles" rel="stylesheet" media="screen" href="<?php echo base_url('assets/leaflet/leaflet.css');?>">

        <!-- Draw -->
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/Leaflet.draw.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/Leaflet.Draw.Event.js');?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('assets/leaflet/plugins/draw/leaflet.draw.css');?>"/>

        <script src="<?php echo base_url('assets/leaflet/plugins/draw/Toolbar.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/Tooltip.js');?>"></script>

        <script src="<?php echo base_url('assets/leaflet/plugins/draw/ext/GeometryUtil.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/ext/LatLngUtil.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/ext/LineUtil.Intersect.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/ext/Polygon.Intersect.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/ext/Polyline.Intersect.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/ext/TouchEvents.js');?>"></script>

        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/DrawToolbar.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.Feature.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.SimpleShape.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.Polyline.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.Marker.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.Circle.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.CircleMarker.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.Polygon.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/draw/handler/Draw.Rectangle.js');?>"></script>

        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/EditToolbar.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/EditToolbar.Edit.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/EditToolbar.Delete.js');?>"></script>

        <script src="<?php echo base_url('assets/leaflet/plugins/draw/Control.Draw.js');?>"></script>

        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/Edit.Poly.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/Edit.SimpleShape.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/Edit.Rectangle.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/Edit.Marker.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/Edit.CircleMarker.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/draw/edit/handler/Edit.Circle.js');?>"></script>
        <!-- EOF. Draw -->

        <!-- For Mask -->
        <script src="<?php echo base_url('assets/leaflet/plugins/rotate/Leaflet.ImageOverlay.Rotated.js');?>"></script>

        <!-- Search -->
        <script src="<?php echo base_url('assets/leaflet/plugins/geosearch/l.control.geosearch.js');?>"></script>
        <script src="<?php echo base_url('assets/leaflet/plugins/geosearch/l.geosearch.provider.openstreetmap.js');?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('assets/leaflet/plugins/geosearch/l.geosearch.css');?>" />

        <!-- Print -->
        <script src="<?php echo base_url('assets/leaflet/plugins/easyprint/bundle.js');?>"></script>

        <!-- EOF. LEAFLET  -->

        <!-- Custom -->
        <script src="<?php echo base_url('assets/template/allatra-da/js/custom.js');?>"></script>

        <!-- VARS -->
        <script>
            var base_url        = "<?php echo base_url();?>";
            var base_url_lang   = "<?php echo base_url($lang);?>";
            var phone_1         = "<?php echo PHONE_1;?>";
            var phone_2         = "<?php echo PHONE_2;?>";
            var phone_3         = "<?php echo PHONE_3;?>";
            <?php if($this->dx_auth->is_logged_in()):?>
            var is_logged_in    = 1;
            <?php else:?>
            var is_logged_in    = 0;
            <?php endif;?>
        </script>
        
    </head>
<body>