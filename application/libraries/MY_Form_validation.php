<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

	public function __construct() {

		parent::__construct();
	}

	/**
	 * Alpha
	 *
	 * @param	string
	 * @return	bool
	 */
   public function alpha($str)
    {
        return ( ! preg_match("/^([a-zа-яёіїє])+$/ui", $str)) ? FALSE : TRUE;
    }

    /**
     * Alpha-numeric
     *
     * @access public
     * @param string
     * @return bool
     */
    public function alpha_numeric($str)
    {
        return ( ! preg_match("/^([a-zа-яёіїє0-9.])+$/ui", $str)) ? FALSE : TRUE;
    }

    public function alpha_numeric_lat($str)
    {
        return ( ! preg_match("/^([a-z0-9])+$/ui", $str)) ? FALSE : TRUE;
    }

	/**
	 * Alpha-numeric w/ spaces
	 *
	 * @param	string
	 * @return	bool
	 */
	public function alpha_numeric_spaces($str)
	{
		return (bool) preg_match('/^[a-zа-яёіїє0-9.\s]+$/ui', $str);
	}

    public function alpha_numeric_lat_spaces($str)
    {
        return ( ! preg_match("/^([a-z0-9\s])+$/ui", $str)) ? FALSE : TRUE;
    }
	/**
	 * Alpha-numeric with underscores and dashes
	 *
	 * @param	string
	 * @return	bool
	 */
    public function alpha_dash($str)
    {
        return ( ! preg_match("/^([-a-z0-9_\s])+$/ui", $str)) ? FALSE : TRUE;
    }

	/**
	 * Alpha-numeric with underscores, dashes . , ! ? :
	 *
	 * @param	string
	 * @return	bool
	 */
    public function my_alpha_dash($str)
    {
        return ( ! preg_match("/^([-a-zа-яёіїє0-9_\.\(\)\'\"\,\!\?\:\s])+$/ui", $str)) ? FALSE : TRUE;
    }

	/**
	 * Numeric
	 *
	 * @param	string
	 * @return	bool
	 */
	public function numeric($str)
	{
		return (bool) preg_match('/^[\-+]?[0-9]*\.?[0-9]+$/', $str);

	}
}