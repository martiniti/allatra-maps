<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed!');

class Display_lib
{
    
    public function user_main_page($data,$name)
    {
        $CI =& get_instance ();
            
        $CI->load->view('preheader_view',$data);
        $CI->load->view('header_view',$data);
        $CI->load->view($name.'_view',$data);
    }
    
    public function user_page($data,$name)
    {
        $CI =& get_instance ();
            
        $CI->load->view('preheader_view',$data);
        $CI->load->view('header_view',$data);
        $CI->load->view($name.'_view',$data);
        $CI->load->view('footer_view',$data);     
    }

    public function service_page($data,$name)
    {
        $CI =& get_instance ();

        $CI->load->view('preheader_view',$data);
        $CI->load->view('header_view',$data);
        $CI->load->view($name.'_view',$data);
        $CI->load->view('footer_view',$data);
    }
    
    public function user_contacts_page($data,$name)
    {
        $CI =& get_instance ();
            
        $CI->load->view('preheader_view',$data);
        $CI->load->view('header_view',$data);
        //$CI->load->view('gmap_view',$data);
        //$CI->load->view('left_view',$data);
        $CI->load->view($name.'_view',$data);
        $CI->load->view('footer_view',$data);     
    }
    
    public function user_info_page($data)
    {
        $CI =& get_instance ();
            
        $CI->load->view('info_preheader_view',$data);
        $CI->load->view('header_view',$data);
        $CI->load->view('info_view',$data);
        $CI->load->view('footer_view');     
    }

    public function error_404_page($data,$name)
    {
        $CI =& get_instance ();

        $CI->load->view('info_preheader_view',$data);
        $CI->load->view('header_view',$data);
        $CI->load->view($name.'_view',$data);
        $CI->load->view('footer_view');
    }

    public function auth_page($data, $name)
    {
        $CI =& get_instance ();
        
        $CI->load->view('info_preheader_view',$data);
        $CI->load->view('header_view',$data);
        $CI->load->view('auth/'.$name,$data);
        $CI->load->view('footer_view');

    }
    
    public function admin_page($data,$name)
    {
        $CI =& get_instance ();
        
        $CI->load->view('admin/preheader_view',$data);
        $CI->load->view('admin/header_view');
        $CI->load->view('admin/'.$name.'_view',$data);
        $CI->load->view('admin/footer_view');
    }
    
    public function admin_info_page($data)
    {
        $CI =& get_instance ();
        
        $CI->load->view('admin/preheader_view');
        $CI->load->view('admin/header_view');
        $CI->load->view('admin/info_view',$data);
        $CI->load->view('admin/footer_view');
    }

}

?>