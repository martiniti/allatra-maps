<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends MY_Crud
{
	// Правила для контактной формы 1
	public $add_geodata_rules = array
	(
	   array
	   (
		 'field' => 'content',
		 'label' => 'Описание места',
		 'rules' => 'trim|required|max_length[128000]'
	   ),
	);

    public $activate_geodata_rules = array
    (
        array
        (
            'field' => 'geoid',
            'label' => 'ID',
            'rules' => 'trim|required|integer'
        ),
    );

    public $delete_geodata_rules = array
    (
        array
        (
            'field' => 'geoid',
            'label' => 'ID',
            'rules' => 'trim|required|integer'
        ),
    );

    // Правила для контактной формы 1
    public $rules_1 = array
    (
        array
        (
            'field' => 'name',
            'label' => 'Имя',
            'rules' => 'trim|required|max_length[255]'
        ),
        array
        (
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|max_length[128]|valid_email'
        ),
        array
        (
            'field' => 'topic',
            'label' => 'Тема',
            'rules' => 'trim|required|max_length[255]'
        ),
        array
        (
            'field' => 'message',
            'label' => 'Сообщение',
            'rules' => 'trim|required|max_length[64000]'
        ),
    );

	// Правила для контактной формы 2
	public $rules_2 = array
	(
	   array
	   (
		 'field' => 'name',
		 'label' => 'Имя',
		 'rules' => 'trim|required|max_length[255]'
	   ),
	   array
	   (
		 'field' => 'phone',
		 'label' => 'Телефон',
		 'rules' => 'trim|required|max_length[64]'
	   ),
	   array
	   (
		 'field' => 'email',
		 'label' => 'Email',
		 'rules' => 'trim|required|max_length[64]|valid_email'
	   ),
	   array
	   (
		 'field' => 'message',
		 'label' => 'Сообщение',
		 'rules' => 'trim|required|max_length[64000]'
	   ),
	);

	// Правила для контактной формы 3
	public $rules_3 = array
	(
	   array
	   (
		 'field' => 'phone',
		 'label' => 'Телефон',
		 'rules' => 'trim|required|max_length[64]'
	   ),
	);

	// Правила для контактной формы 4
	public $rules_4 = array
	(
	   array
	   (
		 'field' => 'email',
		 'label' => 'Email',
		 'rules' => 'trim|required|max_length[60]|valid_email'
	   ),
	);

	// Правила для контактной формы 3
	public $rules_5 = array
	(
	   array
	   (
		 'field' => 'name',
		 'label' => 'Имя',
		 'rules' => 'trim|required|max_length[255]'
	   ),
	   array
	   (
		 'field' => 'theme',
		 'label' => 'Тема',
		 'rules' => 'trim|required|max_length[255]'
	   ),
	   array
	   (
		 'field' => 'email',
		 'label' => 'Email',
		 'rules' => 'trim|required|max_length[64]|valid_email'
	   ),
	   array
	   (
		 'field' => 'message',
		 'label' => 'Сообщение',
		 'rules' => 'trim|required|max_length[64000]'
	   ),
	);

	function __construct(){
		parent::__construct();
	}

	function userfile_check()
	{
		$config['allowed_types']  = 'gif|jpg|png|JPG|GIF|PNG|jpeg|JPEG|doc|docx|rtf|txt|pdf|zip|rar';
		$config['upload_path']    = realpath(APPPATH . '../assets/uploads/files/');;
		$config['max_size']    = '10240'; //5 megabytes

		$this->load->library('upload', $config);

		if ($this->upload->do_upload())
		{
			return $this->upload->data();
		} else {
			echo $this->upload->display_errors();
			//$error = array('error' => $this->upload->display_errors());
			return FALSE;
		}
	}

	public function add_new($data)
	{
		$this->db->insert('feedback',$data);
	}

    public function get_geo($id)
    {
        return $this->db->where('geodata_id',$id)->get('geodata')->row();
    }


    public function add_new_geo($data)
    {
        if($this->db->insert('geodata',$data))
        {
            return true;
        }else{
            return false;
        }
    }

    public function activate_geo($id)
    {
        $geo = $this->get_geo($id);

        $data = [
          'status' => 1
        ];

        if($geo){
            $this->db->where('geodata_id', $id);
            $this->db->update('geodata',$data);
            return true;
        }else{
            return false;
        }

    }

    public function delete_geo($id)
    {
        $geo = $this->get_geo($id);

        if($geo){
            $this->db->where('geodata_id', $id);
            $this->db->delete('geodata');
            return true;
        }else{
            return false;
        }

    }


    public function add_new_geo_george($data)
    {
        if($this->db->insert('george_the_victorious',$data))
        {
            return true;
        }else{
            return false;
        }
    }

    public function activate_geo_george($id)
    {
        $geo = $this->get_geo($id);

        $data = [
            'status' => 1
        ];

        if($geo){
            $this->db->where('geodata_id', $id);
            $this->db->update('george_the_victorious',$data);
            return true;
        }else{
            return false;
        }

    }

    public function delete_geo_george($id)
    {
        $geo = $this->get_geo($id);

        if($geo){
            $this->db->where('geodata_id', $id);
            $this->db->delete('george_the_victorious');
            return true;
        }else{
            return false;
        }

    }


	public function check_email($email)
	{
		$this->db->where('email',$email);
		$this->db->where('form_name','Форма "Промокодов"');
		$query = $this->db->get('feedback');

		//$this->db->last_query();

		//dump_exit($this->db->last_query());

		return $query->row_array();
	}

}
?>