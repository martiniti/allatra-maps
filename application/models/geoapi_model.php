<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Geoapi_model extends MY_Crud
{

public $table = 'geodata'; //Имя таблицы
public $idkey = 'geodata_id'; //Имя ID

public $add_rules = array
(
    array
    (
        'field' => 'latitude',
        'label' => 'Широта',
        'rules' => 'trim|decimal|required'
    ),
    array
    (
        'field' => 'longitude',
        'label' => 'Долгота',
        'rules' => 'trim|decimal|required'
    ),
    array
    (
        'field' => 'description',
        'label' => 'Описание',
        'rules' => 'trim|required|max_length[64000]'
    ),
    array
    (
        'field' => 'image',
        'label' => 'Фото',
        'rules' => 'trim|max_length[255]'
    ),
    array
    (
        'field' => 'officially',
        'label' => 'Официальный источник?',
        'rules' => 'trim|integer|exact_length[1]'
    ),
    array
    (
        'field' => 'source',
        'label' => 'Источник',
        'rules' => 'trim|max_length[255]'
    )
);

public function __construct()
{       
    parent::__construct();
}

public function get_all_places($limit, $start)
{
    $query = "SELECT geodata.geodata_id as id, 
                    GROUP_CONCAT(geodata_lines.longitude) as line_longitudes, 
                    GROUP_CONCAT(geodata_lines.latitude) as line_latitudes, 
                    geodata.type, 
                    geodata.coordinates, 
                    geodata.description, 
                    geodata.source,
                    geodata.officially, 
                    geodata.date, 
                    geodata.status 
        FROM geodata_lines RIGHT JOIN geodata ON geodata.geodata_id=geodata_lines.line_id GROUP BY geodata.geodata_id";

    if($limit){
        $query = $query." LIMIT $limit";
    }

    if($start){
        $query = $query." OFFSET $start";
    }

    $places = [];

    $places = $this->db->query($query)->result_array();

    array_push($places);

    return $places;

}

public function get_nearest_places($lng, $ltd, $radius, $limit, $start)
{
// Every ltd|lng degree° is ~ 111Km
//    $angle_radius = $radius / ( 111 * cos( $ltd ) );
//
//    $min_ltd = $ltd - $angle_radius;
//    $max_ltd = $ltd + $angle_radius;
//    $min_lng = $lng - $angle_radius;
//    $max_lng = $lng + $angle_radius;

//    $results = $this->db->query("SELECT * FROM geodata WHERE latitude BETWEEN $min_ltd AND $max_ltd AND longitude BETWEEN $min_lng AND $max_lng")
//                        ->result_array();

//    dump($limit);
//    dump_exit($start_from);

    //6371 - Km, 3959 - Miles
    //$points = $this->db->query("SELECT *, ( 6371 * acos( cos( radians($ltd) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($ltd) ) * sin( radians( latitude ) ) ) ) AS distance FROM geodata WHERE `type` = 'Point' HAVING distance < $radius ORDER BY distance");

    $points_query = [];


//    $lines = $this->db->query("SELECT geodata_lines.geodata_line_id, geodata.geodata_id, geodata_lines.longitude, geodata_lines.latitude, geodata.type, geodata.coordinates, geodata.description, geodata.image, geodata.content, geodata.source, geodata.officially, geodata.date, geodata.status, geodata.user_add, ( 6371 * acos( cos( radians($ltd) ) * cos( radians( geodata_lines.latitude ) ) * cos( radians( geodata_lines.longitude ) - radians($lng) ) + sin( radians($ltd) ) * sin( radians( geodata_lines.latitude ) ) ) ) AS distance FROM geodata_lines LEFT JOIN geodata ON geodata_lines.line_id=geodata.geodata_id WHERE geodata.type = 'LineString' HAVING distance < $radius ORDER BY distance")
//                      ->result_array();

    $lines_query = "SELECT geodata_lines.geodata_line_id, geodata.geodata_id, geodata_lines.longitude, geodata_lines.latitude, geodata.type, geodata.coordinates, geodata.description, geodata.image, geodata.content, geodata.source, geodata.officially, geodata.date, geodata.status, geodata.user_add, 
                            ( 6371 * acos( cos( radians($ltd) ) * cos( radians( geodata_lines.latitude ) ) * cos( radians( geodata_lines.longitude ) - radians($lng) ) + sin( radians($ltd) ) * sin( radians( geodata_lines.latitude ) ) ) ) 
                            AS distance FROM geodata_lines LEFT JOIN geodata ON geodata_lines.line_id=geodata.geodata_id WHERE geodata.type = 'LineString' HAVING distance < $radius ORDER BY distance";

//    $lines_query = "SELECT geodata_lines.geodata_line_id, geodata.geodata_id, geodata.longitude, geodata.latitude, geodata.type, geodata.coordinates, geodata.description, geodata.image, geodata.content, geodata.source, geodata.officially, geodata.date, geodata.status, geodata.user_add,
//                            ( 6371 * acos( cos( radians($ltd) ) * cos( radians( geodata_lines.latitude ) ) * cos( radians( geodata_lines.longitude ) - radians($lng) ) + sin( radians($ltd) ) * sin( radians( geodata_lines.latitude ) ) ) )
//                            AS distance FROM geodata LEFT JOIN geodata_lines ON geodata_lines.line_id=geodata.geodata_id WHERE geodata.type = 'LineString' HAVING distance < $radius GROUP BY geodata.geodata_id ORDER BY distance";

    if($limit){
        $lines_query = $lines_query." LIMIT $limit";
    }

    if($start){
        $lines_query = $lines_query." OFFSET $start";
    }

    $lines =$this->db->query($lines_query)
                     ->result_array();


    if(empty($lines)){

        $points_query = "SELECT *, ( 6371 * acos( cos( radians($ltd) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($ltd) ) * sin( radians( latitude ) ) ) ) AS distance FROM geodata WHERE `type` = 'Point' HAVING distance < $radius ORDER BY distance";

        if($limit){
            $points_query = $points_query." LIMIT $limit";
        }

        if($start){
            $points_query = $points_query." OFFSET $start";
        }

        $points =$this->db->query($points_query)
            ->result_array();

    } else {

        foreach ($lines as $point){
            $points[] = $point;
        }

    }

    return $points;

}

public function count_nearest_places($lng, $ltd, $radius)
{
    $points = $this->db->query("SELECT *, ( 6371 * acos( cos( radians($ltd) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($ltd) ) * sin( radians( latitude ) ) ) ) AS distance FROM geodata WHERE `type` = 'Point' HAVING distance < $radius ORDER BY distance")
        ->result_array();

    $lines = $this->db->query("SELECT geodata_lines.geodata_line_id, geodata.geodata_id, geodata_lines.longitude, geodata_lines.latitude, geodata.type, geodata.coordinates, geodata.description, geodata.image, geodata.content, geodata.source, geodata.officially, geodata.date, geodata.status, geodata.user_add, ( 6371 * acos( cos( radians($ltd) ) * cos( radians( geodata_lines.latitude ) ) * cos( radians( geodata_lines.longitude ) - radians($lng) ) + sin( radians($ltd) ) * sin( radians( geodata_lines.latitude ) ) ) ) AS distance FROM geodata_lines LEFT JOIN geodata ON geodata_lines.line_id=geodata.geodata_id WHERE geodata.type = 'LineString' HAVING distance < $radius ORDER BY distance")
        ->result_array();

    if(count($lines) > 0){
        $summary = count($lines);
    } else{
        $summary = count($points);
    }

    //$summary = count($points) + count($lines);

    return $summary;
}

public function add_new($data)
{
    $this->db->insert('geodata',$data);
}

public function good_lng_ltd()
{
    /*$query = $this->db->where('type','Point')
                      ->get($this->table)
                      ->result_array();

    foreach ($query as $item){

        $item['coordinates'] = str_replace([']','['],'',$item['coordinates']);

        $coordinates = explode(",",$item['coordinates']);

        $lng_ltd = array(
            'longitude' => $coordinates[0],
            'latitude' => $coordinates[1],
        );

        $this->db->where('geodata_id', $item['geodata_id']);
        $this->db->update('geodata', $lng_ltd);

    }*/
}
        
}
?>