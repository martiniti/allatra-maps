<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sections_model extends MY_Crud
{  

    public $table = 'sections'; //Имя таблицы	
    public $idkey = 'section_id'; //Имя ID
    
    public function count_materials_by($section_id, $lang)
    {
        $this->db->from('materials');
        $this->db->join('content', 'content.fid = materials.material_id');  
        $this->db->where('materials.section',$section_id);    
        $this->db->where('content.show',1);
        $this->db->where('content.table','materials'); 
        $this->db->where('content.language',$lang); 
        
        return $this->db->count_all_results();
    }
    
    public function get_materials_by($section_id, $limit, $start_from, $lang)
    {
        $this->db->select('materials.*, content.title as name, content.anons as anons, content.img_url as image, content.video_url');
        $this->db->from('materials');
        $this->db->join('content', 'content.fid = materials.material_id');  
        $this->db->where('materials.section', $section_id);    
        $this->db->where('content.show', 1);
        $this->db->where('content.table', 'materials'); 
        $this->db->where('content.language', $lang); 
        $this->db->order_by('materials.date', 'desc');
        $this->db->order_by('materials.material_id', 'desc');
        $this->db->limit($limit, $start_from);     
        
        $query = $this->db->get();

        $materials = array();
        
        foreach ($query->result_array() as $item){
            $materials[$item['material_id']] = $item;
            $materials[$item['material_id']]['author'] = $this->get_author($item['author_id'], LANGUAGE);
        };

        return $materials;


    }

	// Получение данных автора;
	public function get_author($author_id, $language = 'ru')
	{
		$this->db->select("authors.author_id as author_id, authors.name_$language as name");
        $this->db->where('author_id', $author_id);
		$query = $this->db->get('authors');

		return $query->row_array(); 
	}

}
?>