<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends MY_Crud
{
    
public $table = 'news'; //Имя таблицы	
public $idkey = 'news_id'; //Имя ID

public function __construct()
{       
    parent::__construct();
}

public function all_with_pag($limit,$start_from)
{
    $this->db->order_by('news_id','desc');
    $this->db->limit($limit,$start_from); 
    
    $query = $this->db->get('news');
    
    return $query->result_array();
}
        
}
?>