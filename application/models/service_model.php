<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service_model extends MY_Crud
{
    
    public $table = 'services'; //Имя таблицы
    public $idkey = 'service_id'; //Имя ID

    public function __construct()
    {
        parent::__construct();
    }

    public function check_service_languages($obj_id,$language)
    {
        $this->db->select('language');
        $this->db->where('fid',$obj_id);
        $query = $this->db->get('services_content')->result_array();

        $languages = array();
        for($i = 0; $i<count($query); $i++){
            $languages[$i] = $query[$i]['language'];
        }
        return $languages;

    }

    public function get_services($lang)
    {
        $this->db->select('services.service_id as service_id, services.url as url, services.priority as priority, services.image as img, services.icon as icon, services_content.title as title, services_content.description as description');
        $this->db->from('services');
        $this->db->join('services_content', 'services_content.fid = services.service_id');
        $this->db->where('services_content.show',1);
        $this->db->where('services_content.language',$lang);
        $this->db->order_by('services.priority','asc');
        $this->db->order_by('services.service_id','desc');

        $query = $this->db->get();

//        dump_exit($query->result_array());

        return $query->result_array();
    }

    public function get_responsible($responsible_id, $lang)
    {
        if($lang == 'ru'){
            $this->db->select("name_ru as name, position_ru as position, photo_url, priority");
        }elseif($lang == 'ua'){
            $this->db->select("name_ua as name, position_ua as position, photo_url, priority");
        }else{
            $this->db->select("name_en as name, position_en as position, photo_url, priority");
        }
        $this->db->where('team_id',$responsible_id);
        $this->db->from('teams');
        $query = $this->db->get();

        return $query->row_array();
    }

    public function get_advantages($service_id, $lang)
    {
        $this->db->select('services.service_id as service_id, advantages.*');
        $this->db->from('services');
        $this->db->join('service_advantage', 'service_advantage.service_id = services.service_id');
        $this->db->join('advantages', 'advantages.advantage_id = service_advantage.advantage_id');
        $this->db->where('service_advantage.service_id',$service_id);
        $this->db->where('advantages.language',$lang);
        $this->db->order_by('service_advantage.priority','asc');

        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_benefits($service_id, $lang)
    {
        $this->db->select("services.service_id as service_id, benefits.title_$lang as title, benefits.description_$lang as description, benefits.icon, benefits.priority, benefits.mp");
        $this->db->from('services');
        $this->db->join('service_benefit', 'service_benefit.service_id = services.service_id');
        $this->db->join('benefits', 'benefits.benefit_id = service_benefit.benefit_id');
        $this->db->where('service_benefit.service_id',$service_id);
        $this->db->order_by('service_benefit.priority','asc');

        $query = $this->db->get();

//        dump_exit($query->result_array());

        return $query->result_array();
    }

    public function get_languages($service_id, $lang)
    {
        $this->db->select('services.service_id as service_id, categories.*');
        $this->db->from('services');
        $this->db->join('service_language', 'service_language.service_id = services.service_id');
        $this->db->join('categories', 'categories.category_id = service_language.language_id');
        $this->db->join('content', 'content.fid = service_language.language_id');
        $this->db->where('service_language.service_id',$service_id);
        $this->db->where('content.table','categories');
        $this->db->where('content.language',$lang);
        $this->db->order_by('service_language.priority','asc');

        $query = $this->db->get();

//        dump_exit($query->result_array());

        return $query->result_array();
    }
        
}
?>