<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Earthquakes_model extends MY_Crud
{
	//Имя таблицы
	public $table = 'quakes';
	//Имя ID
	public $idkey = 'quake_id';

	public function __construct()
	{
		parent::__construct();
	}

    public function get_by_period_quakes($date_begin, $date_end, $magnitude = false, $source = false, $lang){

//        $this->quakes_string();
//        $this->quakes_tree();
//        exit;
        $result = [];
        $count_strongest = 0;
        $max_magnitude = 0;
        $this->db->select('quake_id, datetime, latitude, longitude, magnitude, magnitude_type, depth, region, source, timestamp');
        $this->db->where('timestamp >=', strtotime($date_begin));
        $this->db->where('timestamp <=', strtotime($date_end));
        if($magnitude){
            $this->db->where('magnitude >=', $magnitude);
        }
        if($source){
            $this->db->where('src', $source);
        }
        $this->db->order_by('timestamp','DESC');

        $query = $this->db->get('quakes')->result_array();

        $result = [];
        $result['type'] = "FeatureCollection";
        $result['metadata'] = [];
        $result['metadata']['generated'] = time();
        $result['metadata']['url'] = "https://maps.ramir.space/earthquakes/json";
        $result['metadata']['title'] = "ALLATRA.maps All Earthquakes, Past Day";
        $result['metadata']['status'] = 200;
        $result['metadata']['api'] = "1.0.1";
        $result['features'] = [];
        $result['translations'] = [];

        if($lang == 'ru') {
            $this->lang->load('earthquakes', 'russian');
        } else if($lang == 'it'){
            $this->lang->load('earthquakes', 'italian');
        } else if($lang == 'cs'){
            $this->lang->load('earthquakes', 'czech');
        } else if($lang == 'sk'){
            $this->lang->load('earthquakes', 'slovenian');
        } else {
            $this->lang->load('earthquakes', 'english');
        }

        $result['translations'] = [
            'seismo' => $this->lang->line('seismo'),
            'monitoring' => $this->lang->line('monitoring'),
            'slogan' => $this->lang->line('slogan'),
            'add_to_favorites' => $this->lang->line('add_to_favorites'),
            'add_to_website' => $this->lang->line('add_to_website'),
            'all' => $this->lang->line('all'),
            'during_24_hours' => $this->lang->line('during_24_hours'),
            'yesterday' => $this->lang->line('yesterday'),
            'today' => $this->lang->line('today'),
            'choose_a_date' => $this->lang->line('choose_a_date'),
            'choose_a_period' => $this->lang->line('choose_a_period'),
            'date_begin' => $this->lang->line('date_begin'),
            'date_end' => $this->lang->line('date_end'),
            'apply' => $this->lang->line('apply'),
            'total_amount_of_the_earthquakes' => $this->lang->line('total_amount_of_the_earthquakes'),
            'the_strongest_earthquake' => $this->lang->line('the_strongest_earthquake'),
            'number_of_earthquakes_more_4' => $this->lang->line('number_of_earthquakes_more_4'),
            'solutions' => $this->lang->line('solutions'),
            'earthquakes_table' => $this->lang->line('earthquakes_table'),
            'entries_per_page' => $this->lang->line('entries_per_page'),
            'search' => $this->lang->line('search'),
            'no_entries_to_found' => $this->lang->line('no_entries_to_found'),
            'per_page' => $this->lang->line('per_page'),
            'region' => $this->lang->line('region'),
            'date' => $this->lang->line('date'),
            'magnitude' => $this->lang->line('magnitude'),
            'depth_km' => $this->lang->line('depth_km'),
            'location' => $this->lang->line('location'),
            'source' => $this->lang->line('source'),
            'programs_with_im_danilov' => $this->lang->line('programs_with_im_danilov'),
            'climate_change' => $this->lang->line('climate_change'),
            'tectonic_faults' => $this->lang->line('tectonic_faults'),
            'ctrl_d' => $this->lang->line('ctrl_d'),
            'modal_content' => $this->lang->line('modal_content'),
            'leave_comment' => $this->lang->line('leave_comment'),
            'share_your_opinion' => $this->lang->line('share_your_opinion'),
        ];

        // Определяем количество землетрясений выше 4-х баллов
        $cnt = count($query);
        for($i = 0 ; $i < $cnt ; ++$i){

            $result['features'][$i]['type'] = "Feature";
            $result['features'][$i]['properties'] = [];
            $result['features'][$i]['properties']['mag'] = $query[$i]['magnitude'];
            $result['features'][$i]['properties']['magType'] = $query[$i]['magnitude_type'];
            $result['features'][$i]['properties']['depth'] = $query[$i]['depth'];
            $result['features'][$i]['properties']['place'] = $query[$i]['region'];
            $result['features'][$i]['properties']['time'] = $query[$i]['timestamp'];
            $result['features'][$i]['properties']['datetime'] = date('d-m-Y H:i:s', $query[$i]['timestamp']);
            $result['features'][$i]['properties']['sources'] = $query[$i]['source'];
            $result['features'][$i]['properties']['type'] = "earthquake";
            $result['features'][$i]['properties']['ids'] = $query[$i]['quake_id'];
            $result['features'][$i]['geometry'] = [];
            $result['features'][$i]['geometry']['type'] = "Point";
            $result['features'][$i]['geometry']['coordinates'] = [$query[$i]['longitude'],$query[$i]['latitude']];
            $result['features'][$i]['id'] = $query[$i]['quake_id'];

            if((float)$query[$i]['magnitude'] > 4){
                ++$count_strongest;
            }
            if((float)$query[$i]['magnitude'] > $max_magnitude){
                $max_magnitude = $query[$i]['magnitude'];
            }

        }

        $result['metadata']['count'] = $cnt;
        $result['metadata']['strongest'] = $count_strongest;
        $result['metadata']['max_magnitude'] = $max_magnitude;

        $pages = $this->db->select('pages.page_id, pages.url, content.*')
            ->from('pages')
            ->join('content','content.fid = pages.page_id', 'left')
            ->where_in('pages.url',['earthquakes_map'])
            ->where('content.table', 'pages')
            ->where('content.language', $lang)
            ->get();

        $pages = $pages->result_array();

        $result['pages'] = $pages;

        $banners = $this->db->select('*')
            ->from('banners')
            ->where('lang', $lang)
            ->order_by('priority','asc')
            ->get();

        $banners = $banners->result_array();

        $result['banners'] = $banners;

        $rss = $this->db->select('*')
            ->get('rss');

        $rss = $rss->result_array();

        $result['rss'] = $rss;

        return json_encode($result);
    }

    public function quakes_tree(){
        $array = [];
        $query = $this->db->select('timestamp, magnitude')
            //->where('YEAR(FROM_UNIXTIME(timestamp)) =', date('Y'))
            ->order_by('timestamp','ASC')
            ->get('quakes')->result_array();
//        dump_exit($this->db->last_query());
//        dump_exit($query);
        $cnt = count($query);
//        dump_exit($cnt);
        if($cnt > 0){
            for($i = 0; $i < $cnt; ++$i){
                $year = date('Y', $query[$i]['timestamp']);
                $month = date('m', $query[$i]['timestamp']);
                $day = date('d', $query[$i]['timestamp']);

                $array[$year][$month][$day]['count_all'] = empty($array[$year][$month][$day]['count_all']) ? 1 : $array[$year][$month][$day]['count_all'] + 1;

                if((float)$query[$i]['magnitude'] > 4){
                    $array[$year][$month][$day]['count_strongest'] = empty($array[$year][$month][$day]['count_strongest']) ? 1 : $array[$year][$month][$day]['count_strongest'] + 1;
                }

                if(!empty($array[$year][$month][$day]['max_magnitude'])){
                    $array[$year][$month][$day]['max_magnitude'] = (float)$query[$i]['magnitude'] > $array[$year][$month][$day]['max_magnitude']? $query[$i]['magnitude'] : $array[$year][$month][$day]['max_magnitude'];
                } else {
                    $array[$year][$month][$day]['max_magnitude'] = $query[$i]['magnitude'];
                }

            }
        }

//        pre($array);

        return $array;
    }

    public function quakes_string(){
        $array = [
            'count_all' => '',
            'count_strongest' => '',
            'max_magnitude' => '',
        ];

        $tree = $this->quakes_tree();

//        pre_exit($tree);
        foreach($tree as $year => $value){
            foreach($value as $month => $value1){
                $month = $month-1;
                if(!empty($value1)){
                    foreach($value1 as $day => $value2){
                        $array['count_all'] .= '[Date.UTC('.$year.', '.$month.', '.$day.' ),'. (!empty($value2['count_all']) ? $value2['count_all'] : 0).'],';
                        $array['count_strongest'] .= '[Date.UTC('.$year.', '.$month.', '.$day.' ),'. (!empty($value2['count_strongest']) ? $value2['count_strongest'] : 0) .'],';
                        $array['max_magnitude'] .= '[Date.UTC('.$year.', '.$month.', '.$day.' ),'. (!empty($value2['max_magnitude']) ? $value2['max_magnitude'] : 0).'],';
                    }
                }
            }
        }

        $array['count_all'] = '['.substr($array['count_all'], 0, -1).']';
        $array['count_strongest'] = '['.substr($array['count_strongest'], 0, -1).']';
        $array['max_magnitude'] = '['.substr($array['max_magnitude'], 0, -1).']';

//        pre_exit($array);

        return $array;
    }

    public function quakes_array(){
        $array = [];

        $tree = $this->quakes_tree();

//        pre_exit($tree);
        $i = 0;
        foreach($tree as $year => $value){
            foreach($value as $month => $value1){
                foreach($value1 as $day => $value2){
                    $date = $day.'.'.$month.'.'.$year;
                    $array[$i]['date'] = $date;
                    $array[$i]['count_all'] = (!empty($value2['count_all']) ? $value2['count_all'] : 0);
                    $array[$i]['count_strongest'] = (!empty($value2['count_strongest']) ? $value2['count_strongest'] : 0);
                    $array[$i]['max_magnitude'] = (!empty($value2['max_magnitude']) ? $value2['max_magnitude'] : 0);

                    ++$i;
                }
            }
        }

//        pre_exit($array);

        return $array;
    }

    public function gather_data(){
        // Устанавливаем временную зону по умолчанию в UTC
        date_default_timezone_set('UTC');
        // globalincidentmap.com, здесь USGS Feed, GeoScience Australia, Earthquakes Canada
        $gim = @ file_get_contents('http://quakes.globalincidentmap.com');

        if(! empty($gim)) {
            $tmp_gim = explode('var earthquakes = ', $gim);
            $tmp_gim_1 = explode(';', $tmp_gim[1]);

            //strstr($tmp_gim[1],';',true);

            $array = json_decode(str_replace('\'', '"', $tmp_gim_1[0]), true);

            foreach($array as $key => $value){
                $array[$key]['datetime'] = date('Y-m-d H:i:s', $value['timestamp']);
            }
//            dump($array, 'globalincidentmap');
            if(!empty($array)){
                $this->add_new_data($array);
            }
        }
        // European-Mediterranean Seismological Centre (EMSC)
        $emcs = @ file_get_contents('http://www.emsc-csem.org/Earthquake/');

        if(! empty($emcs)) {
            $tmp = explode('<tbody id="tbody">', $emcs);
            $tmp_1 = explode('</tbody>', $tmp[1]);

            $dom = new DOMDocument;

//            $rows = mb_convert_encoding($tmp_1[0], 'HTML-ENTITIES', 'UTF-8');

            @ $dom->loadHTML($tmp_1[0]);

    //        echo $dom->saveHtml(), PHP_EOL;
            foreach ($dom->getElementsByTagName('tr') as $key => $tr) {
                $tds = $tr->getElementsByTagName('td');
                $count = $tds->length;

                $array_3[$key]['src'] = 'emcs';
                $array_3[$key]['source'] = 'EMSC Feed';
                $array_3[$key]['created'] = date('Y-m-d H:i:s');
                $array_3[$key]['updated'] = date('Y-m-d H:i:s');
//                dump($key, '$key');
                for ($i = 0 ; $i <= $count ; $i++){

                    if($i == 3){
                        $datetime = $tds->item($i)->nodeValue;
                        $array_dt = explode('.', $datetime);
                        $datetime = str_replace(['earthquake', '&nbsp;&nbsp;&nbsp;'], ['', ' '], htmlentities($array_dt[0]));
                        $array_3[$key]['datetime'] = $datetime;
                        $array_3[$key]['datetime_txt'] = date('l F d Y, H:i:s ', strtotime($datetime)).'UTC';
                        $array_3[$key]['timestamp'] = strtotime($datetime);
                        $datetime = '';
//                        dump($array_3);
                    }
                    if($i == 4){
                        $latitude = $tds->item($i)->nodeValue;
                        $latitude = trim(htmlentities($latitude), '&nbsp;');
                    }
                    if($i == 5){
                        $latitude_add = $tds->item($i)->nodeValue;
                        $latitude_add = trim(htmlentities($latitude_add), '&nbsp;');
                        $latitude_add = ($latitude_add == 'S') ? '-' : '';
                        $array_3[$key]['latitude'] = $latitude_add.$latitude;
                        $latitude = '';
                        $latitude_add = '';
                    }
                    if($i == 6){
                        $longitude = $tds->item($i)->nodeValue;
                        $longitude = trim(htmlentities($longitude), '&nbsp;');
                    }
                    if($i == 7){
                        $longitude_add = $tds->item($i)->nodeValue;
                        $longitude_add = trim(htmlentities($longitude_add), '&nbsp;');
                        $longitude_add = ($longitude_add == 'W') ? '-' : '';
                        $array_3[$key]['longitude'] = $longitude_add.$longitude;
                        $longitude = '';
                        $longitude_add = '';
                    }
                    if($i == 8){
                        $depth = $tds->item($i)->nodeValue;
                        $array_3[$key]['depth'] = trim(htmlentities($depth), '&nbsp;');
                        $depth = '';
                    }
                    if($i == 9){
                        $magnitude_type = $tds->item($i)->nodeValue;
                        $array_3[$key]['magnitude_type'] = trim($magnitude_type);
                        $magnitude_type = '';
                    }
                    if($i == 10){
                        $magnitude = $tds->item($i)->nodeValue;
                        $array_3[$key]['magnitude'] = trim(htmlentities($magnitude), '&nbsp;');
                        $magnitude = '';
                    }
                    if($i == 11){
                        $region = $tds->item($i)->nodeValue;
                        $array_3[$key]['region'] = ucwords(strtolower(trim(htmlentities($region), '&nbsp;')));
                        $region = '';
                    }
                }
            }

            if(!empty($array_3)){
                $this->add_new_data($array_3);
//                dump($array_3, 'EMCS');
            }
        }
        // Russian Academy of Sciences (RAS)
        $ras = @ file_get_contents('http://www.ceme.gsras.ru/new/eng/js/equake_last10.js');

        if(! empty($ras)) {
            // разбираем полученные данные
            $tmp_ras = explode('var aEquake = ', $ras);
            $tmp_ras_1 = explode(';', $tmp_ras[1]);
            $tmp_ras_2 = iconv('cp1251', 'UTF-8', $tmp_ras_1[0]);
            $tmp_ras_2 = str_replace('\'', '"', $tmp_ras_2);
            $tmp_ras_2 = str_replace(['{date:', ', lat:', ', lon:', ', depth:', ', nsta:', ', ms:', ', mpv:', ', i0:', ', name_ru:', ', name_en:', ':.', ':-.'], ['{"date":', ', "lat":', ', "lon":', ', "depth":', ', "nsta":', ', "ms":', ', "mpv":', ', "i0":', ', "name_ru":', ', "name_en":', ':0.', ':-0.'], $tmp_ras_2);
            $tmp_ras_2 = preg_replace('/\t/', '', $tmp_ras_2);
            $tmp_ras_2 = preg_replace('/\n/', '', $tmp_ras_2);
//            pre($tmp_ras_2);
            $tmp_array = json_decode($tmp_ras_2, true);
//            dump($tmp_array);

            foreach($tmp_array as $key => $value){
                $array_2[$key]['src'] = 'ras';
                $array_2[$key]['source'] = 'RAS Feed';
                $array_2[$key]['created'] = date('Y-m-d H:i:s');
                $array_2[$key]['updated'] = date('Y-m-d H:i:s');
                $array_2[$key]['datetime'] = $value['date'];
                $array_2[$key]['datetime_txt'] = date('l F d Y, H:i:s ', strtotime($value['date'])).'UTC';
                $array_2[$key]['timestamp'] = strtotime($value['date']);
                $array_2[$key]['latitude'] = $value['lat'];
                $array_2[$key]['longitude'] = $value['lon'];
                $array_2[$key]['depth'] = $value['depth'];
                $array_2[$key]['nst'] = $value['nsta'];
                $magnitude  = explode('/', $value['mpv']);
                $array_2[$key]['magnitude'] = $magnitude[0];
                $array_2[$key]['magnitude_type'] = 'mb';
                $array_2[$key]['region'] = $value['name_en'];
            }


            if(!empty($array_2)){
                $this->add_new_data($array_2);
//                dump($array_2, 'RAS');
            }
        }
        // Deutsches GeoForchungsZentrum (GFZ)
        $dgfz_link = 'http://eida.gfz-potsdam.de/webdc3/';
        $dgfz_link .= 'wsgi/event/geofon?';
        $dgfz_link .= 'start='.date('Y-m-d', strtotime('-2 day'));
        $dgfz_link .= '&end='.date('Y-m-d', strtotime('+1 day'));
        $dgfz_link .= '&minmag=0&maxmag=10&mindepth=0&maxdepth=999&minlat=-90&minlon=-180&maxlat=90&maxlon=180&format=json';
        $dgfz_link .= '&_='.time().rand(100,999);
//        dump($dgfz_link);
        $dgfz = @ file_get_contents($dgfz_link);

        if(! empty($dgfz)) {
            $tmp_dgfz =  json_decode($dgfz, true);
//            dump($tmp_dgfz);
            foreach($tmp_dgfz as $key => $value){
                if($key != 0){
                    $array_5[$key]['src'] = 'gfz';
                    $array_5[$key]['source'] = 'GFZ Feed';
                    $array_5[$key]['created'] = date('Y-m-d H:i:s');
                    $array_5[$key]['updated'] = date('Y-m-d H:i:s');
                    $array_5[$key]['datetime'] = date('Y-m-d H:i:s', strtotime(str_replace('T', '', $value[0])));
                    $array_5[$key]['datetime_txt'] = date('l F d Y, H:i:s ', strtotime($array_5[$key]['datetime'])).'UTC';
                    $array_5[$key]['timestamp'] = strtotime($array_5[$key]['datetime']);
                    $array_5[$key]['latitude'] = $value[3];
                    $array_5[$key]['longitude'] = $value[4];
                    $array_5[$key]['depth'] = $value[5];
                    $array_5[$key]['nst'] = '';
                    $array_5[$key]['magnitude'] = $value[1];
                    $array_5[$key]['magnitude_type'] = $value[2];
                    $array_5[$key]['region'] = $value[7];
                    $array_5[$key]['eqid'] = $value[6];
                }
            }
            if(!empty($array_5)){
                $this->add_new_data($array_5);
//                dump($array_5, 'GFZ');
            }
        }

        // The Unuviversity of Utah Seismograph Stations
        $uuss = @ file_get_contents('http://quake.utah.edu/eqMap/php/json_nonuu.php');

        if(! empty($uuss)) {

            $tmp_uuss = json_decode($uuss, true);
//            dump($tmp_uuss[0], '$uuss');

            $cnt_uuss = count($tmp_uuss);
            for($i = 0 ; $i < $cnt_uuss ; ++$i){
                $date = strtotime($tmp_uuss[$i]['event']['event_time_utc']);
                $array_4[$i]['src'] = 'uuss';
                $array_4[$i]['source'] = 'UUSS Feed';
                $array_4[$i]['created'] = date('Y-m-d H:i:s');
                $array_4[$i]['updated'] = date('Y-m-d H:i:s');
                $array_4[$i]['datetime'] = date('Y-m-d H:i:s', $tmp_uuss[$i]['event']['event_time_epoch']);
                $array_4[$i]['datetime_txt'] = date('l F d Y, H:i:s ', $tmp_uuss[$i]['event']['event_time_epoch']).'UTC';
                $array_4[$i]['timestamp'] = (int) $tmp_uuss[$i]['event']['event_time_epoch'];
                $array_4[$i]['latitude'] = $tmp_uuss[$i]['event']['lat'];
                $array_4[$i]['longitude'] = $tmp_uuss[$i]['event']['lng'];
                $array_4[$i]['depth'] = $tmp_uuss[$i]['event']['depth_km'];
                $array_4[$i]['magnitude'] = $tmp_uuss[$i]['event']['magnitude'];
                $array_4[$i]['magnitude_type'] = '';
                $array_4[$i]['region'] = 'Near Yellowstone National Park';
                $array_4[$i]['eqid'] = $tmp_uuss[$i]['event']['evid'];
            }
//
//            dump($array_4[0], '$array_4');
            if(!empty($array_4)){
                $this->add_new_data($array_4);
//                dump($array_4, 'UUSS');
            }
        }



    }

	// Добавляем новые данные, переданные через форму, в базу
	public function add_new_data($array){
        foreach($array as $key => $value) {
//            $this->db->where('src', $value['src']);
            if(!empty($value['timestamp'])){
                $this->db->where('timestamp', $value['timestamp']);
                $result = $this->db->get($this->table)->row();
                if(empty($result)){
                    $this->db->insert($this->table, $value);
                }
            }
        }
	}

    // Запрос землетрясений по дням недели (весь период)
    public function count_earthquakes_by_day_of_the_week($date_begin = false){
        //dump_exit($date_begin);
        $query = $this->db->query('SELECT COUNT(`quake_id`) as quakes_numb, WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), \'%Y-%m-%d %H:%i:%s\')) as weekday FROM quakes GROUP BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), \'%Y-%m-%d %H:%i:%s\')) ORDER BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), \'%Y-%m-%d %H:%i:%s\'))');

        return $query->result_array();
    }

    // Запрос землетрясений по дням недели (запрашиваемый период)
    public function count_earthquakes_by_day_of_the_week_by_period($days_ago, $date_begin = false, $date_end = false){
//        if($date_begin && $date_end){
//            $date_begin = strtotime($date_begin.' 00:00:00');
//            $date_end = strtotime($date_end.' 23:59:59');
//            $query = $this->db->query("SELECT COUNT(`quake_id`) as quakes_numb, DAYNAME(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) AS day_name FROM `quakes` WHERE `timestamp` >= $date_begin AND`timestamp` <= $date_end GROUP BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) ORDER BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s'))");
////            dump($this->db->last_query());
//        }elseif($date_begin){
//            $date_begin = strtotime($date_begin.' 00:00:00');
//            $query = $this->db->query("SELECT COUNT(`quake_id`) as quakes_numb, DAYNAME(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) AS day_name FROM `quakes` WHERE `timestamp` >= $date_begin GROUP BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) ORDER BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s'))");
//        }elseif($date_end){
//            $date_end = strtotime($date_end.' 23:59:59');
//            $query = $this->db->query("SELECT COUNT(`quake_id`) as quakes_numb, DAYNAME(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) AS day_name FROM `quakes` WHERE `timestamp` <= $date_end GROUP BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) ORDER BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s'))");
//        }else{
            $query = $this->db->query("SELECT COUNT(`quake_id`) as quakes_numb, DAYNAME(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) AS day_name FROM `quakes` WHERE DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s') BETWEEN CURDATE() - INTERVAL $days_ago DAY AND CURDATE() GROUP BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s')) ORDER BY WEEKDAY(DATE_FORMAT(FROM_UNIXTIME(`timestamp`), '%Y-%m-%d %H:%i:%s'))");
//        }
        return $query->result_array();
    }

}