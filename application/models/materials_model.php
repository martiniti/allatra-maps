<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Materials_model extends MY_Crud
{
    
    public $table = 'materials'; //Имя таблицы	
    public $idkey = 'material_id'; //Имя ID
    
    public function __construct()
    {       
        parent::__construct();
    }

    public function get_photos($material_id, $lang = 'ru')
    {
        $query = $this->db->select("photo_id, title_$lang as title, img_url, priority")
                          ->where('material',$material_id)
                          ->order_by('priority','asc')
                          ->get('photos');
        return $query->result_array();
    }
    
    // обновление значения счетчика просмотров
    public function update_counter($material_id,$counter_data)
    {
        $this->db->where('material_id',$material_id);
        $this->db->update('materials',$counter_data);
    } 
        
}
?>