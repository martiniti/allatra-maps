<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends MY_Crud
{
    
public $table = 'categories'; //Имя таблицы	
public $idkey = 'category_id'; //Имя ID

public function __construct()
{       
    parent::__construct();
}

public function get_parents()
{
    $this->db->where('parent',0);
    $query = $this->db->get('categories');
    
    return $query->result_array();
}

public function get_subs($catalog_id)
{
    $this->db->order_by('priority','asc');
    $this->db->where('parent',$catalog_id);
    $query = $this->db->get('categories');
    
    return $query->result_array();
}

public function count_products_by($catalog_id, $lang)
{
    $this->db->from('products');
    $this->db->join('content', 'content.fid = products.product_id');  
    $this->db->where('products.category',$catalog_id);    
    $this->db->where('content.show',1);
    $this->db->where('content.table','products'); 
    $this->db->where('content.language',$lang); 
    $this->db->order_by('products.priority','asc');
    $this->db->order_by('products.product_id','desc');
    
    return $this->db->count_all_results();
}

public function get_products_by($catalog_id, $limit, $start_from, $lang)
{
    $this->db->select('products.*, content.title as name, content.anons as anons, content.img_url as image');
    $this->db->from('products');
    $this->db->join('content', 'content.fid = products.product_id');  
    $this->db->where('products.category', $catalog_id);    
    $this->db->where('content.show', 1);
    $this->db->where('content.table', 'products'); 
    $this->db->where('content.language', $lang); 
    $this->db->order_by('products.priority', 'asc');
    $this->db->order_by('products.product_id', 'desc');
    $this->db->limit($limit, $start_from);     
    
    $query = $this->db->get();
    
    return $query->result_array();
}

function get_overview()
{
    $data = array();
    $data = $this->db->select('category_id, title, parent')
                 ->from('categories')
                 ->get()
                 ->result_array('category_id');

    
    $tree = array();
    
    reset($data);    
    while (list($k, $v) = each($data))
        if (0 == ($pid = $v['parent']))
            $tree[$k] =& $data[$k]; else
            $data[$pid]['children'][$k] =& $data[$k];
    
    reset($data);    
    while (list($k, $v) = each($data))
        if (0 != $v['parent'])
        {
            $ref =& $data[$k];
            $depth = 0;
            do
            {
                if ($depth) $ref =& $data[$ref['parent']];
                $dre =& $ref['depth'];
                if (!isset($dre) || $dre <= $depth) $dre =  $depth++;
                if (isset($ref['children']))
                    $ref['child_count'] = count($ref['children']);
                    else
                {
                    $ref['child_count'] = 0;
                    $ref['children'] = null;
                }
            }
            while ($ref['parent']);
        }

    return $tree;
    
}

    public function get_cats($parent_id = 0, $lang)
    {
        $this->db->select('categories.category_id as category_id, categories.url as url, categories.priority as priority, categories.img_url as img, content.*');
        $this->db->from('categories');
        $this->db->join('content', 'content.fid = categories.category_id');
        $this->db->where('categories.parent',$parent_id);
        $this->db->where('content.show',1);
        $this->db->where('content.table','categories');
        $this->db->where('content.language',$lang);
        $this->db->order_by('categories.priority','asc');
        $this->db->order_by('categories.category_id','desc');

        $query = $this->db->get();

        return $query->result_array() ;
    }

    function get_tree($pid, $lang)
    {
        $tree = $this->db->select('categories.category_id, categories.url, categories.parent, content.title')
                         ->from('categories')
                         ->join('content', 'content.fid = categories.category_id')
                         ->where('content.show',1)
                         ->where('content.table','categories')
                         ->where('content.language',$lang)
                         ->order_by('categories.priority','asc')
                         ->order_by('categories.category_id','desc')
                         ->get()
                         ->result_array('category_id');
        $html = '';

        if($lang == 'ru'){
            $lang_slug = '';
        }else{
            $lang_slug = $lang.'/';
        }

        foreach ($tree as $row)
        {
            if ($row['parent'] == $pid)
            {
                $html .= '<li>' . "\n";
                $html .= '<a href='.base_url($lang_slug.$row['url']).'><i class="fa fa-angle-right"></i>   ' . $row['title'] . "\n".'</a>';
                $html .= '    ' . $this->get_tree($row['category_id'], $lang);
                $html .= '</li>' . "\n";
            }
        }

        return $html ? '<ul class="links">' . $html . '</ul>' . "\n" : '';
    }


    function top_parent($category_id)
    {
        $data = $this->db->select('category_id, title, parent')
                         ->where('category_id',$category_id)
                         ->get('categories')
                         ->row_array();

        if($data['parent'] == 0){
            return $data['category_id'];
        }
        else{
            return $this->top_parent($data['parent']);
        }

    }

    function get_title($top_parent)
    {
        $data = $this->db->select('category_id, title')
                         ->where('category_id',$top_parent)
                         ->get('categories')
                         ->row_array();

        return $data['title'];

    }

    function get_back_url($parent)
    {
        $data = $this->db->select('category_id, url')
                         ->where('category_id',$parent)
                         ->get('categories')
                         ->row_array();

        return $data['url'];

    }

    public function catalog_reorder($items)
    {
    $total_items = count($this->input->post('item'));

    for($item = 0; $item < $total_items; $item++ )
        {
            $data = array(
            'category_id' => $items[$item],
            'priority' => $rank = $item
            );

        $this->db->where('category_id', $data['category_id']);
        $this->db->update('categories', $data);

        }

    return;

    }

    public function get_responsible($responsible_id, $lang)
    {
        $this->db->where('team_id',$responsible_id);
        $this->db->where('language',$lang);
        $query = $this->db->get('teams');

        return $query->row_array();
    }
        
}
?>