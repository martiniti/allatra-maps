<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends MY_Crud
{
    
public $table = 'products'; //Имя таблицы	
public $idkey = 'product_id'; //Имя ID

public function __construct()
{       
    parent::__construct();
}

public function get_similar_products($catalog_id, $limit, $lang)
{
    $this->db->select('products.*, content.title as name, content.anons as anons, content.img_url as image');
    $this->db->from('products');
    $this->db->join('content', 'content.fid = products.product_id');  
    $this->db->where('products.category', $catalog_id);    
    $this->db->where('content.show', 1);
    $this->db->where('content.table', 'products'); 
    $this->db->where('content.language', $lang); 
    $this->db->order_by('products.priority', 'asc');
    $this->db->order_by('products.product_id', 'desc');
    $this->db->limit($limit);     
    
    $query = $this->db->get();
    
    return $query->result_array();
}
        
}
?>