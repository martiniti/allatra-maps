<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Crud extends CI_Model
{
    
    public $table = ''; //Имя таблицы	
    public $idkey = ''; //Имя ID	

    function __construct()
     {
         // Call the Model constructor
         parent::__construct();
     }
    
    // Получение данных об одной записи
    public function get($obj_id)
    {
        $this->db->where($this->idkey,$obj_id);
        $query = $this->db->get($this->table);
        
        return $query->row_array();
    }
    
    // Получение данных об одной записи по url;
    public function get_by_url($url)
    {
        $this->db->where('url',$url);
        $query = $this->db->get($this->table);
        
        return $query->row_array();
    }
    
    public function get_content($obj_id, $lang)
    {
        $this->db->where('fid',$obj_id);
        $this->db->where('table',$this->table);
        $this->db->where('language',$lang);
        $query = $this->db->get('content');
        
        return $query->row_array();
    }

    public function get_service_content($obj_id, $lang)
    {
        $this->db->where('fid',$obj_id);
        $this->db->where('language',$lang);
        $query = $this->db->get('services_content');

        return $query->row_array();
    }

    // Подсчет общего числа записей
    public function count_all()
    {
        return $this->db->count_all($this->table);
    }
    
    public function count_by($section)
    {
        $this->db->where('section',$section);
        
        return $this->db->count_all_results($this->table);
    }

    public function check_languages($obj_id,$language)
    {
        $this->db->select('language');
        $this->db->where('fid',$obj_id);
        $this->db->where('table',$this->table);
        //$this->db->where('language !=',$language);
        $query = $this->db->get('content')->result_array();

        $languages = array();
        for($i = 0; $i<count($query); $i++){
            $languages[$i] = $query[$i]['language'];
        }
        return $languages;

    }
    
   	//this is for building a menu
	function get_menu_tierd($parent=0,$lang = 'ru')
	{
		
        $menu_array	= array();
		$result	= $this->get_menu($parent, $lang);

		foreach ($result as $menu)
		{
			$menu_array[$menu->menu_id]['menu']	= $menu;
			$menu_array[$menu->menu_id]['children']	= $this->get_menu_tierd($menu->menu_id, $lang);
		}
        
		return $menu_array;
	}
    
    function get_menu($parent = false, $lang = 'ru')
	{
		$menu	= array();
		if ($parent !== false)
		{
			$this->db->where('parent_id', $parent);
		}
		$this->db->select('menu_id');
        $this->db->where('show',1);
		$this->db->order_by('menu.sequence', 'ASC');

		//this will alphabetize them if there is no sequence
		$this->db->order_by('title', 'ASC');
		$results	= $this->db->get('menu')->result();
//		dump($results);
		if (!empty($results)){
			foreach($results as $result)
			{
				$sec = $this->get_menu_item($result->menu_id, $lang);
				if (!empty($sec)){
					$menu[]	= $sec;
				}
			}
		}

		return $menu;
	}
    
    function get_menu_item($id, $lang = 'ru')
	{
		$res = array();
		$this->db->select('menu.menu_id as menu_id,
						menu.parent_id as parent_id,
						menu.url as url,
						menu.sequence as sequence,
						menu_content.title as title');
		$this->db->from('menu');
		$this->db->join('menu_content', 'menu_content.fid = menu.menu_id');
		$this->db->where('menu.menu_id', $id);
		$this->db->where('menu_content.language',$lang);

		$result = $this->db->get()->row();

		if (!empty($result)) {
			$res = $result;
		}

		return $res;
	}
    
    public function get_latest_news($limit,$lang)
    {
        $this->db->select('materials.*, content.title as content_title, content.img_url as content_img_url, content.anons as content_anons');
        $this->db->from('materials');
        $this->db->join('content', 'content.fid = materials.material_id');        
        $this->db->where('content.show',1);
        $this->db->where('content.table','materials'); 
        $this->db->where('content.language',$lang); 
        $this->db->where('content.video_url','');
        $this->db->order_by('materials.date','desc');
        $this->db->order_by('materials.material_id','desc');
        $this->db->limit($limit);

        $query = $this->db->get(); 
    
        return $query->result_array() ;
    }
    
   	// Преобразование POST массива в ассоциативный массив
	public function array_from_post($fields, $exceptions = array())
	{
		$data = array();
		foreach ($fields as $field) {
			// Подготавливаем необходимые переменные
            if(in_array($field, $exceptions)){
			     $data[$field] = $this->input->post($field, TRUE);
            }
            else {
                $data[$field] = form_prep($this->input->post($field, TRUE));
            }
		}

		return $data;
	}

    public function get_banners($lang)
    {
        $this->db->select("*");
        $this->db->where('lang',$lang);
        $this->db->order_by('priority','asc');
        $query = $this->db->get('banners');

        return $query->result_array();
    }

    public function get_photos_of_the_day($lang)
    {
        $sections = $this->db->select('section_id')
                             ->where('type',4)
                             ->get('sections')
                             ->result_array();

        $sections_array = array();

        foreach ($sections as $section){
            $sections_array[] = $section['section_id'];
        }

        /* SELECT MATERIAL WITH PHOTO SECTION */
        $results = $this->db->select('material_id, url, materials.title as title')
                            ->from('materials')
                            ->join('content', 'content.fid = materials.material_id')
                            //->where_in('section',$sections_array)
                            //only Photo exhibition!
                            ->where('section',2)
                            ->where('show',1)
                            ->where('content.table','materials')
                            ->where('content.language',$lang)
                            ->order_by('material_id','random')
                            ->limit(1)
                            ->get();

        $sections	= array();

        foreach ($results->result_array() as $result)
        {
            $sections[$result['material_id']]['section']	  = $result;
            $sections[$result['material_id']]['children']	  = $this->get_photos($result['material_id'],$lang);
        }

        //dump_exit($sections);

        return $sections;

    }

    public function get_photos($material_id, $lang = 'ru')
    {
        /* SELECT PHOTOS WITH SELECTED MATERIAL_ID */
        $query = $this->db->select("photo_id, title_$lang as title, img_url, priority")
                          ->where('material',$material_id)
                          ->order_by('priority','asc')
                          ->limit(20)
                          ->get('photos');

        return $query->result_array();
    }

    public function get_video_of_the_day($lang)
    {
        $sections = $this->db->select('section_id')
                             ->get('sections')
                             ->result_array();

        $sections_array = array();

        foreach ($sections as $section){
            $sections_array[] = $section['section_id'];
        }

        /* SELECT MATERIAL WITH PHOTO SECTION */
        $results = $this->db->select('material_id, url, content.title as title, content.video_url as video_url')
                            ->from('materials')
                            ->join('content', 'content.fid = materials.material_id')
                            ->where_in('section',$sections_array)
                            ->where('show',1)
                            ->where('content.table','materials')
                            ->where('content.language',$lang)
                            ->where('content.video_url !=','')
                            ->order_by('material_id','random')
                            ->limit(1)
                            ->get()
                            ->result_array();
        
        return $results;

    }
    
    public function get_farewell($lang)
    {
        $query = $this->db->select("farewell_id, text_$lang as text, author_$lang as author")
            ->from('farewells')
            ->order_by('rand()')
            ->limit(1)
            ->get();

        return $query->row_array();
    }

    public function get_content_with_material_url($obj_id, $lang)
    {
        $this->db->select('content.*, materials.url as url');
        $this->db->from('content');
        $this->db->join('materials', "materials.material_id = content.fid");
        $this->db->where('content.fid',$obj_id);
        $this->db->where('content.table',$this->table);
        $this->db->where('content.language',$lang);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function get_childrens_categories($pid = 0,$lang = 'ru')
    {
        $this->db->select('categories.category_id as category_id, categories.url as url, categories.priority as priority, categories.img_url as img, categories.icon, content.*');
        $this->db->from('categories');
        $this->db->join('content', 'content.fid = categories.category_id');
        $this->db->where('categories.parent',$pid);
        $this->db->where('content.show',1);
        $this->db->where('content.table','categories');
        $this->db->where('content.language',$lang);
        $this->db->order_by('categories.priority','asc');
        $this->db->order_by('categories.category_id','desc');

        $query = $this->db->get();

        return $query->result_array() ;
    }

    function get_clients($language = 'ru')
    {
        $this->db->select('*');
        $this->db->order_by('date', 'DESC');
        $this->db->limit(12);
        $this->db->where('foreign_id', 32);
        $this->db->where('active', 1);
        $this->db->where('language', $language);
        $result = $this->db->get('comments')->result_array();

        return $result;
    }

    function generete_dtp_json($language = 'ru')
    {
        $this->db->select('*');
        $this->db->order_by('date', 'DESC');
        $this->db->limit(12);
        $this->db->where('foreign_id', 32);
        $this->db->where('active', 1);
        $this->db->where('language', $language);
        $result = $this->db->get('comments')->result_array();

        return $result;
    }

}
?>