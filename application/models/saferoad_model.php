<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Saferoad_model extends MY_Crud
{
	//Имя таблицы
	public $table = 'geodata';
	//Имя ID
	public $idkey = 'geodata_id';

	public function __construct()
	{
		parent::__construct();
	}

    public function get_geodata($lang)
    {
        $query = $this->db->select('*')
            ->from('geodata')
            ->where('type !=','Polygon')
            ->order_by('geodata_id','asc')
            ->get();

        $query = $query->result_array();

        $banners = $this->db->select('*')
            ->from('banners')
            ->where('lang', $lang)
            ->order_by('priority','asc')
            ->get();

        $banners = $banners->result_array();

        $pages = $this->db->select('pages.page_id, pages.url, content.*')
            ->from('pages')
            ->join('content','content.fid = pages.page_id', 'left')
            ->where_in('pages.url',['road_experiment','road_experiment_map','road_experiment_abnormal_places'])
            ->where('content.table', 'pages')
            ->where('content.language', $lang)
            ->get();

        $pages = $pages->result_array();


        $rss = $this->db->select('*')
            ->where_in('link',['https://allatra.tv/rss/publications?category=im-danilov','https://allatra.tv/rss/publications?category=climate'])
            ->get('rss');

        $rss = $rss->result_array();


        //prevent large digits after decimal point in coordinates
        ini_set('serialize_precision','-1');

        //Create JSON
        $result = [];
        $result['type'] = "FeatureCollection";
        $result['metadata'] = [];
        $result['metadata']['generated'] = time();
        $result['metadata']['url'] = "https://maps.ramir.space/saferoad/json";
        $result['metadata']['title'] = "ALLATRA.maps :: Road Experiment";
        $result['metadata']['status'] = 200;
        $result['metadata']['api'] = "1.0.1";
        $result['features'] = [];
        $result['banners'] = $banners;
        $result['pages'] = $pages;
        $result['rss'] = $rss;

        $cnt = count($query);

        for($i = 0 ; $i < $cnt ; ++$i){

            $coordinates = json_decode($query[$i]['coordinates']);

            $result['features'][$i]['type'] = "Feature";
            $result['features'][$i]['properties'] = [];
            $result['features'][$i]['properties']['image'] = $query[$i]['image'];
            $result['features'][$i]['properties']['datetime'] = date('d.m.Y', strtotime($query[$i]['date']));
            $result['features'][$i]['properties']['sources'] = $query[$i]['source'];
            $result['features'][$i]['properties']['type'] = "dangerous_place";
            $result['features'][$i]['properties']['description'] = $query[$i]['description'];
            $result['features'][$i]['properties']['ids'] = $query[$i]['geodata_id'];
            $result['features'][$i]['geometry'] = [];
            $result['features'][$i]['geometry']['type'] = $query[$i]['type'];
            $result['features'][$i]['geometry']['coordinates'] = $coordinates;
            $result['features'][$i]['id'] = $query[$i]['geodata_id'];
            if($query[$i]['status'] == 0){
                $result['features'][$i]['properties']['icon'] = 'whiteIcon';
            } else {
                if($query[$i]['officially'] == 0){
                    $result['features'][$i]['properties']['icon'] = 'orangeIcon';
                } else {
                    $result['features'][$i]['properties']['icon'] = 'redIcon';
                }
            }

        }

        return json_encode($result);

    }

}