<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articles_model extends MY_Crud
{
    
public $table = 'articles'; //Имя таблицы	
public $idkey = 'article_id'; //Имя ID

public function __construct()
{       
    parent::__construct();
}

public function get_artsec($artsec_id)
{
    $this->db->where('artsec_id',$artsec_id);
    $query = $this->db->get('artsec');
    
    return $query->row_array();
}

public function count_by($artsec_id)
{
    $this->db->where('section',$artsec_id);
    
    return $this->db->count_all_results('articles');
}

public function all_with_pag($artsec_id,$limit,$start_from)
{
    $this->db->order_by('article_id','desc');
    $this->db->where('section',$artsec_id);
    $this->db->limit($limit,$start_from); 
    
    $query = $this->db->get('articles');
    
    return $query->result_array();
}

public function get_menu()
{
    /* Меню категорий статтей, исключаем распродажу, ее отдельно*/
    $query = $this->db->not_like('artsec_id', 5)
                      ->get('artsec');
    
    return $query->result_array();
}
        
}
?>