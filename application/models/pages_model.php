<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages_model extends MY_Crud
{    
    
    public $table = 'pages'; //Имя таблицы	
    public $idkey = 'page_id'; //Имя ID
       
    
    
    // правила для контактной формы
    public $contact_rules = array
    (   
       array
       (
         'field' => 'name',
         'label' => 'Имя',
         'rules' => 'trim|required|xss_clean|max_length[120]'
       ),
       array
       (
         'field' => 'email',
         'label' => 'Е-mail',
         'rules' => 'trim|required|valid_email|xss_clean|max_length[70]'
       ),
       array
       (
         'field' => 'topic',
         'label' => 'Тема сообщения',
         'rules' => 'xss_clean|max_length[70]'
       ),        
       array
       (
         'field' => 'message',
         'label' => 'Сообщение',
         'rules' => 'required|xss_clean|max_length[5000]'
       ),
       array
       (
         'field' => 'captcha',
         'label' => 'Цифры с картинки',
         'rules' => 'required|numeric|exact_length[5]'
       )
    );   

    public function __construct()
    {       
        parent::__construct();
        //Переменные с дирректориями хранения изображений;
        $this->file_path = realpath(APPPATH . '../assets/uploads/files/'); 
    }    

    public function get_wellcome_page()
    {
    	$query = $this->db->get_where('pages', array('wp' => 1));
    	return $query->row_array();
    }
    
    public function get_slider($lang)
    {
        $query = $this->db->select('*')
                          ->from('slides')
                          ->order_by('priority','asc')
                          ->where('language',$lang)
                          ->limit(10)
                          ->get();
    
        return $query->result_array() ;
    }

    public function get_geodata()
    {
        $query = $this->db->select('*')
            ->from('geodata')
            ->order_by('geodata_id','asc')
            ->get();

        return $query->result_array() ;
    }

    public function get_advantages($lang)
    {
        $query = $this->db->select("*")
            ->from('advantages')
            ->where('language',$lang)
            ->where('mp',1)
            ->order_by('priority','asc')
            ->limit(6)
            ->get();

        return $query->result_array() ;
    }

    public function get_benefits($lang)
    {
        $query = $this->db->select("title_$lang as title, description_$lang as description, icon, priority, mp")
            ->from('benefits')
            ->where('benefits.mp',1)
            ->order_by('priority','asc')
            ->limit(12)
            ->get();

        return $query->result_array() ;
    }

    function get_clients_comments($language = 'ru')
    {
        $this->db->select('*');
        $this->db->order_by('date', 'DESC');
        $this->db->limit(10);
        $this->db->where('foreign_id', 32);
        $this->db->where('active', 1);
        $this->db->where('language', $language);
        $result = $this->db->get('comments')->result_array();

        return $result;
    }

    public function get_faqs($lang)
    {
        $query = $this->db->select("*")
            ->from('faq')
            ->where('language',$lang)
            ->order_by('priority','asc')
            ->get();

        return $query->result_array() ;
    }

    public function get_team($lang)
    {
        if($lang == 'ru'){
            $this->db->select("name_ru as name, position_ru as position, photo_url, priority");
        }elseif($lang == 'ua'){
            $this->db->select("name_ua as name, position_ua as position, photo_url, priority");
        }else{
            $this->db->select("name_en as name, position_en as position, photo_url, priority");
        }
        $this->db->from('teams');
        $this->db->order_by('priority','asc');
        $query = $this->db->get();

        return $query->result_array() ;
    }

    public function get_subjects($lang)
    {
        $query = $this->db->select("subject_id, title_$lang as title, img_url, priority")
            ->from('subjects')
            ->order_by('priority','asc')
            ->limit(6)
            ->get(); 

        return $query->result_array() ;
    }

    public function get_partners($lang)
    {
        $query = $this->db->select('*')
                          ->from('partners')
                          ->order_by('priority','asc')
                          ->where('language',$lang)
                          ->limit(24)
                          ->get();
    
        return $query->result_array() ;
    }
    
    public function get_prices($lang)
    {
        $query = $this->db->select('*')
                          ->from('prices')
                          ->order_by('priority','asc')
                          ->where('language',$lang)
                          ->limit(24)
                          ->get();
    
        return $query->result_array() ;
    }

    public function get_video()
    {
        $query = $this->db->order_by('video_id','desc')
                          ->get('video');
        return $query->result_array();
    }
    
    public function get_certificates($lang = 'ru')
    {
        $query = $this->db->where('language',$lang)
                          ->order_by('priority','asc')
                          ->order_by('certificate_id','desc')
                          ->get('certificates');
                          
        return $query->result_array();
    }

    function userfile_check()
    {
        $config['allowed_types']  = 'jpg|jpeg|gif|png';  
        $config['upload_path']    = $this->file_path;
        $config['max_size']    = '3000';
        
        $this->load->library('upload', $config);
          
        if ($this->upload->do_upload()){         
            
            return $this->upload->data();
        
        } else {
            
            echo $this->upload->display_errors();
            return FALSE;
            
        }
    }

    public function get_all_farewells($lang)
    {
        $query = $this->db->select("farewell_id, text_$lang as text, author_$lang as author")
            ->from('farewells')
            ->order_by('farewell_id','desc')
            ->get();

        return $query->result_array();
    }

    public function get_banner_wp($position,$lang)
    {
        $this->db->select("*");
        $this->db->where('lang',$lang);
        $this->db->where('position',$position);
        $this->db->order_by('priority','asc');
        //$this->db->limit(1);
        $query = $this->db->get('banners');

        return $query->result_array();
    }

    public function get_cost()
    {
        $query = $this->db->select("*")
            ->from('cost')
            ->get();

        return $query->result_array() ;
    }

    public function get_cost_autos()
    {
        $query = $this->db->select("cost_autos.id as id, cost_autos.name as name, cost_autos.img_url as img_url, cost_types.name as type")
                          ->from('cost_autos')
                          ->join('cost_types', 'cost_autos.type = cost_types.id', 'LEFT')
                          ->get();

        return $query->result_array() ;
    }

    public function get_cost_services()
    {
        $query = $this->db->select("*")
            ->from('cost_services')
            ->get();

        return $query->result_array() ;
    }

}
?>