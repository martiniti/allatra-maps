<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administration_model extends CI_Model
{
    
public function __construct()
{       
    parent::__construct();
    $this->get_preferences();
}
//правила для страницы настроек
public $preferences_rules = array
(
    array
    (
      'field' => 'admin_login',
      'label' => 'Логин',
      'rules' => 'alpha_dash|trim|required|max_length[50]'
    ),
    array
    (
      'field' => 'admin_pass',
      'label' => 'Пароль',
      'rules' => 'alpha_dash|trim|required|max_length[50]'
    )
);

//Считывание настроек из базы в массив config для дальнейшего использования
public function get_preferences()
{
    $query = $this->db->get('preferences');
    
    //Получаем в переменную массив со всеми настройками
    $preferences = $query->result_array();
    
    foreach ($preferences as $item)
    {
        $val = $item['value']; 
                
        if(is_numeric($val))
        {
            settype($val,"int");
        }     
        
        //Устанавливаем элементу значение
        $this->config->set_item($item['pref_id'],$val);          
    }    
}
//правила для страницы логина
public $login_rules = array
(
    array
    (
      'field' => 'login',
      'label' => 'Логин',
      'rules' => 'trim|required|alpha_dash|max_length[50]'
    ),
    array
    (
      'field' => 'pass',
      'label' => 'Пароль',
      'rules' => 'trim|required|alpha_dash|max_length[50]'
    )
);

// Формирование RSS-ленты
public function feeds_info()
{
    $this->db->order_by('news_id','desc');
    $this->db->limit(20);
    $query = $this->db->get('news');
    
    //Возвращаем массив с материалами для формирования ленты
    return $query->result_array();
}

}
?>