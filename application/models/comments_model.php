<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comments_model extends MY_Crud
{

public $table = 'comments'; //Имя таблицы
public $idkey = 'comment_id'; //Имя ID

//правила для добавления комментариев
public $add_rules = array
(
    array
    (
      'field' => 'parent_id',
      'label' => 'Parent ID',
      'rules' => 'trim|required|xss_clean'
    ),
    array
    (
      'field' => 'author',
      'label' => 'Имя',
      'rules' => 'trim|required|xss_clean|max_length[70]'
    ),
    array
    (
      'field' => 'email',
      'label' => 'Email',
      'rules' => 'trim|required|valid_email|xss_clean|max_length[70]'
    ),
    array
    (
      'field' => 'comment_text',
      'label' => 'Текст комментария',
      'rules' => 'required|max_length[65535]'
    ),
    /*
    array
    (
     'field' => 'rating',
     'label' => 'Рейтинг',
     'rules' => 'required|numeric|xss_clean|exact_length[1]'
    ),
    */
    array
    (
     'field' => 'captcha',
     'label' => 'Цифры с картинки',
     'rules' => 'required|numeric|exact_length[5]'
    )
);

//правила для добавления комментариев
public $add_logged_rules = array
(
    array
    (
      'field' => 'parent_id',
      'label' => 'Parent ID',
      'rules' => 'trim|required|xss_clean'
    ),
    array
    (
      'field' => 'author',
      'label' => 'Имя',
      'rules' => 'trim|required|xss_clean|max_length[70]'
    ),
    array
    (
      'field' => 'email',
      'label' => 'Email',
      'rules' => 'trim|required|valid_email|xss_clean|max_length[70]'
    ),
    array
    (
      'field' => 'comment_text',
      'label' => 'Текст комментария',
      'rules' => 'required|max_length[65535]'
    )
);

//правила для редактирования комментариев
public $edit_rules = array
(
    array
    (
      'field' => 'name',
      'label' => 'ID',
      'rules' => 'trim|required|integer|xss_clean|max_length[11]'
    ),
    array
    (
      'field' => 'value',
      'label' => 'Имя',
      'rules' => 'required|xss_clean|max_length[65535]'
    )
);

function get_comments($parent = 0, $limit = false, $language = 'ru')
{
	$this->db->select('comments.*,users.image as image,');
    $this->db->order_by('date', 'DESC');

    if($limit)
	{
		$this->db->limit($limit);
	}

	$this->db->where('parent_id', $parent);
    $this->db->where('active', 1);
    $this->db->where('language', $language);
    $this->db->join('users', 'comments.uid = users.id', 'LEFT');
	$result = $this->db->get('comments')->result();

	$return	= array();
	foreach($result as $comment)
	{

        if($comment->parent_id == 0){
            $comment->parent_author = '';
        }else{
            $parent_comment = $this->get_comment($comment->parent_id);
            $comment->parent_author = $parent_comment->author;
        }
       
		$return[$comment->comment_id]				= $comment;
		$return[$comment->comment_id]->children	    = $this->get_comments($comment->comment_id);
        
	}

	return $return;
}

function get_main_comments($limit = FALSE, $lang = 'ru')
{
    $this->db->select('comments.*,users.image as image,');
    $this->db->join('users', 'comments.uid = users.id', 'LEFT');
	$this->db->where('active', 1);
    $this->db->where('language', $lang);
	$this->db->order_by('date', 'DESC');
	if($limit){
		$this->db->limit($limit);
	}
	$result = $this->db->get('comments')->result();
    
    $return	= array();
	
    foreach($result as $comment)
	{
		$return[$comment->comment_id]				= $comment;
        
        if($comment->type == 1){
            
            $this->load->model('materials_model');
            
            $material = $this->materials_model->get_content_with_material_url($comment->foreign_id, $lang);

            if(!empty($material)) {

                $return[$comment->comment_id]->title    = $material['title'];
                $return[$comment->comment_id]->url      = $material['url'];

            }
            
        }elseif($comment->type == 2){
            
            $this->load->model('pages_model');
            
            $page = $this->pages_model->get_content_with_material_url($comment->foreign_id, $lang);

            if(!empty($page)){
            
                $return[$comment->comment_id]->title	    = $page['title'];
                $return[$comment->comment_id]->url	        = $page['url'];

            }
            
        }
		
	}

	return $return;

}

function get_comment($id)
{
	return $this->db->where('comment_id',$id)->get('comments')->row();
}

public function count_comments($fid, $type = 1, $language = 'ru')
{
	$this->db->where('foreign_id', $fid);
	$this->db->where('type', $type);
    $this->db->where('language', $language);
    $this->db->where('active', 1);

	return $this->db->count_all_results('comments');
}

public function get_by($id, $type = 1, $parent = 0, $language = 'ru')
{
    $this->db->select('comments.*,users.image as image,');
    
    $this->db->where('type', $type);
    $this->db->where('parent_id', $parent);
    $this->db->where('foreign_id',$id);
    $this->db->where('language',$language);
    $this->db->where('active', 1);
    $this->db->order_by('date','desc');
    $this->db->join('users', 'comments.uid = users.id', 'LEFT');
    $result = $this->db->get('comments')->result();

   	$return	= array();
    
	foreach($result as $comment)
	{

        if($comment->parent_id == 0){
            $comment->parent_author = '';
        }else{
            $parent_comment = $this->get_comment($comment->parent_id);
            $comment->parent_author = $parent_comment->author;
        }
        
        $return[$comment->comment_id]				= $comment;
		$return[$comment->comment_id]->children	    = $this->get_comments($comment->comment_id, false, $language);
	}
    
    //echo '<pre>'; print_r($return); echo '</pre>';

	return $return;
}

public function get_count_and_rating_comments($fid, $type, $language = 'ru')
{
	$this->db->select('SUM(`rating`) AS rating', false);
	//->group_by('CODE')
	//->order_by('Ratings', 'desc')
	$this->db->where('foreign_id',$fid);
	$this->db->where('type',$type);
    $this->db->where('language',$language);
	$query = $this->db->get('comments')->row_array();

	$query['num'] = $this->db->where('foreign_id',$fid)->where('language',$language)->count_all_results('comments');

	return $query;
}


public function get_latest()
{
    $this->db->order_by('comment_id','desc');
    $this->db->where('active', 1);
    $this->db->limit(5);
    $query = $this->db->get('comments');
    return $query->result_array();//Возвращаем массив со свежими комментариями
}


public function add_new($comment_data)
{
    $this->db->trans_start();
    $this->db->insert('comments',$comment_data);
    $insert_id = $this->db->insert_id();
    $this->db->trans_complete();
    return  $insert_id;
}


// Подсчет общего числа записей
public function count_all_comments($lang = 'ru')
{
	return $this->db->where('language',$lang)->count_all_results('comments');
}

public function get_all($limit,$start_from,$lang = 'ru')
{
   	$this->db->select('*');
	$this->db->where('active', 1);
    $this->db->where('language', $lang);
	$this->db->order_by('date', 'DESC');
	$this->db->limit($limit,$start_from);
	$result = $this->db->get('comments')->result();
    
   	$return	= array();
	
    foreach($result as $comment)
	{
		$return[$comment->comment_id]				= $comment;
        
        if($comment->type == 1){
            
            $this->load->model('materials_model');
            
            $material = $this->materials_model->get($comment->foreign_id);
            
            $return[$comment->comment_id]->title	    = $material['title'];
            $return[$comment->comment_id]->url	        = $material['url'];
            //$return[$comment->comment_id]->img_url	    = $material['img_url'];
            
        }elseif($comment->type == 2){
            
            $this->load->model('pages_model');
            
            $page = $this->pages_model->get($comment->foreign_id);
            
            $return[$comment->comment_id]->title	    = $page['title'];
            $return[$comment->comment_id]->url	        = $page['url'];
            
        }
		
	}

	return $return;

}

function save($data)
{
    if($data['comment_id'])
	{
		$this->db->where('comment_id', $data['comment_id']);
		$this->db->update('comments', $data);
		return $data['comment_id'];
	}
	else
	{
		$this->db->insert('comments', $data);
		return $this->db->insert_id();
	}
}

function delete($id)
{

	$comment	= $this->get_comment($id);
	if ($comment)
	{
		$this->db->where('comment_id', $id);
		$this->db->delete('comments');

		return 'Комментарий "'.$comment->comment_text.'" был удален.';
	}
	else
	{
		return 'Комментарий не найден.';
	}
}


}
?>