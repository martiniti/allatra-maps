<?php
Class Pay_model extends MY_Crud
{
	// Правила для валидации формы ввода справочников
	public $rules = array(
		'fio' => array(
			'field' => 'fio',
			'label' =>'lang:fio',
			'rules' => 'trim|required|max_length[255]|alpha_dash',
			),
		'summ' => array(
			'field' => 'summ',
			'label' => 'lang:summ',
			'rules' => 'trim|required|numeric',
			),
		'email' => array(
			'field' => 'email',
			'label' => 'lang:email',
			'rules' => 'trim|required|valid_email',
			),
		'phone' => array(
			'field' => 'phone',
			'label' => 'lang:phone',
			'rules' => 'trim',
			),
		'offer_check' => array(
			'field' => 'offer_check',
			'label' => 'lang:offer_check',
			'rules' => 'trim|required|integer',
			),
		);
}