
var base_url = 'https://maps.ramir.space/';

//Import CSS!!!
var cssId = 'AmmaCss';  // you could encode the css path itself to generate id..
if (!document.getElementById(cssId))
{
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = cssId;
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = base_url + 'assets/css/allatra-safe-road.css';
    link.media = 'all';
    head.appendChild(link);
}

var safeRoadMap = document.getElementById('safeRoadMap'),
    premaBlockContainer = document.createElement('div'),
    premaBlockRow = document.createElement('div'),
    premaBlockCol_1_3 = document.createElement('div'),
    premaBlockCol_2_3 = document.createElement('div'),
    premaBlockPrema = document.createElement('div'),
    premaBlockBar = document.createElement('div'),
    premaBlockBarRow = document.createElement('div'),
    premaBlockMap = document.createElement('div');

premaBlockContainer.classList.add('ammaContainer');
premaBlockRow.classList.add('ammaRow');
premaBlockCol_1_3.classList.add('ammaCol-1-3');
premaBlockCol_2_3.classList.add('ammaCol-2-3');
premaBlockBar.id = 'ammaPremaBar';
premaBlockMap.id = 'ammaPremaMap';
premaBlockBar.classList.add('ammaBar');
premaBlockMap.style.height = '340px';

premaBlockCol_1_3.appendChild(premaBlockBar);
premaBlockCol_2_3.appendChild(premaBlockMap);
premaBlockRow.appendChild(premaBlockCol_1_3);
premaBlockRow.appendChild(premaBlockCol_2_3);
premaBlockContainer.appendChild(premaBlockRow);
safeRoadMap.appendChild(premaBlockContainer);

// Go Map
var roadMap = L.map('ammaPremaMap', {zoomControl: false}).setView([40, -10], 2);

L.control.zoom({
    position: 'bottomright'
}).addTo(roadMap);

L.control.fullscreen({
    position: 'topright'
}).addTo(roadMap);

new L.Control.GeoSearch({
    provider: new L.GeoSearch.Provider.OpenStreetMap(),
    style: 'bar',
    showMarker: true,
    showPopup: true,
    marker: {                                           // optional: L.Marker    - default L.Icon.Default
        icon: new L.Icon.Default(),
        draggable: true,
    },
    animateZoom: true,
    searchLabel: 'Поиск'
}).addTo(roadMap);

//Disable default scroll
roadMap.scrollWheelZoom.disable();

premaBlockMap.addEventListener('DOMMouseScroll', function(event) {

    event.stopPropagation();
    //e.preventDefault();

    if (event.ctrlKey == true) {
        event.preventDefault();
        roadMap.scrollWheelZoom.enable();
        premaBlockMap.classList.remove('map-scroll');
        setTimeout(function(){
            roadMap.scrollWheelZoom.disable();
        }, 1000);
    } else {
        roadMap.scrollWheelZoom.disable();
        premaBlockMap.classList.add('map-scroll');
        setTimeout(function(){
            premaBlockMap.classList.remove('map-scroll');
        }, 1000);
    }

});

//Saferoad`s table
let srData = {
    "headings": [
        "#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
        "Регион&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
        "Дата",
        "Координаты",
        "Описание"
    ],
    "data": []
}

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    id: 'mapbox.streets'
}).addTo(roadMap);

/* RSS */
// Note: some RSS feeds can't be loaded in the browser due to CORS security.
// To get around this, you can use a proxy.
const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

//go AmmaContent
var ammaContent = document.createElement('div'),
    ammaContentContainer = document.createElement('div');

ammaContent.id = 'ammaContent';
ammaContentContainer.id = 'ammaContentContainer';
ammaContentContainer.className = 'ammaContainer';

ammaContent.appendChild(ammaContentContainer);

safeRoadMap.appendChild(ammaContent);

var request = new XMLHttpRequest();
request.open('GET', 'https://maps.ramir.space/saferoad/json', true);

request.onload = function() {

    // Success!
    if (request.status >= 200 && request.status < 400) {

        let body = document.getElementsByTagName('body')[0];

        //get data!
        var json = JSON.parse(request.responseText);

        var icon = [];

        icon['whiteIcon'] = new L.Icon({
            iconUrl: base_url + 'assets/leaflet/images/white-marker-icon.svg',
            iconRetinaUrl: base_url + 'assets/leaflet/images/white-marker-icon-2x.svg',
            iconSize:    [25, 41],
            iconAnchor:  [12, 41],
            popupAnchor: [1, -34],
            shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
            shadowSize:  [41, 41]
        });

        icon['AllatRaBlackIcon'] = new L.Icon({
            iconUrl: base_url + 'assets/leaflet/images/allatra-marker-icon.svg',
            iconRetinaUrl: base_url + 'assets/leaflet/images/allatra-marker-icon-2x.svg',
            iconSize:    [25, 41],
            iconAnchor:  [12, 41],
            popupAnchor: [1, -34],
            shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
            shadowSize:  [41, 41]
        });

        icon['redIcon'] = new L.Icon({
            iconUrl: base_url + 'assets/leaflet/images/red-marker-icon.svg',
            iconRetinaUrl: base_url + 'assets/leaflet/images/red-marker-icon-2x.svg',
            iconSize:    [25, 41],
            iconAnchor:  [12, 41],
            popupAnchor: [1, -34],
            shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
            shadowSize:  [41, 41]
        });


        icon['orangeIcon'] = new L.Icon({
            iconUrl: base_url + 'assets/leaflet/images/orange-marker-icon.svg',
            iconRetinaUrl: base_url + 'assets/leaflet/images/orange-marker-icon-2x.svg',
            iconSize:    [25, 41],
            iconAnchor:  [12, 41],
            popupAnchor: [1, -34],
            shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
            shadowSize:  [41, 41]
        });

        //Create Map
        geoLayer = L.geoJson(json, {

            style: function(feature) {
                var mag = feature.properties.mag;

                return {
                    weight: 1,
                    fillColor: "#ff0000",
                    fillOpacity: 0.72,
                    color: "#ff0000"
                };

            },

            onEachFeature: function(feature, layer) {

                var popupText = feature.properties.description +
                    "<br><b>Дата:</b> " + feature.properties.datetime +
                    "<br><b>Источник:</b> " + feature.properties.sources;

                layer.bindPopup(popupText, {
                    closeButton: true,
                    offset: L.point(0, -5)
                });
                layer.on('click', function() {
                    layer.openPopup();
                });
            },

            pointToLayer: function(feature, latlng) {

                return L.marker(latlng, {
                    icon: icon[feature.properties.icon]
                });

            },

        }).addTo(roadMap);


        var is_logged_in = 1;

        /* Draw the Map */
        if(is_logged_in == 1){

            var drawnItems = L.geoJson().addTo(roadMap);

            roadMap.addControl(new L.Control.Draw({
                draw: {
                    circle : false,
                    circlemarker : false,
                    polygon: {
                        allowIntersection: false,
                        showArea: true
                    }
                },
                edit: {
                    featureGroup: drawnItems,
                    poly: {
                        allowIntersection: false
                    }
                }
            }));

            roadMap.on(L.Draw.Event.CREATED, function (e) {
                var layer = e.layer,
                    feature = layer.feature = layer.feature || {};
                feature.type = feature.type || "Feature";
                var props = feature.properties = feature.properties || {};
                props.desc = null;
                props.image = null;
                props.source = null;
                props.officially = 0;
                drawnItems.addLayer(layer);
                addPopup(layer);
            });

            function addPopup(layer) {
                /*var content =
                    '<div class="popup-form" style="min-width: 200px;">' +
                        '<div class="form-group"><textarea class="form-control form-control-square form-control-sm" name="desc" placeholder="Описание"></textarea></div>' +
                        '<div class="form-group"><input class="form-control form-control-square form-control-sm" name="source" placeholder="Источник" /></div>' +
                        '<div class="form-group"><div class="custom-control custom-checkbox"> <input class="custom-control-input" name="officially" type="checkbox" id="official-check-1"> <label class="custom-control-label" for="official-check-1">Официальный источник?</label> </div></div>' +
                    '</div>';*/

                var wrapper = document.createElement('div');
                wrapper.className = 'popup-wrapper';
                wrapper.style.width = '200px';

                var formGroup1  = document.createElement('div');
                formGroup1.className = "form-group";

                var formGroup2  = document.createElement('div');
                formGroup2.className = "form-group";

                var formGroup3  = document.createElement('div');
                formGroup3.className = "form-group";

                var customCheckbox = document.createElement('div');
                customCheckbox.className = "custom-control custom-checkbox";

                var labelCheckbox = document.createElement('label');
                labelCheckbox.className = "custom-control-label";
                labelCheckbox.htmlFor = "official-check-1";
                labelCheckbox.innerHTML = "Официальный источник?";

                /* content */

                var content = document.createElement("textarea");
                content.className = "form-control form-control-square form-control-sm";
                content.placeholder  = "Описание места";

                content.addEventListener("keyup", function () {
                    layer.feature.properties.desc = content.value;
                });

                /* eof. content */

                /* source */

                var source = document.createElement("input");
                source.className = "form-control form-control-square form-control-sm";
                source.placeholder = "Ссылка на источник";

                source.addEventListener("keyup", function () {
                    layer.feature.properties.source = source.value;
                });

                /* eof. source */

                /* officially */

                var officially = document.createElement("input");
                officially.className = "custom-control-input";
                officially.type = "checkbox";
                officially.id = "official-check-1";
                officially.value = 0;

                officially.addEventListener("change", function () {

                    if(this.checked) {
                        layer.feature.properties.officially = 1;
                    }else{
                        layer.feature.properties.officially = 0;
                    }

                });

                customCheckbox.appendChild(officially);
                customCheckbox.appendChild(labelCheckbox);

                /* eof. officially */

                layer.on("popupopen", function () {
                    source.value  = layer.feature.properties.source;
                    content.value = layer.feature.properties.desc;
                    officially.value = 1;
                    content.focus();
                });

                formGroup1.appendChild(content);
                formGroup2.appendChild(source);
                formGroup3.appendChild(customCheckbox);

                wrapper.appendChild(formGroup1);
                wrapper.appendChild(formGroup2);
                wrapper.appendChild(formGroup3);

                layer.bindPopup(wrapper).openPopup();
            }

            // NOT WORKING!
            // on click, clear all layers
            /*document.getElementById('delete').onclick = function(e) {
                drawnItems.clearLayers();
            }*/

            var saveControl = L.Control.extend({
                options: {
                    position: 'topleft'
                },
                onAdd: function (roadMap) {
                    var container = L.DomUtil.create('div', 'leaflet-control-save leaflet-bar leaflet-control');
                    container.innerHTML = '<a id="save" style="height: 30px; width: 30px; cursor: pointer; opacity: 0.65; background-size: 16px 16px; background-repeat: no-repeat; background-position: 50% 50%; background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzIgMzIiIGhlaWdodD0iMzJweCIgaWQ9InN2ZzIiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDMyIDMyIiB3aWR0aD0iMzJweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgaWQ9ImJhY2tncm91bmQiPjxyZWN0IGZpbGw9Im5vbmUiIGhlaWdodD0iMzIiIHdpZHRoPSIzMiIvPjwvZz48ZyBpZD0iZmxvcHB5X3g1Rl9kaXNrIj48cGF0aCBkPSJNMjYsMThINnYyaDIwVjE4eiBNMjYsMjJINnYyaDIwVjIyeiBNMjYsMjZINnYyaDIwVjI2eiBNMjYsMGgtMnYxM0g4VjBIMHYzMmgzMlY2TDI2LDB6IE0yOCwzMEg0VjE2aDI0VjMweiBNMjIsMmgtNHY5aDQgICBWMnoiLz48L2c+PC9zdmc+)"></a>';
                    container.style.backgroundColor = 'white';
                    container.style.width = '33px';
                    container.style.height = '33px';
                    container.onclick = function(){
                        console.log('buttonClicked');
                    }
                    return container;
                },
            });

            roadMap.addControl(new saveControl());

            document.getElementById('save').onclick = function(e) {

                var userGeoData = JSON.stringify(drawnItems.toGeoJSON());

                if(userGeoData == '{"type":"FeatureCollection","features":[]}'){

                    alert('Ошибка! Вы ещё ничего не добавили на карту.');

                }else{

                    postAjax(base_url + "feedback/geodata", 'content=' + userGeoData, function(data){ console.log(data); });

                }

            }

        }
        /* EOF. DrawMap */

        //Create a banners
        let banners = json.banners,
            bannersCounter,
            bannersGroup1 = document.createElement('div'),
            bannersGroup2 = document.createElement('div'),
            bannersGroup1Title = document.createElement('h3'),
            bannersGroup2Title = document.createElement('h3'),
            bannersGroupRow1 = document.createElement('div'),
            bannersGroupRow2 = document.createElement('div');

        bannersGroup1.id = 'ammaBar1';
        bannersGroup1.className = 'ammaBar';
        bannersGroup2.id = 'ammaBar2';
        bannersGroup2.className = 'ammaBar';
        bannersGroupRow1.className = 'ammaBarRow';
        bannersGroupRow2.className = 'ammaBarRow';
        bannersGroup1Title.innerText = 'Воздействие знака на физиологию человека';
        bannersGroup2Title.innerText = 'Воздействие знака на биологические среды и воду';
        bannersGroup1.appendChild(bannersGroup1Title);
        bannersGroup2.appendChild(bannersGroup2Title);

        for(bannersCounter = 0; bannersCounter < banners.length; bannersCounter++){

            let bannersGroupAnchor = document.createElement('a'),
                bannersGroupImage  = document.createElement('img'),
                bannersGroupTitle  = document.createElement('h3'),
                bannersGroupText  = document.createElement('div');

            bannersGroupAnchor.href = banners[bannersCounter].link;
            bannersGroupAnchor.className = 'ammaBarItem';
            bannersGroupImage.src = base_url + 'assets/uploads/images/banners/' + banners[bannersCounter].img_url;
            bannersGroupImage.alt = banners[bannersCounter].title;
            bannersGroupTitle.innerText = banners[bannersCounter].title;
            bannersGroupText.innerHTML = banners[bannersCounter].text;
            bannersGroupAnchor.appendChild(bannersGroupImage);
            bannersGroupAnchor.appendChild(bannersGroupTitle);

            if(banners[bannersCounter].position == 1){

                bannersGroupRow1.appendChild(bannersGroupAnchor);

            }else if(banners[bannersCounter].position == 2){

                bannersGroupRow2.appendChild(bannersGroupAnchor);

            } else if(banners[bannersCounter].position == 3){

                ammaPremaBar.appendChild(bannersGroupTitle);
                ammaPremaBar.appendChild(bannersGroupText);

            }

        }

        bannersGroup1.appendChild(bannersGroupRow1);
        bannersGroup2.appendChild(bannersGroupRow2);


        /* RSS */

        //go RSS block1
        var rssBlock1 = document.createElement('div'),
            rssBlock1Container = document.createElement('div'),
            rssBlock1ButtonNext = document.createElement('button'),
            rssBlock1ButtonPrev = document.createElement('button'),
            rssBlock1Title = document.createElement('h2');

        rssBlock1.id = 'rssBlock1';
        rssBlock1Container.className = 'ammaRSSBlock';
        rssBlock1Title.className = 'ammaRSSTitle';
        rssBlock1ButtonNext.id = 'rssBlock1Next';
        rssBlock1ButtonPrev.id = 'rssBlock1Prev';

        // rss: Igor Mihajlovich Danilov
        rssBlock1Title.innerText = 'Передачи с И.М.ДАНИЛОВЫМ';
        rssBlock1.appendChild(rssBlock1Title);

        json.rss.forEach(function (item) {
            if(item.link == 'https://allatra.tv/rss/publications?category=im-danilov'){

                let rssItem = document.createElement('div'),
                    rssItemAnchor = document.createElement('a'),
                    rssItemParagraph = document.createElement('p'),
                    rssItemImage = document.createElement('img');

                rssItem.className = 'ammaRSSItem';
                rssItemAnchor.href = item.url;
                rssItemImage.src = item.img;
                rssItemImage.alt = item.title;

                rssItemAnchor.appendChild(rssItemImage);
                rssItemParagraph.innerHTML = item.title;
                rssItemAnchor.appendChild(rssItemParagraph);
                rssItem.appendChild(rssItemAnchor);

                rssBlock1Container.appendChild(rssItem);
                rssBlock1.appendChild(rssBlock1Container);
                rssBlock1.appendChild(rssBlock1ButtonPrev);
                rssBlock1.appendChild(rssBlock1ButtonNext);

            }
        });

        // rss: Climate Changes
        var rssBlock2 = document.createElement('div'),
            rssBlock2Container = document.createElement('div'),
            rssBlock2ButtonNext = document.createElement('button'),
            rssBlock2ButtonPrev = document.createElement('button'),
            rssBlock2Title = document.createElement('h2');

        rssBlock2.id = 'rssBlock2';
        rssBlock2Container.className = 'ammaRSSBlock';
        rssBlock2Title.className = 'ammaRSSTitle';
        rssBlock2ButtonNext.id = 'rssBlock2Next';
        rssBlock2ButtonPrev.id = 'rssBlock2Prev';

        rssBlock2Title.innerText = 'Изменение климата';
        rssBlock2.appendChild(rssBlock2Title);

        json.rss.forEach(function (item) {
            if(item.link == 'https://allatra.tv/rss/publications?category=climate'){

                let rssItem = document.createElement('div'),
                    rssItemAnchor = document.createElement('a'),
                    rssItemParagraph = document.createElement('p'),
                    rssItemImage = document.createElement('img');

                rssItem.className = 'ammaRSSItem';
                rssItemAnchor.href = item.url;
                rssItemImage.src = item.img;
                rssItemImage.alt = item.title;

                rssItemAnchor.appendChild(rssItemImage);
                rssItemParagraph.innerHTML = item.title;
                rssItemAnchor.appendChild(rssItemParagraph);
                rssItem.appendChild(rssItemAnchor);

                rssBlock2Container.appendChild(rssItem);
                rssBlock2.appendChild(rssBlock2Container);
                rssBlock2.appendChild(rssBlock2ButtonPrev);
                rssBlock2.appendChild(rssBlock2ButtonNext);

            }
        });

        //Create a pages
        let pages = json.pages,
            pagesCounter,
            page1 = document.createElement('div'),
            page2 = document.createElement('div'),
            page3 = document.createElement('div');

        page1.className = 'ammaPage';
        page1.innerHTML = json.pages[0].anons + '<div class="ammaPageHidden"><div class="ammaPageFull">' + json.pages[0].text + '</div></div>';
        page2.className = 'ammaPage';
        page2.innerHTML = json.pages[1].anons + '<div class="ammaPageHidden"><div class="ammaPageFull">' + json.pages[1].text + '</div></div>';
        page3.className = 'ammaPage';
        page3.innerHTML = json.pages[2].anons + '<div class="ammaPageHidden"><div class="ammaPageFull">' + json.pages[2].text + '</div></div>';


        //Assembly Pages + Banners
        let ammaRowGroup1 = document.createElement('div'),
            ammaRowGroup2 = document.createElement('div'),
            ammaRowGroup1Col_2_3 = document.createElement('div'),
            ammaRowGroup1Col_1_3 = document.createElement('div'),
            ammaRowGroup2Col_2_3 = document.createElement('div'),
            ammaRowGroup2Col_1_3 = document.createElement('div');

        ammaRowGroup1.className = 'ammaRow';
        ammaRowGroup2.className = 'ammaRow';
        ammaRowGroup1Col_2_3.className = 'ammaCol-2-3';
        ammaRowGroup1Col_1_3.className = 'ammaCol-1-3';
        ammaRowGroup2Col_2_3.className = 'ammaCol-2-3';
        ammaRowGroup2Col_1_3.className = 'ammaCol-1-3';

        ammaRowGroup1Col_2_3.appendChild(page1);
        ammaRowGroup1Col_2_3.appendChild(page2);
        ammaRowGroup1Col_1_3.appendChild(bannersGroup1);
        ammaRowGroup1.appendChild(ammaRowGroup1Col_2_3);
        ammaRowGroup1.appendChild(ammaRowGroup1Col_1_3);
        ammaContentContainer.appendChild(ammaRowGroup1);
        ammaContentContainer.appendChild(rssBlock1);

        ammaRowGroup2Col_2_3.appendChild(page3);
        ammaRowGroup2Col_1_3.appendChild(bannersGroup2);
        ammaRowGroup2.appendChild(ammaRowGroup2Col_2_3);
        ammaRowGroup2.appendChild(ammaRowGroup2Col_1_3);
        ammaContentContainer.appendChild(ammaRowGroup2);
        ammaContentContainer.appendChild(rssBlock2);


        //Go RSS 1 Carousel
        const Siema1 = new Siema({
            selector: '#rssBlock1 .ammaRSSBlock',
            easing: 'ease-out',
            perPage: {
                768: 2,
                1024: 3,
                1280: 4,
            },
            draggable: true,
            multipleDrag: true,
            loop: true,
        });

        const prev1 = document.getElementById('rssBlock1Prev');
        const next1 = document.getElementById('rssBlock1Next');

        prev1.addEventListener('click', () => Siema1.prev());
        next1.addEventListener('click', () => Siema1.next());

        //Go RSS 2 Carousel
        const Siema2 = new Siema({
            selector: '#rssBlock2 .ammaRSSBlock',
            easing: 'ease-out',
            perPage: {
                768: 2,
                1024: 3,
                1280: 4,
            },
            draggable: true,
            multipleDrag: true,
            loop: true,
        });

        const prev2 = document.getElementById('rssBlock2Prev');
        const next2 = document.getElementById('rssBlock2Next');

        prev2.addEventListener('click', () => Siema2.prev());
        next2.addEventListener('click', () => Siema2.next());


        //Create a EQ Table
        tableCreate(json);

        let tbl     = document.createElement('table'),
            eqNum   = json.features.length;

        tbl.id = 'earthquakesMap';
        tbl.className = 'table';
        tbl.style.width = '100%';
        tbl.style.textAlign = 'left';

        ammaContentContainer.prepend(tbl);

        let dataEqTable = new DataTable(tbl, {
            data: srData,
            // the maximum number of rows to display on each page
            perPage: 5,
            // the per page options in the dropdown
            perPageSelect: [5, 25, 50, 100, 1000],
        });

    } else {
        // We reached our target server, but it returned an error
        console.log('There was some error in json request!')
    }

};

function postAjax(url, data, success) {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
        function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
    ).join('&');

    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.responseType = 'json';
    xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) {
            let message = document.createElement('p');
            if(xhr.response.err == 0){
                message.classList.add('ammaMessage', 'ammaSuccess');
            } else {
                message.classList.add('ammaMessage', 'ammaError');
            }
            message.innerHTML = xhr.response.content;
            document.getElementById('ammaPremaMap').parentNode.appendChild(message);
            setTimeout(function () {
                message.style.opacity = 0;
                setTimeout(function () {
                    message.remove();
                }, 1000);
            }, 6000);
        }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
}

request.onerror = function() {
    // There was a connection error of some sort
};

//init json data request
request.send();

function tableCreate(json) {

    let srNum = json.features.length;
    let y = 0;

    //Create tbody
    for(let i=0;i < srNum; i++){

        if(json.features[i].geometry.type == 'Point'){
            srData.data[y] = [];
            srData.data[y][0] = y;
            srData.data[y][1] = '-';
            srData.data[y][2] = json.features[i].properties.datetime;
            srData.data[y][3] = json.features[i].geometry.coordinates;
            srData.data[y][4] = json.features[i].properties.description;
            y++
        }

    }

}

//Header
//go Header
var headerBlock = document.createElement('div'),
    headerBlockContainer = document.createElement('div'),
    headerBlockLogo = document.createElement('a'),
    headerBlockTitle = document.createElement('div'),
    headerBlockAction = document.createElement('div'),
    headerBlockTitleH1 = document.createElement('h1'),
    headerBlockActionBtn = document.createElement('a');

headerBlock.id = 'safeRoadHeader';
headerBlockContainer.id = 'ammaHeaderContainer';
headerBlockContainer.className = 'ammaContainer';
headerBlockLogo.id = 'safeRoadLogo';
headerBlockLogo.href = 'https://allatra.tv/safe-road-map';
headerBlockTitle.id = 'safeRoadTitle';
headerBlockAction.id = 'safeRoadAction';

headerBlockLogo.innerHTML = '<img src="https://maps.ramir.space/assets/templates/allatra-da/img/logo_saferoad.svg" alt="Знак АллатРа. Международный cоциально-исследовательский проект по изучению воздействия знака на окружающую среду и психофизику человека" />';

headerBlockTitleH1.innerText = 'Знак АллатРа. Международный cоциально-исследовательский проект по изучению воздействия знака на окружающую среду и психофизику человека';

headerBlockActionBtn.href = '#';
headerBlockActionBtn.innerHTML = '<span>Обратная связь</span>';

headerBlockTitle.appendChild(headerBlockTitleH1);
headerBlockAction.appendChild(headerBlockActionBtn);
headerBlockContainer.appendChild(headerBlockLogo);
headerBlockContainer.appendChild(headerBlockTitle);
headerBlockContainer.appendChild(headerBlockAction);
headerBlock.appendChild(headerBlockContainer);
safeRoadMap.prepend(headerBlock);



//OpenList
var ammaPages = document.getElementsByClassName("ammaPage"),
    ammaPagesCounter,
    allPage,
    fullPage,
    hiddenPage,
    hiddenPages = document.getElementsByClassName("ammaPageHidden"),
    readmoreCounter,
    hiddenPageCounter;

setTimeout(function(){

    for(ammaPagesCounter = 0; ammaPagesCounter < ammaPages.length; ammaPagesCounter++){

        let ammaReadmore = document.createElement('a');
            ammaReadmore.href = '#';
            ammaReadmore.className = 'ammaReadmore';

        ammaPages[ammaPagesCounter].appendChild(ammaReadmore)

    }

    let readmoreBtns = document.getElementsByClassName("ammaReadmore");

    for (readmoreCounter = 0; readmoreCounter < readmoreBtns.length; readmoreCounter++) {

        readmoreBtns[readmoreCounter].addEventListener("click", function(e) {

            e.preventDefault();

            allPage = this.closest('.ammaPage');
            this.style.display = "none";
            hiddenPage = allPage.querySelector('.ammaPageHidden');
            fullPage = allPage.querySelector('.ammaPageFull');
            hiddenPage.classList.add("open");
            hiddenPage.style.maxHeight = fullPage.scrollHeight + "px";

        });
    }

}, 1000);

//Create Modals
var ammaModalWrapper = document.createElement("div"),
    ammaModalContainer = document.createElement("div"),
    ammaModal = document.createElement("div"),
    ammaModalClose = document.createElement("a"),
    ammaModalChoose = document.createElement("div"),
    ammaModalAuth = document.createElement("div"),
    ammaModalRegister = document.createElement("div"),
    ammaModalFeedback = document.createElement("div"),
    ammaModalForgotPassword = document.createElement("div"),
    ammaModalAuthForm = document.createElement("form"),
    ammaModalRegisterForm = document.createElement("form"),
    ammaModalFeedbackForm = document.createElement("form"),
    ammaModalForgotPasswordForm = document.createElement("form"),
    ammaModalRequired = document.createElement("div");

ammaModalWrapper.id = 'ammaModalWrapper';
ammaModalWrapper.className = 'ammaHidden';
ammaModalContainer.id = 'ammaModalContainer';
ammaModal.id = 'ammaModal';
ammaModalClose.id = 'ammaModalClose';
ammaModalClose.href = '#';
ammaModalChoose.id = 'ammaModalChoose';

ammaModalChoose.innerHTML = '<a href="#" data-action="ammaModalAuth" class="ammaHidden">Вход</a><a href="#" data-action="ammaModalRegister" class="ammaHidden">Регистрация</a><a href="#" data-action="ammaModalForgotPassword" class="ammaHidden">Восстановление пароля</a><a href="#" data-action="ammaModalFeedback" class="selected">Обратная связь</a>';
ammaModalClose.innerHTML = 'X';
ammaModal.appendChild(ammaModalChoose);
ammaModal.appendChild(ammaModalClose);

ammaModalAuth.id = 'ammaModalAuth';
ammaModalAuth.className = 'ammaHidden ammaModalBlock';
ammaModalAuthForm.id = 'ammaModalAuthForm';
ammaModalAuthForm.name = 'ammaAuth';
ammaModalAuthForm.method = 'POST';
ammaModalAuthForm.action = base_url + 'auth';
ammaModalAuthForm.innerHTML += '<div class="ammaFormGroup"><label for="username"><span class="ammaAsterisk">*</span> Имя или Email:</label><input id="username" type="text" name="username" value="" required /></div>';
ammaModalAuthForm.innerHTML += '<div class="ammaFormGroup"><label for="password"><span class="ammaAsterisk">*</span> Пароль:</label><input id="password" type="password" name="password" value="" required /></div>';
ammaModalAuthForm.innerHTML += '<div class="ammaFormGroup"><label><input type="checkbox" name="remember" value="1" id="remember" /> <span>Запомнить меня</span></label></div>';
ammaModalAuthForm.innerHTML += '<button name="login" type="submit">Войти</button>';
ammaModalAuth.appendChild(ammaModalAuthForm);

ammaModalRegister.id = 'ammaModalRegister';
ammaModalRegister.className = 'ammaHidden ammaModalBlock';
ammaModalRegisterForm.id = 'ammaModalRegisterForm';
ammaModalRegisterForm.name = 'ammaRegister';
ammaModalRegisterForm.method = 'POST';
ammaModalRegisterForm.action = base_url + 'auth/register';
ammaModalRegisterForm.innerHTML = '<div class="ammaFormGroup"><label for="username"><span class="ammaAsterisk">*</span> Имя:</label><input id="username" type="text" name="username" value="" required /></div>';
ammaModalRegisterForm.innerHTML += '<div class="ammaFormGroup"><label for="email"><span class="ammaAsterisk">*</span> Email:</label><input id="email" type="email" name="email" value="" required /></div>';
ammaModalRegisterForm.innerHTML += '<div class="ammaFormGroup"><label for="password"><span class="ammaAsterisk">*</span> Пароль:</label><input id="password" type="password" name="password" value="" required /></div>';
ammaModalRegisterForm.innerHTML += '<div class="ammaFormGroup"><label for="confirm_password"><span class="ammaAsterisk">*</span> Подтверждение пароля:</label><input id="confirm_password" type="password" name="confirm_password" value="" required /></div>';
ammaModalRegisterForm.innerHTML += '<button name="register" type="submit">Регистрация</button>';
ammaModalRegister.appendChild(ammaModalRegisterForm);

ammaModalFeedback.id = 'ammaModalFeedback';
ammaModalFeedback.className = 'ammaModalBlock';
ammaModalFeedbackForm.id = 'ammaModalFeedbackForm';
ammaModalFeedbackForm.name = 'ammaFeedback';
ammaModalFeedbackForm.method = 'POST';
ammaModalFeedbackForm.action = base_url + 'feedback';
ammaModalFeedbackForm.innerHTML = '<div class="ammaFormGroup"><label for="name"><span class="ammaAsterisk">*</span> Имя:</label><input id="name" type="text" name="name" value="" required /></div>';
ammaModalFeedbackForm.innerHTML += '<div class="ammaFormGroup"><label for="email"><span class="ammaAsterisk">*</span> Email:</label><input id="email" type="email" name="email" value="" required /></div>';
ammaModalFeedbackForm.innerHTML += '<div class="ammaFormGroup"><label for="topic"><span class="ammaAsterisk">*</span> Тема сообщения:</label><input id="topic" type="text" name="topic" value="" required /></div>';
ammaModalFeedbackForm.innerHTML += '<div class="ammaFormGroup"><label for="message"><span class="ammaAsterisk">*</span> Сообщение:</label><textarea id="message" type="text" name="message" value="" required></textarea></div>';
ammaModalFeedbackForm.innerHTML += '<div class="ammaFormGroup"><label for="userfile">Прикрепить файл:</label><input id="userfile" type="file" name="userfile" value="" /></div>';
ammaModalFeedbackForm.innerHTML += '<button name="send_message" type="submit">Отправить</button>';
ammaModalFeedback.appendChild(ammaModalFeedbackForm);

ammaModalForgotPassword.id = 'ammaModalForgotPassword';
ammaModalForgotPassword.className = 'ammaHidden ammaModalBlock';
ammaModalForgotPasswordForm.id = 'ammaModalForgotPasswordForm';
ammaModalForgotPasswordForm.name = 'ammaForgotPassword';
ammaModalForgotPasswordForm.method = 'POST';
ammaModalForgotPasswordForm.action = base_url + 'auth/forgot_password';
ammaModalForgotPasswordForm.innerHTML = '<div class="ammaFormGroup"><label for="username"><span class="ammaAsterisk">*</span> Введите логин или пароль:</label><input id="username" type="text" name="username" value="" required /></div>';
ammaModalForgotPasswordForm.innerHTML += '<button name="reset" type="submit">Восстановить пароль</button>';
ammaModalForgotPasswordForm.innerHTML += '<div class="ammaClear"></div>';
ammaModalForgotPassword.appendChild(ammaModalForgotPasswordForm);

ammaModal.appendChild(ammaModalAuth);
ammaModal.appendChild(ammaModalRegister);
ammaModal.appendChild(ammaModalFeedback);
ammaModal.appendChild(ammaModalForgotPassword);

ammaModalRequired.id = 'ammaModalRequired';
ammaModalRequired.innerHTML = 'Поля отмеченные звёздочкой <span>*</span> обязательны для заполнения';
ammaModal.appendChild(ammaModalRequired);

ammaModalContainer.appendChild(ammaModal);
ammaModalWrapper.appendChild(ammaModalContainer);
safeRoadMap.appendChild(ammaModalWrapper);

let chooseModal = document.querySelectorAll("#ammaModalChoose a");
let ammaModals = document.querySelectorAll(".ammaModalBlock");

for (chooseModalCounter = 0; chooseModalCounter < chooseModal.length; chooseModalCounter++) {

    chooseModal[chooseModalCounter].addEventListener("click", function(e) {

        e.preventDefault();

        for(let i = 0; i < chooseModal.length; i++){
            chooseModal[i].classList.remove('selected');
        }
        this.classList.add('selected');

        let choosenModal = this.getAttribute('data-action');
        console.log(choosenModal);
        for(let y = 0; y < ammaModals.length; y++){
            ammaModals[y].classList.add('ammaHidden');
        }
        document.getElementById(choosenModal).classList.remove('ammaHidden');

    });

}

//Close Modal
closeModalBtn = document.getElementById('ammaModalClose');

closeModalBtn.addEventListener("click", function(e) {

    e.preventDefault();
    document.getElementById('ammaModalContainer').style.top = '-100%';
    setTimeout(function () {
        document.getElementById('ammaModalWrapper').classList.add('ammaHidden');
    },600);
    document.body.classList.remove('ammaHiddenOverflowY');

});

//Open Modal
openModalBtn = document.querySelector('#safeRoadAction a');

openModalBtn.addEventListener("click", function(e) {

    e.preventDefault();
    document.getElementById('ammaModalWrapper').classList.remove('ammaHidden');
    setTimeout(function () {
        document.getElementById('ammaModalContainer').style.top = '15%';
    },200);
    document.body.classList.add('ammaHiddenOverflowY');

});

//Feedback
// document.addEventListener('submit', function (e) {
//
//     e.preventDefault();
//
//     // creating form
//     var formData = new FormData(document.getElementById('ammaModalFeedbackForm'));
//
//     // add more
//     formData.append("url", window.location.href);
//     formData.append("feedback", 1);
//
//     // send
//     var xhr = new XMLHttpRequest();
//     xhr.open("POST", base_url + "feedback");
//     xhr.setRequestHeader('Content-Type', 'multipart/form-data');
//     xhr.send(formData);
//
// });

document.getElementById('ammaModalFeedbackForm').onsubmit = function(e) {

    e.preventDefault();

    // creating form
    var formData = new FormData(document.getElementById('ammaModalFeedbackForm'));

    // add more
    formData.append("url", window.location.href);
    formData.append("feedback", 1);

    var request = new XMLHttpRequest();
    request.open("POST", base_url + 'feedback');
    request.responseType = 'json';
    request.onreadystatechange = function() {
        if (request.readyState>3 && request.status==200) {
            let message = document.createElement('p');
            if(request.response.err == 0){
                message.classList.add('ammaMessage', 'ammaSuccess');
            } else {
                message.classList.add('ammaMessage', 'ammaError');
            }
            message.innerHTML = request.response.content;
            document.getElementById('ammaModalFeedbackForm').prepend(message);
            setTimeout(function () {
                message.style.opacity = 0;
                setTimeout(function () {
                    message.remove();
                }, 1000);
            }, 6000);
        }
    };
    request.send(formData);

    return request;
}


