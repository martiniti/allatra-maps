var base_url = 'https://maps.ramir.space/',
    current_url = window.location.href.split('?')[0],
    currentUrlWithGet = new URL(window.location.href),
    magnitude = currentUrlWithGet.searchParams.get("magnitude"),
    begin = currentUrlWithGet.searchParams.get("begin"),
    end = currentUrlWithGet.searchParams.get("end"),
    lang = document.getElementById('AmmaEqScript').getAttribute('lang');
    langUrl = lang == 'en' ? '' : '/' + lang;
    eqURL = base_url + 'earthquakes/json' + langUrl;

//Import CSS!!!
var cssId = 'AmmaCss';  // you could encode the css path itself to generate id..
if (!document.getElementById(cssId))
{
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = cssId;
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = base_url + 'assets/css/earthquakes.css';
    link.media = 'all';
    head.appendChild(link);
}

function showDatePickerForm(e) {

    var formToShow = document.getElementById(e);

    document.getElementById('AmmaEqksDate').classList.add('AmmaEqksHidden');
    document.getElementById('AmmaEqksPeriod').classList.add('AmmaEqksHidden');

    formToShow.classList.remove('AmmaEqksHidden');

}

if(location.search !== ''){

    eqURL = eqURL + '?';

    if(magnitude){
        eqURL = eqURL + 'magnitude=' + magnitude + '&';
    }

    if(begin){
        eqURL = eqURL + 'begin=' + begin + '&';
    }

    if(end){
        eqURL = eqURL + 'end=' + end;
    }

}

var request = new XMLHttpRequest();
request.open('GET', eqURL, true);

request.onload = function() {

    // Success!
    if (request.status >= 200 && request.status < 400) {

        let body = document.getElementsByTagName('body')[0];

        //get data!
        var json = JSON.parse(request.responseText);
        var eqQTY = json.features.length;

        //Table Data
        //Earthquake`s table
        let eqData = {
            "headings": [
                "#",
                json.translations.region,
                json.translations.date,
                json.translations.magnitude,
                json.translations.depth_km
            ],
            "data": []
        }

        function tableCreate(json) {

            let eqNum   = json.features.length;

            //Create tbody
            for(let i=0;i < eqNum; i++){

                eqData.data[i] = [];
                eqData.data[i][0] = i;
                eqData.data[i][1] = json.features[i].properties.place;
                eqData.data[i][2] = json.features[i].properties.datetime;
                eqData.data[i][3] = json.features[i].properties.mag;
                eqData.data[i][4] = json.features[i].properties.depth;

            }

        }

        //Wrapper + Header
        var AmmaEqksWrapper = document.getElementById('AmmaEqksWrapper'),
            AmmaEqksMapWrapper = document.createElement('div'),
            AmmaEqksMapHeader = document.createElement('div'),
            AmmaEqksLogo = document.createElement('a'),
            AmmaEqksLogoImg = document.createElement('div'),
            AmmaEqksLogoImage = document.createElement('img'),
            AmmaEqksSlogan = document.createElement('div'),
            AmmaEqksSloganH1 = document.createElement('h1'),
            AmmaEqksSloganH2 = document.createElement('h2'),
            AmmaEqksSloganLive = document.createElement('span'),
            AmmaEqksShare = document.createElement('div'),
            AmmaEqksShareBookmarks = document.createElement('a'),
            AmmaEqksShareModal = document.createElement('a'),
            AmmaEqksClear1 = document.createElement('div'),
            AmmaEqksFilter1 = document.createElement('p'),
            AmmaEqksFilter1Legend = document.createElement('span'),
            AmmaEqksFilter1Anchor1 = document.createElement('a'),
            AmmaEqksFilter1Anchor2 = document.createElement('a'),
            AmmaEqksFilter1Anchor3 = document.createElement('a'),
            AmmaEqksFilter1Anchor4 = document.createElement('a'),
            AmmaEqksFilter1Anchor5 = document.createElement('a'),
            AmmaEqksFilter2 = document.createElement('p'),
            AmmaEqksFilter2Legend = document.createElement('span'),
            AmmaEqksFilter2Anchor1 = document.createElement('a'),
            AmmaEqksFilter2Anchor2 = document.createElement('a'),
            AmmaEqksFilter2Anchor3 = document.createElement('a'),
            AmmaEqksFilter2Anchor4 = document.createElement('a'),
            AmmaEqksFilter2Anchor5 = document.createElement('a'),
            AmmaEqksClear2 = document.createElement('div'),
            AmmaEqksDateForm = document.createElement('form'),
            AmmaEqksDateFormInput0 = document.createElement('input'),
            AmmaEqksDateFormInput = document.createElement('input'),
            AmmaEqksDateFormButton = document.createElement('button'),
            AmmaEqksPeriodForm = document.createElement('form'),
            AmmaEqksPeriodFormInput0 = document.createElement('input'),
            AmmaEqksPeriodFormInput1 = document.createElement('input'),
            AmmaEqksPeriodFormInput2 = document.createElement('input'),
            AmmaEqksPeriodFormButton = document.createElement('button'),
            AmmaEqksMap = document.createElement('div');

        AmmaEqksWrapper.classList.add('lang-' + lang);
        AmmaEqksMapWrapper.id = 'AmmaEqksMapWrapper';
        AmmaEqksMapHeader.id = 'AmmaEqksMapHeader';
        AmmaEqksLogo.id = 'AmmaEqksLogo';
        AmmaEqksLogo.href = 'https://allatra.tv/earthquakes';
        AmmaEqksLogoImg.id = 'AmmaEqksLogoImg';
        AmmaEqksLogoImage.src = base_url + 'assets/templates/allatra-da/img/allatra.png';
        AmmaEqksLogoImage.alt = 'АллатРа';
        AmmaEqksSlogan.id = 'AmmaEqksSlogan';
        AmmaEqksSloganH1.innerHTML = json.translations.seismo + json.translations.monitoring;
        AmmaEqksSloganH2.innerHTML = json.translations.slogan;
        AmmaEqksSloganLive.id = 'AmmaEqksSloganLive';
        AmmaEqksShare.id = 'AmmaEqksShare';
        AmmaEqksShareBookmarks.id = 'AmmaEqksShareBookmarks';
        AmmaEqksShareBookmarks.classList.add('AmmaEqksHidden');
        AmmaEqksShareBookmarks.href = '#';
        //AmmaEqksShareBookmarks.setAttribute('onclick', addToFavorite('test'));
        AmmaEqksShareBookmarks.innerHTML = '★ ' + '<span>' + json.translations.add_to_favorites + '</span>';
        AmmaEqksShareModal.id = 'AmmaEqksShareModal';
        AmmaEqksShareModal.href = '#';
        AmmaEqksShareModal.innerHTML = '<...> ' + '<span>' + json.translations.add_to_website + '</span>';

        AmmaEqksClear1.classList.add('AmmaEqksClear');

        AmmaEqksFilter1.classList.add('AmmaEqksFilter');
        AmmaEqksFilter1Legend.classList.add('AmmaEqksFilterLegend');
        AmmaEqksFilter1Legend.innerHTML = json.translations.magnitude + ': ';
        AmmaEqksFilter1Anchor1.href = current_url + (begin ? '?begin=' + begin : '') + (end ? '&end=' + end : '');
        AmmaEqksFilter1Anchor1.innerHTML = json.translations.all;
        AmmaEqksFilter1Anchor2.href = current_url + '?magnitude=2' + (begin ? '&begin=' + begin : '') + (end ? '&end=' + end : '');
        AmmaEqksFilter1Anchor2.innerHTML = '&ge; 2';
        AmmaEqksFilter1Anchor3.href = current_url + '?magnitude=3' + (begin ? '&begin=' + begin : '') + (end ? '&end=' + end : '');
        AmmaEqksFilter1Anchor3.innerHTML = '&ge; 3';
        AmmaEqksFilter1Anchor4.href = current_url + '?magnitude=4' + (begin ? '&begin=' + begin : '') + (end ? '&end=' + end : '');
        AmmaEqksFilter1Anchor4.innerHTML = '&ge; 4';
        AmmaEqksFilter1Anchor5.href = current_url + '?magnitude=5' + (begin ? '&begin=' + begin : '') + (end ? '&end=' + end : '');
        AmmaEqksFilter1Anchor5.innerHTML = '&ge; 5';

        if(magnitude){

            switch (magnitude) {

                case "2":

                    AmmaEqksFilter1Anchor2.classList.add('selected');

                    break;

                case "3":

                    AmmaEqksFilter1Anchor3.classList.add('selected');

                    break;

                case "4":

                    AmmaEqksFilter1Anchor4.classList.add('selected');

                    break;

                case "5":

                    AmmaEqksFilter1Anchor5.classList.add('selected');

                    break;

            }

        } else {

            AmmaEqksFilter1Anchor1.classList.add('selected');

        }

        AmmaEqksFilter2.classList.add('AmmaEqksFilter');
        AmmaEqksFilter2Legend.classList.add('AmmaEqksFilterLegend');
        AmmaEqksFilter2Legend.innerHTML = json.translations.date + ': ';
        AmmaEqksFilter2Anchor1.innerHTML = json.translations.during_24_hours;
        AmmaEqksFilter2Anchor1.href = current_url + (magnitude ? '?magnitude=' + magnitude : '');
        AmmaEqksFilter2Anchor2.innerHTML = json.translations.today;
        AmmaEqksFilter2Anchor2.href = current_url + (magnitude ? '?magnitude=' + magnitude + '&' : '?') + 'begin=' + moment().endOf('day').format('DD-MM-YYYY');
        AmmaEqksFilter2Anchor3.innerHTML = json.translations.yesterday;
        AmmaEqksFilter2Anchor3.href = current_url + (magnitude ? '?magnitude=' + magnitude + '&' : '?') + 'begin=' + moment().subtract(1, 'day').format('DD-MM-YYYY');

        AmmaEqksFilter2Anchor4.id = 'AmmaEqksShowDate';
        AmmaEqksFilter2Anchor4.setAttribute('onclick','showDatePickerForm(\'AmmaEqksDate\'); return false;');
        AmmaEqksFilter2Anchor4.href = '#';
        AmmaEqksFilter2Anchor4.innerHTML = json.translations.choose_a_date;

        AmmaEqksFilter2Anchor5.id = 'AmmaEqksShowPeriod';
        AmmaEqksFilter2Anchor5.setAttribute('onclick', 'showDatePickerForm(\'AmmaEqksPeriod\'); return false;');
        AmmaEqksFilter2Anchor5.href = '#';
        AmmaEqksFilter2Anchor5.innerHTML = json.translations.choose_a_period;

        AmmaEqksClear2.classList.add('AmmaEqksClear');

        AmmaEqksDateForm.id = 'AmmaEqksDate';
        AmmaEqksDateForm.action = current_url;
        AmmaEqksDateForm.method = 'GET';
        AmmaEqksDateForm.classList.add('AmmaEqksHidden');

        AmmaEqksDateFormInput0.name = 'magnitude';
        AmmaEqksDateFormInput0.type = 'hidden';
        AmmaEqksDateFormInput0.value = magnitude ? magnitude : '';
        if(!magnitude){
            AmmaEqksDateFormInput0.disabled = 'disabled';
        }

        AmmaEqksDateFormInput.id = 'datepicker';
        AmmaEqksDateFormInput.name = 'begin';
        AmmaEqksDateFormInput.type = 'text';
        AmmaEqksDateFormInput.value = begin ? begin : '';
        AmmaEqksDateFormInput.placeholder = begin ? begin : json.translations.date;
        AmmaEqksDateFormInput.setAttribute('autocomplete','off');
        AmmaEqksDateFormButton.type = 'submit';
        AmmaEqksDateFormButton.innerHTML = json.translations.apply;

        AmmaEqksPeriodForm.id = 'AmmaEqksPeriod';
        AmmaEqksPeriodForm.action = current_url;
        AmmaEqksPeriodForm.method = 'GET';
        AmmaEqksPeriodForm.classList.add('AmmaEqksHidden');

        AmmaEqksPeriodFormInput0.name = 'magnitude';
        AmmaEqksPeriodFormInput0.type = 'hidden';
        AmmaEqksPeriodFormInput0.value = magnitude ? magnitude : '';
        if(!magnitude){
            AmmaEqksPeriodFormInput0.disabled = 'disabled';
        }

        AmmaEqksPeriodFormInput1.id = 'beginDate';
        AmmaEqksPeriodFormInput1.name = 'begin';
        AmmaEqksPeriodFormInput1.type = 'text';
        AmmaEqksPeriodFormInput1.value = begin ? begin : '';
        AmmaEqksPeriodFormInput1.placeholder = begin ? begin : json.translations.date_begin;
        AmmaEqksPeriodFormInput1.setAttribute('autocomplete','off');
        AmmaEqksPeriodFormInput2.id = 'endDate';
        AmmaEqksPeriodFormInput2.name = 'end';
        AmmaEqksPeriodFormInput2.type = 'text';
        AmmaEqksPeriodFormInput2.value = end ? end : '';
        AmmaEqksPeriodFormInput2.placeholder = end ? end : json.translations.date_end;
        AmmaEqksPeriodFormInput2.setAttribute('autocomplete','off');
        AmmaEqksPeriodFormButton.type = 'submit';
        AmmaEqksPeriodFormButton.innerHTML = json.translations.apply;

        if(begin){

            if(end){

                AmmaEqksFilter2Anchor5.classList.add('selected');
                setTimeout(function () {
                    document.getElementById('AmmaEqksPeriod').classList.remove('AmmaEqksHidden');
                },1000)

            } else {

                AmmaEqksFilter2Anchor4.classList.add('selected');
                setTimeout(function () {
                    document.getElementById('AmmaEqksDate').classList.remove('AmmaEqksHidden');
                }, 1000);

                if(begin == moment().endOf('day').format('DD-MM-YYYY')){

                    AmmaEqksFilter2Anchor2.classList.add('selected');

                } else if(begin == moment().subtract(1, 'day').format('DD-MM-YYYY')){

                    AmmaEqksFilter2Anchor3.classList.add('selected');

                }

            }

        } else {

            AmmaEqksFilter2Anchor1.classList.add('selected');

        }

        AmmaEqksMap.id = 'AmmaEqksMap';

        ///////////////////
        // Nesting Doll //
        /////////////////

        AmmaEqksSloganH1.appendChild(AmmaEqksSloganLive);
        AmmaEqksLogoImg.appendChild(AmmaEqksLogoImage);
        AmmaEqksSlogan.appendChild(AmmaEqksSloganH1);
        AmmaEqksSlogan.appendChild(AmmaEqksSloganH2);
        AmmaEqksLogo.appendChild(AmmaEqksLogoImg);
        AmmaEqksLogo.appendChild(AmmaEqksSlogan);

        AmmaEqksShare.appendChild(AmmaEqksShareBookmarks);
        AmmaEqksShare.appendChild(AmmaEqksShareModal);

        AmmaEqksFilter1.appendChild(AmmaEqksFilter1Legend);
        AmmaEqksFilter1.appendChild(AmmaEqksFilter1Anchor1);
        AmmaEqksFilter1.appendChild(AmmaEqksFilter1Anchor2);
        AmmaEqksFilter1.appendChild(AmmaEqksFilter1Anchor3);
        AmmaEqksFilter1.appendChild(AmmaEqksFilter1Anchor4);
        AmmaEqksFilter1.appendChild(AmmaEqksFilter1Anchor5);

        //Form Date
        AmmaEqksDateForm.appendChild(AmmaEqksDateFormInput0);
        AmmaEqksDateForm.appendChild(AmmaEqksDateFormInput);
        AmmaEqksDateForm.appendChild(AmmaEqksDateFormButton);

        //Form Period
        AmmaEqksPeriodForm.appendChild(AmmaEqksPeriodFormInput0);
        AmmaEqksPeriodForm.appendChild(AmmaEqksPeriodFormInput1);
        AmmaEqksPeriodForm.appendChild(AmmaEqksPeriodFormInput2);
        AmmaEqksPeriodForm.appendChild(AmmaEqksPeriodFormButton);

        //Forms Date & Period
        AmmaEqksFilter2.appendChild(AmmaEqksFilter2Legend);
        AmmaEqksFilter2.appendChild(AmmaEqksFilter2Anchor1);
        AmmaEqksFilter2.appendChild(AmmaEqksFilter2Anchor2);
        AmmaEqksFilter2.appendChild(AmmaEqksFilter2Anchor3);
        AmmaEqksFilter2.appendChild(AmmaEqksFilter2Anchor4);
        AmmaEqksFilter2.appendChild(AmmaEqksFilter2Anchor5);
        AmmaEqksFilter2.appendChild(AmmaEqksClear2);
        AmmaEqksFilter2.appendChild(AmmaEqksDateForm);
        AmmaEqksFilter2.appendChild(AmmaEqksPeriodForm);


        AmmaEqksMapHeader.appendChild(AmmaEqksLogo);
        AmmaEqksMapHeader.appendChild(AmmaEqksShare);
        AmmaEqksMapHeader.appendChild(AmmaEqksClear1);
        AmmaEqksMapHeader.appendChild(AmmaEqksFilter1);
        AmmaEqksMapHeader.appendChild(AmmaEqksFilter2);

        AmmaEqksMapWrapper.appendChild(AmmaEqksMapHeader);
        AmmaEqksMapWrapper.appendChild(AmmaEqksMap);

        AmmaEqksWrapper.appendChild(AmmaEqksMapWrapper);


        // Date & Time Picker

        var datePicker = new Lightpick({
            field: document.getElementById('datepicker'),
            format: 'DD-MM-YYYY',
            maxDate: moment().endOf('day'),
            lang: lang
        });

        var periodPicker = new Lightpick({
            field: document.getElementById('beginDate'),
            secondField: document.getElementById('endDate'),
            singleDate: false,
            format: 'DD-MM-YYYY',
            //minDate: moment().startOf('month').add(7, 'day'),
            //maxDate: moment().endOf('month').subtract(7, 'day'),
            maxDate: moment().endOf('day'),
            lang: lang
        });

        // Функция для определения "мобильности" браузера
        function MobileDetect() {
            var UA = navigator.userAgent.toLowerCase();
            return (/android|webos|iris|bolt|mobile|iphone|ipad|ipod|iemobile|blackberry|windows phone|opera mobi|opera mini/i
                .test(UA)) ? true : false;
        }

        // Если браузер НЕ мобильный, отображаем ссылку
        if (!MobileDetect()) {
            document.getElementById('AmmaEqksShareBookmarks').classList.remove('AmmaEqksHidden');
        }

        // Функция для добавления в закладки избранного | https://sheensay.ru?p=710
        AmmaEqksShareBookmarks.addEventListener('click', function (e) {

            e.preventDefault();

            let title = document.title,
                url = document.location,
                UA = navigator.userAgent.toLowerCase(),
                isFF = UA.indexOf('firefox') != -1,
                isMac = UA.indexOf('mac') != -1,
                isWebkit = UA.indexOf('webkit') != -1,
                isIE = UA.indexOf('.net') != -1,
                combination = isMac ? 'Command/Cmd + D' : 'Ctrl + D',
                text = json.translations.ctrl_d;

            text = text.replace("{combination}", combination);

            if ((isIE) && window.external) { // IE, Firefox
                window.external.AddFavorite(url, title);
            }

            if (isMac || isWebkit || isFF) { // Webkit (Chrome, Opera), Mac
                this.innerHTML = text;
            }

        });


        //Create Map
        var EqksMap = L.map('AmmaEqksMap', {
            maxBounds:L.latLngBounds(L.latLng(-90, -180), L.latLng(90, 180)),
            maxBoundsViscosity: 1.0
        }).setView([30, 0], 3);

        //Disable default scroll
        EqksMap.scrollWheelZoom.disable();

        AmmaEqksMap.addEventListener('DOMMouseScroll', function(event) {

            event.stopPropagation();

            if (event.ctrlKey == true) {
                event.preventDefault();
                EqksMap.scrollWheelZoom.enable();
                AmmaEqksMap.classList.remove('map-scroll');
                setTimeout(function(){
                    EqksMap.scrollWheelZoom.disable();
                }, 2400);
            } else {
                EqksMap.scrollWheelZoom.disable();
                AmmaEqksMap.classList.add('map-scroll');
                setTimeout(function(){
                    AmmaEqksMap.classList.remove('map-scroll');
                }, 2400);
            }

        });

        AmmaEqksMap.addEventListener('mousewheel', function(event) {

            event.stopPropagation();

            if (event.ctrlKey == true) {
                event.preventDefault();
                EqksMap.scrollWheelZoom.enable();
                AmmaEqksMap.classList.remove('map-scroll');
                setTimeout(function(){
                    EqksMap.scrollWheelZoom.disable();
                }, 2400);
            } else {
                EqksMap.scrollWheelZoom.disable();
                AmmaEqksMap.classList.add('map-scroll');
                setTimeout(function(){
                    AmmaEqksMap.classList.remove('map-scroll');
                }, 2400);
            }

        });

        // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        //     maxZoom: 18,
        //     id: 'mapbox.streets'
        // }).addTo(EqksMap);

        L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
            maxZoom: 18,
            id: 'mapbox.streets'
        }).addTo(EqksMap);

        L.control.fullscreen({
            position: 'topright'
        }).addTo(EqksMap);

        // Add geodata to map
        geoLayer = L.geoJson(json, {

            style: function(feature) {
                var mag = feature.properties.mag;
                if (mag >= 5.0) {
                    return {
                        weight: 1,
                        fillColor: "#C90400",
                        fillOpacity: 0.72,
                        color: "#910300"
                    };
                } else if (mag >= 3.0) {
                    return {
                        weight: 1,
                        fillColor: "#ff9e00",
                        fillOpacity: 0.72,
                        color: "#c47a00"
                    };
                } else if (mag >= 2.0) {
                    return {
                        weight: 1,
                        fillColor: "#c9b25a",
                        fillOpacity: 0.72,
                        color: "#9a8647"
                    };
                } else {
                    return {
                        weight: 1,
                        fillColor: "#4b8f32",
                        fillOpacity: 0.72,
                        color: "#3d7a2d"
                    }
                }
            },

            onEachFeature: function(feature, layer) {

                var popupText = "<b>" + json.translations.location + ":</b><br>" + feature.properties.place +
                    "<br><b>" + json.translations.magnitude + ":</b> " + feature.properties.mag +
                    "<br><b>" + json.translations.date + ":</b>" + feature.properties.datetime +
                    "<br><b>" + json.translations.source + ":</b>" + feature.properties.sources;

                layer.bindPopup(popupText, {
                    closeButton: true,
                    offset: L.point(0, -20)
                });
                layer.on('click', function() {
                    layer.openPopup();
                });
            },

            pointToLayer: function(feature, latlng) {
                return L.circleMarker(latlng, {
                    radius: Math.round(feature.properties.mag) * 2.5,
                });
            },
        });


        if(eqQTY > 1000){

            // Clusterization
            var markers = L.markerClusterGroup();

            markers.addLayer(geoLayer);

            EqksMap.addLayer(markers);
            EqksMap.fitBounds(markers.getBounds());

        } else {

            geoLayer.addTo(EqksMap);

        }

        var control = L.control.layers(null, null, { collapsed:false, position: 'bottomright' }).addTo(EqksMap);

        // Instantiate KMZ parser (async)
        var kmzParser = new L.KMZParser({
            onKMZLoaded: function(layer, name) {
                control.addOverlay(layer, json.translations.tectonic_faults);
                layer.addTo(EqksMap);
            }
        });
        // Add remote KMZ files as layers (NB if they are 3rd-party servers, they MUST have CORS enabled)
        kmzParser.load(base_url + 'assets/kml/evisons_wall.kmz');

        //Create TOP blocks
        let topBlocks   = document.createElement('div'),
            topNumBlock = document.createElement('div'),
            topMaxMagnitudeBlock = document.createElement('div'),
            topStrongestBlock = document.createElement('div');

        topBlocks.id = 'AmmaEqksTopBlocks';
        topBlocks.className = 'AmmaEqksTopBlocks';
        topNumBlock.id = 'AmmaEqksTopNumBlock';
        topMaxMagnitudeBlock.id = 'AmmaEqksTopMagnitudeBlock';
        topStrongestBlock.id = 'AmmaEqksTopStrongestBlock';

        topNumBlock.innerHTML = json.translations.total_amount_of_the_earthquakes + "<br><b>" + json.metadata.count + "</b>";
        topMaxMagnitudeBlock.innerHTML = json.translations.the_strongest_earthquake + "<br><b>" + json.metadata.max_magnitude + "</b>";
        topStrongestBlock.innerHTML = json.translations.number_of_earthquakes_more_4 + "<br><b>" + json.metadata.strongest + "</b>";

        topBlocks.appendChild(topNumBlock);
        topBlocks.appendChild(topMaxMagnitudeBlock);
        topBlocks.appendChild(topStrongestBlock);

        AmmaEqksWrapper.appendChild(topBlocks);

        //Comment Button
        var commentShareWrapper = document.createElement('div'),
            leaveComment = document.createElement('div'),
            leaveCommentLegend = document.createElement('span'),
            leaveCommentBtn = document.createElement('a');

        commentShareWrapper.id = 'AmmaEqksCommentShareWrapper';

        leaveComment.id = 'AmmaEqksLeaveComment';
        leaveCommentLegend.id = 'AmmaEqksLeaveCommentLegend';
        leaveCommentLegend.innerHTML = json.translations.share_your_opinion;
        leaveCommentBtn.id = 'AmmaEqksLeaveCommentBtn';
        leaveCommentBtn.innerHTML = json.translations.leave_comment;
        leaveCommentBtn.href = '#';

        leaveComment.appendChild(leaveCommentLegend);
        leaveComment.appendChild(leaveCommentBtn);

        //SocialSharing
        var shareBtns = document.createElement('div');

        shareBtns.id = 'AmmaEqksShareBtns';
        shareBtns.classList.add('share-btn');

        // if(window.location.hostname == 'dev.allatra.tv' || window.location.hostname == 'allatra.tv'){
        //     commentShareWrapper.appendChild(leaveComment);
        //     commentShareWrapper.appendChild(shareBtns);
        //     AmmaEqksWrapper.appendChild(commentShareWrapper);
        // } else {
            AmmaEqksWrapper.appendChild(shareBtns);
        // }


        var social = [
            {id: 'vk', className: 'btn-vk', icon: 'fa fa-vk', name: 'VK'},
            {id: 'fb', className: 'btn-facebook', icon: 'fa fa-facebook', name: 'Facebook'},
            {id: 'tw', className: 'btn-twitter', icon: 'fa fa-twitter', name: 'Twitter'},
            {id: 'tg', className: 'btn-telegram', icon: 'fa fa-telegram', name: 'Telegram'},
            //{id: 'pk', className: 'btn-pocket', icon: 'fa fa-get-pocket', name: 'Pocket'},
            //{id: 're', className: 'btn-reddit', icon: 'fa fa-reddit', name: 'Reddit'},
            //{id: 'ev', className: 'btn-evernote', icon: 'fa fa-sticky-note', name: 'Evernote'},
            {id: 'in', className: 'btn-linkedin', icon: 'fa fa-linkedin', name: 'LinkedIn'},
            {id: 'pi', className: 'btn-pinterest', icon: 'fa fa-pinterest', name: 'Pinterest'},
            {id: 'sk', className: 'btn-skype', icon: 'fa fa-skype', name: 'Skype'},
            {id: 'wa', className: 'btn-whatsapp', icon: 'fa fa-whatsapp', name: 'WhatsApp'},
            {id: 'ok', className: 'btn-ok', icon: 'fa fa-odnoklassniki', name: 'Odnoklassniki'},
            {id: 'tu', className: 'btn-tumblr', icon: 'fa fa-tumblr', name: 'Tumblr'},
            //{id: 'hn', className: 'btn-hn', icon: 'fa fa-hacker-news', name: 'Hacker News'},
            //{id: 'xi', className: 'btn-xing', icon: 'fa fa-xing', name: 'Xing'},
            {id: 'mail', className: 'btn-mail', icon: 'fa fa-at', name: 'EMail'},
            {id: 'btn-print', className: 'print', icon: 'fa fa-print', name: 'Print'},
        ];

        var shareBtnContainer = shareBtns;

        for (var i = 0; i < social.length; i++) {
            var s = social[i];
            shareBtnContainer.innerHTML += '<a class="' + s.className + '" data-id="' + s.id + '"><i class="' + s.icon + '"></i></a>';
        }

        window.ShareButtons.update();

        //Create a banners
        let banners = json.banners,
            bannersCounter,
            bannersGroups = document.createElement('div'),
            bannersGroup1 = document.createElement('div'),
            bannersGroup2 = document.createElement('div'),
            bannersGroupsTitle = document.createElement('h2'),
            bannersGroupRow1 = document.createElement('div'),
            bannersGroupRow2 = document.createElement('div'),
            bannersGroup1Control = document.createElement('div'),
            bannersGroup1Next = document.createElement('button'),
            bannersGroup1Prev = document.createElement('button');

        bannersGroups.id = 'ammaEqBars';
        bannersGroup1.id = 'ammaEqBar1';
        bannersGroup1.className = 'ammaEqBar';
        bannersGroup2.id = 'ammaEqBar2';
        bannersGroup2.className = 'ammaEqBar';
        bannersGroupRow1.className = 'ammaEqBarRow';
        bannersGroupRow2.className = 'ammaEqBarRow';
        bannersGroup1Control.id = 'ammaEqBar1Control';
        bannersGroup1Next.id = 'ammaEqBar1Next';
        bannersGroup1Next.innerHTML = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
        bannersGroup1Prev.id = 'ammaEqBar1Prev';
        bannersGroup1Prev.innerHTML = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
        bannersGroupsTitle.innerText = json.translations.solutions;
        bannersGroups.appendChild(bannersGroupsTitle);

        for(bannersCounter = 0; bannersCounter < banners.length; bannersCounter++){

            let bannersGroupAnchor = document.createElement('a'),
                bannersGroupImage  = document.createElement('img'),
                bannersGroupTitle  = document.createElement('h5'),
                bannersGroupText   = document.createElement('p');

            bannersGroupAnchor.href = banners[bannersCounter].link;
            bannersGroupAnchor.className = 'ammaEqBarItem';
            bannersGroupImage.src = base_url + 'assets/uploads/images/banners/' + banners[bannersCounter].img_url;
            bannersGroupImage.alt = banners[bannersCounter].title;
            bannersGroupTitle.innerText = banners[bannersCounter].title;
            bannersGroupText.innerText = banners[bannersCounter].text;
            bannersGroupAnchor.appendChild(bannersGroupImage);
            bannersGroupAnchor.appendChild(bannersGroupTitle);
            bannersGroupAnchor.appendChild(bannersGroupText);

            if(banners[bannersCounter].position == 5){

                bannersGroupRow1.appendChild(bannersGroupAnchor);

            }else if(banners[bannersCounter].position == 6){

                bannersGroupRow2.appendChild(bannersGroupAnchor);

            }

        }

        bannersGroup1Control.appendChild(bannersGroup1Prev);
        bannersGroup1Control.appendChild(bannersGroup1Next);
        bannersGroup1.appendChild(bannersGroup1Control);
        bannersGroup1.appendChild(bannersGroupRow1);
        bannersGroup2.appendChild(bannersGroupRow2);

        //if(window.location.hostname != 'dev.allatra.tv' && window.location.hostname != 'allatra.tv'){
            bannersGroups.appendChild(bannersGroup1);
            bannersGroups.appendChild(bannersGroup2);
        //}

        //Create a EQ Table
        tableCreate(json);

        let tbl = document.createElement('table'),
            tblWrapper = document.createElement('div'),
            tblTitle = document.createElement('h2'),
            eqNum = json.features.length;

        tbl.id = 'earthquakesTable';
        tbl.className = 'table';
        tbl.style.width = '100%';
        tbl.style.textAlign = 'left';
        tblWrapper.id = 'AmmaEqksTable';
        tblTitle.innerHTML = json.translations.earthquakes_table;

        tblWrapper.appendChild(tblTitle);
        tblWrapper.appendChild(tbl);
        AmmaEqksWrapper.appendChild(tblWrapper);
        //if(window.location.hostname != 'dev.allatra.tv' && window.location.hostname != 'allatra.tv'){
            AmmaEqksWrapper.appendChild(bannersGroups);
        //}

        let dataEqTable = new DataTable(tbl, {
            data: eqData,
            // the maximum number of rows to display on each page
            perPage: 15,
            // the per page options in the dropdown
            perPageSelect: [15, 25, 50, 100, 1000],
            labels: {
                placeholder: json.translations.search,
                perPage: json.translations.entries_per_page,
                noRows: json.translations.no_entries_to_found,
                info: json.translations.per_page,
            }
        });

        //Create a pages
        let pages = json.pages,
            page1 = document.createElement('div');

        page1.className = 'AmmaEqksPage';
        page1.innerHTML = '<h1>' + pages[0].title +  '</h1>' + pages[0].anons + pages[0].text;

        if(!location.search && window.location.hostname == 'allatra.tv'){
            AmmaEqksWrapper.appendChild(page1);
        }

        /* RSS */

        //go RSS block1
        var rssBlock1 = document.createElement('div'),
            rssBlock1Container = document.createElement('div'),
            rssBlock1ButtonNext = document.createElement('button'),
            rssBlock1ButtonPrev = document.createElement('button'),
            rssBlock1Title = document.createElement('h2');

        rssBlock1.id = 'rssBlock1';
        rssBlock1Container.className = 'AmmaEqksRSSBlock';
        rssBlock1Title.className = 'AmmaEqksRSSTitle';
        rssBlock1ButtonNext.id = 'rssBlock1Next';
        rssBlock1ButtonPrev.id = 'rssBlock1Prev';

        // rss: Igor Mihajlovich Danilov
        rssBlock1Title.innerText = json.translations.programs_with_im_danilov;
        rssBlock1.appendChild(rssBlock1Title);

        json.rss.forEach(function (item) {

            let IM_DANILOV_link = '';

            switch(lang){

                case 'en':

                    IM_DANILOV_link = 'https://allatra.tv/en/rss/publications?category=im-danilov';

                    break;

                case 'ru':

                    IM_DANILOV_link = 'https://allatra.tv/rss/publications?category=im-danilov';

                    break;

                case 'it':

                    IM_DANILOV_link = 'https://allatra.tv/en/rss/publications?category=im-danilov';

                    break;

                case 'cs':

                    IM_DANILOV_link = 'https://allatra.tv/cs/rss/publications?category=im-danilov';

                    break;

                case 'sk':

                    IM_DANILOV_link = 'https://allatra.tv/sk/rss/publications?category=im-danilov';

                    break;

                default:

                    IM_DANILOV_link = 'https://allatra.tv/en/rss/publications?category=im-danilov';

                    break;


            }

            if(item.link == IM_DANILOV_link){

                let rssItem = document.createElement('div'),
                    rssItemAnchor = document.createElement('a'),
                    rssItemParagraph = document.createElement('p'),
                    rssItemImage = document.createElement('img');

                rssItem.className = 'AmmaEqksRSSItem';
                rssItemAnchor.href = item.url;
                rssItemImage.src = item.img;
                rssItemImage.alt = item.title;

                rssItemAnchor.appendChild(rssItemImage);
                rssItemParagraph.innerHTML = item.title;
                rssItemAnchor.appendChild(rssItemParagraph);
                rssItem.appendChild(rssItemAnchor);

                rssBlock1Container.appendChild(rssItem);
                rssBlock1.appendChild(rssBlock1Container);
                rssBlock1.appendChild(rssBlock1ButtonPrev);
                rssBlock1.appendChild(rssBlock1ButtonNext);

            }

        });

        // rss: Climate Changes
        var rssBlock2 = document.createElement('div'),
            rssBlock2Container = document.createElement('div'),
            rssBlock2ButtonNext = document.createElement('button'),
            rssBlock2ButtonPrev = document.createElement('button'),
            rssBlock2Title = document.createElement('h2');

        rssBlock2.id = 'rssBlock2';
        rssBlock2Container.className = 'AmmaEqksRSSBlock';
        rssBlock2Title.className = 'AmmaEqksRSSTitle';
        rssBlock2ButtonNext.id = 'rssBlock2Next';
        rssBlock2ButtonPrev.id = 'rssBlock2Prev';

        rssBlock2Title.innerText = json.translations.climate_change;
        rssBlock2.appendChild(rssBlock2Title);

        json.rss.forEach(function (item) {

            let climateLink = '';

            switch(lang){

                case 'en':

                    climateLink = 'https://allatra.tv/en/rss/publications?category=climate';

                    break;

                case 'ru':

                    climateLink = 'https://allatra.tv/rss/publications?category=climate';

                    break;

                case 'it':

                    climateLink = 'https://allatra.tv/it/rss?type=videos&category=cambiamento-climatico';

                    break;

                case 'cs':

                    climateLink = 'https://allatra.tv/cs/rss?type=videos&category=zmena-klimatu';

                    break;

                case 'sk':

                    climateLink = 'https://allatra.tv/sk/rss?type=videos&category=zmeny-klimy';

                    break;

                default:

                    climateLink = 'https://allatra.tv/en/rss/publications?category=climate';

                    break;


            }

            if(item.link == climateLink){

                let rssItem = document.createElement('div'),
                    rssItemAnchor = document.createElement('a'),
                    rssItemParagraph = document.createElement('p'),
                    rssItemImage = document.createElement('img');

                rssItem.className = 'AmmaEqksRSSItem';
                rssItemAnchor.href = item.url;
                rssItemImage.src = item.img;
                rssItemImage.alt = item.title;

                rssItemAnchor.appendChild(rssItemImage);
                rssItemParagraph.innerHTML = item.title;
                rssItemAnchor.appendChild(rssItemParagraph);
                rssItem.appendChild(rssItemAnchor);

                rssBlock2Container.appendChild(rssItem);
                rssBlock2.appendChild(rssBlock2Container);
                rssBlock2.appendChild(rssBlock2ButtonPrev);
                rssBlock2.appendChild(rssBlock2ButtonNext);

            }

        });



        //if(window.location.hostname != 'dev.allatra.tv' && window.location.hostname != 'allatra.tv'){
            //Place RSS
            AmmaEqksWrapper.appendChild(rssBlock1);
            AmmaEqksWrapper.appendChild(rssBlock2);
        //}

        //Go RSS 1 Carousel
        const Siema1 = new Siema({
            selector: '#rssBlock1 .AmmaEqksRSSBlock',
            easing: 'ease-out',
            perPage: {
                768: 2,
                1024: 3,
                1280: 4,
            },
            draggable: true,
            multipleDrag: true,
            loop: true,
        });

        const prev1 = document.getElementById('rssBlock1Prev');
        const next1 = document.getElementById('rssBlock1Next');

        prev1.addEventListener('click', () => Siema1.prev());
        next1.addEventListener('click', () => Siema1.next());

        //Go RSS 2 Carousel
        const Siema2 = new Siema({
            selector: '#rssBlock2 .AmmaEqksRSSBlock',
            easing: 'ease-out',
            perPage: {
                768: 2,
                1024: 3,
                1280: 4,
            },
            draggable: true,
            multipleDrag: true,
            loop: true,
        });

        const prev2 = document.getElementById('rssBlock2Prev');
        const next2 = document.getElementById('rssBlock2Next');

        prev2.addEventListener('click', () => Siema2.prev());
        next2.addEventListener('click', () => Siema2.next());

        //Go Banners RSS
        const Siema3 = new Siema({
            selector: '#ammaEqBar1 .ammaEqBarRow',
            duration: 400,
            easing: 'ease-out',
            perPage: {
                768: 1,
                1024: 1,
                1280: 1,
            },
            draggable: true,
            multipleDrag: true,
            loop: true,
        });


        // listen for keydown event
        setInterval(() => Siema3.next(), 7000)

        const prev3 = document.getElementById('ammaEqBar1Prev');
        const next3 = document.getElementById('ammaEqBar1Next');

        prev3.addEventListener('click', () => Siema3.prev());
        next3.addEventListener('click', () => Siema3.next());

        // Modal

        //Create Modals
        var ammaModalWrapper = document.createElement("div"),
            ammaModalContainer = document.createElement("div"),
            ammaModal = document.createElement("div"),
            ammaModalClose = document.createElement("a"),
            ammaModalContent = document.createElement("div");

        ammaModalWrapper.id = 'AmmaEqksModalWrapper';
        ammaModalWrapper.className = 'AmmaEqksHidden';
        ammaModalContainer.id = 'AmmaEqksModalContainer';
        ammaModal.id = 'AmmaEqksModal';
        ammaModalClose.id = 'AmmaEqksModalClose';
        ammaModalClose.href = '#';
        ammaModalContent.id = 'AmmaEqksModalContent';

        ammaModalClose.innerHTML = 'X';
        ammaModalContent.innerHTML = '<p>' + json.translations.modal_content + '</p>';
        ammaModal.appendChild(ammaModalClose);
        ammaModal.appendChild(ammaModalContent);

        ammaModalContainer.appendChild(ammaModal);
        ammaModalWrapper.appendChild(ammaModalContainer);
        AmmaEqksWrapper.appendChild(ammaModalWrapper);

        //Close Modal
        closeModalBtn = document.getElementById('AmmaEqksModalClose');

        closeModalBtn.addEventListener("click", function(e) {

            e.preventDefault();
            document.getElementById('AmmaEqksModalContainer').style.top = '-100%';
            setTimeout(function () {
                document.getElementById('AmmaEqksModalWrapper').classList.add('AmmaEqksHidden');
            },600);
            document.body.classList.remove('AmmaEqksHiddenOverflowY');

        });

        //Open Modal
        openModalBtn = document.querySelector('#AmmaEqksShareModal');

        openModalBtn.addEventListener("click", function(e) {

            e.preventDefault();
            document.getElementById('AmmaEqksModalWrapper').classList.remove('AmmaEqksHidden');
            setTimeout(function () {
                document.getElementById('AmmaEqksModalContainer').style.top = '15%';
            },200);
            document.body.classList.add('AmmaEqksHiddenOverflowY');

        });

    } else {
        // We reached our target server, but it returned an error
        console.log('There was some error in json request!')
    }

};

request.onerror = function() {
    // There was a connection error of some sort
};

//init json data request
request.send();