document.addEventListener("DOMContentLoaded", ready);

function ready(){

    var base_url = 'https://maps.ramir.space/';

    //Import CSS!!!
    var cssId = 'AmmaCss';  // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId))
    {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = cssId;
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = base_url + 'assets/css/allatra-safe-road.css';
        link.media = 'all';
        head.appendChild(link);
    }

    var safeRoadMap = document.getElementById('safeRoadMap'),
        premaBlockContainer = document.createElement('div'),
        premaBlockRow = document.createElement('div'),
        premaBlockCol_1 = document.createElement('div'),
        premaBlockCol_2 = document.createElement('div'),
        premaBlockBar = document.createElement('div'),
        premaBlockMap = document.createElement('div');

    premaBlockContainer.classList.add('ammaContainer');
    premaBlockRow.classList.add('ammaRow');
    premaBlockCol_1.classList.add('ammaCol-1-2');
    premaBlockCol_2.classList.add('ammaCol-1-2');
    premaBlockBar.id = 'ammaPremaBar';
    premaBlockMap.id = 'ammaPremaMap';
    premaBlockBar.classList.add('ammaBar');
    premaBlockMap.style.height = '340px';

    premaBlockCol_1.appendChild(premaBlockMap);
    premaBlockCol_2.appendChild(premaBlockBar);
    premaBlockRow.appendChild(premaBlockCol_1);
    premaBlockRow.appendChild(premaBlockCol_2);
    premaBlockContainer.appendChild(premaBlockRow);
    safeRoadMap.appendChild(premaBlockContainer);

    // Go Map

    // Hook for showing markers in visible area. Working only with virtual markers!!!
    L.Marker.addInitHook(function() {
        if (this.options.virtual) {
            // setup virtualization after marker was added
            this.on('add', function() {

                this._updateIconVisibility = function() {
                    var map = this._map,
                        isVisible = map.getBounds().contains(this.getLatLng()),
                        wasVisible = this._wasVisible,
                        icon = this._icon,
                        iconParent = this._iconParent,
                        shadow = this._shadow,
                        shadowParent = this._shadowParent;

                    // remember parent of icon
                    if (!iconParent) {
                        iconParent = this._iconParent = icon.parentNode;
                    }
                    if (shadow && !shadowParent) {
                        shadowParent = this._shadowParent = shadow.parentNode;
                    }

                    // add/remove from DOM on change
                    if (isVisible != wasVisible) {
                        if (isVisible) {
                            iconParent.appendChild(icon);
                            if (shadow) {
                                shadowParent.appendChild(shadow);
                            }
                        } else {
                            iconParent.removeChild(icon);
                            if (shadow) {
                                shadowParent.removeChild(shadow);
                            }
                        }

                        this._wasVisible = isVisible;

                    }
                };

                // on map size change, remove/add icon from/to DOM
                this._map.on('resize moveend zoomend', this._updateIconVisibility, this);
                this._updateIconVisibility();

            }, this);
        }
    });

    var roadMap = L.map('ammaPremaMap', {zoomControl: false, minZoom: 6});


    // Detecting location

    roadMap.setView([50.401699, 30.252512], 8);

    L.control.zoom({
        position: 'bottomright'
    }).addTo(roadMap);

    L.control.fullscreen({
        position: 'topright'
    }).addTo(roadMap);


    new L.Control.GeoSearch({
        provider: new L.GeoSearch.Provider.OpenStreetMap(),
        style: 'bar',
        showMarker: true,
        showPopup: true,
        marker: {                                           // optional: L.Marker    - default L.Icon.Default
            icon: new L.Icon.Default(),
            draggable: true,
        },
        animateZoom: true,
        searchLabel: 'Поиск'
    }).addTo(roadMap);

//Disable default scroll
    roadMap.scrollWheelZoom.disable();

    premaBlockMap.addEventListener('DOMMouseScroll', function(event) {

        event.stopPropagation();
        //e.preventDefault();

        if (event.ctrlKey == true) {
            event.preventDefault();
            roadMap.scrollWheelZoom.enable();
            premaBlockMap.classList.remove('map-scroll');
            setTimeout(function(){
                roadMap.scrollWheelZoom.disable();
            }, 1000);
        } else {
            roadMap.scrollWheelZoom.disable();
            premaBlockMap.classList.add('map-scroll');
            setTimeout(function(){
                premaBlockMap.classList.remove('map-scroll');
            }, 1000);
        }

    });

//Saferoad`s table
    let srData = {
        "headings": [
            "#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
            "Регион&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
            "Дата",
            "Координаты",
            "Описание"
        ],
        "data": []
    }

    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
        id: 'mapbox.streets'
    }).addTo(roadMap);

    // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    //     maxZoom: 18,
    //     id: 'mapbox.streets'
    // }).addTo(roadMap);

    /* RSS */
// Note: some RSS feeds can't be loaded in the browser due to CORS security.
// To get around this, you can use a proxy.
    const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

//go AmmaContent
    var ammaContent = document.createElement('div'),
        ammaContentContainer = document.createElement('div');

    ammaContent.id = 'ammaContent';
    ammaContentContainer.id = 'ammaContentContainer';
    ammaContentContainer.className = 'ammaContainer';

    ammaContent.appendChild(ammaContentContainer);

    safeRoadMap.appendChild(ammaContent);

    var request = new XMLHttpRequest();
    request.open('GET', 'https://maps.ramir.space/saferoad/json', true);

    request.onload = function() {

        // Success!
        if (request.status >= 200 && request.status < 400) {

            let body = document.getElementsByTagName('body')[0];

            //get data!
            var json = JSON.parse(request.responseText);

            var icon = [];

            icon['whiteIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/white-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/white-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                shadowSize:  [41, 41]
            });

            icon['AllatRaBlackIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/allatra-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/allatra-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                shadowSize:  [41, 41]
            });

            icon['redIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/red-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/red-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                shadowSize:  [41, 41]
            });


            icon['orangeIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/orange-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/orange-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                shadowSize:  [41, 41]
            });

            //Create Map
            geoLayer = L.geoJson(json, {

                style: function(feature) {
                    var mag = feature.properties.mag;

                    return {
                        weight: 1,
                        fillColor: "#ff0000",
                        fillOpacity: 0.72,
                        color: "#ff0000"
                    };

                },

                onEachFeature: function(feature, layer) {

                    var popupText = feature.properties.description +
                        "<br><b>Дата:</b> " + feature.properties.datetime +
                        "<br><b>Источник:</b> " + feature.properties.sources;

                    layer.bindPopup(popupText, {
                        closeButton: true,
                        offset: L.point(0, -5)
                    });
                    layer.on('click', function() {
                        layer.openPopup();
                    });
                },

                pointToLayer: function(feature, latlng) {

                    return L.marker(latlng, {
                        virtual: true,
                        icon: icon[feature.properties.icon]
                    });

                },

            }).addTo(roadMap);


            var is_logged_in = 1;

            /* Draw the Map */
            if(is_logged_in == 1){

                var drawnItems = L.geoJson().addTo(roadMap);

                roadMap.addControl(new L.Control.Draw({
                    draw: {
                        circle : false,
                        circlemarker : false,
                        polygon: {
                            allowIntersection: false,
                            showArea: true
                        }
                    },
                    edit: {
                        featureGroup: drawnItems,
                        poly: {
                            allowIntersection: false
                        }
                    }
                }));

                roadMap.on(L.Draw.Event.CREATED, function (e) {
                    var layer = e.layer,
                        feature = layer.feature = layer.feature || {};
                    feature.type = feature.type || "Feature";
                    var props = feature.properties = feature.properties || {};
                    props.desc = null;
                    props.image = null;
                    props.source = null;
                    props.officially = 0;
                    drawnItems.addLayer(layer);
                    addPopup(layer);
                });

                function addPopup(layer) {

                    var wrapper = document.createElement('div');
                    wrapper.className = 'popup-wrapper';
                    wrapper.style.width = '200px';

                    var formGroup1  = document.createElement('div');
                    formGroup1.className = "form-group";

                    var formGroup2  = document.createElement('div');
                    formGroup2.className = "form-group";

                    var formGroup3  = document.createElement('div');
                    formGroup3.className = "form-group";

                    var customCheckbox = document.createElement('div');
                    customCheckbox.className = "custom-control custom-checkbox";

                    var labelCheckbox = document.createElement('label');
                    labelCheckbox.className = "custom-control-label";
                    labelCheckbox.htmlFor = "official-check-1";
                    labelCheckbox.innerHTML = "Официальный источник?";

                    /* content */

                    var content = document.createElement("textarea");
                    content.className = "form-control form-control-square form-control-sm";
                    content.placeholder  = "Описание места";

                    content.addEventListener("keyup", function () {
                        layer.feature.properties.desc = content.value;
                    });

                    /* eof. content */

                    /* source */

                    var source = document.createElement("input");
                    source.className = "form-control form-control-square form-control-sm";
                    source.placeholder = "Ссылка на источник";

                    source.addEventListener("keyup", function () {
                        layer.feature.properties.source = source.value;
                    });

                    /* eof. source */

                    /* officially */

                    var officially = document.createElement("input");
                    officially.className = "custom-control-input";
                    officially.type = "checkbox";
                    officially.id = "official-check-1";
                    officially.value = 0;

                    officially.addEventListener("change", function () {

                        if(this.checked) {
                            layer.feature.properties.officially = 1;
                        }else{
                            layer.feature.properties.officially = 0;
                        }

                    });

                    customCheckbox.appendChild(officially);
                    customCheckbox.appendChild(labelCheckbox);

                    /* eof. officially */

                    layer.on("popupopen", function () {
                        source.value  = layer.feature.properties.source;
                        content.value = layer.feature.properties.desc;
                        officially.value = 1;
                        content.focus();
                    });

                    formGroup1.appendChild(content);
                    formGroup2.appendChild(source);
                    formGroup3.appendChild(customCheckbox);

                    wrapper.appendChild(formGroup1);
                    wrapper.appendChild(formGroup2);
                    wrapper.appendChild(formGroup3);

                    layer.bindPopup(wrapper).openPopup();
                }

                // NOT WORKING!
                // on click, clear all layers
                /*document.getElementById('delete').onclick = function(e) {
                    drawnItems.clearLayers();
                }*/

                var saveControl = L.Control.extend({
                    options: {
                        position: 'topleft'
                    },
                    onAdd: function (roadMap) {
                        var container = L.DomUtil.create('div', 'leaflet-control-save leaflet-bar leaflet-control');
                        container.innerHTML = '<a id="save" style="height: 30px; width: 30px; cursor: pointer; opacity: 0.65; background-size: 16px 16px; background-repeat: no-repeat; background-position: 50% 50%; background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzIgMzIiIGhlaWdodD0iMzJweCIgaWQ9InN2ZzIiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDMyIDMyIiB3aWR0aD0iMzJweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgaWQ9ImJhY2tncm91bmQiPjxyZWN0IGZpbGw9Im5vbmUiIGhlaWdodD0iMzIiIHdpZHRoPSIzMiIvPjwvZz48ZyBpZD0iZmxvcHB5X3g1Rl9kaXNrIj48cGF0aCBkPSJNMjYsMThINnYyaDIwVjE4eiBNMjYsMjJINnYyaDIwVjIyeiBNMjYsMjZINnYyaDIwVjI2eiBNMjYsMGgtMnYxM0g4VjBIMHYzMmgzMlY2TDI2LDB6IE0yOCwzMEg0VjE2aDI0VjMweiBNMjIsMmgtNHY5aDQgICBWMnoiLz48L2c+PC9zdmc+)"></a>';
                        container.style.backgroundColor = 'white';
                        container.style.width = '33px';
                        container.style.height = '33px';
                        container.onclick = function(){
                            console.log('buttonClicked');
                        }
                        return container;
                    },
                });

                roadMap.addControl(new saveControl());

                // document.getElementById('save').onclick = function(e) {
                //
                //     var userGeoData = JSON.stringify(drawnItems.toGeoJSON());
                //
                //     if(userGeoData == '{"type":"FeatureCollection","features":[]}'){
                //
                //         alert('Ошибка! Вы ещё ничего не добавили на карту.');
                //
                //     }else{
                //
                //         postAjax(base_url + "feedback/geodata", 'content=' + userGeoData, function(data){ console.log(data); });
                //
                //     }
                //
                // }

            }
            /* EOF. DrawMap */

            //Create a banners
            let banners = json.banners,
                bannersCounter,
                bannersGroup1 = document.createElement('div'),
                bannersGroupRow1 = document.createElement('div');

            bannersGroup1.id = 'ammaBar1';
            bannersGroup1.className = 'ammaBar';
            bannersGroupRow1.className = 'ammaBarRow';

            for(bannersCounter = 0; bannersCounter < banners.length; bannersCounter++){

                let bannersGroupTitle  = document.createElement('h3'),
                    bannersGroupText  = document.createElement('div');

                bannersGroupTitle.innerText = banners[bannersCounter].title;
                bannersGroupText.innerHTML = banners[bannersCounter].text;

                if(banners[bannersCounter].position == 3){

                    ammaPremaBar.appendChild(bannersGroupTitle);
                    ammaPremaBar.appendChild(bannersGroupText);

                }

            }

            bannersGroup1.appendChild(bannersGroupRow1);

            //Create a EQ Table
            tableCreate(json);

            let tbl     = document.createElement('table'),
                eqNum   = json.features.length;

            tbl.id = 'earthquakesMap';
            tbl.className = 'table';
            tbl.style.width = '100%';
            tbl.style.textAlign = 'left';

            ammaContentContainer.prepend(tbl);

            let dataEqTable = new DataTable(tbl, {
                data: srData,
                // the maximum number of rows to display on each page
                perPage: 5,
                // the per page options in the dropdown
                perPageSelect: [5, 25, 50, 100, 1000],
                labels: {
                    placeholder: 'Поиск',
                    perPage: '{select} Записей на страницу',
                    noRows: 'Записей не найдено',
                    info: 'Показано {start} до {end} из {rows} записей',
                }
            });

        } else {
            // We reached our target server, but it returned an error
            console.log('There was some error in json request!')
        }

    };

    function postAjax(url, data, success) {
        var params = typeof data == 'string' ? data : Object.keys(data).map(
            function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');

        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xhr.open('POST', url);
        xhr.responseType = 'json';
        xhr.onreadystatechange = function() {
            if (xhr.readyState>3 && xhr.status==200) {
                let message = document.createElement('p');
                if(xhr.response.err == 0){
                    message.classList.add('ammaMessage', 'ammaSuccess');
                } else {
                    message.classList.add('ammaMessage', 'ammaError');
                }
                message.innerHTML = xhr.response.content;
                document.getElementById('ammaPremaMap').parentNode.appendChild(message);
                setTimeout(function () {
                    message.style.opacity = 0;
                    setTimeout(function () {
                        message.remove();
                    }, 1000);
                }, 6000);
            }
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
        return xhr;
    }

    request.onerror = function() {
        // There was a connection error of some sort
    };

//init json data request
    request.send();

    function tableCreate(json) {

        let srNum = json.features.length;
        let y = 0;

        //Create tbody
        for(let i=0;i < srNum; i++){

            if(json.features[i].geometry.type == 'Point'){
                srData.data[y] = [];
                srData.data[y][0] = y;
                srData.data[y][1] = '-';
                srData.data[y][2] = json.features[i].properties.datetime;
                srData.data[y][3] = json.features[i].geometry.coordinates;
                srData.data[y][4] = json.features[i].properties.description;
                y++
            }

        }

    }

//OpenList
    var ammaPages = document.getElementsByClassName("ammaPage"),
        ammaPagesCounter,
        allPage,
        fullPage,
        hiddenPage,
        hiddenPages = document.getElementsByClassName("ammaPageHidden"),
        readmoreCounter,
        hiddenPageCounter;

    setTimeout(function(){

        for(ammaPagesCounter = 0; ammaPagesCounter < ammaPages.length; ammaPagesCounter++){

            let ammaReadmore = document.createElement('a');
            ammaReadmore.href = '#';
            ammaReadmore.className = 'ammaReadmore';

            ammaPages[ammaPagesCounter].appendChild(ammaReadmore)

        }

        let readmoreBtns = document.getElementsByClassName("ammaReadmore");

        for (readmoreCounter = 0; readmoreCounter < readmoreBtns.length; readmoreCounter++) {

            readmoreBtns[readmoreCounter].addEventListener("click", function(e) {

                e.preventDefault();

                allPage = this.closest('.ammaPage');
                this.style.display = "none";
                hiddenPage = allPage.querySelector('.ammaPageHidden');
                fullPage = allPage.querySelector('.ammaPageFull');
                hiddenPage.classList.add("open");
                hiddenPage.style.maxHeight = fullPage.scrollHeight + "px";

            });
        }

    }, 1000);

}