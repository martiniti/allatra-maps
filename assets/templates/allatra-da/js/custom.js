$(document).ready(function () {

    var roadMap = L.map('roadAllatRaMap').setView([51.7013254,39.1444605], 5);

    const options = {delay: 400, dashArray: [10,20], weight: 5, color: "#0000FF", pulseColor: "#FFFFFF"};

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(roadMap);

    //L.Icon.Default.prototype.options.imagePath = "http://allatra-da.ramir.space/assets/template/allatra-da/img/map-marker.png";
    //L.Icon.Default.prototype.options.shadowUrl = 'http://allatra-da.ramir.space/assets/template/allatra-da/img/map-marker.png';
    //L.Icon.Default.prototype.options.iconRetinaUrl = 'http://allatra-da.ramir.space/assets/template/allatra-da/img/map-marker.png';

    //L.Icon.Default.prototype.options.imagePath('http://allatra-da.ramir.spaceassets/template/allatra-da/img/map-marker.png');


    new L.Control.GeoSearch({
        provider: new L.GeoSearch.Provider.OpenStreetMap(),
        style: 'bar',
        showMarker: true,
        showPopup: true,
        marker: {                                           // optional: L.Marker    - default L.Icon.Default
            icon: new L.Icon.Default(),
            draggable: true,
        },
        animateZoom: true,
        searchLabel: 'Поиск'
    }).addTo(roadMap);

    var defaultIcon = new L.Icon({
        iconUrl: base_url + 'assets/leaflet/images/marker-icon.png',
        iconRetinaUrl: base_url + 'assets/leaflet/images/marker-icon-2x.png',
        iconSize:    [25, 41],
        iconAnchor:  [12, 41],
        popupAnchor: [1, -34],
        shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
        shadowSize:  [41, 41]
    });

    var blackIcon = new L.Icon({
        iconUrl: base_url + 'assets/leaflet/images/marker-icon-black.png',
        iconRetinaUrl: base_url + 'assets/leaflet/images/marker-icon-black-2x.png',
        iconSize:    [25, 41],
        iconAnchor:  [12, 41],
        popupAnchor: [1, -34],
        shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
        shadowSize:  [41, 41]
    });

    var AllatRaIcon = new L.Icon({
        iconUrl: base_url + 'assets/leaflet/images/marker-icon.svg',
        iconRetinaUrl: base_url + 'assets/leaflet/images/marker-icon-2x.svg',
        iconSize:    [25, 41],
        iconAnchor:  [12, 41],
        popupAnchor: [1, -34],
        shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
        shadowSize:  [41, 41]
    });

    var layerGroup = L.geoJSON(geolines, {
        onEachFeature: function (feature, layer) {
            layer.bindPopup('<p>'+feature.properties.desc+'</p>');
            if (layer instanceof L.Polyline) {
                layer.setStyle({
                    'color': feature.properties.color
                });
            }
        },
        pointToLayer: function(feature, latlng) {

            if (feature.properties.icon == 'defaultIcon') {

                return L.marker(latlng, {
                    icon: defaultIcon
                });

            }else if(feature.properties.icon == 'blackIcon') {

                return L.marker(latlng, {
                    icon: blackIcon
                });

            }else if(feature.properties.icon == 'AllatRaIcon'){

                return L.marker(latlng, {
                    icon: AllatRaIcon
                });

            }


        },
    }).addTo(roadMap);

    var popup = L.popup();

    var printer = L.easyPrint({
        //tileLayer: tiles,
        sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
        filename: 'myMap',
        exportOnly: true,
        hideControlContainer: true
    }).addTo(roadMap);

    function manualPrint () {
        printer.printMap('CurrentSize', 'MyManualPrint')
    }

    //Disable default scroll
    roadMap.scrollWheelZoom.disable();

    $("#roadAllatRaMap").bind('mousewheel DOMMouseScroll', function (event) {

        event.stopPropagation();
        if (event.ctrlKey == true) {
            event.preventDefault();
            roadMap.scrollWheelZoom.enable();
            $('#roadAllatRaMap').removeClass('map-scroll');
            setTimeout(function(){
                roadMap.scrollWheelZoom.disable();
            }, 1000);
        } else {
            roadMap.scrollWheelZoom.disable();
            $('#roadAllatRaMap').addClass('map-scroll');
            setTimeout(function(){
                $('#roadAllatRaMap').removeClass('map-scroll');
            }, 1000);
        }

    });

    $(window).bind('mousewheel DOMMouseScroll', function (event) {
        $('#roadAllatRaMap').removeClass('map-scroll');
    });


    /* Draw the Map */
    if(is_logged_in == 1){

        var drawnItems = L.geoJson().addTo(roadMap);

        roadMap.addControl(new L.Control.Draw({
            draw: {
                circle : false,
                circlemarker : false,
                polygon: {
                    allowIntersection: false,
                    showArea: true
                }
            },
            edit: {
                featureGroup: drawnItems,
                poly: {
                    allowIntersection: false
                }
            }
        }));

        roadMap.on(L.Draw.Event.CREATED, function (e) {
            var layer = e.layer,
                feature = layer.feature = layer.feature || {};
            feature.type = feature.type || "Feature";
            var props = feature.properties = feature.properties || {};
            props.desc = null;
            props.image = null;
            props.source = null;
            props.officially = 0;
            drawnItems.addLayer(layer);
            addPopup(layer);
        });

        function addPopup(layer) {
            /*var content =
                '<div class="popup-form" style="min-width: 200px;">' +
                    '<div class="form-group"><textarea class="form-control form-control-square form-control-sm" name="desc" placeholder="Описание"></textarea></div>' +
                    '<div class="form-group"><input class="form-control form-control-square form-control-sm" name="source" placeholder="Источник" /></div>' +
                    '<div class="form-group"><div class="custom-control custom-checkbox"> <input class="custom-control-input" name="officially" type="checkbox" id="official-check-1"> <label class="custom-control-label" for="official-check-1">Официальный источник?</label> </div></div>' +
                '</div>';*/

            var wrapper = document.createElement('div');
            wrapper.className = 'popup-wrapper';
            wrapper.style.width = '200px';

            var formGroup1  = document.createElement('div');
            formGroup1.className = "form-group";

            var formGroup2  = document.createElement('div');
            formGroup2.className = "form-group";

            var formGroup3  = document.createElement('div');
            formGroup3.className = "form-group";

            var customCheckbox = document.createElement('div');
            customCheckbox.className = "custom-control custom-checkbox";

            var labelCheckbox = document.createElement('label');
            labelCheckbox.className = "custom-control-label";
            labelCheckbox.htmlFor = "official-check-1";
            labelCheckbox.innerHTML = "Официальный источник?";

            /* content */

            var content = document.createElement("textarea");
            content.className = "form-control form-control-square form-control-sm";
            content.placeholder  = "Описание места";

            content.addEventListener("keyup", function () {
                layer.feature.properties.desc = content.value;
            });

            /* eof. content */

            /* source */

            var source = document.createElement("input");
            source.className = "form-control form-control-square form-control-sm";
            source.placeholder = "Ссылка на источник";

            source.addEventListener("keyup", function () {
                layer.feature.properties.source = source.value;
            });

            /* eof. source */

            /* officially */

            var officially = document.createElement("input");
            officially.className = "custom-control-input";
            officially.type = "checkbox";
            officially.id = "official-check-1";
            officially.value = 0;

            officially.addEventListener("change", function () {

                if(this.checked) {
                    layer.feature.properties.officially = 1;
                }else{
                    layer.feature.properties.officially = 0;
                }

            });

            customCheckbox.appendChild(officially);
            customCheckbox.appendChild(labelCheckbox);

            /* eof. officially */

            layer.on("popupopen", function () {
                source.value  = layer.feature.properties.source;
                content.value = layer.feature.properties.desc;
                officially.value = 1;
                content.focus();
            });

            formGroup1.appendChild(content);
            formGroup2.appendChild(source);
            formGroup3.appendChild(customCheckbox);

            wrapper.appendChild(formGroup1);
            wrapper.appendChild(formGroup2);
            wrapper.appendChild(formGroup3);

            layer.bindPopup(wrapper).openPopup();
        }

        // NOT WORKING!
        // on click, clear all layers
        /*document.getElementById('delete').onclick = function(e) {
            drawnItems.clearLayers();
        }*/

        var saveControl = L.Control.extend({
            options: {
                position: 'topleft'
            },
            onAdd: function (roadMap) {
                var container = L.DomUtil.create('div', 'leaflet-control-save leaflet-bar leaflet-control');
                container.innerHTML = '<a id="save" style="height: 30px; width: 30px; cursor: pointer; opacity: 0.65; background-size: 16px 16px; background-repeat: no-repeat; background-position: 50% 50%; background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzIgMzIiIGhlaWdodD0iMzJweCIgaWQ9InN2ZzIiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDMyIDMyIiB3aWR0aD0iMzJweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgaWQ9ImJhY2tncm91bmQiPjxyZWN0IGZpbGw9Im5vbmUiIGhlaWdodD0iMzIiIHdpZHRoPSIzMiIvPjwvZz48ZyBpZD0iZmxvcHB5X3g1Rl9kaXNrIj48cGF0aCBkPSJNMjYsMThINnYyaDIwVjE4eiBNMjYsMjJINnYyaDIwVjIyeiBNMjYsMjZINnYyaDIwVjI2eiBNMjYsMGgtMnYxM0g4VjBIMHYzMmgzMlY2TDI2LDB6IE0yOCwzMEg0VjE2aDI0VjMweiBNMjIsMmgtNHY5aDQgICBWMnoiLz48L2c+PC9zdmc+)"></a>';
                container.style.backgroundColor = 'white';
                container.style.width = '33px';
                container.style.height = '33px';
                container.onclick = function(){
                    console.log('buttonClicked');
                }
                return container;
            },
        });

        roadMap.addControl(new saveControl());

        document.getElementById('save').onclick = function(e) {

            var userGeoData = JSON.stringify(drawnItems.toGeoJSON());

            //console.log(userGeoData);

            if(userGeoData == '{"type":"FeatureCollection","features":[]}'){
                alert('Ошибка! Вы ещё ничего не добавили на карту.');
            }else{

                $.ajax({
                    url: base_url_lang + "feedback/geodata",
                    type: 'post',
                    dataType: 'json',
                    data: {content: userGeoData} ,
                    success: function(data) {
                        alert(data.content);
                        drawnItems.clearLayers();
                        location.reload();
                    }
                });

            }

        }


    }

    /* Latvia */

    var tmpl = {title:"<b>Melnais punkts</span>", content:"{AC_NOSAUKUMS} {KM_NO}"};
    var csvlayer = new CSVLayer({
        url: base_url + "assets/uploads/files/csv/CSV_SIC_MPPOINT.csv",
        opacity: 1,
        refreshInterval:5,
        popupTemplate:tmpl
    });
    csvlayer.renderer = {
        type: "simple",
        symbol:  {type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
            url: base_url + 'assets/leaflet/images/marker-icon.svg',
            width: "25px",
            height: "41px"
        }
    };
    roadMap.add(csvlayer);

    /* Mask */

    var point1 = L.latLng(50.529339, 30.325596),
        point2 = L.latLng(50.533628, 30.68018),
        point3 = L.latLng(50.363949, 30.330162);

    var	KievGeoBounds = new L.LatLngBounds(point1,point2).extend(point3);

    var KievGeoOverlay = new L.ImageOverlay.Rotated("http://allatra-da.ramir.space/assets/uploads/images/masks/kiev-geo-min.jpg", point1, point2, point3, {
        opacity: 0.5,
        interactive: true,
        attribution: 'Зоны геомагнитных разломов'
    });



    $('select.GeoMasks').on('change', function () {

        var mask = $(this).val();

        if(mask == 'geoKiev'){

            var marker1 = L.marker(point1, {draggable: true} ).addTo(roadMap),
                marker2 = L.marker(point2, {draggable: true} ).addTo(roadMap),
                marker3 = L.marker(point3, {draggable: true} ).addTo(roadMap);

            marker1.on('drag dragend', repositionImage);
            marker2.on('drag dragend', repositionImage);
            marker3.on('drag dragend', repositionImage);

            function repositionImage() {
                KievGeoOverlay.reposition(marker1.getLatLng(), marker2.getLatLng(), marker3.getLatLng());
                console.log(marker1.getLatLng() + ' - ' +  marker2.getLatLng() + ' - ' +  marker3.getLatLng());
            };

            roadMap.addLayer(KievGeoOverlay);
            roadMap.fitBounds(KievGeoBounds);

        } else {

            roadMap.removeLayer(KievGeoOverlay);

        }

    });

    $('select.OverlayOpacity').on('change', function () {

        opacity = $(this).val();

        KievGeoOverlay.setOpacity(opacity);

    });

});

function setOverlayOpacity(opacity) {
    overlay.setOpacity(opacity);
}

$(document).on('click','.activateGeodata',function () {

    if (confirm('Вы действительно хотите одобрить эту метку?')) {

        geoId = $(this).attr('geo-id');

        if ($.isNumeric(geoId)) {

            $.ajax({
                url: base_url_lang + "feedback/activate_geodata",
                type: 'post',
                dataType: 'json',
                data: {geoid: geoId},
                success: function (data) {

                    alert(data.content);
                    location.reload();

                }
            });

        } else {
            alert('Ошибка! Вы пытаетесь одобрить несуществующую метку.');
        }

    }

    return false;

});

$(document).on('click','.deleteGeodata',function () {

    if (confirm('Вы действительно хотите удалить эту метку?')) {

        geoId = $(this).attr('geo-id');

        if ($.isNumeric(geoId)) {

            $.ajax({
                url: base_url_lang + "feedback/delete_geodata",
                type: 'post',
                dataType: 'json',
                data: {geoid: geoId},
                success: function (data) {

                    alert(data.content);
                    location.reload();

                }
            });

        } else {
            alert('Ошибка! Вы пытаетесь удалить несуществующую метку.');
        }

    }

    return false;

});