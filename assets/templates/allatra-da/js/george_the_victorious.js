document.addEventListener("DOMContentLoaded", ready);

function ready(){

    var base_url = 'https://maps.ramir.space/';

    //Import CSS!!!
    var cssId = 'AmmaCss';  // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId))
    {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = cssId;
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = base_url + 'assets/css/george_the_victorious.css';
        link.media = 'all';
        head.appendChild(link);
    }

    var safeRoadMap = document.getElementById('safeRoadMap'),
        premaBlockMap = document.createElement('div');

    premaBlockMap.id = 'ammaPremaMap';

    safeRoadMap.appendChild(premaBlockMap);

    // Go Map

    // Hook for showing markers in visible area. Working only with virtual markers!!!
    L.Marker.addInitHook(function() {
        if (this.options.virtual) {
            // setup virtualization after marker was added
            this.on('add', function() {

                this._updateIconVisibility = function() {
                    var map = this._map,
                        isVisible = map.getBounds().contains(this.getLatLng()),
                        wasVisible = this._wasVisible,
                        icon = this._icon,
                        iconParent = this._iconParent,
                        shadow = this._shadow,
                        shadowParent = this._shadowParent;

                    // remember parent of icon
                    if (!iconParent) {
                        iconParent = this._iconParent = icon.parentNode;
                    }
                    if (shadow && !shadowParent) {
                        shadowParent = this._shadowParent = shadow.parentNode;
                    }

                    // add/remove from DOM on change
                    if (isVisible != wasVisible) {
                        if (isVisible) {
                            iconParent.appendChild(icon);
                            if (shadow) {
                                shadowParent.appendChild(shadow);
                            }
                        } else {
                            iconParent.removeChild(icon);
                            if (shadow) {
                                shadowParent.removeChild(shadow);
                            }
                        }

                        this._wasVisible = isVisible;

                    }
                };

                // on map size change, remove/add icon from/to DOM
                this._map.on('resize moveend zoomend', this._updateIconVisibility, this);
                this._updateIconVisibility();

            }, this);
        }
    });

    var roadMap = L.map('ammaPremaMap', {zoomControl: false}).setView([30.401699, 30.252512], 3);


    L.control.zoom({
        position: 'bottomright'
    }).addTo(roadMap);

    L.control.fullscreen({
        position: 'topright'
    }).addTo(roadMap);

    new L.Control.GeoSearch({
        provider: new L.GeoSearch.Provider.OpenStreetMap(),
        style: 'bar',
        showMarker: true,
        showPopup: true,
        marker: {                                           // optional: L.Marker    - default L.Icon.Default
            icon: new L.Icon.Default(),
            draggable: true,
        },
        animateZoom: true,
        searchLabel: 'Поиск'
    }).addTo(roadMap);

    var printer = L.easyPrint({
        //tileLayer: tiles,
        sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
        filename: 'myMap',
        exportOnly: true,
        hideControlContainer: true
    }).addTo(roadMap);

    function manualPrint () {
        printer.printMap('CurrentSize', 'MyManualPrint')
    }

    //Disable default scroll
    roadMap.scrollWheelZoom.disable();

    premaBlockMap.addEventListener('DOMMouseScroll', function(event) {

        event.stopPropagation();
        //e.preventDefault();

        if (event.ctrlKey == true) {
            event.preventDefault();
            roadMap.scrollWheelZoom.enable();
            premaBlockMap.classList.remove('map-scroll');
            setTimeout(function(){
                roadMap.scrollWheelZoom.disable();
            }, 1000);
        } else {
            roadMap.scrollWheelZoom.disable();
            premaBlockMap.classList.add('map-scroll');
            setTimeout(function(){
                premaBlockMap.classList.remove('map-scroll');
            }, 1000);
        }

    });

    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
        variant: 'World_Street_Map',
        attribution: 'Tiles &copy; Esri'
    }).addTo(roadMap);

    // L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
    //     id: 'mapbox.streets'
    // }).addTo(roadMap);


    /* RSS */
    // Note: some RSS feeds can't be loaded in the browser due to CORS security.
    // To get around this, you can use a proxy.
    const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

    var request = new XMLHttpRequest();
    request.open('GET', 'https://maps.ramir.space/george_the_victorious/json', true);

    request.onload = function() {

        // Success!
        if (request.status >= 200 && request.status < 400) {

            let body = document.getElementsByTagName('body')[0];

            //get data!
            var json = JSON.parse(request.responseText);

            var icon = [];

            icon['whiteIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/white-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/white-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                //shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                //shadowSize:  [41, 41]
            });

            icon['AllatRaBlackIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/allatra-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/allatra-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                //shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                //shadowSize:  [41, 41]
            });

            icon['redIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/red-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/red-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                // shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                // shadowSize:  [41, 41]
            });


            icon['orangeIcon'] = new L.Icon({
                iconUrl: base_url + 'assets/leaflet/images/orange-marker-icon.svg',
                iconRetinaUrl: base_url + 'assets/leaflet/images/orange-marker-icon-2x.svg',
                iconSize:    [25, 41],
                iconAnchor:  [12, 41],
                popupAnchor: [1, -34],
                // shadowUrl: base_url + 'assets/leaflet/images/marker-shadow.png',
                // shadowSize:  [41, 41]
            });

            //Create Map
            geoLayer = L.geoJson(json, {

                style: function(feature) {
                    var mag = feature.properties.mag;

                    return {
                        weight: 1,
                        fillColor: "#ff0000",
                        fillOpacity: 0.72,
                        color: "#ff0000"
                    };

                },

                onEachFeature: function(feature, layer) {

                    console.log(feature);

                    var popupText =
                        "<img src='" + base_url + 'assets/uploads/images/george_the_victorious/' + feature.properties.image + "' alt='" + feature.properties.description + "' />" +
                        "<p>" + feature.properties.description + "</p>" +
                        "<p><b>Дата:</b> " + feature.properties.datetime + "</p>" +
                        "<p><b>Источник:</b> " + feature.properties.sources + "</p>" +
                        "<p><a href='#' class='deleteMarker' attr-id='" + feature.id + "'>Удалить</a> <a href='#' class='approveMarker' attr-id='" + feature.id + "'>Одобрить</a></p>";

                    layer.bindPopup(popupText, {
                        closeButton: true,
                        offset: L.point(0, -5)
                    });
                    layer.on('click', function() {
                        layer.openPopup();
                    });
                },

                pointToLayer: function(feature, latlng) {

                    return L.marker(latlng, {
                        virtual: true,
                        icon: icon[feature.properties.icon]
                    });

                },

            }).addTo(roadMap);


            var is_logged_in = 1;

            /* Draw the Map */
            if(is_logged_in == 1){

                var drawnItems = L.geoJson().addTo(roadMap);

                roadMap.addControl(new L.Control.Draw({
                    draw: {
                        circle : false,
                        circlemarker : false,
                        polygon: false,
                        polyline: false,
                        rectangle: false
                    },
                    edit: {
                        featureGroup: drawnItems,
                        poly: {
                            allowIntersection: false
                        }
                    }
                }));

                roadMap.on(L.Draw.Event.CREATED, function (e) {
                    var layer = e.layer,
                        feature = layer.feature = layer.feature || {};
                    feature.type = feature.type || "Feature";
                    var props = feature.properties = feature.properties || {};
                    props.desc = null;
                    props.image = null;
                    props.source = null;
                    props.officially = 0;
                    drawnItems.addLayer(layer);
                    addPopup(layer);
                });

                function addPopup(layer) {

                    var wrapper = document.createElement('div');
                    wrapper.className = 'popup-wrapper';
                    wrapper.style.width = '200px';

                    var formGroup1  = document.createElement('div');
                    formGroup1.className = "form-group";

                    var formGroup2  = document.createElement('div');
                    formGroup2.className = "form-group";

                    var formGroup3  = document.createElement('div');
                    formGroup3.className = "form-group";

                    var customCheckbox = document.createElement('div');
                    customCheckbox.className = "custom-control custom-checkbox";

                    var labelCheckbox = document.createElement('label');
                    labelCheckbox.className = "custom-control-label";
                    labelCheckbox.htmlFor = "official-check-1";
                    labelCheckbox.innerHTML = "Официальный источник?";

                    /* content */

                    var content = document.createElement("textarea");
                    content.className = "form-control form-control-square form-control-sm";
                    content.placeholder  = "Описание места";

                    content.addEventListener("keyup", function () {
                        layer.feature.properties.desc = content.value;
                    });

                    /* eof. content */

                    /* source */

                    var source = document.createElement("input");
                    source.className = "form-control form-control-square form-control-sm";
                    source.placeholder = "Ссылка на источник";

                    source.addEventListener("keyup", function () {
                        layer.feature.properties.source = source.value;
                    });

                    /* eof. source */

                    /* officially */

                    var officially = document.createElement("input");
                    officially.className = "custom-control-input";
                    officially.type = "checkbox";
                    officially.id = "official-check-1";
                    officially.value = 0;

                    officially.addEventListener("change", function () {

                        if(this.checked) {
                            layer.feature.properties.officially = 1;
                        }else{
                            layer.feature.properties.officially = 0;
                        }

                    });

                    customCheckbox.appendChild(officially);
                    customCheckbox.appendChild(labelCheckbox);

                    /* eof. officially */

                    layer.on("popupopen", function () {
                        source.value  = layer.feature.properties.source;
                        content.value = layer.feature.properties.desc;
                        officially.value = 1;
                        content.focus();
                    });

                    formGroup1.appendChild(content);
                    formGroup2.appendChild(source);
                    formGroup3.appendChild(customCheckbox);

                    wrapper.appendChild(formGroup1);
                    wrapper.appendChild(formGroup2);
                    wrapper.appendChild(formGroup3);

                    layer.bindPopup(wrapper).openPopup();
                }

                var saveControl = L.Control.extend({
                    options: {
                        position: 'topleft'
                    },
                    onAdd: function (roadMap) {
                        var container = L.DomUtil.create('div', 'leaflet-control-save leaflet-bar leaflet-control');
                        container.innerHTML = '<a id="save" style="height: 30px; width: 30px; cursor: pointer; opacity: 0.65; background-size: 16px 16px; background-repeat: no-repeat; background-position: 50% 50%; background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzIgMzIiIGhlaWdodD0iMzJweCIgaWQ9InN2ZzIiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDMyIDMyIiB3aWR0aD0iMzJweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgaWQ9ImJhY2tncm91bmQiPjxyZWN0IGZpbGw9Im5vbmUiIGhlaWdodD0iMzIiIHdpZHRoPSIzMiIvPjwvZz48ZyBpZD0iZmxvcHB5X3g1Rl9kaXNrIj48cGF0aCBkPSJNMjYsMThINnYyaDIwVjE4eiBNMjYsMjJINnYyaDIwVjIyeiBNMjYsMjZINnYyaDIwVjI2eiBNMjYsMGgtMnYxM0g4VjBIMHYzMmgzMlY2TDI2LDB6IE0yOCwzMEg0VjE2aDI0VjMweiBNMjIsMmgtNHY5aDQgICBWMnoiLz48L2c+PC9zdmc+)"></a>';
                        container.style.backgroundColor = 'white';
                        container.style.width = '33px';
                        container.style.height = '33px';
                        container.onclick = function(){

                            console.log('buttonClicked1');

                            var userGeoData = JSON.stringify(drawnItems.toGeoJSON());

                            if(userGeoData == '{"type":"FeatureCollection","features":[]}'){

                                alert('Ошибка! Вы ещё ничего не добавили на карту.');

                            }else{

                                postAjax(base_url + "feedback/geoDataGeorge", 'content=' + userGeoData, function(data){ console.log(data); });

                            }

                        }
                        return container;
                    },
                });



                roadMap.addControl(new saveControl());

            }
            /* EOF. DrawMap */

        } else {
            // We reached our target server, but it returned an error
            console.log('There was some error in json request!')
        }

    };

    function postAjax(url, data, success) {
        var params = typeof data == 'string' ? data : Object.keys(data).map(
            function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');

        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xhr.open('POST', url);
        xhr.responseType = 'json';
        xhr.onreadystatechange = function() {
            if (xhr.readyState>3 && xhr.status==200) {
                let message = document.createElement('p');
                if(xhr.response.err == 0){
                    message.classList.add('ammaMessage', 'ammaSuccess');
                } else {
                    message.classList.add('ammaMessage', 'ammaError');
                }
                message.innerHTML = xhr.response.content;
                document.getElementById('ammaPremaMap').parentNode.appendChild(message);
                setTimeout(function () {
                    message.style.opacity = 0;
                    setTimeout(function () {
                        message.remove();
                    }, 1000);
                }, 6000);
            }
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
        return xhr;
    }

    request.onerror = function() {
        // There was a connection error of some sort
    };

    //init json data request
    request.send();

}