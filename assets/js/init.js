$(document).ready(function(){
      
    $('.search').click(function () {
        if($('.search-btn').hasClass('fa-search')){
            $('.search-open').fadeIn(500);
            $('.search-btn').removeClass('fa-search');
            $('.search-btn').addClass('fa-times');
        } else {
            $('.search-open').fadeOut(500);
            $('.search-btn').addClass('fa-search');
            $('.search-btn').removeClass('fa-times');
        }   
    }); 
    
    /*
     *  Fancybox
     */
    
    $('.fancybox').fancybox();
    
    // Change title type, overlay opening speed and opacity
    $(".fancybox-effects-a").fancybox({
    	helpers: {
    		title : {
    			type : 'outside'
    		},
    		overlay : {
    			speedIn : 500,
    			opacity : 0.95
    		}
    	}
    });
    
    // Disable opening and closing animations, change title type
    $(".fancybox-effects-b").fancybox({
    	openEffect  : 'none',
    	closeEffect	: 'none',
    
    	helpers : {
    		title : {
    			type : 'over'
    		}
    	}
    });
    
    // Set custom style, close if clicked, change title type and overlay color
    $(".fancybox-effects-c").fancybox({
    	wrapCSS    : 'fancybox-custom',
    	closeClick : true,
    
    	helpers : {
    		title : {
    			type : 'inside'
    		},
    		overlay : {
    			css : {
    				'background-color' : '#eee'
    			}
    		}
    	}
    });
    
    // Remove padding, set opening and closing animations, close if clicked and disable overlay
    $(".fancybox-effects-d").fancybox({
    	padding: 0,
    
    	openEffect : 'elastic',
    	openSpeed  : 150,
    
    	closeEffect : 'elastic',
    	closeSpeed  : 150,
    
    	closeClick : true,
    
    	helpers : {
    		overlay : null
    	}
    });
    
    /*
     *  Button helper. Disable animations, hide close button, change title type and content
     */
    
    $('.fancybox-buttons').fancybox({
    	openEffect  : 'none',
    	closeEffect : 'none',
    
    	prevEffect : 'none',
    	nextEffect : 'none',
    
    	closeBtn  : false,
    
    	helpers : {
    		title : {
    			type : 'inside'
    		},
    		buttons	: {}
    	},
    
    	afterLoad : function() {
    		this.title = 'Фото ' + (this.index + 1) + ' из ' + this.group.length + (this.title ? ' - ' + this.title : '');
    	}
    });
    
    
    /*
     *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
     */
    
    $('.fancybox-thumbs').fancybox({
    	prevEffect : 'none',
    	nextEffect : 'none',
    
    	closeBtn  : false,
    	arrows    : false,
    	nextClick : true,
    
    	helpers : {
    		thumbs : {
    			width  : 50,
    			height : 50
    		}
    	}
    });
    
    /*
     *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
    */
    $('.fancybox-media')
    	.attr('rel', 'media-gallery')
    	.fancybox({
    		openEffect : 'none',
    		closeEffect : 'none',
    		prevEffect : 'none',
    		nextEffect : 'none',
    
    		arrows : false,
    		helpers : {
    			media : {},
    			buttons : {}
    		}
    	});
    
    /*
     *  Open manually
     */
    
    $("#fancybox-manual-a").click(function() {
    	$.fancybox.open('1_b.jpg');
    });
    
    $("#fancybox-manual-b").click(function() {
    	$.fancybox.open({
    		href : 'iframe.html',
    		type : 'iframe',
    		padding : 5
    	});
    });
    
    $("#fancybox-manual-c").click(function() {
    	$.fancybox.open([
    		{
    			href : '1_b.jpg',
    			title : 'My title'
    		}, {
    			href : '2_b.jpg',
    			title : '2nd title'
    		}, {
    			href : '3_b.jpg'
    		}
    	], {
    		helpers : {
    			thumbs : {
    				width: 75,
    				height: 50
    			}
    		}
    	});
    });
    
    
    // Map
    
    function initialize() {
        var latlng = new google.maps.LatLng(50.478483,30.352081);
        var myOptions = {
            zoom: 14,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);

        setMarkers(map, places);


    }
    var places = [
        ['Стиль-М',50.478483,30.352081,300],
    ];
    function setMarkers(map, locations) {
        var latlngbounds = new google.maps.LatLngBounds();

        var image = new google.maps.MarkerImage('http://style-m.com.ua/assets/template/img/marker.png',
            new google.maps.Size(25, 35),
            new google.maps.Point(0,0),
            new google.maps.Point(0, 10));

        var myLatLng = new google.maps.LatLng(locations[0][1], locations[0][2]);
        latlngbounds.extend(myLatLng);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: image,
            title: locations[0][0]
        });

        var content = document.createElement('div');
        content.innerHTML = "<div style='dislay:block; width:260px; height:174px; color: #333;'><img src='http://style-m.com.ua/assets/template/img/logo_blue.png'><strong>Презентация нашей продукции:</strong><br/><strong>ул.Булаховского 2/1</strong><br/><i>ТЦ 'Агромат' зал 'A'<br/>Тел.: (044) 232-54-00</i><br/><a style='color: #2a6496;' href='mailto:2325400@mail.ru'>2325400@mail.ru</a></div>";
        var infowindow = new google.maps.InfoWindow({
            content: content
        });

        infowindow.open(map, marker);

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });

    };
    
    google.maps.event.addDomListener(window, 'load', initialize);
    

	$(".navbar-collapse ul li a").each(function() {
		if (this.href == window.location) {
            $(this).parent('li').addClass("active");
		};
	});



});