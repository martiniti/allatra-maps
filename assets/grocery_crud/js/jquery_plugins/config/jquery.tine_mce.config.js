$(function() {
    var tinymce_path = default_texteditor_path+'/tiny_mce/';

    var tinymce_options = {

        // Location of TinyMCE script
        script_url : tinymce_path +"tinymce.min.js",
		selector: 'textarea',
		language: 'ru',
		width: tinymce_width,
		height: 500,
		theme: 'modern',
        plugins: 'print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample code table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help moxiemanager spellchecker',
        toolbar1: 'formatselect | bold italic strikethrough blockquote forecolor backcolor | image | link unlink | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | spellchecker | code noindex',
		toolbar_items_size: 'small',
		content_css: [
            '//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
			'//fonts.googleapis.com/css?family=Open+Sans:100,200,300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic-ext',
			'//www.tinymce.com/css/codepen.min.css'
		],
		rel_list: [
			{title: 'none', value: ''},
			{title: 'canonical', value: 'canonical'},
			{title: 'follow', value: 'follow'},
			{title: 'nofollow', value: 'nofollow'},
			{title: 'noopener', value: 'noopener'},
			{title: 'noreferrer noopener', value: 'noreferrer noopener'},
		],
		// without images_upload_url set, Upload tab won't show up
		automatic_uploads: true,
		images_upload_url: tinymce_image_upload,//set in preheader_view
		images_upload_base_path: images_upload_path,//set in preheader_view
		images_upload_credentials: true,
        spellchecker_languages: "Russian=ru,Ukrainian=uk,English=en",
        spellchecker_language: "ru",  // default language
        spellchecker_rpc_url: "//speller.yandex.net/services/tinyspell",
		extended_valid_elements : "noindex",
        allow_unsafe_link_target: true,
		content_style: [
			'body{padding: 5px 15px !important;} h1,h2,h3,h4,h5,h6{font-family: "Open Sans",sans-serif;} ul,li,ol{font-family:"Open Sans",sans-serif;} p, div {margin: 10px 0 !important;font-family:"Open Sans",sans-serif;} a {color: #4CAF50;} img{max-width: 100%; height: auto;} blockquote {font-family: Georgia;font-style: italic;border-left: 5px solid #0e78a8; padding: 10px 20px;margin: 0 0 20px;}'
		],
		setup: function(editor) {

			function getHtmlNoindex() {
				html = '<noindex>' + tinymce.activeEditor.selection.getContent() + '</noindex>';
				editor.insertContent(html);
			}

			editor.addButton('noindex', {
				//icon: 'insertdatetime',
				image: base_url + 'assets/grocery_crud/texteditor/tiny_mce/images/noindex.gif',
				tooltip: "<noindex></noindex>",
				onclick: getHtmlNoindex
			});
		}
    };

    $('textarea.texteditor').tinymce(tinymce_options);

    var minimal_tinymce_options = $.extend({}, tinymce_options);
    minimal_tinymce_options.theme = "simple";

    $('textarea.mini-texteditor').tinymce(minimal_tinymce_options);

});